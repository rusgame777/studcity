<?php

/**
 * @var $this yii\web\View
 * @var $meta common\models\Page
 * @var $settings common\models\Settings
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Review;
use common\models\ReviewReply;
use frontend\models\ReviewForm;
use frontend\components\Notice;

$this->title = $meta['metatitle'];
$this->params['metaDescription'] = $meta['description'];
$this->params['metaKeywords'] = $meta['keywords'];

$pathname = '/' . Yii::$app->request->getPathInfo();
$params = [
    'meta' => $meta,
];

$opinions =
    Review::find()->where(['subm' => 1])->orderBy(['date_add' => SORT_DESC, 'time_add' => SORT_DESC])->asArray()->all();

$results = ReviewReply::find()->orderBy(['date_add' => SORT_DESC, 'time_add' => SORT_DESC])->asArray()->all();
$replies = [];
foreach ($results as $result) {
    $comment = $result['comment'];
    if (!array_key_exists($comment, $replies)) {
        $replies[$comment] = [];
    }
    $replies[$comment][$result['id']] = $result;
}
$this->registerJsFile('/js/orders.js?20180218', ['position' => yii\web\View::POS_END]);

$reviewForm = new ReviewForm();

?>
<div id='content'>
<?= $this->renderFile('@app/views/layouts/search.php', ['settings' => $settings]); ?>
    <div id='order'>
    <?php Notice::init(); ?>
        <?= !empty($meta->content) ? sprintf('<div id="order_text">%s</div>', $meta->content) : ''; ?>
        <div id='order_descr'>
    <div id='order_descr_title'>- Предметы нашей специализации -</div>
    <div id='order_descr_info'>
    <div class='order_descr_info'><div>
    <div class='order_descr_title'><div>Технические предметы</div></div>
    <div class='order_descr_text'><div><?= $settings->order1 ?></div></div>
    </div></div>
    <div class='order_descr_info'><div>
    <div class='order_descr_title'><div>Гуманитарные предметы</div></div>
    <div class='order_descr_text'><div><?= $settings->order2 ?></div></div>
    </div></div>
    </div>
    </div>
    <div id='order_submit' class='md-trigger md-setperspective' data-modal='modal-2'>Оформить заказ</div>
</div>
</div>
