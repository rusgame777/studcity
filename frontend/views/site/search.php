<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use backend\components\Common;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Image\BoxInterface;

$db = Yii::$app->db;
$request = Yii::$app->request;
$pathname = '/' . $request->getPathInfo();
$uri = $request->getUrl();
$word = $request->get('word');

$this->title = "Поиск";
if ($word != '') {
    $this->title .= "\"{$word}\"";
}
$this->params['metaDescription'] = '';
$this->params['metaKeywords'] = '';

$this->registerJsFile('/js/catalog.js', ['position' => yii\web\View::POS_END]);
$this->registerCssFile('/css/catalog.css', ['position' => yii\web\View::POS_BEGIN]);

$this->registerJsFile('/layout/bootstrap.min.js', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/layout/dopoln/bootstrap-hover-dropdown.min.js', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/lib/owl-carousel/owl.carousel.js', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/layout/echo.min.js', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/layout/bootstrap-slider.min.js', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/layout/wow.min.js', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/layout/scripts.js', ['position' => yii\web\View::POS_END]);
#$this->registerCssFile('/css/main.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/layout/bootstrap.min.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/layout/main.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/lib/owl-carousel/owl.carousel.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/lib/owl-carousel/owl.transitions.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/layout/animate.min.css', ['position' => yii\web\View::POS_BEGIN]);
#$this->registerCssFile('/layout/font-awesome.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/layout/bootstrap-select.min.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('https://fonts.googleapis.com/css?family=Exo+2:400,600&amp;subset=cyrillic', ['position' => yii\web\View::POS_BEGIN]);

$uploaddir = Yii::getAlias('@frontendDocroot/uploads/catalog/');
$urldir = Yii::getAlias('@frontendWebroot/uploads/catalog/');

$concat =
    $db->createCommand("SELECT id_cat, title, cat FROM catalog_cat_cat, catalog_cat WHERE catalog_cat_cat.id_cat = catalog_cat.id AND foto!='' AND archive='0'")
        ->queryAll();
foreach ($concat as $mcat) {
    $array[$mcat['cat']][$mcat['id_cat']] = $mcat['title'];
    $array2[$mcat['id_cat']][$mcat['cat']] = $mcat['title'];
}
?>
<div class="body-content outer-top-xs" id="top-banner-and-menu">
    <div class="container">
        <div class="row">
            <div class="col-xs-3 sidebar">
            <?php
            $params = [];
            echo $this->render('@frontend/views/site/aside_catalog', $params);
            ?>
            </div>
            <div class="col-xs-9 homebanner-holder">
                <div id='catalog'>
                    <div id='catalog_top'>
                        <div id='catalog_title'>
                            <div>
                            Каталог
                                <?php
                                if ($word != '') {
                                    echo "<span>/</span><span class='active'>{$word}</span>";
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                        if ($word != '') {
                            ?>
                            <div id='catalog_nav'>
                                <div>
                                    <a href='/catalog/' title='Каталог'>Каталог</a>
                                    <span>/</span>
                                    <?= $word ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                    $conpos =
                        $db->createCommand("SELECT DISTINCT id FROM catalog_position, catalog_position_cat WHERE catalog_position_cat.id_position = catalog_position.id AND (article LIKE :word OR title LIKE :word) AND foto!='' AND archive='0'")
                            ->bindValues(['word' => '%' . $word . '%'])->queryAll();
                    $itemsperpage = 20;
                    $itemscount = !empty($conpos) ? count($conpos) : 0;
                    if ($itemscount != 0) {
                        $cpage = !is_null($request->get('page')) ? $request->get('page') : 1;
                        extract(Common::pagination($itemscount, $cpage, $itemsperpage), EXTR_OVERWRITE);
                        $conpos =
                            $db->createCommand("SELECT DISTINCT id, title, article, price, foto, (SELECT cat FROM catalog_position_cat WHERE catalog_position_cat.id_position = catalog_position.id ORDER BY sort DESC, id ASC LIMIT 1) AS cat FROM catalog_position, catalog_position_cat WHERE catalog_position_cat.id_position = catalog_position.id AND (article LIKE :word OR title LIKE :word) AND foto!='' AND archive='0' ORDER BY sort DESC, id ASC LIMIT $offset_page, $itemsperpage")
                                ->bindValues(['word' => '%' . $word . '%'])->queryAll();
                        if (empty($conpos) == false) {
                            ?>
                            <div id='catalog_pos'>
                            <?php
                            foreach ($conpos as $mcat) {
                                extract($mcat, EXTR_PREFIX_ALL, 'catalog');
                                $catalog_title = Html::encode($catalog_title);
                                $catalog_url = "/catalog/?id=" . $catalog_id;
                                $catalog_price_word = number_format($catalog_price, 0, '.', ',') . ' руб.';
                                $nav_cat = "";
                                $cur_cat = $catalog_cat;
                                while ($cur_cat != 0) {
                                    $url_cat = "/catalog/?cat=" . $cur_cat;
                                    $key = array_keys($array2[$cur_cat])[0];
                                    $name_cat = $array2[$cur_cat][$key];
                                    if ($cur_cat != $catalog_cat) {
                                        $nav_cat = "<span>/</span><a href='{$url_cat}'>{$name_cat}</a>" . $nav_cat;
                                    }
                                    $cur_cat = array_search($name_cat, $array2[$cur_cat]);
                                    if ($key == null) {
                                        $cur_cat = 0;
                                    }
                                }
                                $nav_cat = "<a href='/catalog/' title='Каталог'>Каталог</a>" . $nav_cat;
                                ?>
                                <div class='catalog_pos' data-id='<?= $catalog_id ?>'>
                                    <div>
                                        <span>
                                        <?php
                                        if (($catalog_foto != '') && (file_exists($uploaddir . $catalog_foto))) {
                                            ?>
                                            <div class='catalog_pos_foto'>
                                                <a title='<?= $catalog_title ?>' href='<?= $catalog_url ?>'>
                                                    <img src='<?= $urldir . $catalog_foto ?>'>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                            <div class='catalog_cat_desc'>
                                                 <div>
                                                    <a title='<?= $catalog_title ?>' href='<?= $catalog_url ?>' class='catalog_pos_title'>
                                                        <?= $catalog_title ?>
                                                    </a>
                                                    <div class='catalog_pos_nav'>
                                                        <?= $nav_cat ?>
                                                    </div>
                                                     <?php
                                                     if ($catalog_article != '') {
                                                         echo "<div class='catalog_pos_article'>{$catalog_article}</div>";
                                                     }
                                                     ?>
                                                </div>
                                            </div>
                                            <div class='catalog_pos_price'>
                                                <div class='catalog_pos_pricebox'>
                                                    <div>
                                                        <?= $catalog_price_word ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                            if ($pagescount > 1) {
                                ?>
                                <div id='catalog_pg'>
                                <?php
                                $arr = ['page', 'word'];
                                $query_array = [];
                                foreach ($arr as $value) if ($request->get($value) != null) {
                                    $query_array[$value] = $request->get($value);
                                }
                                echo Common::pagination_layout_show($query_array, $pagescount, $cpage, $stpage, $endpage, '');
                                ?>
                                </div>
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>