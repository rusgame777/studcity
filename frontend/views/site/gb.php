<?php

/**
 * @var $this yii\web\View
 * @var $meta common\models\Page
 * @var $settings common\models\Settings
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Review;
use common\models\ReviewReply;
use backend\components\Common;
use frontend\models\ReviewForm;
use frontend\components\Notice;

$this->title = $meta['metatitle'];
$this->params['metaDescription'] = $meta['description'];
$this->params['metaKeywords'] = $meta['keywords'];

$pathname = '/' . Yii::$app->request->getPathInfo();
$params = [
    'meta' => $meta,
];

$opinions =
    Review::find()->where(['subm' => 1])->orderBy(['date_add' => SORT_DESC, 'time_add' => SORT_DESC])->asArray()->all();

$results = ReviewReply::find()->orderBy(['date_add' => SORT_DESC, 'time_add' => SORT_DESC])->asArray()->all();
$replies = [];
foreach ($results as $result) {
    $comment = $result['comment'];
    if (!array_key_exists($comment, $replies)) {
        $replies[$comment] = [];
    }
    $replies[$comment][$result['id']] = $result;
}
$this->registerJsFile('/js/gb.js?20180218', ['position' => yii\web\View::POS_END]);

$reviewForm = new ReviewForm();

?>
<div id='content'>
<div id='catalog'>

    <?= $this->renderFile('@app/views/layouts/search.php', ['settings' => $settings]); ?>
    <div id='catalog_title' class="gb_title">
    <h1><span><?= $meta->title ?></span></h1>
    <div id='gb_click'><span>Написать отзыв</span></div>
    </div>
    <?php Notice::init(); ?>
    <div id='gb_form'>

    <?
    //    $fieldConfig = [
    //        'options' => ['class' => 'site-form-group gb_inputform'],
    //        'hintOptions' => [
    //            'tag' => 'p',
    //            'class' => 'site-hint-block',
    //        ],
    //        'errorOptions' => [
    //            'tag' => 'p',
    //            'class' => 'site-help-block site-help-block-error',
    //        ],
    //        'labelOptions' => [
    //            'class' => 'control-label gb_input_title'
    //        ],
    //        'inputOptions' => [
    //            'class' => 'site-form-control gb_input',
    //        ],
    //    ];
    $fieldConfig = [
        'options' => ['class' => 'site-form-group gb_inputform'],
        'hintOptions' => [
            'tag' => 'p',
            'class' => 'site-hint-block',
        ],
        'errorOptions' => [
            'tag' => 'p',
            'class' => 'site-help-block site-help-block-error',
        ],
        'labelOptions' => [
            'class' => 'control-label',
        ],
        'inputOptions' => [
            'class' => 'site-form-control',
        ],
    ];
    $form = ActiveForm::begin([
        'id' => 'review-form',
        'action' => [
            Url::to([
                'site/review-create',
                'id' => $reviewForm->id,
            ]),
        ],
        'fieldConfig' => $fieldConfig,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validationDelay' => 5000,
        'validationUrl' => Url::to(['site/review-validate']),
    ]);
    ?>
        <?= $form->field($reviewForm, 'avtor')->textInput([]); ?>
        <?= $form->field($reviewForm, 'phone')->textInput([]); ?>
        <?= $form->field($reviewForm, 'title')->textInput([]); ?>
        <?= $form->field($reviewForm, 'message', [
            'options' => ['class' => 'site-form-group gb_textareaform'],
        ])->textarea([]); ?>
        <div id='gb_send'><span>Отправить отзыв</span></div>
    <div id='gb_status'></div>
    </div>
    <?php ActiveForm::end(); ?>
    <div id='gb_comments'>
    <?php
    if (!empty($opinions)) {
        foreach ($opinions as $index => $opinion) {
            $comment = $opinion['id'];
            $avtor = Html::encode($opinion['avtor']);
            $message = nl2br(Html::encode($opinion['message'], false));
//            $date_add = Yii::$app->formatter->asDate($opinion['date_add'], 'php:d.m.Y');
            $date_add = Common::getDateTime($opinion['date_add'], 'd.m.Y');
            $reply = array_key_exists($comment, $replies) ? current($replies[$comment]) : [];
            $align = ($index % 2 == 1) ? " style='float: right; clear: none;'" : "";
            ?>
            <div class='gb_comment'<?= $align ?>>
            <div class='gb_comment_theme'><?= Html::encode($opinion['title']) ?></div>
            <div class='gb_comment_message'><?= $message ?></div>
            <div class='gb_comment_info'><?= $date_add . ', ' . $avtor ?></div>
                <?php
                if (!empty($reply)) {
                    ?>
                    <div class='gb_comment_stripe'></div>
                    <div class='gb_comment_reply'><span>Администрация:</span> <?= Html::encode($reply['message']) ?></div>
                    <?php
                }
                ?>
            </div>
            <?php
        }
    }
    ?>
</div>

</div>
</div>