<?php

/**
 * @var $meta common\models\Page
 */

use yii\helpers\Url;

?>
<div id='content'>
    <?= $this->renderFile('@app/views/layouts/search.php', ['settings' => $settings]); ?>
    <div id='cont'>
    <div id='cont_title'><h1>
            <a href='<?= Url::current() ?>' title='<?= $meta->title ?>'><?= $meta->title ?></a>
        </h1></div>
    <div id='cont_text'><?= $meta->content ?></div>
    </div>
</div>