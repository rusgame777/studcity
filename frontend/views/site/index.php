<?php

/**
 * @var $this yii\web\View
 * @var $meta common\models\Page
 * @var $settings common\models\Settings
 */

$this->title = $meta['metatitle'];
$this->params['metaDescription'] = $meta['description'];
$this->params['metaKeywords'] = $meta['keywords'];

$pathname = '/' . Yii::$app->request->getPathInfo();
$params = [
    'meta' => $meta,
    'settings' => $settings,
];

if ($pathname == '/') {
    // 
} else {
    echo $this->render('page/id', $params);
}