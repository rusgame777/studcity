<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 08.10.2017
 * Time: 22:11
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use frontend\models\ReverseForm;
use frontend\models\OrderForm;
use frontend\models\OrderUpload;
use himiklab\yii2\recaptcha\ReCaptcha;

$reverseForm = new ReverseForm();
$reverseUpload = new OrderUpload();
$fieldConfig = [
    'options' => ['class' => 'site-form-group reverse_input'],
    'hintOptions' => [
        'tag' => 'p',
        'class' => 'site-hint-block',
    ],
    'errorOptions' => [
        'tag' => 'p',
        'class' => 'site-help-block site-help-block-error',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'inputOptions' => [
        'class' => 'site-form-control',
    ],
];

?>
<div class='md-modal md-effect-1' id="modal-1">
<div class='md-content'>

<div id='reversebox'>
<div id="boxreverse">
<div id='reversetitle'>Обратная связь</div>

<div id='reversedesc'>Напишите нам и наши менеджеры свяжутся с Вами в самое ближайшее время.</div>
    <?php
    $form = ActiveForm::begin([
        'id' => 'reverse-form',
        'action' => [
            Url::to([
                'site/reverse-create',
            ]),
        ],
        'fieldConfig' => $fieldConfig,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validationDelay' => 5000,
        'validationUrl' => Url::to(['site/reverse-validate']),
    ]);
    ?>
    <div id='reverseinfo'>
        <?= $form->field($reverseForm, 'dataArray')->hiddenInput()->label(false); ?>
        <?= $form->field($reverseForm, 'name')->textInput(['placeholder' => $reverseForm->getAttributeLabel('name')])
            ->label(false); ?>
        <?= $form->field($reverseForm, 'phone')->textInput(['placeholder' => $reverseForm->getAttributeLabel('phone')])
            ->label(false); ?>
        <?= $form->field($reverseForm, 'email')->textInput(['placeholder' => $reverseForm->getAttributeLabel('email')])
            ->label(false); ?>
        <?= $form->field($reverseForm, 'desc', [
            'options' => ['class' => 'site-form-group reverse_textarea'],
        ])->textarea([
            'placeholder' => $reverseForm->getAttributeLabel('desc'),
        ])->label(false); ?>
    </div>
    <div class="form-captcha">
        <?= $form->field($reverseForm, 'reCaptcha', [
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
        ])->widget(ReCaptcha::class, [
            'name' => 'reCaptcha',
            'siteKey' => '6LctgIsUAAAAAKvByBXDrvuox0fcfAeOEbwmHI9u',
        ])->label(false) ?>
    </div>
    <?
    echo $form->field($reverseUpload, 'file', [
        'template' => implode("\n", [
            "{label}",
            "<button type='button'>Выбрать вложение</button>",
            "<div>",
            "</div>",
            "{input}",
            "{hint}",
            "{error}",
        ]),
        'options' => ['class' => 'form-group form-file', 'id' => 'reverse_file'],
    ])->fileInput([
        'multiple' => true,
        'accept' => '.doc, .docx, .rar, .zip, .jpg, .png, .pdf',
    ])->label(false);
    ?>
    <?php ActiveForm::end(); ?>
    <div id='reverse_status'><span></span></div>
<div id='reverse_enter'><span>Отправить сообщение</span></div>
</div>
<div class='md-close' id="reverse_close"></div>
</div>

</div>
</div>
<?php
$orderForm = new OrderForm();
$orderUpload = new OrderUpload();
$fieldConfig = [
    'options' => ['class' => 'site-form-group order_input'],
    'hintOptions' => [
        'tag' => 'p',
        'class' => 'site-hint-block',
    ],
    'errorOptions' => [
        'tag' => 'p',
        'class' => 'site-help-block site-help-block-error',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'inputOptions' => [
        'class' => 'site-form-control',
    ],
];
?>
<div class='md-modal md-effect-1' id="modal-2">
<div class='md-content'>

<div id='orderbox'>
<div id="boxorder">
<div id='ordertitle'>Заказать работу</div>
<div id='orderdesc'>Напишите нам и наши менеджеры свяжутся с Вами в самое ближайшее время.</div>
    <?php
    $form = ActiveForm::begin([
        'id' => 'order-form',
        'action' => [
            Url::to([
                'site/order-create',
            ]),
        ],
        'fieldConfig' => $fieldConfig,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validationDelay' => 5000,
        'validationUrl' => Url::to(['site/order-validate']),
    ]);
    ?>
    <div id='orderinfo'>
        <?= $form->field($orderForm, 'dataArray')->hiddenInput()->label(false); ?>
        <?= $form->field($orderForm, 'name')->textInput(['placeholder' => $reverseForm->getAttributeLabel('name')])
            ->label(false); ?>
        <?= $form->field($orderForm, 'phone')->textInput(['placeholder' => $reverseForm->getAttributeLabel('phone')])
            ->label(false); ?>
        <?= $form->field($orderForm, 'email')->textInput(['placeholder' => $reverseForm->getAttributeLabel('email')])
            ->label(false); ?>
        <?= $form->field($orderForm, 'desc', [
            'options' => ['class' => 'site-form-group order_textarea'],
        ])->textarea([
            'placeholder' => $reverseForm->getAttributeLabel('desc'),
        ])->label(false); ?>
        
    </div>
    <div class="form-captcha">
        <?= $form->field($orderForm, 'reCaptcha', [
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
        ])->widget(ReCaptcha::class, [
            'name' => 'reCaptcha',
            'siteKey' => '6LctgIsUAAAAAKvByBXDrvuox0fcfAeOEbwmHI9u',
        ])->label(false) ?>
    </div>
    <?
    echo $form->field($orderUpload, 'file[]', [
        'template' => implode("\n", [
            "{label}",
            "<button type='button'>Выбрать вложение</button>",
            "<div>",
            "</div>",
            "{input}",
            "{hint}",
            "{error}",
        ]),
        'options' => ['class' => 'form-group form-file', 'id' => 'order_file'],
    ])->fileInput([
        'multiple' => true,
        'accept' => '.doc, .docx, .rar, .zip, .jpg, .png, .pdf',
    ])->label(false);
    ?>
    <?php ActiveForm::end(); ?>
    <div id='order_enter'><span>Заказать работу</span></div>
<div id='order_status'></div>
</div>
<div class='md-close' id="order_close"></div>
</div>

</div>
</div>