<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 09.10.2017
 * Time: 0:16
 */
use yii\helpers\Html;
use common\models\University;
use common\models\CatalogPosition;
use common\models\CatalogCat;
use common\models\CatalogSubcatCat;
use common\models\CatalogSubcat;

$manual = Html::encode(Yii::$app->request->get('manual'));
$id_university = Yii::$app->request->get('university');
$universities =
    University::find()->from([University::tableName() . ' university'])->innerJoin(CatalogSubcatCat::tableName() .
            ' catalog_subcat_cat', 'catalog_subcat_cat.id_university = university.id')
        ->innerJoin(CatalogSubcat::tableName() . ' catalog_subcat', 'catalog_subcat.id = catalog_subcat_cat.id_subcat')
        ->innerJoin(CatalogCat::tableName() . ' catalog_cat', 'catalog_cat.id = catalog_subcat_cat.id_cat')
        ->innerJoin(CatalogPosition::tableName() . ' catalog_position', 'catalog_position.subcat = catalog_subcat.id')
        ->orderBy([
            'sort' => SORT_ASC,
            'title' => SORT_ASC,
        ])->distinct()->asArray()->all();
?>
<div id='catalog_searchbox'>
<div id='catalog_university'>
<select id='university' name='university' size='1'>
    <option value='0'>Выберите ВУЗ</option>
    <?php
    foreach ($universities as $university) {
        echo vsprintf('<option %3$s value="%1$s">%2$s</option>', [
            1 => $university['id'],
            2 => $university['title'],
            3 => $university['id'] == $id_university ? "selected" : "",
        ]);
    }
    ?>
</select>
</div>
<div id='catalog_search'><div class='catalog_search'>
<div class='catalog_search_input'>
<input type='text' name='manual' placeholder='Введите название искомой работы и мы найдем её для вас!' value='<?= $manual ?>'>
</div>
<div class='catalog_search_icon'><div id='catalog_search_icon'></div></div>
</div></div>
    <?= $this->renderFile('@app/views/layouts/recycle_show.php', ['settings' => $settings]); ?>
</div>


