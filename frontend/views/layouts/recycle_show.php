<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 15.10.2017
 * Time: 23:23
 */

use yii\db\Expression;
use common\models\Clients;
use common\models\Orders;
use common\models\OrderDesc;

$id_client = Yii::$app->params['id_client'];
$sum = 0;
$num_desc = 0;
if ($id_client != 0) {
    $order = Orders::find()->where([
        'and',
        ['id_user' => $id_client],
        ['send' => 0],
        ['>', 'date_end', new Expression('NOW()')],
    ])->orderBy(['id' => SORT_DESC])->limit(1)->one();
    $id_order = !empty($order->id) ? $order->id : 0;
//    $num_desc = OrderDesc::find()->where(['id_order' => $id_order])->count();
    $sum = OrderDesc::find()->where(['id_order' => $id_order])->sum('sum');
}
$sum_word = $sum . ' руб.';
$show = $sum == 0 ? "style='display: none;'" : "";
?>
<div <?= $show ?> id='catalog_recyclebox'><div id='catalog_recycle'>
<div id='catalog_recycle_icon'></div>
<div id='catalog_recycle_sum'><?= $sum_word ?></div>
</div></div>