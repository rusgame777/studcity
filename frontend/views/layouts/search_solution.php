<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 09.10.2017
 * Time: 0:16
 */
use yii\helpers\Html;
use common\models\SolutionCat;
use common\models\SolutionPosition;

$manual = Html::encode(Yii::$app->request->get('manual'));
$cat = Yii::$app->request->get('cat');
$categories =
    SolutionCat::find()->from([SolutionCat::tableName() . ' solution_cat'])->innerJoin(SolutionPosition::tableName() .
        ' solution_position', 'solution_position.cat = solution_cat.id')->orderBy([
        'sort' => SORT_ASC,
        'title' => SORT_ASC,
    ])->asArray()->all();
?>
<div id='solution_searchbox'>
<div id='solution_category'>
<select id='cat' name='cat'>
<option value='0'>выберите дисциплину</option>
    <?php
    foreach ($categories as $category) {
        echo vsprintf('<option %3$s value="%1$s">%2$s</option>', [
            1 => $category['id'],
            2 => $category['title'],
            3 => $category['id'] == $cat ? "selected" : "",
        ]);
    }
    ?>
</select>
</div>
<div id='solution_search'><div class='solution_search'>
<div class='solution_search_input'>
<input type='text' name='manual' placeholder='Введите название искомой РГР и мы найдем её для вас!' value='<?= $manual ?>'>
</div>
<div class='solution_search_icon'><div id='solution_search_icon'></div></div>
</div></div>
</div>