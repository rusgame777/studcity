<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\models\Page;
use common\models\Settings;

$this->registerCssFile('/css/recycle.css?20190213', []);
$this->registerCssFile('/css/option.css', []);
$this->registerCssFile('/css/reverse.css?201803034', []);
$this->registerCssFile('/css/order.css?201803034', []);
$this->registerCssFile('/css/component.css', []);
$this->registerCssFile('/css/success.css?2', []);
$this->registerCssFile('/css/solution.css?201803034', []);
$this->registerJsFile('/js/top.js?20190210', ['position' => yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/recycle.js?20180218', ['position' => yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/shop.js?20190210', ['position' => yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/option.js?201901291', ['position' => yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/reverse.js?2018030363', ['position' => yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/order.js?201803036', ['position' => yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/success.js?20180218', ['position' => yii\web\View::POS_BEGIN]);
$this->registerJsFile('/js/solution.js?20190210', ['position' => yii\web\View::POS_BEGIN]);
AppAsset::register($this);

$settings = Settings::findOne(1);

$pageContent = '';
$page = Page::findOne(1);
if ($page !== null && !empty($page->content)) {
    $pageContent = Html::tag('div', $page->content, ['class' => 'main-page-content']);
}

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $this->params['metaDescription'],
    ]); ?>
    <?php
    $this->registerMetaTag([
        'name' => 'keywords',
        'content' => $this->params['metaKeywords'],
    ]); ?>
    <?php echo !empty($settings->code_head) ? $settings->code_head : ''; ?>
    <?php $this->head() ?>
</head>
<body>
<script>
$(document).ready(function () {
    var container = $(".container");
//    var instance = container.data("vide"); // Get instance
//    var video = instance.getVideoObject(); // Get video object
//    instance.destroy(); // Destroy instance
    container.vide({
        'mp4': 'video/background',
        'webm': 'video/background',
        'ogv': 'video/background',
        'poster': 'video/background'
    }, {
        'posterType': 'jpg'
    });
});
</script>
    <?php $this->beginBody() ?>
    <?= $this->renderFile('@app/views/layouts/forms.php', ['settings' => $settings]); ?>
    <div class="container">
        <div id="container">

    <div id='main_header'>
    <div id='main_header_left'>
    <a href='/' id='main_logo' title='<?= Yii::$app->params['name_site'] ?>'></a>
    </div>
    <div id='main_header_right'>
    <a id='main_order_click' href='<?=Url::to(['site/order'])?>' title='Заказать работу'>Заказать работу</a>
    <div id='main_reverse_click' class='md-trigger md-setperspective' data-modal='modal-1' title='Обратная связь'>Обратная связь</div>
        <?php echo !empty($settings->url_vk) ? sprintf('<a id="main_vk" href="%s" target="_blank"></a>', $settings->url_vk) : "";
        ?>
        <div id='main_menu'>
    <a class='main_menu' href='<?=Url::to(['catalog/index'])?>' title='Готовые работы'>Готовые работы</a>
    <a class='main_menu' href='<?=Url::to(['solutions/index'])?>' title='Решение за мгновение'>Решение за мгновение</a>
    <a class='main_menu' href='<?=Url::to(['site/about'])?>' title='О нас'>О нас</a>
    <a class='main_menu' href='<?=Url::to(['site/gb'])?>' title='Отзывы'>Отзывы</a>
    </div>
    </div>
    </div>
    <script>
    $(document).ready(function () {
        var slider = $("#slogan");
        slider.owlCarousel({
            navigation: false,
            navigationText: false,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            pagination: false,
            autoPlay: 10000,
            autoHeight: true,
            transitionStyle: "fade"
        });
    });
    </script>
                <div id='slogan' class="owl-carousel owl-theme">
        <div class='slogan'><div>Помощь в написании учебных работ</div></div>
        <div class='slogan'><div>Более 100.000.000 заданий</div></div>
        <div class='slogan'><div>Объем нашей базы более 500 ГБ готовых решений, проверенных преподавателями</div></div>
        </div>
        <div id='advantages'>
           <div id='advantage1' class='advantage'><span>
            Мы собрали команду квалифицированных преподавателей
            </span></div>
            <div id='advantage2' class='advantage'><span>
            Мы установили минимальные цены
            </span></div>
            <div id='advantage3' class='advantage'><span>
            Мы всегда следим за качеством выполняемых работ
            </span></div>
        </div>
            <?= $content ?>
            <?= $this->renderFile('@app/views/layouts/footer.php', ['settings' => $settings]); ?>
    </div>
    </div>
    <div class='show_box'></div>
    <div id='show_box'></div>
    <div class="md-overlay"></div>
    <?php $this->endBody() ?>
    <?php echo !empty($settings->code_body) ? $settings->code_body : ''; ?>
    <?
    $this->registerJsFile('/lib/classie.js', ['position' => yii\web\View::POS_END]);
    $this->registerJsFile('/lib/modalEffects.js?2', ['position' => yii\web\View::POS_END]);
    ?>
   

    </body>
    </html>
<?php $this->endPage() ?>