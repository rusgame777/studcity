<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 10.10.2017
 * Time: 0:29
 */

/**
 * @var $meta common\models\Page;
 * @var $settings common\models\Settings;
 */

use yii\helpers\Url;
use yii\helpers\Html;
use common\models\CatalogCat;
use common\models\CatalogSubcat;
use common\models\CatalogPosition;
use common\models\Orders;
use common\models\OrderDesc;
use common\models\CatalogFiles;
use common\models\University;
use frontend\components\Notice;

$cat = Yii::$app->request->get('cat');
$subcat = Yii::$app->request->get('subcat');
$manual = Yii::$app->request->get('manual');
$id_university = intval(Yii::$app->request->get('university'));

//$results =
//    CatalogPosition::find()->where(['subcat' => $subcat])->orderBy(['sort ASC, title ASC'])->asArray()->all();
$results = CatalogFiles::find()->asArray()->all();
$files = [];
foreach ($results as $result) {
    $position = $result['position'];
    if (!array_key_exists($position, $files)) {
        $files[$position] = [];
    }
    $files[$position][$result['id']] = $result;
}

$name_cat = '';
$category = CatalogCat::find()->where(['id' => $cat])->limit(1)->one();
$name_cat = !empty($category->title) ? $category->title : '';
$university = University::find()->where(['id' => $id_university])->limit(1)->one();
$name_university = !empty($university->title) ? $university->title : '';
$name_cat = !empty($name_cat) ? implode('', [
    $name_cat,
    ' (',
    $name_university,
    ')',
]) : $name_university;

$name_subcat = '';
$subcategory = CatalogSubcat::find()->where(['id' => $subcat])->limit(1)->one();
$name_subcat = !empty($subcategory->title) ? Html::encode($subcategory->title) : '';
$author = !empty($subcategory->author) ? Html::encode($subcategory->author) : '';
$year = !empty($subcategory->year) ? Html::encode($subcategory->year) : '';
if ($author != '') {
    $name_subcat .= "/" . $author;
}
if ($year != '') {
    $name_subcat .= "/" . $year . "г";
}
$id_client = Yii::$app->params['id_client'];
?>
<div id='content'>
    <div id='catalog'>

        <?= $this->renderFile('@app/views/layouts/search.php', ['settings' => $settings]); ?>
        <?php Notice::init(); ?>
        <div id='catalog_title'>
            <h1>
                <span>Выберите необходимый вариант</span> /
                <a href='<?= Url::canonical() ?>' title='<?= $name_cat ?>'><?= $name_cat ?></a> /
                <a href='<?= Url::canonical() . "?" . http_build_query([
                    'cat' => $cat,
                    'university' => $id_university,
                ]) ?>' title='<?= $name_subcat ?>'><?= $name_subcat ?></a>
            </h1>
        </div>
        <?php
        $results =
            CatalogPosition::find()->where(['subcat' => $subcat])->orderBy(['sort' => SORT_ASC, 'title' => SORT_ASC])
                ->asArray()->all();
        $numResults = count($results);

        $pos_array = [];
        $num = 0;
        $l = 0;
        for ($j = 0; $j <= 3; $j++) {
            $num = $j * ceil($numResults / 4);
            if ($numResults >= $num) {
                $pos_array[$l] = $num;
                $l++;
            }
        }
        if ($pos_array[$l - 1] != $numResults) {
            $pos_array[$l] = $numResults;
        } else {
            $l -= 1;
        }
        if ($numResults > 0) {
            ?>
            <div id='catalog_position'>
                <?php
                $id_order = 0;
                if ($id_client > 0) {
                    $order = Orders::find()->where([
                        'and',
                        ['id_user' => $id_client],
                        ['send' => 0],
                        'date_end > NOW()',
                    ])->orderBy('id DESC')->limit(1)->one();
                    $id_order = !empty($order->id) ? $order->id : 0;
                }
                foreach ($results as $index => $result) {
                    $id_pos = $result['id'];
                    $name_pos = $result['title'];
                    $type_option = $result['type_option'];
                    $price = $result['price'];
                    if ($type_option == 1) {
                        $tasks = array_key_exists($id_pos, $files) ? $files[$id_pos] : [];
                        $price .= " руб./зад.";
                        $name_pos = "<span class='catalog_option_add'>{$name_pos}</span>";
                    } else {
                        $price .= " руб.";
                    }
                    $style = $index % 2 == 0 ? "background: #f7f7f7;" : "";

                    $show_recycle = "<div class='catalog_position_add'></div>";
                    if ($id_client != 0) {
                        $ordersdesc =
                            OrderDesc::find()->where(['id_position' => $id_pos, 'id_order' => $id_order])->asArray()
                                ->all();
                        if (count($ordersdesc) >= 1) {
                            $show_recycle = "<div class='catalog_position_delete'></div>";
                        }
                    }
                    if (in_array($index, $pos_array)) {
                        $key = array_search($index, $pos_array); ?>
                        <div class='catalog_positionbox' <?php
                        if ($key == $l - 1) {
                            echo "style='padding-right: 0;'";
                        } ?>>
                        <div class='catalog_position'>
                        <div class='catalog_position_tr'>
                            <div class='catalog_position_th'>Номер варианта</div>
                            <div class='catalog_position_th middle_th' style='width: 27.5%;'>Цена</div>
                            <div class='catalog_position_th last_th' style='width: 20.5%;'></div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class='catalog_position_tr' data-id='<?= $id_pos ?>'>
                        <div class='catalog_position_td' style='<?= $style ?>'><?= $name_pos ?></div>
                        <div class='catalog_position_td middle_td' style='width: 27.5%; <?= $style ?>'><?= $price ?></div>
                        <div class='catalog_position_td middle_td last_td' style='width: 20.5%; <?= $style ?>'><?= $show_recycle ?></div>
                    </div>
                    <?php
                    if (in_array($index+1, $pos_array)) {
                        ?>
                        </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>