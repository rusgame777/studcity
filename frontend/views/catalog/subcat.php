<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 09.10.2017
 * Time: 1:35
 */

/**
 * @var $meta common\models\Page;
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;
use frontend\models\CatalogUpload;
use common\models\CatalogCat;
use common\models\CatalogSubcat;
use common\models\University;
use backend\components\Common;
use frontend\components\Notice;

$id_cat = Yii::$app->request->get('cat');
$manual = Yii::$app->request->get('manual');
$id_university = intval(Yii::$app->request->get('university'));

$categories = CatalogSubcat::find()->asArray()->all();
$categories = ArrayHelper::index($categories, 'id');
$universities = University::find()->asArray()->all();
$universities = ArrayHelper::index($universities, 'id');

$name_cat = '';
$category = CatalogCat::find()->where(['id' => $id_cat])->limit(1)->one();
$name_cat = !empty($category->title) ? $category->title : '';
$university = University::find()->where(['id' => $id_university])->limit(1)->one();
$name_university = !empty($university->title) ? $university->title : '';
$name_cat = !empty($name_cat) ? implode('', [
    $name_cat,
    ' (',
    $name_university,
    ')',
]) : $name_university;

?>
<div id='content'>
    <div id='catalog'>

        <?= $this->renderFile('@app/views/layouts/search.php', ['settings' => $settings]); ?>
        <?php Notice::init(); ?>
        <div id='catalog_title'>
            <h1>
                <span>Выберите методичку</span>
                <?php
                if ($id_cat != 0) {
                    ?>
                    /  <a href='<?= Url::canonical() ?>' title='<?= $name_cat ?>'><?= $name_cat ?></a>
                    <?php
                }
                ?>
            </h1>
        </div>
        <?php
        $fileUrl = CatalogUpload::fileUrl();
        $filePath = CatalogUpload::filePath();
    
        $filter = '';
        if ($id_cat != 0) {
            $filter .= " AND id_cat='{$id_cat}'";
        }
        if ($id_university != 0) {
            $filter .= " AND id_university='{$id_university}'";
        }
        if ($manual != '') {
            $filter .= " AND (title LIKE '%{$manual}%' OR author LIKE '%{$manual}%' OR content LIKE '%{$manual}%')";
        }
        $query = "
        SELECT id,
            id_cat,
            title,
            author,
            year,
            content,
            id_university,
            foto,
            foto_small
        FROM catalog_subcat subcat, catalog_subcat_cat subcat_cat
        WHERE subcat.id = subcat_cat.id_subcat
        $filter
        ORDER BY sort ASC, title ASC
        ";
        $results = Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($results)) {
            ?>
            <div id='catalog_subcat'>
                <?php
                foreach ($results as $index => $result) {
                    $direction = $index % 4 == 3 ? 2 : 1;
                    $name_subcat = Html::encode($result['title']);
                    $author = Html::encode($result['author']);
                    $year = Html::encode($result['year']);
                    $content = HtmlPurifier::process($result['content']);
    
                    $university =
                        array_key_exists($result['id_university'], $universities) ? $universities[$result['id_university']] : [];
                    $name_university = array_key_exists('title', $university) ? $university['title'] : '';
    
                    $url_subcat = Url::canonical() . "?" . http_build_query([
                            'cat' => $result['id_cat'],
                            'subcat' => $result['id'],
                            'university' => $result['id_university'],
                        ]);
    
                    $foto = $result['foto'];
                    $foto_small = $result['foto_small'];
                    $pathFoto =
                        is_file($filePath . $foto) ? $filePath . $foto : Yii::getAlias('@frontendDocroot') . "/uploads/no_manual.png";
                    $urlFoto =
                        is_file($filePath . $foto) ? $fileUrl . $foto : Yii::getAlias('@frontendWebroot') . "/uploads/no_manual.png";
    
                    $boxSize = [
                        283,
                        388,
                    ];
                    $getFotoStyle = Common::getFotoStyle($pathFoto, $boxSize);
                    $boxSizeSmall = [
                        140,
                        188,
                    ];
                    $pathFotoSmall =
                        is_file($filePath . $foto_small) ? $filePath . $foto_small : Yii::getAlias('@frontendDocroot') . "/uploads/no_manual.png";
                    $urlFotoSmall =
                        is_file($filePath . $foto_small) ? $fileUrl . $foto_small : Yii::getAlias('@frontendWebroot') . "/uploads/no_manual.png";
    
                    $getFotoStyleSmall = Common::getFotoStyle($pathFotoSmall, $boxSizeSmall);
                    ?>
                    <a data-id='<?= $result['id'] ?>' data-num='<?= $direction ?>' class='catalog_subcat' href='<?= $url_subcat ?>'>
                        <div data-id='<?= $result['id'] ?>' class='position'>
                            <div class='position_foto'>
                        <img <?= $getFotoStyle ?> src="<?= $urlFoto ?>" alt="<?= $name_subcat ?>">
                            </div>
                            <div class='position_info'>
                                <div class='position_title'><?= $name_subcat ?></div>
                                <div class='position_desc'>
                                    <?php
                                    if ($author != '') { ?>
                                        <div class='position_pos'>
                                            <div class='position_pos1'>Автор:</div>
                                            <div class='position_pos2'><?= $author ?></div>
                                        </div>
                                        <?php
                                    }
                                    if ($year != '') {
                                        ?>
                                        <div class='position_pos'>
                                            <div class='position_pos1'>Год:</div>
                                            <div class='position_pos2'><?= $year ?></div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class='position_pos'>
                                        <div class='position_pos1'>ВУЗ:</div>
                                        <div class='position_pos2'><?= $name_university ?></div>
                                    </div>
                                </div>
                                <div class='position_text'><?= $content ?></div>
                            </div>
                        </div>

                        <div class='catalog_subcat_box'>
                            <div class='catalog_subcat_fotobox'>
                                <div class='catalog_subcat_foto'>
                                    <img <?= $getFotoStyleSmall ?> src="<?= $urlFotoSmall ?>" alt="<?= $name_subcat ?>">
                                </div>
                            </div>
                            <div class='catalog_subcat_info'>
                                <div class='catalog_subcat_title'><?= $name_subcat ?></div>
                                <div class='catalog_subcat_desc'>
                                    <?php
                                    if ($author != '') {
                                        ?>
                                        <div class='catalog_subcat_position'>
                                            <div class='catalog_subcat_position1'>Автор:</div>
                                            <div class='catalog_subcat_position2'><?= $author ?></div>
                                        </div>
                                        <?php
                                    }
                                    if ($year != '') {
                                        ?>
                                        <div class='catalog_subcat_position'>
                                            <div class='catalog_subcat_position1'>Год:</div>
                                            <div class='catalog_subcat_position2'><?= $year ?></div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class='catalog_subcat_position'>
                                        <div class='catalog_subcat_position1'>ВУЗ:</div>
                                        <div class='catalog_subcat_position2'><?= $name_university ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                <? } ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>