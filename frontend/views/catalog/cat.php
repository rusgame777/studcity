<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 09.10.2017
 * Time: 1:35
 */

/**
 * @var $meta common\models\Page;
 */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\CatalogUpload;
use backend\components\Common;
use frontend\components\Notice;

$fileUrl = CatalogUpload::fileUrl();
$filePath = CatalogUpload::filePath();

$manual = Yii::$app->request->get('manual');
$id_university = intval(Yii::$app->request->get('university'));
?>
<div id='content'>
    <div id='catalog'>

        <?= $this->renderFile('@app/views/layouts/search.php', ['settings' => $settings]); ?>
        <?= !empty($meta->content) ? sprintf('<div id="catalog_text">%s</div>', $meta->content) : ''; ?>
        <?php Notice::init(); ?>
        <div id='catalog_title'>
            <h1>
                <span>Выберите необходимый предмет</span>
            </h1>
        </div>
        <?php
        $boxSize = [
            420,
            271,
        ];
        $filter = '';
        if ($id_university != 0) {
            $filter .= " AND subcat_cat.id_university={$id_university}";
        }
        $query = "
        SELECT DISTINCT cat.id,
            cat.title,
            cat.foto,
            subcat_cat.id_university,
            university.title name_university
        FROM catalog_cat cat, catalog_subcat_cat subcat_cat, university, catalog_position position
        WHERE cat.foto!=''
        AND cat.id = subcat_cat.id_cat
        AND university.id = subcat_cat.id_university
        AND position.subcat = subcat_cat.id_subcat
        $filter
        ORDER BY cat.sort ASC, cat.title ASC
    ";
        $results = Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($results)) {
            ?>
            <div id='catalog_cat'>
            <?php
            foreach ($results as $index => $result) {
                $foto = $result['foto'];
                $filename = $filePath . $foto;
                if (is_file($filename)) {
                    $url_cat = Url::base() . "?" . http_build_query([
                            'cat' => $result['id'],
                            'university' => $result['id_university'],
                        ]);
                    $name_cat = $result['title'] . ' (' . $result['name_university'] . ')';
                    $name_cat = Html::encode($name_cat);

                    $style = '';
                    $style .= $index % 4 == 0 ? "clear: both;" : "";
                    $style .= $index % 4 == 3 ? "margin-right: 0;" : "";
                    $style = !empty($style) ? 'style="' . $style . '"' : '';
                    $getFotoStyle = Common::getFotoStyle($filename, $boxSize);
                    ?>
                    <a class='catalog_cat' title='<?= $name_cat ?>' href='<?= $url_cat ?>' <?= $style ?>>
                        <div class='catalog_cat_foto'>
                            <img <?= $getFotoStyle ?> src="<?= $fileUrl . $foto ?>" alt="<?= $name_cat ?>">
                        </div>
                        <div class='catalog_cat_base'><span><?= $name_cat ?></span></div>
                    </a>
                    <?php
                }
            }
            ?>
            </div>
            <?php
        }

        ?>
        <div id='catalog_order'>
            <div>
                <div id='catalog_order_title'>Не нашли нужную Вам работу? –
                    <a href='/order/' title='Заказать работу'>Закажите ее!</a></div>
            </div>
        </div>

    </div>
</div>