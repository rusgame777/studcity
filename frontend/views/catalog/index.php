<?php

use yii\web\View;
use common\models\CatalogCat;
use common\models\CatalogSubcat;
use common\models\University;

/**
 * @var $this yii\web\View
 * @var $meta common\models\Page
 * @var $settings common\models\Settings
 */

$manual = Yii::$app->request->get('manual');
$id_university = intval(Yii::$app->request->get('university'));
$cat = intval(Yii::$app->request->get('cat'));
$subcat = intval(Yii::$app->request->get('subcat'));

if ($cat == 0) {
    //
} elseif (($cat != 0) && ($id_university != 0) && ($subcat == 0)) {
    $meta = CatalogCat::findOne($cat);
    $university = University::findOne($id_university);
    $meta->metatitle = !empty($meta->metatitle) ? $meta->metatitle : '';
    $university = University::find()->where(['id' => $id_university])->one();
    $name_university = !empty($university->title) ? $university->title : '';
    $meta->metatitle = !empty($meta->metatitle) ? implode('', [
        $meta->metatitle,
        ' (',
        $name_university,
        ')',
    ]) : $name_university;
} elseif ($subcat != 0) {
    $meta = CatalogSubcat::findOne($subcat);
}

$this->title = $meta->metatitle;
$this->params['metaDescription'] = $meta->description;
$this->params['metaKeywords'] = $meta->keywords;
$this->registerJsFile('/js/catalog.js?20180218', ['depends' => 'yii\web\YiiAsset']);

$pathname = '/' . Yii::$app->request->getPathInfo();
$params = [
    'meta' => $meta,
    'settings' => $settings,
];

$manual = Yii::$app->request->get('manual');
$id_university = intval(Yii::$app->request->get('university'));
$cat = intval(Yii::$app->request->get('cat'));
$subcat = intval(Yii::$app->request->get('subcat'));

if (($cat == 0) && ($manual == '')) {
    echo $this->render('cat', $params);
} elseif (($cat != 0) && ($id_university != 0) && ($subcat == 0) || ($manual != '')) {
    echo $this->render('subcat', $params);
} elseif ($subcat != 0) {
    echo $this->render('position', $params);
}