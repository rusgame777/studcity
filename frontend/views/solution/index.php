<?php

use yii\web\View;
use common\models\SolutionCat;

/**
 * @var $this yii\web\View
 * @var $meta common\models\Page
 * @var $settings common\models\Settings
 */

$manual = Yii::$app->request->get('manual');
$cat = intval(Yii::$app->request->get('cat'));

if (($cat == 0)) {
    //
} elseif ($cat != 0) {
    $meta = SolutionCat::findOne($cat);
}

$this->title = $meta['metatitle'];
$this->params['metaDescription'] = $meta['description'];
$this->params['metaKeywords'] = $meta['keywords'];

$pathname = '/' . Yii::$app->request->getPathInfo();
$params = [
    'meta' => $meta,
    'settings' => $settings,
];
$cat = intval(Yii::$app->request->get('cat'));
$manual = Yii::$app->request->get('manual');

if (($cat == 0) && ($manual == '')) {
    echo $this->render('cat', $params);
} elseif (($cat != 0) || ($manual != '')) {
    echo $this->render('position', $params);
}