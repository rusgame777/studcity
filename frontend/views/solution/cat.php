<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 09.10.2017
 * Time: 1:35
 */

/**
 * @var $meta common\models\Page;
 */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\SolutionUpload;
use common\models\SolutionCat;
use backend\components\Common;
use frontend\components\Notice;

$id_cat = Yii::$app->request->get('cat');
$manual = Yii::$app->request->get('manual');

$fileUrl = SolutionUpload::fileUrl();
$filePath = SolutionUpload::filePath();

?>
<div id='content'>
    <div id='catalog'>

        <?= $this->renderFile('@app/views/layouts/search_solution.php', ['settings' => $settings]); ?>
        <?= !empty($meta->content) ? sprintf('<div id="solution_text">%s</div>', $meta->content) : ''; ?>
        <?php Notice::init(); ?>
        <div id='solution_title'>
            <h1>
                <?php
                if ($manual == '') {
                    ?>
                    <span>Выберите необходимый предмет</span>
                    <?php
                }
                ?>
            </h1>
        </div>
        <?php
        if ($manual == '') {
            $boxSize = [
                420,
                271,
            ];
            $results =
                SolutionCat::find()->where(['!=', 'foto', ''])->orderBy(['sort' => SORT_ASC, 'title' => SORT_ASC])
                    ->asArray()->all();
            if (!empty($results)) {
                ?>
                <div id='solution_cat'>
                <?php
                foreach ($results as $index => $result) {
                    $foto = $result['foto'];
                    $filename = $filePath . $foto;
                    if (is_file($filename)) {
                        $url_cat = Url::canonical() . "?" . http_build_query([
                                'cat' => $result['id'],
                            ]);
                        $name_cat = Html::encode($result['title']);
        
                        $style = '';
                        $style .= $index % 4 == 0 ? "clear: both;" : "";
                        $style .= $index % 4 == 3 ? "margin-right: 0;" : "";
                        $style = !empty($style) ? 'style="' . $style . '"' : '';
                        $getFotoStyle = Common::getFotoStyle($filename, $boxSize);
                        ?>
                        <a class='solution_cat' title='<?= $name_cat ?>' href='<?= $url_cat ?>' <?= $style ?>>
                            <div class='solution_cat_foto'>
                                <img <?= $getFotoStyle ?> src="<?= $fileUrl . $foto ?>" alt="<?= $name_cat ?>">
                            </div>
                            <div class='solution_cat_base'><span><?= $name_cat ?></span></div>
                        </a>
                        <?php
                    }
                }
                ?>
                </div>
                <?php
            }
        }

        if ($manual == '') {
            ?>
            <div id='solution_order'><div>
            <div id='solution_order_title'>
                Не нашли нужную Вам работу? –
                <a href='/order/' title='Заказать работу'>Закажите ее!</a>
            </div>
            </div></div>
            <?php
        }
        ?>
    </div>
</div>