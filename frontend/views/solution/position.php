<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 09.10.2017
 * Time: 1:35
 */

/**
 * @var $meta common\models\Page;
 */

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\SolutionUpload;
use common\models\SolutionCat;
use backend\components\Common;
use frontend\components\Notice;

$id_cat = Yii::$app->request->get('cat');
$manual = Yii::$app->request->get('manual');

$fileUrl = SolutionUpload::fileUrl();
$filePath = SolutionUpload::filePath();

$category = SolutionCat::find()->where(['id' => $id_cat])->limit(1)->one();
$name_cat = Html::encode(!empty($category->title) ? $category->title : '');
?>
<div id='content'>
    <div id='catalog'>
        <?= $this->renderFile('@app/views/layouts/search_solution.php', ['settings' => $settings]); ?>
        <?php Notice::init(); ?>
        <div id='solution_title'>
            <h1>
                <?php
                if (($id_cat != 0) || ($manual != '')) {
                    ?>
                    <span>Выберите РГР</span>
                      <?php
                    if ($id_cat != 0) {
                        ?>
                        / <a href='<?= Url::canonical() ?>' title='<?= $name_cat ?>'><?= $name_cat ?></a>
                        <?php
                    }

                } ?>
            </h1>
        </div>
        <?php

        $filter = '';
        if ($id_cat != 0) {
            $filter .= " AND cat='{$id_cat}'";
        }
        if ($manual != '') {
            $filter .= implode('', [
                " AND (",
                implode(' OR ', [
                    "title LIKE '%{$manual}%'",
                    "content LIKE '%{$manual}%'",
                ]),
                ")",
            ]);
        }

        $query = "
        SELECT id,
            title,
            price,
            limitation,
            comments,
            content,
            foto,
            foto_small
        FROM solution_position
        WHERE 1
        $filter
        ORDER BY sort ASC, title ASC
        ";
        $results = Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($results)) {
            ?>
            <div id='solution_position'>
            <?php
            foreach ($results as $index => $result) {
                $direction = $index % 4 == 3 ? 2 : 1;

                $name_position = Html::encode($result['title']);
                $price = Html::encode($result['price']);
                $limitation = Html::encode($result['limitation']);
                $comments = Html::encode($result['comments']);
                $content = Html::encode($result['content']);

                $foto = $result['foto'];
                $foto_small = $result['foto_small'];
                $pathFoto =
                    is_file($filePath . $foto) ? $filePath . $foto : Yii::getAlias('@frontendDocroot') . "/uploads/no_manual.png";
                $urlFoto =
                    is_file($filePath . $foto) ? $fileUrl . $foto : Yii::getAlias('@frontendWebroot') . "/uploads/no_manual.png";

                $boxSize = [
                    283,
                    388,
                ];
                $getFotoStyle = Common::getFotoStyle($pathFoto, $boxSize);
                $boxSizeSmall = [
                    140,
                    188,
                ];
                $pathFotoSmall =
                    is_file($filePath . $foto_small) ? $filePath . $foto_small : Yii::getAlias('@frontendDocroot') . "/uploads/no_manual.png";
                $urlFotoSmall =
                    is_file($filePath . $foto_small) ? $fileUrl . $foto_small : Yii::getAlias('@frontendWebroot') . "/uploads/no_manual.png";

                $getFotoStyleSmall = Common::getFotoStyle($pathFotoSmall, $boxSizeSmall);

                $style = '';
                $style .= $index % 4 == 0 ? "clear: both;" : "";
                $style .= $index % 4 == 3 ? "margin-right: 0;" : "";
                $style = !empty($style) ? 'style="' . $style . '"' : '';
                ?>
                <div data-id='<?= $result['id'] ?>' data-num='<?= $direction ?>' class='solution_position' <?= $style ?>>
                    <div data-id='<?= $result['id'] ?>' class='solution_pos'>
                    <div class='solution_pos_foto'>
                    <img <?= $getFotoStyle ?> src="<?= $urlFoto ?>" alt="<?= $name_position ?>">
                    </div>
                    <div class='solution_pos_info'>
                    <div class='solution_pos_title'><?= $name_position ?></div>
                    <div class='solution_pos_desc'>
                    <?php
                    if ($price != '') {
                        ?>
                        <div class='solution_pos_value'>
                        <div>Цена:</div>
                        <div><?= $price ?> руб.</div>
                        </div>
                        <?php
                    }
                    if ($limitation != '') {
                        ?>
                        <div class='solution_pos_value'>
                        <div>Срок:</div>
                        <div><?= $limitation ?></div>
                        </div>
                        <?php
                    }
                    ?>
                    </div>
                    <div class='solution_pos_text'><?= $content ?></div>
                    </div>
                    </div>

                    <div class='solution_position_box'>
                    <div class='solution_position_fotobox'><div class='solution_position_foto'>
                    <img <?= $getFotoStyleSmall ?> src="<?= $urlFotoSmall ?>" alt="<?= $name_position ?>">
                    </div></div>
                    <div class='solution_position_info'>
                    <div class='solution_position_title'><?= $name_position ?></div>
                    <div class='solution_position_desc'>
                    <?php
                    if ($price != '') {
                        ?>
                        <div class='solution_position_value'>
                        <div>Цена:</div>
                        <div><?= $price ?> руб.</div>
                        </div>
                        <?php
                    }
                    if ($limitation != '') {
                        ?>
                        <div class='solution_position_value'>
                        <div>Срок:</div>
                        <div><?= $limitation ?></div>
                        </div>
                        <?php
                    }
                    if ($comments != '') {
                        ?>
                        <div class='solution_position_value'>
                        <?= $comments ?>
                        </div>
                    <?php }
                    ?>
                    </div>
                    </div>
                    </div>
                </div>
                <?php
            }
            ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>