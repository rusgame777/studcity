<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\db\Expression;
use common\models\SolutionData;
use common\models\SolutionPosition;
use frontend\models\SolutionForm;
use frontend\models\SolutionUpload;

$data = Json::decode($data, true);
$id = array_key_exists('id', $data) ? intval($data['id']) : 0;
if ($id == 0) {
    exit;
}

$position = SolutionPosition::findOne($id);
if ($position === null) {
    exit;
}
$name_pos = Html::encode($position->title);
$solutionParams = SolutionData::find()->where(['id_position' => $id])->orderBy(['id' => SORT_ASC])->all();
$solutionForm = new SolutionForm();
$solutionForm->id = $id;
$solutionUpload = new SolutionUpload();

$fieldConfig = [
    'options' => ['class' => 'site-form-group solution_contacts_input'],
    'hintOptions' => [
        'tag' => 'p',
        'class' => 'site-hint-block',
    ],
    'errorOptions' => [
        'tag' => 'p',
        'class' => 'site-help-block site-help-block-error',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'inputOptions' => [
        'class' => 'site-form-control',
    ],
];

$form = ActiveForm::begin([
    'id' => 'solution-form',
    'action' => [
        Url::to([
            'site/solution-create',
        ]),
    ],
    'fieldConfig' => $fieldConfig,
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validationDelay' => 5000,
    'validationUrl' => Url::to(['site/solution-validate']),
]);
?>
    <div id="solution_box"><div>
    <div id='solution_name'>Решение за мгновение "<?= $name_pos ?>"</div>
    <div id='solution_comments'>Вводимые данные конфиденциальны и не при каких условиях не будут переданы третьим лицам</div>
    <div id='solution_left'>
    <?php
    if (count($solutionParams) > 0) {
        ?>
        <div id='solution_data_title'>Данные для РГР</div>
        <div id='solution_data_input'>
            <?php
            foreach ($solutionParams as $indexItem => $solutionParam) {
                /**
                 * @var $solutionParam common\models\SolutionOrderData
                 */
                $placeholder = $solutionParam->name;
                $solutionParam->name = '';
                echo $form->field($solutionParam, '[' . $indexItem . ']id')->hiddenInput()->hint(false)->label(false);
                echo $form->field($solutionParam, '[' . $indexItem . ']name', [
                    'options' => ['class' => 'site-form-group solution_data_input'],
                ])->textInput(['placeholder' => $placeholder])->label(false);
            }
            ?>
        </div>
        <?php
    }
    ?>
    </div>
    <div id='solution_right'>
    <div id='solution_contacts_title'>Заполните ваши данные</div>
    <div id='solution_contacts_input'>
    <?= $form->field($solutionForm, 'id')->hiddenInput()->hint(false)->label(false); ?>
    <?= $form->field($solutionForm, 'dataArray')->hiddenInput()->hint(false)->label(false); ?>
    <?= $form->field($solutionForm, 'email')->textInput(['placeholder' => $solutionForm->getAttributeLabel('email')])
        ->label(false); ?>
    <?= $form->field($solutionForm, 'name')->textInput(['placeholder' => $solutionForm->getAttributeLabel('name')])
        ->label(false); ?>
    <?= $form->field($solutionForm, 'phone')->textInput(['placeholder' => $solutionForm->getAttributeLabel('phone')])
        ->label(false); ?>
    </div>
    </div>
            <?
            echo $form->field($solutionUpload, 'file', [
                'template' => implode("\n", [
                    "{label}",
                    "<button type='button'>Выбрать вложение</button>",
                    "<div>",
                    "</div>",
                    "{input}",
                    "{hint}",
                    "{error}",
                ]),
                'options' => ['class' => 'form-group form-file', 'id' => 'solution_file'],
            ])->fileInput([
                'multiple' => true,
                'accept' => '.doc, .docx, .rar, .zip, .jpg, .png, .pdf',
            ])->label(false);
            ?>
            <div id='solution_enter'><span>Оформить заказ</span></div>
    <div id="solution_close"></div>
    </div>
    </div>
<?php ActiveForm::end(); ?>