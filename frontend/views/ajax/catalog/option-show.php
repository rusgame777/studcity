<?php

use yii\helpers\ArrayHelper;
use common\models\CatalogPosition;
use common\models\CatalogFiles;
use common\models\CatalogFoto;
use common\models\Orders;
use common\models\OrderDesc;
use yii\helpers\Json;

/**
 * @var $data array
 */

$status = 0;
$array = [];
$data = Json::decode($data, true);
$id_position = array_key_exists('id_position', $data) ? $data['id_position'] : 0;

$id_client = Yii::$app->params['id_client'];
$id_order = 0;

if ($id_position != 0) {
    $position = CatalogPosition::findOne($id_position);
    if (null === $position) {
        return true;
    }
    $price = $position->price;
    $price_word = $price . " руб.";
    
    $results =
        CatalogFiles::find()->where(['position' => $id_position])->orderBy(["CAST(REPLACE(title, '-', '') AS UNSIGNED)" => SORT_ASC])
            ->asArray()->all();
    $numResults = count($results);
    
    $photos = CatalogFoto::find()->where(['position' => $id_position])->orderBy(["CAST(REPLACE(title, '-', '') AS UNSIGNED)" => SORT_ASC])
        ->asArray()->all();
    $photos = ArrayHelper::index($photos, 'title');
    
    $pos_array = [];
    $num = 0;
    $l = 0;
    for ($j = 0; $j <= 1; $j++) {
        $num = $j * ceil($numResults / 2);
        if ($numResults >= $num) {
            $pos_array[$l] = $num;
            $l++;
        }
    }
    if ($pos_array[$l - 1] != $numResults) {
        $pos_array[$l] = $numResults;
    } else {
        $l -= 1;
    }
    
    ?>
    <div id="option_box">
        <div class="option_box">
        <div id='option_title'>Выберите задачу</div>
            <?php
            if ($numResults > 0) {
                ?>
                <div id='option_list'>
                <?
                if ($id_client > 0) {
                    $order = Orders::find()->where([
                        'and',
                        ['id_user' => $id_client],
                        ['send' => 0],
                        'date_end > NOW()',
                    ])->orderBy('id DESC')->limit(1)->one();
                    $id_order = !empty($order->id) ? $order->id : 0;
                }
                foreach ($results as $index => $result) {
                    $id_option = $result['id'];
                    $name_option = $result['title'];
                    $num_ordersdesc = 0;
                    if ($id_order != 0) {
                        $ordersdesc = OrderDesc::find()->where([
                            'id_order' => $id_order,
                            'id_position' => $id_position,
                            'id_option' => $id_option,
                        ])->asArray()->all();
                        $num_ordersdesc = count($ordersdesc);
                    }
                    if (in_array($index, $pos_array)) {
                        $key = array_search($index, $pos_array);
                        $style = ($key == $l - 1) ? "style='padding-right: 0;'" : ''; ?>
                        <div class='option_list' <?= $style ?>>
                        <?php
                    }
                    $inRecycle = $num_ordersdesc == 1 ? 'checked' : '';
                    ?>
                    <div class='option_check' data-id='<?= $id_option ?>'>
                    <div class='option_checkbox'>
                    <input type="checkbox" <?= $inRecycle ?> id='option<?= $id_option ?>' name='option<?= $id_option ?>'>
                    <label for='option<?= $id_option ?>'>Задача №<?= $name_option ?></label>
                        <?php
                        if (array_key_exists($name_option, $photos)) {
                            ?>
                            <a class="photo" data-position="<?= $id_position ?>" data-option="<?= $id_option ?>" data-fancybox="gallery" href="<?= CatalogFoto::fileUrl() .
                            $photos[$name_option]['file'] ?>">
                                <img src="/img/camera.png">
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class='option_slash'>/</div>
                    <div class='option_price'><?= $price_word ?></div>
                    </div>
                    <?php
                    if (in_array($index + 1, $pos_array)) { ?>
                        </div>
                        <?php
                    }
                }
                ?>
                </div>
                <?php
            }
            $sum_all = 0;
            if ($id_order != 0) {
                $positions =
                    OrderDesc::find()->where(['id_order' => $id_order, 'id_position' => $id_position])->asArray()
                        ->all();
                $sum_all = count($positions) * $price;
            }
            $sum_all .= " руб.";
            ?>
            <div class='option_stripe'></div>
            <div class='option_sum'>
            <div class='option_sum_title'>Итоговая стоимость:</div>
            <div class='option_sum_value'><?= $sum_all ?></div>
            </div>
        </div>
        <div id="option_close"></div>
    </div>
    <?php
}
