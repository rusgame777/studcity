<?php

use common\models\Orders;
use common\components\SolutionOrderRemember;
use common\models\OrderType;
use common\models\OrdersRemember;
use common\models\OrderStatus;
use backend\components\Common;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

$orders = Orders::find()->select([Orders::tableName() . '.id'])->where([
    'and',
    ['id_type' => OrderType::solution],
    ['send' => 1],
    ['status' => OrderStatus::send],
    [
        '<=',
        new Expression('IFNULL(remember.date_next_send, NOW())'),
        new Expression('NOW()'),
    ],
])->leftJoin(OrdersRemember::tableName() . ' remember', [
    'and',
    [
        '=',
        'remember.id_order',
        new Expression(Orders::tableName() . '.id'),
    ],
])->asArray()->all();

$orders = ArrayHelper::getColumn($orders, 'id');
foreach ($orders as $id_order) {
    if (($orderRemember = OrdersRemember::findOne(['id_order' => $id_order])) === null) {
        $orderRemember = new OrdersRemember();
    }
    $data = [
        'id_order' => $id_order,
        'date_next_send' => Common::getDateTime('', 'Y-m-d H:i:s', '+3 day'),
    ];
    
    if ($orderRemember->load($data) && $orderRemember->save()) {
        
        $orderSolutionRemember = new SolutionOrderRemember($id_order);
        $orderSolutionRemember->response();
    }
}
var_dump($orders);