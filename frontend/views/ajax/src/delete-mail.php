<?php

use frontend\models\OrderUpload;
use common\models\Downloads;
use backend\components\Common;

//$curDate = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
$curDate = Common::getDateTime('', 'Y-m-d H:i:s');
$patternOrder = "/^([\d]{1,})-([\d]{1,})-([\d]{1,})-([\d]{1,}).eml$/";
$day = 60 * 60 * 24;
$hour = 60 * 60;

$nameDir = Yii::getAlias('@frontend/runtime/debug/mail');
if (is_dir($nameDir)) {
    $handleDir = opendir($nameDir);
    while (false !== ($nameFile = readdir($handleDir))) {
        if ($nameFile != '.' && $nameFile != '..') {
            $tmpPath = $nameDir . '/' . $nameFile;
            if ((is_file($tmpPath)) && (preg_match($patternOrder, $nameFile))) {
                $date = DateTime::createFromFormat('Ymd-His', preg_replace($patternOrder, "$1-$2", $nameFile));
                $date_add = $date->format('Y-m-d H:i:s');
                $diffdays = floor((strtotime($curDate) - strtotime($date_add)) / $day);
                if ($diffdays > 2) {
                    $id_order = preg_replace($patternOrder, "$1-$2", $nameFile);
                    unlink($tmpPath);
                    echo "копия письма №" . $id_order . " удален<br>";
                }
            }
        }
    }
    closedir($handleDir);
}