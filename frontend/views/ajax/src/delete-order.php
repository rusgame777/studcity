<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 22.01.2018
 * Time: 23:22
 */

use frontend\models\OrderUpload;
use common\models\Downloads;
use backend\components\Common;

//$curDate = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
$curDate = Common::getDateTime('', 'Y-m-d H:i:s');
$patternOrder = "/^order([\d]{1,}).zip$/";
$day = 60 * 60 * 24;
$hour = 60 * 60;

$nameDir = OrderUpload::filePath();
if (is_dir($nameDir)) {
    $handleDir = opendir($nameDir);
    while (false !== ($nameFile = readdir($handleDir))) {
        if ($nameFile != '.' && $nameFile != '..') {
            $tmpPath = $nameDir . '/' . $nameFile;
            if ((is_file($tmpPath)) && (preg_match($patternOrder, $nameFile))) {
                $date_add = Yii::$app->formatter->asDate(filemtime($tmpPath), 'php:Y-m-d H:i:s');
//                $date_add = date('Y-m-d H:i:s', filemtime($tmpPath));
                $diffdays = floor((strtotime($curDate) - strtotime($date_add)) / $day);
                if ($diffdays > 2) {
                    $id_order = preg_replace($patternOrder, "$1", $nameFile);
                    
                    $orders = Downloads::findAll(['id_order' => $id_order]);
                    if (!empty($orders)) {
                        Downloads::updateAll(['bit_delete' => 1], ['id_order' => $id_order]);
                    }

                    unlink($tmpPath);
                    echo "архив заказа №" . $id_order . " удален<br>";
                }
            }
        }
    }
    closedir($handleDir);
}