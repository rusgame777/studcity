<?

use common\components\Phone;
use common\models\SmsAuthor;
use common\models\SmsClient;
use yii\db\Expression;

$results = SmsAuthor::find()->where([
    'and',
    ['!=', 'phone', ''],
    ['in', 'status', Phone::waiting],
    ['>=', 'date_send', new Expression('DATE_SUB(CURDATE(), INTERVAL 1 day)')],
])->all();
if (!empty($results)) {
    foreach ($results as $result) {
        /**
         * @var $result SmsAuthor
         */
        list($status, $time) = Phone::get_status($result->num, $result->phone, 0);
        if (!empty($time) && ($time >= 0)) {
            SmsAuthor::updateAll(['status' => $status], ['id_sms' => $result->id_sms]);
            echo $result->num . "<br>";
        }
    }
}

$results = SmsClient::find()->where([
    'and',
    ['!=', 'phone', ''],
    ['in', 'status', Phone::waiting],
    ['>=', 'date_send', new Expression('DATE_SUB(CURDATE(), INTERVAL 1 day)')],
])->all();
if (!empty($results)) {
    foreach ($results as $result) {
        /**
         * @var $result SmsAuthor
         */
        list($status, $time) = Phone::get_status($result->num, $result->phone, 0);
        if (!empty($time) && ($time >= 0)) {
            SmsClient::updateAll(['status' => $status], ['id_sms' => $result->id_sms]);
            echo $result->num . "<br>";
        }
    }
}
