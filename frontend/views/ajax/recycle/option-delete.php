<?php

use yii\helpers\Json;
use yii\db\Expression;
use common\models\Clients;
use common\models\Orders;
use common\models\OrderDesc;

$status = 0;
$array = [];
$data = Json::decode($data, true);

if (empty($data)) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}

$id_position = intval($data['id_position']);
$id_option = intval($data['id_option']);
$count_user = 0;
$id_order = 0;
$id_client = Clients::create();

$count_user = Orders::find()->where([
    'and',
    ['id_user' => $id_client],
    ['send' => 0],
    ['>', 'date_end', new Expression('NOW()')],
])->count('id');
$date_end = (new \DateTime('', Yii::$app->params['timezone']))->modify("+ 1 month")->format('Y-m-d H:i:s');

$order = Orders::find()->where([
    'and',
    ['id_user' => $id_client],
    ['send' => 0],
    ['>', 'date_end', new Expression('NOW()')],
])->orderBy(['id' => SORT_DESC])->limit(1)->one();
/**
 * @var $order Orders
 */
$id_order = !empty($order->id) ? $order->id : 0;

if ($id_order == 0) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}

$options = OrderDesc::deleteAll([
    'id_option' => $id_option,
    'id_position' => $id_position,
    'id_order' => $id_order,
]);

$sum = OrderDesc::find()->where(['id_order' => $id_order])->sum('sum');
$sum = $sum !== null ? $sum : 0;

$sum_position = OrderDesc::find()->where(['id_order' => $id_order, 'id_position' => $id_position])->sum('sum');
$sum_position = $sum_position !== null ? $sum_position : 0;

$order->date_end = $date_end;
$order->sum = $sum;
$order->update();

$status = 1;
$array['sum'] = $sum;
$array['sum_position'] = $sum_position;
$array['status'] = $status;
echo Json::encode($array);