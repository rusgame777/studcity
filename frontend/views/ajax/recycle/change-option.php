<?php

use yii\helpers\Json;
use yii\web\Cookie;
use yii\db\Expression;
use common\models\Clients;
use common\models\Orders;
use common\models\OrderDesc;
use common\models\CatalogPosition;
use common\models\CatalogSubcatCat;
use common\models\CatalogFiles;

$status = 0;
$array = [];
$data = Json::decode($data, true);

if (empty($data)) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}

$id_client = Yii::$app->params['id_client'];
$id_option = intval($data['id_option']);
$value_option = intval($data['value_option']);
$count_user = 0;
$id_avt = Yii::$app->request->cookies->getValue('id_avt');
$id_order = 0;
if ($id_client == 0) {
    $id_avt = md5(uniqid(rand(), 1));
    $params = [
        'id_avt' => $id_avt,
    ];
    $client = new Clients();
    if ($client->load($params) && $client->save()) {
        $id_client = $client->getPrimaryKey();
    }
    
    Yii::$app->response->cookies->add(new Cookie([
        'name' => 'id_avt',
        'value' => $id_avt,
        'expire' => time() + 3600 * 24 // 1 day,
    ]));
}

$count_user = Orders::find()->where([
    'and',
    ['id_user' => $id_client],
    ['send' => 0],
    ['>', 'date_end', new Expression('NOW()')],
])->count('id');
$date_end = (new \DateTime('', Yii::$app->params['timezone']))->modify("+ 1 month")->format('Y-m-d H:i:s');

if ($count_user == 0) {
    
    $num_orders = Orders::find()->where([
        'and',
        ['id_user' => $id_client],
        ['send' => 0],
        ['<', 'date_end', new Expression('NOW()')],
    ])->count('id');
    if ($num_orders > 0) {
        Clients::updateAll(['send' => 1, 'status' => 6], ['id_user' => $id_client]);
    }
    
    $params = [
        'id_user' => $id_client,
        'date_end' => $date_end,
    ];
    $order = new Orders();
    if ($order->load($params) && $order->save()) {
        $id_order = $order->getPrimaryKey();
    }
}

$order = Orders::find()->where([
    'and',
    ['id_user' => $id_client],
    ['send' => 0],
    ['>', 'date_end', new Expression('NOW()')],
])->orderBy(['id' => SORT_DESC])->limit(1)->one();
$id_order = !empty($order->id) ? $order->id : 0;

if ($id_order == 0) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}
$order->date_end = $date_end;
$order->update();

$option = CatalogFiles::findOne($id_option);
if ($option == null) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}
$id_position = $option->position;

$position = CatalogPosition::findOne($id_position);
if ($position == null) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}
$id_subcat = $position->subcat;
$price = $position->price;
$type_option = $position->type_option;

$subcategory = CatalogSubcatCat::find()->where(['id_subcat' => $id_subcat])->limit(1)->one();
if ($subcategory == null) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}
$id_cat = $subcategory->id_cat;

$numb = 1;
$sum = $price * $numb;

$check = OrderDesc::find()->where([
    'id_order' => $id_order,
    'id_position' => $id_position,
    'id_option' => $id_option,
])->count(0);

if ($value_option == 1) {
    $params = [
        'id_option' => $id_option,
        'id_position' => $id_position,
        'subcat' => $id_subcat,
        'cat' => $id_cat,
        'numb' => $numb,
        'price' => $price,
        'sum' => $sum,
        'id_order' => $id_order,
    ];
    $orderdesc = new OrderDesc();
    if ($orderdesc->load($params) && $orderdesc->save()) {
        //
    }
} elseif ($value_option == 0) {
    OrderDesc::deleteAll([
        'id_option' => $id_option,
        'id_position' => $id_position,
        'id_order' => $id_order,
    ]);
}

$status = 1;
$num_desc = OrderDesc::find()->where(['id_order' => $id_order])->count(0);
if ($num_desc > 0) {
    $sum = OrderDesc::find()->where(['id_order' => $id_order])->sum('sum');
} else {
    $sum = 0;
}
Orders::updateAll(['sum' => $sum], ['id' => $id_order]);
$array['sum'] = $sum;

$sum_options = OrderDesc::find()->where([
    'id_position' => $id_position,
    'id_order' => $id_order,
])->sum('sum');
if (is_null($sum_options)) {
    $sum_options = 0;
}

$array['sum_options'] = $sum_options;
$array['id_position'] = $id_position;

$array['status'] = $status;
echo Json::encode($array);