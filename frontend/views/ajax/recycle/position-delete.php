<?php

use yii\helpers\Json;
use yii\db\Expression;
use common\models\Clients;
use common\models\Orders;
use common\models\OrderDesc;

$status = 0;
$array = [];
$data = Json::decode($data, true);

if (empty($data)) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}

$id_position = intval($data['id_position']);
$count_user = 0;
$id_order = 0;

$id_client = Clients::create();

$count_user = Orders::find()->where([
    'and',
    ['id_user' => $id_client],
    ['send' => 0],
    ['>', 'date_end', new Expression('NOW()')],
])->count('id');
$date_end = (new \DateTime('', Yii::$app->params['timezone']))->modify("+ 1 month")->format('Y-m-d H:i:s');

if ($count_user == 0) {
    
    $num_orders = Orders::find()->where([
        'and',
        ['id_user' => $id_client],
        ['send' => 0],
        ['<', 'date_end', new Expression('NOW()')],
    ])->count('id');
    if ($num_orders > 0) {
        Clients::updateAll(['send' => 1, 'status' => 6], ['id_user' => $id_client]);
    }
    
    $params = [
        'id_user' => $id_client,
        'date_end' => $date_end,
    ];
    $order = new Orders();
    if ($order->load($params) && $order->save()) {
        $id_order = $order->getPrimaryKey();
    }
}

$order = Orders::find()->where([
    'and',
    ['id_user' => $id_client],
    ['send' => 0],
    ['>', 'date_end', new Expression('NOW()')],
])->orderBy(['id' => SORT_DESC])->limit(1)->one();
$id_order = !empty($order->id) ? $order->id : 0;

if ($id_order == 0) {
    $array['status'] = $status;
    echo Json::encode($array);
    return true;
}
$order->date_end = $date_end;
$order->update();

$check = OrderDesc::find()->where([
    'id_order' => $id_order,
    'id_position' => $id_position,
])->count(0);

if ($check > 0) {
    OrderDesc::deleteAll([
        'id_order' => $id_order,
        'id_position' => $id_position,
    ]);
    $status = 1;
}

if ($status == 1) {
    $num_desc = OrderDesc::find()->where(['id_order' => $id_order])->count(0);
    if ($num_desc > 0) {
        $sum = OrderDesc::find()->where(['id_order' => $id_order])->sum('sum');
    } else {
        $sum = 0;
    }
    Orders::updateAll(['sum' => $sum], ['id' => $id_order]);
    $array['sum'] = $sum;
}

$array['status'] = $status;
echo Json::encode($array);