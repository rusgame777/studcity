<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\db\Expression;
use common\models\Orders;
use common\models\OrderDesc;
use common\models\CatalogPosition;
use common\models\CatalogSubcatCat;
use common\models\CatalogSubcat;
use common\models\CatalogFoto;
use common\models\University;
use frontend\models\RecycleForm;

$recycleForm = new RecycleForm();
$fieldConfig = [
    'options' => ['class' => 'site-form-group recycle_input'],
    'hintOptions' => [
        'tag' => 'p',
        'class' => 'site-hint-block',
    ],
    'errorOptions' => [
        'tag' => 'p',
        'class' => 'site-help-block site-help-block-error',
    ],
    'labelOptions' => [
        'class' => 'control-label',
    ],
    'inputOptions' => [
        'class' => 'site-form-control',
    ],
];

$id_client = Yii::$app->params['id_client'];
$order = Orders::find()->where([
    'and',
    ['id_user' => $id_client],
    ['send' => 0],
    ['>', 'date_end', new Expression('NOW()')],
])->orderBy(['id' => SORT_DESC])->limit(1)->one();

if ($order === null) {
    exit;
}

$id_order = $order->id;
$recycleForm->id = $id_order;
$sum_all = OrderDesc::find()->where(['id_order' => $id_order])->sum('sum');
$sum_all .= " руб.";

$positions = CatalogPosition::find()->asArray()->all();
$positions = ArrayHelper::index($positions, 'id');
$subcategories = CatalogSubcat::find()->asArray()->all();
$subcategories = ArrayHelper::index($subcategories, 'id');
$universities = University::find()->asArray()->all();
$universities = ArrayHelper::index($universities, 'id');

$cat2subcat = [];
$rows = CatalogSubcatCat::find()->asArray()->all();
foreach ($rows as $row) {
    $cat2subcat[$row['id_cat']][$row['id_subcat']] = $row['id_university'];
}

?>
<div id="recycle_box">
    <div class="recycle_box">
        <div id='recycle_title'>Оформление заказа</div>
        <div id="recycle_table">
            <div class='recycle_header'>
                <div class='position_th first_th' style='width: 799px;'>Позиция заказа</div>
                <div class='position_th last_th' style='width: 118px;'>Цена, руб.</div>
            </div>
            <?
            $ordersdesc =
                OrderDesc::find()->select(['id_position', 'sum'])->where(['id_order' => $id_order])->distinct()
                    ->asArray()->all();
            $num_orderdesc = count($ordersdesc);
            foreach ($ordersdesc as $index => $orderdesc) {
                $id_position = $orderdesc['id_position'];
                $sum = $orderdesc['sum'];
                $position = array_key_exists($id_position, $positions) ? $positions[$id_position] : [];
                $name_variant = array_key_exists('title', $position) ? $position['title'] : '';
                $file_variant = array_key_exists('file', $position) ? $position['file'] : '';
                $type_option = array_key_exists('type_option', $position) ? $position['type_option'] : '';
                $subcat = array_key_exists('subcat', $position) ? $position['subcat'] : 0;
                $cat = array_key_exists('cat', $position) ? $position['cat'] : 0;
                $name_variant = str_replace("вар", "", $name_variant);
        
                $photos = CatalogFoto::find()->where(['position' => $id_position])
                    ->orderBy(["CAST(REPLACE(title, '-', '')  AS UNSIGNED)" => SORT_ASC])->asArray()->all();
                $photos = ArrayHelper::index($photos, 'title');
        
                $subcategory = array_key_exists($subcat, $subcategories) ? $subcategories[$subcat] : [];
                $name_manual = array_key_exists('title', $subcategory) ? $subcategory['title'] : '';
                $author = array_key_exists('author', $subcategory) ? $subcategory['author'] : '';
                $year = array_key_exists('year', $subcategory) ? $subcategory['year'] : '';
        
                $id_university = 0;
                if (array_key_exists($cat, $cat2subcat)) {
                    if (array_key_exists($subcat, $cat2subcat[$cat])) {
                        $id_university = $cat2subcat[$cat][$subcat];
                    }
                }
        
                $university = array_key_exists($id_university, $universities) ? $universities[$id_university] : [];
                $name_university = array_key_exists('title', $university) ? $university['title'] : '';
        
                $text = vsprintf(implode('', [
                    '<span class="recycle_manual">%1$s',
                    !empty($author) ? ' / ' . $author : '',
                    !empty($year) ? ' / ' . $year . 'г . ' : '',
                    '</span>',
                    '<span class="recycle_option_block">',
                    '<span class="recycle_option">Вариант №%2$s</span>',
                ]), [
                    1 => $name_manual,
                    2 => $name_variant,
                ]);
        
                if ($type_option == 1) {
                    $options =
                        Yii::$app->db->createCommand("SELECT id_option, title, file FROM orderdesc, catalog_files WHERE orderdesc.id_option =  catalog_files.id AND id_order = :id_order AND id_position = :id_position ORDER BY CAST(REPLACE(title, '-', '') AS UNSIGNED) ASC", [
                            'id_order' => $id_order,
                            'id_position' => $id_position,
                        ])->queryAll();
                    $num_options = count($options);
                    if ($num_options > 0) {
                        $text .= "  / Задача ";
                        foreach ($options as $i => $option) {
                            if ($i > 0) {
//                                $text .= ", ";
                            }
                            $name_option = $option['title'];
                            
                            $text .= implode("", [
                                sprintf("<span title='удалить задачу' data-option='%s'>", $option['id_option']),
                                $name_option,
                                "</span>",
                            ]);
    
                            if (array_key_exists($name_option, $photos)) {
                                ob_start();
                                ?>
                                <a class="photo-recycle" data-option="<?= $option['id_option'] ?>" data-fancybox="gallery" href="<?= CatalogFoto::fileUrl() .
                                $photos[$name_option]['file'] ?>">
                                <img src="/img/camera.png">
                            </a>
                                <?php
                                $output = ob_get_clean();
                                $text .= $output;
                            }
                    
                        }
                    }
                    $sum *= $num_options;
                }
                $sum .= " руб.";
                $text .= "</span>";
                $text .= "<span class='recycle_university'>{$name_university}</span>";
                ?>
                
                
                
                
                
                
                <div data-id='<?= $id_position ?>' class='position_tr'>
                    <div class='position_td first_td' style='width: 799px;'>
                        <div class='recycle_info'>
                            <?= $text ?>
                            <div class='recycle_delete'></div>
                        </div>
                    </div>
                    <div class='position_td last_td' style='width: 118px;'><?= $sum ?></div>
                </div>
                <?php
            }
            ?>
            <div class='position_tr'>
                <div class='position_td first_td sum_all_title'>Итоговая стоимость, руб.:</div>
                <div class='position_td last_td sum_all_value'><?= $sum_all ?></div>
            </div>
        </div>
        <div id='recycle_comments'>
            Вводимые данные конфиденциальны и не при каких условиях не будут переданы третьим лицам
        </div>
        <div id='recycle_data'>Заполните ваши данные</div>
        <?php
        $form = ActiveForm::begin([
            'id' => 'recycle-form',
            'action' => [
                Url::to([
                    'site/recycle-create',
                ]),
            ],
            'fieldConfig' => $fieldConfig,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validationDelay' => 5000,
            'validationUrl' => Url::to(['site/recycle-validate']),
        ]);
        ?>
        <div id='recycle_input'>
        <?= $form->field($recycleForm, 'id')->hiddenInput()->label(false); ?>
        <?= $form->field($recycleForm, 'name')->textInput(['placeholder' => $recycleForm->getAttributeLabel('name')])
            ->label(false); ?>
        <?= $form->field($recycleForm, 'phone')->textInput(['placeholder' => $recycleForm->getAttributeLabel('phone')])
            ->label(false); ?>
        <?= $form->field($recycleForm, 'email')->textInput(['placeholder' => $recycleForm->getAttributeLabel('email')])
            ->label(false); ?>
        </div>
        <?php ActiveForm::end(); ?>
        <div id='recycle_status'></div>
        <div id='recycle_enter'><span>Оформить заказ</span></div>
    </div>
    <div id="recycle_close"></div>
</div>