<?php

$srok = intval(Yii::$app->request->cookies->getValue('srok'));
Yii::$app->response->cookies->remove('srok');
?>
<div id="success_box">
<div class="success_box">
<div id='success_title'>Спасибо, ваш заказ оплачен!</div>
<div id='success_info'>
Заказ отправлен на email указанный вами при оплате.
    <?php
    if ($srok == 0) {
        echo "Если через 5 минут письмо не пришло, проверьте, не оказалось ли оно в папке «Спам».";
    } else {
        echo "Если в течении указанного срока письмо не пришло, проверьте, не оказалось ли оно в папке «Спам».";
    } ?>
    По всем возникшим вопросам пишите на <a href='mailto:admin@stud-city.ru' target='_blank'>admin@stud-city.ru</a>
Надеемся на долгосрочное сотрудничество!!!
</div>

<div id='success_status'></div>
<div id='success_enter'><span>Вернуться на сайт</span></div>
</div>
<div id="success_close"></div>
</div>