<?php

use yii\helpers\Html;

$title = $model->title;

//seo
$this->title = empty($model->metatitle) ? $title : $model->metatitle;
if (!empty($model->description))
	$this->params['metaDescription'] = $model->description;
if (!empty($model->keywords))
	$this->params['metaKeywords'] = $model->keywords;

?>
<h1><?= Html::encode($title) ?></h1>

<?= $model->content ?>
