<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Page;
use common\models\Settings;
use common\models\Clients;

/**
 * Catalog controller
 */
class CatalogController extends Controller
{
    public $layout = '@app/views/layouts/standard.php';
    public $defaultAction = 'index';
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/index',
            ],
        ];
    }
    
    /**
     * Displays homepage.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $path = '/' . Yii::$app->request->getPathInfo();
        $meta = Page::find()->where(['nameurl' => $path])->one();
        if (null === $meta) {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
        $settings = Settings::findOne(1);
        
        return $this->render('index', [
            'meta' => $meta,
            'settings' => $settings,
        ]);
    }
    
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Clients::getClientId();
            return true;
        }
        return false;
    }
    
 }