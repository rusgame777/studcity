<?php
namespace frontend\controllers;

use Yii;
use yii\bootstrap\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Cookie;
use frontend\components\Notice;
use backend\models\OrdersDocs;
use common\models\Clients;
use common\models\Downloads;
use common\models\Page;
use common\models\Settings;
use common\models\SettingsEmail;
use common\models\SolutionData;
use common\models\SolutionPosition;
use common\models\OrderCheck;
use common\models\SolutionOrderDesc;
use common\models\Orders;
use common\models\SolutionOrderData;
use common\models\OrderStatus;
use common\models\OrderType;
use common\components\Email;
use common\components\Order;
use frontend\models\OrderForm;
use frontend\models\OrderUpload;
use frontend\models\ReviewForm;
use frontend\models\ReverseForm;
use frontend\models\RecycleForm;
use frontend\models\SolutionForm;
use backend\components\Common;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = '@app/views/layouts/standard.php';
    public $defaultAction = 'index';
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/index',
            ],
        ];
    }
    
    /**
     * Displays homepage.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/main.php';
        $path = '/' . Yii::$app->request->getPathInfo();
        $meta = Page::find()->where(['nameurl' => $path])->one();
        if (null === $meta) {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
        $settings = Settings::findOne(1);
        
        return $this->render('index', [
            'meta' => $meta,
            'settings' => $settings,
        ]);
    }
    
    public function actionAbout()
    {
        $this->layout = '@app/views/layouts/standard.php';
        $path = '/' . Yii::$app->request->pathInfo;
        $meta = Page::find()->where(['nameurl' => $path])->one();
        if (null === $meta) {
            $this->redirect('/')->send();
            exit;
        }
        $settings = Settings::findOne(1);
        
        return $this->render('index', [
            'meta' => $meta,
            'settings' => $settings,
        ]);
    }
    
    public function actionGb()
    {
        $this->layout = '@app/views/layouts/standard.php';
        $path = '/' . Yii::$app->request->pathInfo;
        $meta = Page::find()->where(['nameurl' => $path])->one();
        if (null === $meta) {
            $this->redirect('/')->send();
            exit;
        }
        $settings = Settings::findOne(1);
        
        return $this->render('gb', [
            'meta' => $meta,
            'settings' => $settings,
        ]);
    }
    
    public function actionOrder()
    {
        $this->layout = '@app/views/layouts/standard.php';
        $path = '/' . Yii::$app->request->pathInfo;
        $meta = Page::find()->where(['nameurl' => $path])->one();
        if (null === $meta) {
            $this->redirect('/')->send();
            exit;
        }
        $settings = Settings::findOne(1);
        
        return $this->render('order', [
            'meta' => $meta,
            'settings' => $settings,
        ]);
    }
    
    public function actionReviewValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new ReviewForm();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionReviewCreate()
    {
        if (Yii::$app->request->isPost) {
            $model = new ReviewForm();
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->save()) {
                
                $id = $model->getPrimaryKey();
                $url = ['gb'];
                
                $params = [];
                $params['subject'] = "Новый отзыв на вашем сайте";
                $message = "
                <html>
                <body>
                <p>Здравствуйте! <br>";
                $message .= sprintf('На вашем сайте оставили отзыв в %s %s!<br><br>', Common::getDateTime($model->time_add, 'H:i:s'), Common::getDateTime($model->date_add, 'd.m.Y'));
                $message .= "---------------------------------------------------------------<br>";
                $message .= !empty($model->avtor) ? sprintf('Имя: %s<br>', HtmlPurifier::process($model->avtor)) : '';
                $message .= !empty($model->phone) ? sprintf('Телефон: %s<br>', HtmlPurifier::process($model->phone)) : '';
                $message .= !empty($model->title) ? sprintf('Тема: %s<br>', HtmlPurifier::process($model->title)) : '';
                $message .= sprintf("Сообщение:<br>%s<br><br><br><br>", Yii::$app->formatter->asNtext($model->message));
                $message .= sprintf("Спасибо что пользуетесь нашими услугами, с уважением %s</p>", Yii::getAlias('@frontendWebroot'));
                $message .= "
                </body> 
                </html>";
                
                $params['message'] = $message;
                $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];

//                $settings = Settings::findOne(1);
//                $typeEmail = TypeEmail::Reverse;
//                if (!empty($settings->$typeEmail)) {
//                    $params['email'] = [$settings->$typeEmail];
//                    Email::send($params);
//                }
                
                $emailList = SettingsEmail::find()->all();
                
                if (!empty($emailList)) {
                    foreach ($emailList as $email) {
                        if (!empty($email->email)) {
                            $params['email'] = [$email->email];
                            Email::send($params);
                        }
                    }
                }
                
                $errorFilesArray = [];
                $errorText = '';
                foreach ($errorFilesArray as $key => $array) {
                    if (!empty($array)) {
                        $errorText .= ' ' . implode(' ', $array);
                    }
                }
                if (empty($errorText)) {
                    Notice::send($message = 'Ваш отзыв успешно отправлен!', $id_type = Notice::info, $id =
                        Url::to($url));
                } else {
                    Notice::send($message = 'Ваш отзыв отправлен с некоторыми ошибками.' . $errorText, $id_type =
                        Notice::warning, $id = Url::to($url));
                }
//                $this->redirect($url)->send();
//                return Yii::$app->response->redirect($url);
                
                echo "<script>document.location='" . Url::to($url) . "';</script>";
                exit;
            }
        }
        return false;
    }
    
    public function actionReverseValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new ReverseForm();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionReverseCreate()
    {
        if (Yii::$app->request->isPost) {
            $model = new ReverseForm();
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->validate($post)) {
                
                $params = [];
                $filePath = OrderUpload::filePath();
                $dataArray = Json::decode($model->dataArray);
                if (!empty($dataArray)) {
                    foreach ($dataArray as $data) {
                        $nameFile = OrderUpload::upload($data);
                        if (!is_null($nameFile)) {
                            $params['attachments'][$filePath . $nameFile] = [
                                'fileName' => $nameFile,
                            ];
                        }
                    }
                }
                
                $url =
                    !empty(Yii::$app->request->referrer) ? str_replace(Yii::getAlias('@frontendWebroot'), '', Yii::$app->request->referrer) : ['index'];
                
                $params['subject'] = "Новое сообщение на вашем сайте";
                $message = "
                <html>
                <body>
                <p>Здравствуйте! <br>
                На вашем сайте написали сообщение!!<br><br>
                ---------------------------------------------------------------<br>";
                $message .= !empty($model->name) ? sprintf('Имя: %s<br>', HtmlPurifier::process($model->name)) : '';
                $message .= !empty($model->phone) ? sprintf('Телефон: %s<br>', HtmlPurifier::process($model->phone)) : '';
                $message .= !empty($model->email) ? sprintf('E-mail: %s<br>', HtmlPurifier::process($model->email)) : '';
                $message .= sprintf("Сообщение:<br>%s<br><br><br><br>", Yii::$app->formatter->asNtext($model->desc));
                $message .= sprintf("
                Спасибо что пользуетесь нашими услугами, с уважением %s</p> 
                </body> 
                </html>", Yii::getAlias('@frontendWebroot'));
                
                $params['message'] = $message;
                $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];

//                $settings = Settings::findOne(1);
//                $typeEmail = TypeEmail::Reverse;
//                if (!empty($settings->$typeEmail)) {
//                    $params['email'] = [$settings->$typeEmail];
//                    Email::send($params);
//                }
                
                $emailList = SettingsEmail::find()->all();
                
                if (!empty($emailList)) {
                    foreach ($emailList as $email) {
                        if (!empty($email->email)) {
                            $params['email'] = [$email->email];
                            Email::send($params);
                        }
                    }
                }
                
                $errorFilesArray = [];
                $errorText = '';
                foreach ($errorFilesArray as $key => $array) {
                    if (!empty($array)) {
                        $errorText .= ' ' . implode(' ', $array);
                    }
                }
                if (empty($errorText)) {
                    Notice::send($message = 'Ваше сообщение успешно отправлено!', $id_type = Notice::info, $id =
                        Url::to($url));
                } else {
                    Notice::send($message = 'Ваше сообщение отправлено с некоторыми ошибками.' . $errorText, $id_type =
                        Notice::warning, $id = Url::to($url));
                }

//                return $this->redirect($url);
//                return Yii::$app->response->redirect($url);\
                echo "<script>document.location='" . Url::to($url) . "';</script>";
                exit;
            }
        }
        return false;
    }
    
    public function actionRecycleValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new RecycleForm();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionRecycleCreate()
    {
        if (Yii::$app->request->isPost) {
            $model = new RecycleForm();
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->validate($post)) {
                
                $id_client = Yii::$app->params['id_client'];
                $order = Orders::find()->where([
                    'and',
                    ['id_user' => $id_client],
                    ['send' => 0],
                    ['>', 'date_end', new Expression('NOW()')],
                ])->orderBy(['id' => SORT_DESC])->limit(1)->one();
                $id_order = $order->id;
                $sum_all = $order->sum;
                
                $client = Clients::findOne($id_client);
                if ($client->load($post, $model->formName()) && $client->save()) {
                    
                }
                
                $date_end =
                    (new \DateTime('', Yii::$app->params['timezone']))->modify("+ 1 month")->format('Y-m-d H:i:s');
                $order->send = 1;
                $order->status = OrderStatus::newOrder;
                $order->date_end = $date_end;
                $order->update();
                
                $mrh_login = "studcity";
                $mrh_pass1 = "V3N9GcXl2EPR9nRmSSl9";
                $inv_id = $id_order;
                $inv_desc = "Покупка готовых решений";
                $out_summ = number_format($sum_all, 2, '.', '');
                $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");
                $in_curr = "";
                $culture = "ru";
                
                $clientQuery = http_build_query([
                    "MrchLogin" => $mrh_login,
                    "OutSum" => $out_summ,
                    "InvId" => $inv_id,
                    "Desc" => $inv_desc,
                    "SignatureValue" => $crc,
                    "IncCurrLabel" => $in_curr,
                    "Culture" => $culture,
                ]);
                
                $paymentFormAddress = "https://auth.robokassa.ru/Merchant/Index.aspx?" . $clientQuery;
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'srok',
                    'value' => 0,
                    'expire' => time() + 3600 // 1 hour,
                ]));
                
                echo "<script>document.location='$paymentFormAddress';</script>";
                exit;
            }
        }
        return false;
    }
    
    public function actionSolutionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new SolutionForm();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                
                if (isset($post['data'])) {
                    $modelItems = [];
                    foreach ($post['data'] as $index => $data) {
                        $modelItem = new SolutionData();
                        if ($modelItem->load($data, '')) {
                            $modelItems[$index] = $modelItem;
                        }
                    }
                    $arrayError = array_merge($arrayError, ActiveForm::validateMultiple($modelItems));
                }
                
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionSolutionCreate()
    {
        if (Yii::$app->request->isPost) {
            $model = new SolutionForm();
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->validate($post)) {
                
                $position = SolutionPosition::findOne($model->id);
                $sum_all = !empty($position->price) ? $position->price : 0;
                $cat = !empty($position->cat) ? $position->cat : 0;
                $dataSolution = SolutionData::find()->all();
                $dataSolution = ArrayHelper::index($dataSolution, 'id');
                
                $id_avt = md5(uniqid(rand(), 1));
                $params = [
                    'id_avt' => $id_avt,
                    'name' => $model->name,
                    'email' => $model->email,
                    'phone' => $model->phone,
                ];
                $client = new Clients();
                if ($client->load($params) && $client->save()) {
                    $id_client = $client->getPrimaryKey();

//                    $date_end = Yii::$app->formatter->asDate(strtotime("+ 1 month"), 'php:Y-m-d H:i:s');
                    $date_end = Common::getDateTime('', 'Y-m-d H:i:s', "+ 1 month");
                    
                    $params = [
                        'id_user' => $id_client,
                        'sum' => $sum_all,
                        'date_end' => $date_end,
                        'id_type' => OrderType::solution,
                        'send' => 1,
                        'status' => OrderStatus::newOrder,
                    ];
                    
                    $id_order = 0;
                    $order = new Orders();
                    if ($order->load($params) && $order->save()) {
                        $id_order = $order->getPrimaryKey();
                    }
                    
                    if ($id_order != 0) {
                        $params = [
                            'id_order' => $id_order,
                        ];
                        $orderCheck = new OrderCheck();
                        $orderCheck->load($params);
//                        if ($orderCheck->load($params) && $orderCheck->save()) {
                        if ($orderCheck->save()) {
                            //
                        }
                        $params = [
                            'id_order' => $id_order,
                            'id_position' => $model->id,
                            'cat' => $cat,
                        ];
                        $orderDesc = new SolutionOrderDesc();
                        if ($orderDesc->load($params) && $orderDesc->save()) {
                            //
                        }
                        
                        $dataArray = Json::decode($model->dataArray);
                        if (!empty($dataArray)) {
                            foreach ($dataArray as $data) {
                                $nameFile = OrderUpload::upload($data);
                                if (!is_null($nameFile)) {
                                    $modelFile = new OrdersDocs();
                                    $modelFile->file = $nameFile;
                                    $modelFile->title = str_replace("." .
                                        OrdersDocs::getExtensionByMimeType($data['mime_type']), '', $data['name_file']);
                                    $modelFile->id_order = $id_order;
                                    if ($modelFile->save()) {
                                        //
                                    }
                                }
                            }
                        }
                        
                        if (isset($post['data'])) {
                            foreach ($post['data'] as $key => $data) {
                                $modelItem = new SolutionData();
                                if ($modelItem->load($data, '')) {
                                    $params = [
                                        'name' => array_key_exists($modelItem->id, $dataSolution) &&
                                        !empty($dataSolution[$modelItem->id]['name']) ? $dataSolution[$modelItem->id]['name'] : '',
                                        'value' => $modelItem->name,
                                        'id_order' => $id_order,
                                    ];
                                    $orderDataSolution = new SolutionOrderData();
                                    if ($orderDataSolution->load($params, '') && $orderDataSolution->save()) {
                                        //
                                    } else {
//                                        file_put_contents('errors.txt', var_export($orderDataSolution->errors, true) . PHP_EOL, FILE_APPEND);
                                    }
                                }
                            }
                        }
                        
                    }
                    
                    $mrh_login = "studcity";
                    $mrh_pass1 = "V3N9GcXl2EPR9nRmSSl9";
                    $inv_id = $id_order;
                    $inv_desc = "Решение за мгновение";
                    $out_summ = number_format($sum_all, 2, '.', '');
                    $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");
                    $in_curr = "";
                    $culture = "ru";
                    
                    $clientQuery = http_build_query([
                        "MrchLogin" => $mrh_login,
                        "OutSum" => $out_summ,
                        "InvId" => $inv_id,
                        "Desc" => $inv_desc,
                        "SignatureValue" => $crc,
                        "IncCurrLabel" => $in_curr,
                        "Culture" => $culture,
                    ]);
                    
                    $paymentFormAddress = "https://auth.robokassa.ru/Merchant/Index.aspx?" . $clientQuery;
                    Yii::$app->response->cookies->add(new Cookie([
                        'name' => 'srok',
                        'value' => 1,
                        'expire' => time() + 3600 // 1 hour,
                    ]));
                    
                    echo "<script>document.location='$paymentFormAddress';</script>";
                    exit;
                    
                }
            }
        }
        return false;
    }
    
    public function actionOrderValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new OrderForm();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionOrderCreate()
    {
        if (Yii::$app->request->isPost) {
            $model = new OrderForm();
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->validate($post)) {

//                $errorFilesArray = [
//                    'file' => [],
//                ];

//                $modelUpload = new OrderUpload();
//                $modelUpload->load($post);
//                $files = UploadedFile::getInstances($modelUpload, 'file');
//
//                foreach ($files as $file) {
//                    $modelUpload->file = $file;
//                    if (($file && $file->tempName) && ($modelUpload->validate())) {
//                        $nameFile = implode('', [
//                            md5(microtime(true)),
//                            '.',
//                            $file->extension,
//                        ]);
//                        if ($file->saveAs($filePath . $nameFile)) {
//                            $params['attachments'][$filePath . $nameFile] = [
//                                'fileName' => $nameFile,
//                            ];
//                        }
//                    } elseif (!empty($modelUpload->errors['file'])) {
//                        $errorFilesArray['file'] = array_merge($errorFilesArray['file'], $modelUpload->errors['file']);
//                    }
//                }
                
                $params = [];
                $filePath = OrderUpload::filePath();
                $dataArray = Json::decode($model->dataArray);
                if (!empty($dataArray)) {
                    foreach ($dataArray as $data) {
                        $nameFile = OrderUpload::upload($data);
                        if (!is_null($nameFile)) {
                            $params['attachments'][$filePath . $nameFile] = [
                                'fileName' => $nameFile,
                            ];
                        }
                    }
                }
                
                $url =
                    !empty(Yii::$app->request->referrer) ? str_replace(Yii::getAlias('@frontendWebroot'), '', Yii::$app->request->referrer) : ['index'];
                
                $params['subject'] = "Новое сообщение на вашем сайте";
                $message = "
                <html>
                <body>
                <p>Здравствуйте! <br>
                На вашем сайте написали сообщение!!<br><br>
                ---------------------------------------------------------------<br>";
                $message .= !empty($model->name) ? sprintf('Имя: %s<br>', HtmlPurifier::process($model->name)) : '';
                $message .= !empty($model->phone) ? sprintf('Телефон: %s<br>', HtmlPurifier::process($model->phone)) : '';
                $message .= !empty($model->email) ? sprintf('E-mail: %s<br>', HtmlPurifier::process($model->email)) : '';
                $message .= sprintf("Сообщение:<br>%s<br><br><br><br>", Yii::$app->formatter->asNtext($model->desc));
                $message .= sprintf("
                Спасибо что пользуетесь нашими услугами, с уважением %s</p> 
                </body> 
                </html>", Yii::getAlias('@frontendWebroot'));
                
                $params['message'] = $message;
                $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];

//                $settings = Settings::findOne(1);
//                $typeEmail = TypeEmail::Order;
//                if (!empty($settings->$typeEmail)) {
//                    $params['email'] = [$settings->$typeEmail];
//                    Email::send($params);
//                }
                
                $emailList = SettingsEmail::find()->all();
                if (!empty($emailList)) {
                    foreach ($emailList as $email) {
                        if (!empty($email->email)) {
                            $params['email'] = [$email->email];
                            Email::send($params);
                        }
                    }
                }
                
                $errorFilesArray = [];
                $errorText = '';
                foreach ($errorFilesArray as $key => $array) {
                    if (!empty($array)) {
                        $errorText .= ' ' . implode(' ', $array);
                    }
                }
                if (empty($errorText)) {
                    Notice::send($message = 'Ваше сообщение успешно отправлено!', $id_type = Notice::info, $id =
                        Url::to($url));
                } else {
                    Notice::send($message = 'Ваше сообщение отправлено с некоторыми ошибками.' . $errorText, $id_type =
                        Notice::warning, $id = Url::to($url));
                }

//                return $this->redirect($url);
//                return Yii::$app->response->redirect($url);\
                echo "<script>document.location='" . Url::to($url) . "';</script>";
                exit;
            }
        }
        return false;
    }
    
    public function actionSuccess()
    {
        if (Yii::$app->request->isGet) {
            $order = new Order(Yii::$app->request->get());
            return $order->response();
        }
        return false;
    }
    
    public function actionDownload($id)
    {
        if (!is_null($modelDownloads = Downloads::findOne(['hash' => $id]))) {
            $id_order = $modelDownloads->id_order;
            if (!is_null($model = Orders::findOne(['id' => $id_order]))) {
                $pathFile = OrderUpload::filePath() . 'order' . $model->id . '.zip';
                if (is_file($pathFile)) {
                    return Yii::$app->response->sendFile($pathFile);
                }
            }
        }
        
        return Yii::$app->response->sendFile(OrderUpload::filePathDefault());
        
    }
    
    public function beforeAction($action)
    {
        if ($action->id != 'index') {
            if (parent::beforeAction($action)) {
                Clients::getClientId();
                return true;
            }
        } else {
            return true;
        }
        return false;
    }
}