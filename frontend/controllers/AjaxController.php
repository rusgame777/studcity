<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Clients;
use common\models\OrderCheck;

/**
 * Site controller
 */
class AjaxController extends Controller
{
    public $layout = '@app/views/layouts/main.php';
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionRecyclePositionDelete()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('recycle/position-delete', [
                'data' => $data,
            ]);
        }
        return false;
    }
    
    public function actionRecyclePositionAdd()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('recycle/position-add', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionRecycleDelete()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('recycle/delete', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionRecycleOptionDelete()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('recycle/option-delete', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionRecycleChangeOption()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('recycle/change-option', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionRecycleShow()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('recycle/show', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionSolutionShow()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('solution/show', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionSuccessShow()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('success/show', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionCatalogOptionShow()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('catalog/option-show', [
                'data' => $data,
            ]);
        }
        return true;
    }
    
    public function actionStatusSms()
    {
        return $this->renderAjax('src/status-sms');
    }
    
    public function actionCheckUser($id)
    {
        if (!empty($id)) {
            OrderCheck::updateAll(['check_user' => 1], ['id_order' => intval($id)]);
        }
    }
    
    public function actionCheckAuthor($id)
    {
        if (!empty($id)) {
            OrderCheck::updateAll(['check_author' => 1], ['id_order' => intval($id)]);
        }
    }
    
    public function actionDeleteOrder()
    {
        return $this->renderAjax('src/delete-order');
    }
    
    public function actionDeleteMail()
    {
        return $this->renderAjax('src/delete-mail');
    }
    
    public function actionRememberOrder()
    {
        return $this->renderAjax('src/remember-order');
    }
    
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Clients::getClientId();
            return true;
        }
        return false;
    }
}
