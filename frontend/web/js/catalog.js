function search_manual() {
    var cat = parseInt($.getUrlVar('cat')) || 0,
        manual = $('input[name=manual]').val() || '',
        university = parseInt($('select[name=university]').val()),
        url_search = '/catalog/?search=ok';
    if (manual != '') {
        url_search += '&manual=' + manual;
    }
    if (university != 0) {
        url_search += '&university=' + university;
    }
    if (cat != 0) {
        url_search += '&cat=' + cat;
    }
    document.location = url_search;
}

screenshotPreview = function () {
    var xOffset,
        yOffset,
        catalog = $(".catalog_subcat");

    $(catalog).on('mouseenter', function () {
        var id = $(this).data('id');
        $(this).find('.position').appendTo("body");
        var bubble = $('body .position[data-id=' + id + ']');
        bubble.fadeIn("fast");
    });

    $(catalog).on('mouseleave', function () {
        var id = $(this).data('id');
        var position = '.catalog_subcat[data-id=' + id + ']';
        var bubble = $('body .position[data-id=' + id + ']');
        bubble.hide();
        bubble.appendTo(position);
    });

    $(catalog).on('mousemove', function (e) {
        var id = $(this).data('id');
        var num = $(this).data('num');
        var bubble = $('body .position[data-id=' + id + ']');
        if (num == 1) {
            xOffset = 20;
            yOffset = -400;
        } else {
            xOffset = -660;
            yOffset = -400;
        }
        bubble.css("top", (e.pageY + yOffset) + "px").css("left", (e.pageX + xOffset) + "px");
    });

};

$(function () {
    var search = $('#catalog_search_icon');
    search.on('click', function () {
        search_manual();
    });

    $('input[name=manual]').on('keydown', function (e) {
        if (e.keyCode == 13) {
            search_manual();
        }
    });

    $('select[name=university]').on('change', function () {
        search_manual();
    });

    search.on('click', function () {
        search_manual();
    });

    screenshotPreview();
});