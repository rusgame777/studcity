$(function () {

    var order_form = $('#order-form');
    $("#orderform-phone").mask("+7 999 999 9999");

    $('#order_enter').on('click', function () {
        order_form.trigger('submit');
    });

    var maxFiles = 5,
        dataArray = [],
        orderBox = $('#orderbox');

    orderBox.on('change', '#order_file input[name^=file]', function () {
        var files = $(this)[0].files;
        if (files.length + dataArray.length <= maxFiles) {
            loadCheck(files);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
            files.length = 0;
        }
    });

    function loadCheck(files) {
        var orderFile = $('#order_file').find('> div');
        $.each(files, function (index, file) {
            if (!files[index].type.match('application/msword|application/vnd.openxmlformats-officedocument.wordprocessingml.document|image/jpeg|image/pjpeg|image/png|application/x-rar-compressed|application/x-zip-compressed|application/zip|application/pdf')) {
                alert('Загружаются только doc/docx, pdf, zip, rar, jpg, png!');
                return false;
            }

            if ((dataArray.length + files.length) > maxFiles) {
                alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
                return false;
            }

            var fileReader = new FileReader();
            fileReader.onload = (function (file) {
                return function () {
                    dataArray.push({name_file: file.name, mime_type: file.type, file: this.result});
                    $('input[name="OrderForm[dataArray]"]').val(JSON.stringify(dataArray));
                    orderFile.append([
                        '<div class="order_file">',
                        '<div class="order_file_name">',
                        file.name,
                        '</div>',
                        '<div class="order_file_remove"></div>',
                        '</div>'
                    ].join(""));
                };
            })(files[index]);
            fileReader.readAsDataURL(file);
        });
    }

    function order_hide() {
        order_form[0].reset();
        order_form.find('.order_file').remove();
        dataArray.length = 0;
    }

    $('.md-overlay').on('click', function () {
        order_hide();
    });

    $('#order_close').on('click', function () {
        order_hide();
    });

    $(document).on('keydown', function (e) {
        if (e.keyCode == 27) {
            order_hide();
        }
    });

    orderBox.on('click', '#order_file button', function () {
        $(this).parents('#order_file').find('input[type^=file]').trigger('click');
    });

    $(window).resize(function () {
        $("#order_file").find("input[type^=file]").triggerHandler("change");
    });

    orderBox.on('click', '.order_file_remove', function () {
        dataArray.splice($(this).parents('.order_file').index(), 1);
        $(this).parents('.order_file').remove();
    });

});