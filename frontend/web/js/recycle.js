$(function () {

    var show_box = $('#show_box'),
        show_window = $('.show_box');

    function recycle_show() {
        show_box.hide();
        show_box.empty();
        $.ajax({
            url: "/ajax/recycle-show/",
            type: "GET",
            data: {},
            cache: false,
            success: function (response) {
                show_box.html(response);
                window.initOptionGallery();
                $('.container').css('position', 'fixed');
                show_window.show();
                $("#recycleform-phone").mask("+7 999 999 9999");
                var top = $(window).scrollTop() + $(window).height() / 5,
                    left = $(window).scrollLeft(),
                    cssObj = {
                        'display': 'block',
                        'top': top + 'px',
                        'left': left + 'px'
                    };
                show_box.css(cssObj);
                $('#recycle_table').find('.position_tr').each(function () {
                    var num = $(this).data('num');
                    if (num == undefined) {
                        $(this).data('num', num);
                    }
                });
            }
        });
    }

    /*function recycle_enter() {
        var name = $('input[name=recycle_name]').val(),
            phone = $('input[name=recycle_phone]').val(),
            email = $('input[name=recycle_email]').val(),
            location = document.location.href;

        $("#recycle_status").empty();
        $.ajax({
            url: "/ajax/recycle.php",
            type: "GET",
            data: {"name": name, "phone": phone, "email": email, "location": location},
            cache: false,
            success: function (response) {
                $("#recycle_status").append(response);
            }
        });
    }*/

    function recycle_hide() {
        show_box.hide();
        show_window.hide();
        $('.container').css('position', 'relative');
        full_height();
    }

    $('#catalog_searchbox').on('click', '#catalog_recycle', function () {
        recycle_show();
        full_height();
    });

    show_box.on('click', '#recycle_enter', function () {
        $('#recycle-form').trigger('submit');
        // recycle_enter();
    });

    show_window.on('click', function () {
        recycle_hide();
    });

    show_box.on('click', function (e) {
        if ($(e.target).attr('id') == 'show_box') {
            recycle_hide();
        }
        if ($(e.target).attr('id') == 'recycle_close') {
            recycle_hide();
        }
    });

    $(document).on('keydown', function (e) {
        if (e.keyCode == 27) {
            recycle_hide();
        }
    });

});