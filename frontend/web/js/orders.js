function search_manual() {
    var manual = $('input[name=manual]').val() || '',
        university = parseInt($('select[name=university]').val()),
        url_search = '/catalog/?search=ok';
    if (manual != '') {
        url_search += '&manual=' + manual;
    }
    if (university != 0) {
        url_search += '&university=' + university;
    }
    document.location = url_search;
}

$(function () {

    $('#catalog_search_icon').on('click', function () {
        search_manual();
    });

    $('input[name=manual]').on('keydown', function (e) {
        if (e.keyCode == 13) {
            search_manual();
        }
    });

    $('#order').find('a > img').each(function () {
        var href_cont = $(this).parent().attr('href'),
            array_href = href_cont.split('.'),
            length_array = array_href.length,
            ext = array_href[length_array - 1],
            ext_array = ['doc', 'xls', 'docx', 'xlsx', 'pdf'];
        if (in_array(ext, ext_array)) {
            if (ext == 'pdf') {
                $(this).parent().attr('target', '_blank');
            }
            $(this).parent().attr('rel', '0');
        }
        if ($(this).parent().attr('rel') != '0') {
            $(this).parent().attr('rel', 'lightbox[roadtrip]');
        }
    });

});	