$(function () {

    var reverse_form = $('#reverse-form');
    $("#reverseform-phone").mask("+7 999 999 9999");

    $('#reverse_enter').on('click', function () {
        reverse_form.trigger('submit');
    });

    var maxFiles = 5,
        dataArray = [],
        reverseBox = $('#reversebox');

    reverseBox.on('change', '#reverse_file input[name^=file]', function () {
        var files = $(this)[0].files;
        if (files.length <= maxFiles) {
            loadCheck(files);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
            files.length = 0;
        }
    });

    function loadCheck(files) {
        var reverseFile = $('#reverse_file').find('> div');
        $.each(files, function (index, file) {
            if (!files[index].type.match('application/msword|application/vnd.openxmlformats-officedocument.wordprocessingml.document|image/jpeg|image/pjpeg|image/png|application/x-rar-compressed|application/x-zip-compressed|application/zip|application/pdf')) {
                alert('Загружаются только doc/docx, pdf, zip, rar, jpg, png!');
                return false;
            }

            if ((dataArray.length + files.length) > maxFiles) {
                alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
                return false;
            }

            var fileReader = new FileReader();
            fileReader.onload = (function (file) {
                return function () {
                    dataArray.push({name_file: file.name, mime_type: file.type, file: this.result});
                    $('input[name="ReverseForm[dataArray]"]').val(JSON.stringify(dataArray));
                    reverseFile.append([
                        '<div class="reverse_file">',
                        '<div class="reverse_file_name">',
                        file.name,
                        '</div>',
                        '<div class="reverse_file_remove"></div>',
                        '</div>'
                    ].join(""));
                };
            })(files[index]);
            fileReader.readAsDataURL(file);
        });
    }

    function reverse_hide() {
        reverse_form[0].reset();
        reverse_form.find('.reverse_file').remove();
        dataArray.length = 0;
    }

    $('.md-overlay').on('click', function () {
        reverse_hide();
    });

    $('#reverse_close').on('click', function () {
        reverse_hide();
    });

    $(document).on('keydown', function (e) {
        if (e.keyCode == 27) {
            reverse_hide();
        }
    });

    reverseBox.on('click', '#reverse_file button', function () {
        $(this).parents('#reverse_file').find('input[type^=file]').trigger('click');
    });

    $(window).resize(function () {
        $("#reverse_file").find("input[type^=file]").triggerHandler("change");
    });

    reverseBox.on('click', '.reverse_file_remove', function () {
        dataArray.splice($(this).parents('.reverse_file').index(), 1);
        $(this).parents('.reverse_file').remove();
    });

});