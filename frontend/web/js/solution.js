$(function () {
    var show_box = $("#show_box");

    function search_manual() {
        var cat = parseInt($('select[name=cat]').val()),
            manual = $('input[name=manual]').val() || '',
            url_search = '/solutions/?search=ok';
        if (manual != '') {
            url_search += '&manual=' + manual;
        }
        if (cat != 0) {
            url_search += '&cat=' + cat;
        }
        document.location = url_search;
    }


    var screenshotPreview = function () {
        var solution_position = $(".solution_position");
        solution_position.on('mouseenter', function () {
            var id = $(this).data('id'),
                bubble = $('body .solution_pos[data-id=' + id + ']');
            $(this).find('.solution_pos').appendTo("body");
            bubble.fadeIn("fast");
        });

        solution_position.on('mouseleave', function () {
            var id = $(this).data('id'),
                position = '.solution_position[data-id=' + id + ']',
                bubble = $('body .solution_pos[data-id=' + id + ']');
            bubble.hide();
            bubble.appendTo(position);
        });

        solution_position.on('mousemove', function (e) {
            var id = $(this).data('id'),
                num = $(this).data('num'),
                bubble = $('body .solution_pos[data-id=' + id + ']'),
                xOffset,
                yOffset;
            if (num == 1) {
                xOffset = 20;
                yOffset = -400;
            }
            else {
                xOffset = -660;
                yOffset = -400;
            }
            bubble.css("top", (e.pageY + yOffset) + "px").css("left", (e.pageX + xOffset) + "px");
        });
    };


    screenshotPreview();
    $("#cat").chosen({no_results_text: "Результаты не соответствуют", width: "280px"});

    function solution_show(id) {
        show_box.hide();
        show_box.empty();
        $.ajax({
            type: "POST",
            url: "/ajax/solution-show/",
            data: JSON.stringify({"id": id}),
            cache: false,
            async: true,
            success: function (response) {
                show_box.html(response);
                $('.container').css('position', 'fixed');
                $('.show_box').show();
                $("#solutionform-phone").mask("+7 999 999 9999");
                var top = $(window).scrollTop() + $(window).height() / 5,
                    left = $(window).scrollLeft(),
                    cssObj = {
                        'display': 'block',
                        'top': top + 'px',
                        'left': left + 'px'
                    };
                show_box.css(cssObj);
                $('#solution_box').data('id', id);
            }
        });
    }

    // function solution_enter() {
    //     var id = $('#solution_box').data('id'),
    //         name = $('input[name=solution_name]').val(),
    //         phone = $('input[name=solution_phone]').val(),
    //         email = $('input[name=solution_email]').val(),
    //         file = dataArray[0].file,
    //         mime_type = dataArray[0].mime_type,
    //         name_file;
    //
    //     if (dataArray[0] !== undefined) {
    //         file = dataArray[0].file;
    //         mime_type = dataArray[0].mime_type;
    //         name_file = dataArray[0].name_file;
    //     }
    //     else {
    //         file = '';
    //         mime_type = '';
    //         name_file = '';
    //     }
    //
    //     var value_array = '',
    //         value = '',
    //         i = 0;
    //     $('input[name^=solution_value]').each(function () {
    //         i++;
    //         value = $(this).val();
    //         if (i != 1) {
    //             value_array += "_";
    //         }
    //         value_array += value;
    //     });
    //
    //     var post_data = {
    //         id: id,
    //         name: name,
    //         phone: phone,
    //         email: email,
    //         value: value_array,
    //         file: file,
    //         mime_type: mime_type,
    //         name_file: name_file
    //     };
    //
    //     $.ajax({
    //         type: "POST",
    //         url: "/ajax/solution/",
    //         dataType: "json",
    //         data: JSON.stringify(post_data),
    //         contentType: "application/json; charset=utf-8",
    //         traditional: true,
    //         cache: false,
    //         async: true,
    //         success: function (response, textStatus, jqXHR) {
    //             var responseArr = JSON.parse(jqXHR.responseText);
    //             var status = parseInt(responseArr.status);
    //             if (status == 1) {
    //                 var error = responseArr.error;
    //                 if (error != undefined) {
    //                     alert(error);
    //                 } else {
    //                     document.location = responseArr.location;
    //                 }
    //             }
    //         }
    //     });
    // }

    function solution_hide() {
        show_box.hide();
        $('.show_box').hide();
        $('.container').css('position', 'relative');
        full_height();
        var solution_form = $('#solution-form');
        // solution_form[0].reset();
        solution_form.find('.solution_file').remove();
        dataArray.length = 0;
    }


    var maxFiles = 5,
        dataArray = [];

    function loadCheck(files) {
        var solutionFile = $('#solution_file').find('> div');
        $.each(files, function (index, file) {
            if (!files[index].type.match('application/msword|application/vnd.openxmlformats-officedocument.wordprocessingml.document|image/jpeg|image/pjpeg|image/png|application/x-rar-compressed|application/x-zip-compressed|application/zip|application/pdf')) {
                alert('Загружаются только doc/docx, pdf, zip, rar, jpg, png!');
                return false;
            }

            if ((dataArray.length + files.length) > maxFiles) {
                alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
                return false;
            }

            var fileReader = new FileReader();
            fileReader.onload = (function (file) {
                return function () {
                    dataArray.push({name_file: file.name, mime_type: file.type, file: this.result});
                    $('input[name="SolutionForm[dataArray]"]').val(JSON.stringify(dataArray));
                    solutionFile.append([
                        '<div class="solution_file">',
                        '<div class="solution_file_name">',
                        file.name,
                        '</div>',
                        '<div class="solution_file_remove"></div>',
                        '</div>'
                    ].join(""));
                };
            })(files[index]);
            fileReader.readAsDataURL(file);
        });
    }


    $('#solution_search_icon').on('click', function () {
        search_manual();
    });

    $('input[name=manual]').on('keydown', function (e) {
        if (e.keyCode == 13) {
            search_manual();
        }
    });

    $('select[name=cat]').on('change', function () {
        search_manual();
    });

    $('.solution_position').on('click', function () {
        var id = $(this).data('id');
        solution_show(id);
        full_height();
    });

    show_box.on('click', '#solution_enter', function () {
        $('#solution-form').trigger('submit');
    });

    $('.show_box').on('click', function () {
        solution_hide();
    });

    show_box.on('click', function (e) {
        if ($(e.target).attr('id') == 'show_box') {
            solution_hide();
        }
        if ($(e.target).attr('id') == 'solution_close') {
            solution_hide();
        }
    });

    $(document).on('keydown', function (e) {
        if (e.keyCode == 27) {
            solution_hide();
        }
    });

    show_box.on('change', '#solution_file input[name^=file]', function () {
        var files = $(this)[0].files;
        if (files.length <= maxFiles) {
            loadCheck(files);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
            files.length = 0;
        }
    });

    show_box.on('click', '#solution_file button', function () {
        $(this).parents('#solution_file').find('input[type^=file]').trigger('click');
    });

    $(window).resize(function () {
        $("#solution_file").find("input[type^=file]").triggerHandler("change");
    });

    show_box.on('click', '.solution_file_remove', function () {
        dataArray.splice($(this).parents('.solution_file').index(), 1);
        $(this).parents('.solution_file').remove();
    });

});