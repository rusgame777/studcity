$(function () {

    var show_box = $('#show_box'),
        show_window = $('.show_box');

    function success_show() {
        show_box.hide();
        show_box.empty();
        $.ajax({
            url: "/ajax/success-show/",
            type: "GET",
            data: {},
            cache: false,
            success: function (response) {
                show_box.html(response);
                show_window.width($(document).width()).height($(document).height());
                show_window.show();
                var top = $(window).scrollTop() + $(window).height() / 5,
                    left = $(window).scrollLeft(),
                    cssObj = {
                        'display': 'block',
                        'top': top + 'px',
                        'left': left + 'px'
                    };
                show_box.css(cssObj);
            }
        });
    }

    function success_hide() {
        show_box.hide();
        show_window.hide();
        history.pushState(null, null, '/');

    }

    var success = $.getUrlVar('success');
    if (success == 'ok') {
        success_show();
    }

    show_box.on('click', '#success_enter, #success_close', function () {
        success_hide();
    });

    $(window).on('resize', function () {
        show_window.width($(document).width()).height($(document).height());
    });

    show_window.on('click', function () {
        var bit = $('#show_box').find('#success_close').length;
        if (bit == 1) {
            success_hide();
        }
    });

    show_box.on('click', function (e) {
        var bit = $('#show_box').find('#success_close').length;
        if (bit == 1) {
            if ($(e.target).attr('id') == 'show_box') {
                success_hide();
            }
            if ($(e.target).attr('id') == 'success_close') {
                success_hide();
            }
        }
    });

    $(document).on('keydown', function (e) {
        if (e.keyCode == 27) {
            var bit = $('#show_box').find('#success_close').length;
            if (bit == 1) {
                success_hide();
            }
        }
    });

});