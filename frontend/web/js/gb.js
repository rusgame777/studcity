function search_manual() {
    var manual = $('input[name=manual]').val() || '',
        university = parseInt($('select[name=university]').val()),
        url_search = '/catalog/?search=ok';
    if (manual != '') {
        url_search += '&manual=' + manual;
    }
    if (university != 0) {
        url_search += '&university=' + university;
    }
    document.location = url_search;
}

$(function () {

    $('#catalog_search_icon').on('click', function () {
        search_manual();
    });

    $('input[name=manual]').on('keydown', function (e) {
        if (e.keyCode == 13) {
            search_manual();
        }
    });

    $("#reviewform-phone").mask("+7 999 999 9999");
    var pattern_phone = new RegExp(/^8\d{10}$/),
        search_phone = new RegExp(/(\+7) (\d{3}) (\d{3}) (\d{4})/i),
        replacement_phone = new RegExp(/8$2$3$4/);

    $("#gb_click").on('click', function () {
        var form = $('#gb_form'),
            display = form.css('display'),
            html = $('html'),
            container = $('#container');
        /*container_height = container.height();
         container_height += 300;
         container.height(container_height);*/
        if (display == 'none') {
            /*if (container.css('height') != '0px')
             {
             content_height = $('#content').height();
             content_height += 300;
             $('#content').height(content_height);
             }*/
            var height_window = html.height(),
                height_document = $(window).height(),
                height_html = parseInt(html.data('start-height')),
                content_height;

            if (height_document > height_window) {
                container.height(height_document);
                $('#footer').css('position', 'absolute');
            }
            else if (height_document < height_html) {
                container.height('auto');
                $('#footer').css('position', 'relative');
            }
            else if (height_document > height_html) {
                container.height(height_document);
                $('#footer').css('position', 'absolute');
            }

            if (container.css('height') == '0px') {
                content_height = height_document - $('#footer').height() - $('#header').height();
            } else {
                content_height = container.height() - $('#footer').height() - $('#header').height();
            }
            if (container.css('height') != '0px') {
                if (content_height < 700) {
                    $('#content').height('700px');
                } else if (content_height >= 700) {
                    $('#content').height(content_height);
                } else if (height_document > height_window) {
                    $('#content').height('auto');
                }
            }
            else {
                $('#content').height('auto');
            }
            form.slideToggle(500);
            setTimeout(function () {
                window.full_height();
            }, 500);
        }
        else {
            form.slideToggle(500);
            /*container_height = container.height();
             container_height -= 300;
             container.height(container_height);*/
            setTimeout(function () {
                /*content_height = $('#content').height();
                 content_height -= 300;
                 $('#content').height(content_height);*/
                height_window = html.height();
                height_document = $(window).height();
                height_html = parseInt(html.data('start-height'));

                if (height_document > height_window) {
                    container.height(height_document);
                    $('#footer').css('position', 'absolute');
                }
                else if (height_document < height_html) {
                    container.height('auto');
                    $('#footer').css('position', 'relative');
                }
                else if (height_document > height_html) {
                    container.height(height_document);
                    $('#footer').css('position', 'absolute');
                }

                if (container.css('height') == '0px') {
                    content_height = height_document - $('#footer').height() - $('#header').height();
                } else {
                    content_height = container.height() - $('#footer').height() - $('#header').height();
                }
                if (container.css('height') != '0px') {
                    if (content_height < 400) {
                        $('#content').height('400px');
                    } else if (content_height >= 400) {
                        $('#content').height(content_height);
                    } else if (height_document > height_window) {
                        $('#content').height('auto');
                    }
                }
                else {
                    $('#content').height('auto');
                }
                window.full_height();
            }, 500);
        }
    });

    $('#review-form').on('submit', function () {
        console.log(arguments);
    });

    $('#gb_send').on('click', function () {
        $('#review-form').trigger('submit');
        // var avtor = $('input[name=avtor]').val();
        // if (avtor == '') {
        //     alert('Введите имя!');
        //     return false;
        // }
        //
        // var phone = $('input[name=phone]').val();
        // phone = phone.replace(search_phone, replacement_phone);
        // phone = phone.replace(new RegExp("/", 'g'), "");
        // if (phone == '') {
        //     alert('Введите телефон!');
        //     return false;
        // }
        // if ((phone != '') && (!pattern_phone.test(phone))) {
        //     alert('Пожалуйста, введите правильный телефон!');
        //     return false;
        // }
        //
        // var title = $('input[name=title]').val();
        // if (title == '') {
        //     alert('Введите тему отзыва!');
        //     return false;
        // }
        //
        // var message = $('textarea[name=message]').val();
        // if (message == '') {
        //     alert('Введите комментарий!');
        //     return false;
        // }
        //
        // var location = document.location.href;
        // $("#gb_status").empty();
        // $.ajax({
        //     url: "/ajax/gb.php",
        //     type: "GET",
        //     data: {"location": location, "avtor": avtor, "phone": phone, "title": title, "message": message},
        //     cache: false,
        //     success: function (response) {
        //         $("#gb_status").append(response);
        //     }
        // });
        return true;
    });

    function alignComments() {
        var i = 0,
            comments = $('.gb_comment');
        comments.css({'height': 'auto'});
        comments.each(function () {
            i++;
            if (i % 2 != 0) {
                var height,
                    height1 = $(this).height(),
                    height2 = comments.eq(i).height();
                if (height2 > height1) {
                    height = height2;
                } else {
                    height = height1;
                }
                comments.eq(i).height(height);
            }
        });
        full_height();
    }

    alignComments();
    $(window).on('resize', function () {
        alignComments();
    });


});