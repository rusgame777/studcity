$(function () {

    var show_box = $('#show_box'),
        show_window = $('.show_box');

    $('.catalog_position_tr').on('click', '.catalog_option_add', function () {
        var id_position = $(this).parents('.catalog_position_tr').data('id');
        option_show(id_position);
    });

    show_window.on('click', function () {
        option_hide();
    });

    show_box.on('click', function (e) {
        if ($(e.target).attr('id') == 'show_box') {
            option_hide();
        }
        if ($(e.target).attr('id') == 'option_close') {
            option_hide();
        }
    });

    $(document).on('keydown', function (e) {
        if (e.keyCode == 27) {
            option_hide();
        }
    });


    function option_show(id_position) {
        var data = {"id_position": id_position};
        show_box.hide();
        show_box.empty();
        $.ajax({
            url: "/ajax/catalog-option-show/",
            type: "POST",
            data: JSON.stringify(data),
            cache: false,
            async: true,
            success: function (response) {
                show_box.html(response);
                $('.container').css('position', 'fixed');
                show_window.show();
                window.initOptionGallery();
                var top = $(window).scrollTop() + $(window).height() / 5,
                    left = $(window).scrollLeft(),
                    cssObj = {
                        'display': 'block',
                        'top': top + 'px',
                        'left': left + 'px'
                    };
                show_box.css(cssObj);
                /*$('#option_table').find('.position_tr').each(function () {
                 var num = $(this).data('num');
                 if (num == undefined) {
                 value = $(this).data('num');
                 $(this).data('num', value);
                 }
                 });*/
            },
            fail: function () {
                console.log(arguments);
            },
            error: function () {
                console.log(arguments);
            }
        });
    }

    function option_hide() {
        show_box.hide();
        show_window.hide();
        $('.container').css('position', 'relative');
        full_height();
    }

});