function cBrowser() {
    var ua = navigator.userAgent,
        bName = function () {
            if (ua.search(/MSIE/) > -1) {
                return "ie";
            }
            if (ua.search(/Firefox/) > -1) {
                return "firefox";
            }
            if (ua.search(/Opera/) > -1) {
                return "opera";
            }
            if (ua.search(/Chrome/) > -1) {
                return "chrome";
            }
            if (ua.search(/Safari/) > -1) {
                return "safari";
            }
            if (ua.search(/Konqueror/) > -1) {
                return "konqueror";
            }
            if (ua.search(/Iceweasel/) > -1) {
                return "iceweasel";
            }
            if (ua.search(/SeaMonkey/) > -1) {
                return "seamonkey";
            }
        }();
    return bName;
}

function Auto(str) {
    var replacer = {
        "й": "q",
        "ц": "w",
        "у": "e",
        "к": "r",
        "е": "t",
        "н": "y",
        "г": "u",
        "ш": "i",
        "щ": "o",
        "з": "p",
        "ф": "a",
        "ы": "s",
        "в": "d",
        "а": "f",
        "п": "g",
        "р": "h",
        "о": "j",
        "л": "k",
        "д": "l",
        "я": "z",
        "ч": "x",
        "с": "c",
        "м": "v",
        "и": "b",
        "т": "n",
        "ь": "m"
    };

    for (i = 0; i < str.length; i++) {
        if (replacer[str[i].toLowerCase()] != undefined) {
            if (str[i] == str[i].toLowerCase()) {
                replace = replacer[str[i].toLowerCase()];
            }
            else if (str[i] == str[i].toUpperCase()) {
                replace = replacer[str[i].toLowerCase()].toUpperCase();
            }
            str = str.replace(str[i], replace);
        }
    }
    return str;
}

function in_array(what, where) {
    for (var i = 0; i < where.length; i++) {
        if (what == where[i]) {
            return true;
        }
    }
    return false;
}

$(document).ready(function () {

    $('input[placeholder], textarea[placeholder]').placeholder();

    function activeteClosingNotices() {
        var continueClose = setInterval(function () {
            var alerts = $('#costainer').find('.alert'),
                length = alerts.length;
            if (length > 0) {
                alerts.eq(length - 1).find('.close').trigger('click');
            } else {
                clearInterval(continueClose);
            }
        }, 1000);
    }

    // автоматическое закрытие Alert
    setTimeout(function () {
        activeteClosingNotices();
    }, 5000);


    $.extend({
        getUrlVars: function () {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getUrlVar: function (name) {
            return $.getUrlVars()[name];
        }
    });


    function full_height() {
        var pathname = document.location.pathname,
            cat = parseInt($.getUrlVar('cat')) || 0,
            subcat = parseInt($.getUrlVar('subcat')) || 0,
            manual = $.getUrlVar('manual') || '',
            width_max = 1800,
            width_min = 900,
            width_window = $(window).outerWidth(true) + 20,
            width_catalog = width_window - 120,
            browser = cBrowser(),
            zoom;


        if (pathname == '/catalog/') {
            if ((cat == 0) && (manual == '')) {
                var catalog_cat = $('#catalog_cat');
                if ((width_catalog <= width_max) && (width_catalog > width_min)) {
                    catalog_cat.css('width', '1800px');
                    zoom = width_catalog / width_max;
                    catalog_cat.css('zoom', zoom);
                    catalog_cat.css('-ms-zoom', zoom);
                    catalog_cat.css('-moz-transform', 'scale(' + zoom + ')');
                    catalog_cat.css('-o-transform', 'scale(' + zoom + ')');
                }
                else if (width_catalog > width_max) {
                    catalog_cat.css('width', '100%');
                    catalog_cat.css('zoom', 1);
                    catalog_cat.css('-ms-zoom', 1);
                    catalog_cat.css('-moz-transform', 'scale(1)');
                    catalog_cat.css('-o-transform', 'scale(1)');
                }
                else {
                    catalog_cat.css('width', '1800px');
                    zoom = width_min / width_max;
                    catalog_cat.css('zoom', zoom);
                    catalog_cat.css('-ms-zoom', zoom);
                    catalog_cat.css('-moz-transform', 'scale(' + zoom + ')');
                    catalog_cat.css('-o-transform', 'scale(' + zoom + ')');
                }
            }
            if (((cat != 0) && (subcat == 0)) || (manual != '')) {
                if (width_window >= 1600) {
                    $('.catalog_subcat').data('num', 1);
                    $('.catalog_subcat:nth-of-type(4n)').data('num', 2);
                }
                else if ((width_window >= 1200) && (width_window < 1600)) {
                    $('.catalog_subcat').data('num', 1);
                    $('.catalog_subcat:nth-of-type(3n)').data('num', 2);
                }
                else if (width_window < 1200) {
                    $('.catalog_subcat').data('num', 1);
                    $('.catalog_subcat:nth-of-type(2n)').data('num', 2);
                }
            }
        }

        if (pathname == '/solutions/') {
            if ((cat == 0) && (manual == '')) {
                var solution_cat = $('#solution_cat');
                if ((width_catalog <= width_max) && (width_catalog > width_min)) {
                    solution_cat.css('width', '1800px');
                    zoom = width_catalog / width_max;
                    solution_cat.css('zoom', zoom);
                    solution_cat.css('-ms-zoom', zoom);
                    solution_cat.css('-moz-transform', 'scale(' + zoom + ')');
                    solution_cat.css('-o-transform', 'scale(' + zoom + ')');
                }
                else if (width_catalog > width_max) {
                    solution_cat.css('width', '100%');
                    solution_cat.css('zoom', 1);
                    solution_cat.css('-ms-zoom', 1);
                    solution_cat.css('-moz-transform', 'scale(1)');
                    solution_cat.css('-o-transform', 'scale(1)');
                }
                else {
                    solution_cat.css('width', '1800px');
                    zoom = width_min / width_max;
                    solution_cat.css('zoom', zoom);
                    solution_cat.css('-ms-zoom', zoom);
                    solution_cat.css('-moz-transform', 'scale(' + zoom + ')');
                    solution_cat.css('-o-transform', 'scale(' + zoom + ')');
                }
            }
            if ((cat != 0) || (manual != '')) {
                if (width_window >= 1600) {
                    $('.catalog_position').data('num', 1);
                    $('.catalog_position:nth-of-type(4n)').data('num', 2);
                }
                else if ((width_window >= 1200) && (width_window < 1600)) {
                    $('.catalog_position').data('num', 1);
                    $('.catalog_position:nth-of-type(3n)').data('num', 2);
                }
                else if (width_window < 1200) {
                    $('.catalog_position').data('num', 1);
                    $('.catalog_position:nth-of-type(2n)').data('num', 2);
                }
            }
        }

        if (pathname == '/gb/') {
            var i = 0,
                comment = $('.gb_comment');
            comment.css({'height': 'auto'});
            comment.each(function (i) {
                if (i % 2 == 0) {
                    var height1 = $(this).height();
                    var height2 = comment.eq(i + 1).height();
                    if (height2 > height1) {
                        height = height2;
                    } else {
                        height = height1;
                    }
                    comment.eq(i).height(height);
                    comment.eq(i + 1).height(height);
                }
            });
        }

        if (pathname == '/order/') {
            var height = 0,
                descr_text = $('.order_descr_text > div');
            descr_text.each(function () {
                if ($(this).height() > height) {
                    height = $(this).height();
                }
            });
            descr_text.height(height);
        }

        var html = $('html'),
            container = $('#container'),
            content_height,
            height_window = html.height(),
            height_document = $(window).height(),
            height_html = parseInt(html.data('start-height'));

        if (height_document > height_window) {
            container.height(height_document);
            $('#footer').css('position', 'absolute');
        }
        else if (height_document < height_html) {
            container.height('auto');
            $('#footer').css('position', 'relative');
        }
        else if (height_document > height_html) {
            container.height(height_document);
            $('#footer').css('position', 'absolute');
        }

        if (container.css('height') == '0px') {
            content_height = height_document - $('#footer').height() - $('#header').height();
        } else {
            content_height = container.height() - $('#footer').height() - $('#header').height();
        }
        if (container.css('height') != '0px') {
            if (content_height < 400) {
                $('#content').height('400px');
            } else if (content_height >= 400) {
                $('#content').height(content_height);
            } else if (height_document > height_window) {
                $('#content').height('auto');
            }
        }
        else {
            $('#content').height('auto');
        }
    }

    $.fancybox.defaults.btnTpl.shop = '<button data-fancybox-shop class="fancybox-button fancybox-button--shop" title="Купить">' +
        '<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 19.25 19.25" style="enable-background:new 0 0 19.25 19.25;" xml:space="preserve">' +
        '<g>' +
        '<path style="fill:#fff;" d="M19.006,2.97c-0.191-0.219-0.466-0.345-0.756-0.345H4.431L4.236,1.461C4.156,0.979,3.739,0.625,3.25,0.625H1c-0.553,0-1,0.447-1,1s0.447,1,1,1h1.403l1.86,11.164c0.008,0.045,0.031,0.082,0.045,0.124 c0.016,0.053,0.029,0.103,0.054,0.151c0.032,0.066,0.075,0.122,0.12,0.179c0.031,0.039,0.059,0.078,0.095,0.112 c0.058,0.054,0.125,0.092,0.193,0.13c0.038,0.021,0.071,0.049,0.112,0.065c0.116,0.047,0.238,0.075,0.367,0.075 c0.001,0,11.001,0,11.001,0c0.553,0,1-0.447,1-1s-0.447-1-1-1H6.097l-0.166-1H17.25c0.498,0,0.92-0.366,0.99-0.858l1-7 C19.281,3.479,19.195,3.188,19.006,2.97z M17.097,4.625l-0.285,2H13.25v-2H17.097z M12.25,4.625v2h-3v-2H12.25z M12.25,7.625v2 h-3v-2H12.25z M8.25,4.625v2h-3c-0.053,0-0.101,0.015-0.148,0.03l-0.338-2.03H8.25z M5.264,7.625H8.25v2H5.597L5.264,7.625z M13.25,9.625v-2h3.418l-0.285,2H13.25z"/>' +
        '<circle style="fill:#fff;" cx="6.75" cy="17.125" r="1.5"/>' +
        '<circle style="fill:#fff;" cx="15.75" cy="17.125" r="1.5"/>' +
        '</g>' +
        '</svg>' +
        '</button>';

    window.currentOption = false;
    window.currentPosition = false;
    window.initOptionGallery = function () {
        $('.option_checkbox .photo[data-fancybox]').fancybox({
            afterShow: function (instance, current) {
                window.currentOption = current.opts.option;
                window.currentPosition = current.opts.position;
                var checkbox = $('.option_check[data-id="' + window.currentOption + '"]').find('.option_checkbox input[type="checkbox"]');
                if (checkbox.prop('checked') == true) {
                    $('[data-fancybox-shop]').addClass('active');
                } else {
                    $('[data-fancybox-shop]').removeClass('active');
                }
                // fancybox-button--shop
            },
            buttons: [
                "shop",
                "zoom",
                "close"
            ]
        });
        $('.recycle_info .photo-recycle[data-fancybox]').fancybox({
            buttons: [
                "zoom",
                "close"
            ]
        });
    };

    window.full_height = function () {
        full_height();
    };

    var pathname = document.location.pathname,
        html = $('html');
    html.data('start-height', html.height());
    if (pathname != '/gb/') {
        full_height();
    }

    $('#menu').find('a.menu').each(function () {
        if ($(this).attr('href') == pathname) {
            $(this).addClass('active');
        }
    });

    $(window).on('load', function () {
        full_height();
    });

    $(window).on('resize', function () {
        full_height();
    });

});