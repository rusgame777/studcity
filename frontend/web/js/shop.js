$(function () {

    var show_box = $('#show_box'),
        positions = $('#catalog_position');


    function recycle_update(sum) {
        var sum_word = sum + ' руб.';
        $('#catalog_recycle_sum').html(sum_word);
        if (sum > 0) {
            $('#catalog_recyclebox').show();
        } else {
            $('#catalog_recyclebox').hide();
        }
    }

    function shop_add(id_position) {
        var post_data = {
            id_position: id_position
        };

        $.ajax({
            type: "POST",
            url: "/ajax/recycle-position-add/",
            dataType: "json",
            data: JSON.stringify(post_data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            cache: false,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var responseArr = JSON.parse(jqXHR.responseText),
                    status = parseInt(responseArr.status);
                switch (status) {
                    case 1:
                        var sum = parseInt(responseArr.sum);
                        recycle_update(sum);
                        $('.catalog_position_tr[data-id=' + id_position + '] .catalog_position_add').attr('class', 'catalog_position_delete');
                        alert('Вариант добавлен в корзину');
                        break;

                    default:
                }
            }
        });
    }

    function change_option(id_option, value_option) {
        var post_data = {
            id_option: id_option,
            value_option: value_option
        };

        $.ajax({
            type: "POST",
            url: "/ajax/recycle-change-option/",
            dataType: "json",
            data: JSON.stringify(post_data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            cache: false,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var responseArr = JSON.parse(jqXHR.responseText),
                    status = parseInt(responseArr.status);
                switch (status) {
                    case 1:
                        var sum = parseInt(responseArr.sum),
                            sum_options = parseInt(responseArr.sum_options),
                            sum_options_word = sum_options + ' руб.',
                            id_position = parseInt(responseArr.id_position),
                            shop_action = $('.catalog_position_tr[data-id=' + id_position + '] .last_td');
                        $('.option_sum_value').html(sum_options_word);
                        if (sum_options > 0) {
                            shop_action.html("<div class='catalog_position_delete'></div>");
                        } else {
                            shop_action.html("<div class='catalog_position_add'></div>");
                        }
                        recycle_update(sum);
                        break;

                    default:
                }
            }
        });
    }

    function shop_delete(id_position) {
        var post_data = {
            id_position: id_position
        };
        $.ajax({
            type: "POST",
            url: "/ajax/recycle-position-delete/",
            dataType: "json",
            data: JSON.stringify(post_data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            cache: false,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var responseArr = JSON.parse(jqXHR.responseText),
                    status = parseInt(responseArr.status);

                switch (status) {
                    case 1:
                        var sum = parseInt(responseArr.sum);
                        recycle_update(sum);
                        $('.catalog_position_tr[data-id=' + id_position + '] .catalog_position_delete').attr('class', 'catalog_position_add');
                        alert('Вариант удален из корзины');
                        break;

                    default:
                }
            }
        });
    }

    function recycle_delete(id_position) {
        var post_data = {
                id_position: id_position
            },
            position = $('.position_tr[data-id=' + id_position + ']');
        $.ajax({
            type: "POST",
            url: "/ajax/recycle-delete/",
            dataType: "json",
            data: JSON.stringify(post_data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            cache: false,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var responseArr = JSON.parse(jqXHR.responseText),
                    status = parseInt(responseArr.status);
                if (status == 1) {
                    position.remove();
                    if ($('.position_tr').length == 1) {
                        document.location = '/';
                    }
                    var sum = parseInt(responseArr.sum),
                        sum_word = sum + " руб.",
                        shop_action = $('.catalog_position_tr[data-id=' + id_position + '] .last_td');
                    recycle_update(sum);
                    $('.position_tr .sum_all_value').html(sum_word);
                    shop_action.html("<div class='catalog_position_add'></div>");
                }
            }
        });
    }

    function recycle_option_delete(id_position, id_option) {
        var post_data = {
                id_position: id_position,
                id_option: id_option
            },
            position = $('.position_tr[data-id=' + id_position + ']'),
            option = position.find('span[data-option="' + id_option + '"]');
        $.ajax({
            type: "POST",
            url: "/ajax/recycle-option-delete/",
            dataType: "json",
            data: JSON.stringify(post_data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            cache: false,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var responseArr = JSON.parse(jqXHR.responseText),
                    status = parseInt(responseArr.status);
                if (status == 1) {
                    option.remove();
                    if (position.find('span[data-option]').length == 0) {
                        position.remove();
                    }
                    if ($('.position_tr').length == 1) {
                        document.location = '/';
                    }
                    var sum = parseInt(responseArr.sum),
                        sum_word = sum + " руб.",
                        sum_position = parseInt(responseArr.sum_position) + " руб.",
                        shop_action = $('.catalog_position_tr[data-id=' + id_position + '] .last_td');
                    recycle_update(sum);
                    $('.position_tr .sum_all_value').html(sum_word);
                    $('.position_tr[data-id="' + id_position + '"]').find('.position_td.last_td').html(sum_position);
                    shop_action.html("<div class='catalog_position_add'></div>");
                }
            }
        });
    }

    show_box.on('click', '.recycle_delete', function () {
        var id = $(this).parents('.position_tr').data('id');
        recycle_delete(id);
    });

    show_box.on('click', '.recycle_option_block span[data-option]', function () {
        var id = $(this).parents('.position_tr').data('id'),
            id_option = $(this).data('option');
        recycle_option_delete(id, id_option);
    });

    show_box.on('change', '.option_checkbox input', function () {
        var id_option = $(this).parents('.option_check').data('id'),
            value_option;
        if ($(this).prop('checked') == true) {
            value_option = 1;
        } else {
            value_option = 0;
        }
        change_option(id_option, value_option);
    });

    positions.on('click', '.catalog_position_tr .catalog_position_add', function () {
        var id_position = $(this).parents('.catalog_position_tr').data('id');
        shop_add(id_position);
    });

    positions.on('click', '.catalog_position_tr .catalog_position_delete', function () {
        var id_position = $(this).parents('.catalog_position_tr').data('id');
        shop_delete(id_position);
    });


    $('body').on('click', '[data-fancybox-shop]', function () {
        var id_option = window.currentOption,
            id_position = window.currentPosition,
            checkbox = $('.option_check[data-id="' + id_option + '"]').find('.option_checkbox input[type="checkbox"]'),
            value_option;
        if (checkbox.prop('checked') == true) {
            checkbox.prop('checked', false);
            $(this).removeClass('active');
        } else {
            checkbox.prop('checked', true);
            $(this).addClass('active');
        }
        value_option =  $(checkbox).prop('checked') == true ? 1 : 0;
        change_option(id_option, value_option);
    });

});