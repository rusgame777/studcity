<?php
namespace frontend\components;

use Yii;
use yii\helpers\Url;
use yii\bootstrap\Alert;

class Notice
{
    const info = 'info';
    const success = 'success';
    const warning = 'warning';
    const danger = 'danger';

    public static function statusCode()
    {
        return [
            self::info,
            self::success,
            self::warning,
            self::danger,
        ];
    }

    public static function classNotice()
    {
        return [
            self::info => 'alert-info',
            self::success => 'alert-info',
            self::warning => 'alert-warning',
            self::danger => 'alert-danger',
        ];
    }

    public static function init()
    {
        $alert = [];
        $currentUrl = Url::to();
        foreach (self::statusCode() as $status) {
            $statusArray = Yii::$app->session->getFlash($status);
            if (Yii::$app->session->hasFlash($status)) {
                if (!empty($statusArray) && isset($statusArray[$currentUrl])) {
                    $alert[] = Alert::widget([
                        'options' => [
                            'class' => self::classNotice()[$status],
                        ],
                        'body' => $statusArray[$currentUrl],
                    ]);
                    unset($statusArray[$currentUrl]);
                }
                Yii::$app->session->setFlash($status, $statusArray);
            }
        }
        echo !empty($alert) ? implode('', [
            '<div class="container_alert">',
            implode('', $alert),
            '</div>',
        ]) : '';

    }

    public static function send($message, $id_type = self::info, $id)
    {
        $info =
            array_merge(!empty(Yii::$app->session->getFlash($id_type)) ? Yii::$app->session->getFlash($id_type) : [], [$id => $message]);
        Yii::$app->session->setFlash($id_type, $info);
    }
}