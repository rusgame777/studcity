<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class LibAsset extends AssetBundle
{
    public $css = [
        'lib/owl-carousel/owl.carousel.css',
        'lib/owl-carousel/owl.transitions.css',
        'lib/owl-carousel/owl.theme.css',
    ];
    public $cssOptions = ['position' => View::POS_HEAD];
    public $js = [
        'lib/md5.js',
        'lib/owl-carousel/owl.carousel.js',
        'lib/jquery.placeholder.js',
        'lib/modernizr.custom.js',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
