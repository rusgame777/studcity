<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
    ];
    public $cssOptions = ['position' => View::POS_HEAD];
    public $js = [
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'frontend\assets\LibAsset',
        'frontend\assets\FontAwesomeAsset',
        'frontend\assets\NpmAsset',
        'frontend\assets\BowerAsset',
    ];
}
