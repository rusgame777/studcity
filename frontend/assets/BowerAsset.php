<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BowerAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $js = [
        'chosen/chosen.jquery.js',
        'html5-history-api/history.min.js',
        'js-cookie/src/js.cookie.js',
        'jquery-ui/jquery-ui.min.js',
        'jquery-ui/ui/i18n/datepicker-ru.js',
        'jquery.maskedinput/dist/jquery.maskedinput.min.js',
        'jquery.scrollTo/jquery.scrollTo.min.js',
        'vide/dist/jquery.vide.js',
//        'vide/dist/jquery.vide.min.js',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $css = [
        'chosen/chosen.css',
        'jquery-ui/themes/base/jquery-ui.min.css',
    ];
    public $cssOptions = ['position' => View::POS_HEAD];
}
