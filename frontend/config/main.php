<?php
$params =
    array_merge(require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php'));

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'assetManager' => [
            'linkAssets' => false,
            'appendTimestamp' => true,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'trace', 'error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'baseUrl' => '/',
            'rules' => [
                'solutions' => 'solutions/index',
                'catalog' => 'catalog/index',
                '<action:(logout|serts|about|profile|profile-edit|brands)>' => 'site/<action>',
                'download/<id:[\w]+>' => 'site/download',
                'check-author/<id:[\d]+>' => 'ajax/check-author',
                'check-user/<id:[\d]+>' => 'ajax/check-user',
                'status-sms/<id:[\d]+>' => 'ajax/status-sms',
                'delete-order' => 'ajax/delete-order',
                'delete-mail' => 'ajax/delete-mail',
                'remember-order' => 'ajax/remember-order',
                '<id>' => 'site/<id>',
            ],
            'suffix' => '/',
        ],
        'formatter' => [
            'sizeFormatBase' => 1024,
        ],
    ],
    
    'controllerMap' => [
        'solutions' => [
            'class' => 'frontend\controllers\SolutionController',
        ],
    ],
    
    'defaultRoute' => 'site/index',
    'language' => 'ru',
    'params' => $params,
];