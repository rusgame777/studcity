<?php
namespace frontend\models;

use common\models\Review;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\components\Table;
use yii\web\IdentityInterface;

/**
 * Review model
 *
 * @property integer $id
 * @property string $avtor
 * @property string $phone
 * @property string $title
 * @property string $message
 * @property integer $subm
 * @property string $date_add
 * @property string $time_add
 */

class ReviewForm extends Review
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('gb');
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avtor' => 'Имя',
            'phone' => 'Телефон',
            'title' => 'Тема',
            'message' => 'Комментарий',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'subm',
                'default',
                'value' => self::STATUS_INACTIVE,
            ],
            [
                'subm',
                'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE],
            ],
            [
                [
                    'avtor',
                    'phone',
                    'title',
                    'message',
                ],
                'string',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^8\d{10}$/i',
            ],
            [
                [
                    'avtor',
                    'message',
                ],
                'required',
            ],
            [
                'id',
                'integer',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                if ($insert) {
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function beforeValidate()
    {
        try {
            $search_phone = "/(\+7) (\d{3}) (\d{3}) (\d{4})/i";
            $replacement_phone =  "8\$2\$3\$4";
            $this->phone = preg_replace($search_phone, $replacement_phone, $this->phone);
        } catch (Exception $e) {
            //
        }
        return true;
    }
}
