<?php
namespace frontend\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * SolutionForm model
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $dataArray
 */
class SolutionForm extends Model
{
    public $id;
    public $name;
    public $phone;
    public $email;
    public $dataArray;

    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия, Имя',
            'phone' => 'Номер телефона',
            'email' => 'Электронная почта',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'phone',
                    'email',
                    'dataArray',
                ],
                'string',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^8\d{10}$/i',
            ],
            [
                'email',
                'email',
                'checkDNS' => true,
            ],
            [
                [
                    'id',
                    'name',
                    'phone',
                    'email',
                ],
                'required',
            ],
            [
                'id',
                'integer',
            ]
        ];
    }
    
    public function beforeValidate()
    {
        try {
            $search_phone = "/(\+7) (\d{3}) (\d{3}) (\d{4})/i";
            $replacement_phone = "8\$2\$3\$4";
            $this->phone = preg_replace($search_phone, $replacement_phone, $this->phone);
        } catch (Exception $e) {
            //
        }
        return true;
    }
}
