<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\validators\FileValidator;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

class CatalogUpload extends Model
{
    public function formName()
    {
        return '';
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'foto' => 'Выберите файл',
            'delete_foto' => 'удалить',
        ];
    }
    
    public static function mimeType()
    {
        return [
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'pdf' => 'application/pdf',
        ];
    }
    
    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/catalog/');
    }
    
    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/catalog/');
    }
    
    public static function size()
    {
        return [
            'foto' => [
                800,
                600,
            ],
        ];
    }
    
    public static function changeSize($foto, $nameAttr)
    {
        $imagine = new Imagine();
        $fileName = self::filePath() . $foto;
        $image = $imagine->open($fileName);
        $curSize = $image->getSize();
        $curWidth = $curSize->getWidth();
        $curHeight = $curSize->getHeight();
        $size = self::size()[$nameAttr];
        if ($size[0] > $size[1]) {
            if ($curWidth > $size[0]) {
                $newwidth = $size[0];
            } else {
                $newwidth = $curWidth;
            }
            $newheight = $curHeight * $newwidth / $curWidth;
        } else {
            if ($curHeight > $size[1]) {
                $newheight = $size[1];
            } else {
                $newheight = $curHeight;
            }
            $newwidth = $curWidth * $newheight / $curHeight;
        }
        $image->resize(new Box($newwidth, $newheight))->save($fileName);
    }
}