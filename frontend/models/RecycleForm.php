<?php
namespace frontend\models;

use backend\components\Common;
use common\components\Order;
use common\models\OrderDesc;
use common\models\OrdersErrors;
use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * RecycleForm model
 *
 * @property integer $id order ID
 * @property string $name
 * @property string $phone
 * @property string $email
 */
class RecycleForm extends Model
{
    public $id;
    public $name;
    public $phone;
    public $email;
    
    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия, Имя',
            'phone' => 'Номер телефона',
            'email' => 'Электронная почта',
        ];
    }
    
    public function checkOptions($attribute)
    {
        
        $id_order = $this->id;
        $options = OrderDesc::find()->select(['id_position'])->where(['id_order' => $id_order])->distinct()->count('id_position');
        if ($options >= 5) {
            $this->addError($attribute, 'Ваш заказ заблокирован!' . chr(10) . chr(13) . 'Обратитесь к администратору.');
    
            if (null === ($ordersErrors = OrdersErrors::findOne(['id_order' => $id_order]))) {
                $ordersErrors = new OrdersErrors();
                $ordersErrorsValues = [
                    'status' => $status = 1,
                    'id_order' => $id_order,
                ];
                if ($ordersErrors->load($ordersErrorsValues) && $ordersErrors->save()) {
                    $order = new Order(['InvId' => $id_order]);
                    $order->responseError($status = 1);
                }
            } else {
//                $ordersErrors->date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                $ordersErrors->date_add = Common::getDateTime('', 'Y-m-d H:i:s');
            }
        }
        
        return true;
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'phone',
                    'email',
                ],
                'string',
            ],
            [
                [
                    'id',
                ],
                'integer',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^8\d{10}$/i',
            ],
            [
                'email',
                'email',
                'checkDNS' => true,
            ],
            [
                [
                    'name',
                    'phone',
                    'email',
                ],
                'required',
            ],
            [
                'email',
                'checkOptions',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            $search_phone = "/(\+7) (\d{3}) (\d{3}) (\d{4})/i";
            $replacement_phone = "8\$2\$3\$4";
            $this->phone = preg_replace($search_phone, $replacement_phone, $this->phone);
        } catch (Exception $e) {
            //
        }
        return true;
    }
}
