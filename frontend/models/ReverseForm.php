<?php
namespace frontend\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * Reverse model
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $desc
 * @property string $dataArray
 * @property string $reCaptcha
 */
class ReverseForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $desc;
    public $dataArray;
    public $reCaptcha;

    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'phone' => 'Телефон',
            'email' => 'E-Mail',
            'desc' => 'Ваше сообщение',
        ];
    }
    
    public function checkContacts($attribute, $params)
    {
        if (empty($this->email) && empty($this->phone)) {
            $this->addError('email', 'Введите e-mail или телефон');
            $this->addError('phone', 'Введите e-mail или телефон');
        }
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [
                [
                    'name',
                    'phone',
                    'email',
                    'desc',
                    'dataArray',
                ],
                'string',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^8\d{10}$/i',
            ],
            [
                'email',
                'email',
                'checkDNS' => true,
            ],
            [
                [
                    'name',
                    'phone',
                    'email',
                    'desc',
                ],
                'required',
            ],
//            [
//                [
//                    'phone',
//                    'email',
//                ],
//                'checkContacts',
//                'skipOnEmpty' => false,
//                'skipOnError' => false,
//            ],
        ];
    
        if (!Yii::$app->request->isAjax) {
            $rules[] = [
                ['reCaptcha'],
                ReCaptchaValidator::class,
                'secret' => '6LfOGGgUAAAAAOc5Frgid7voa7clDfDzdI4sugU1',
                'uncheckedMessage' => 'Пожалуйста подтвердите, что вы не бот',
            ];
        }
        
        return $rules;
    }
    
    public function beforeValidate()
    {
        try {
            $search_phone = "/(\+7) (\d{3}) (\d{3}) (\d{4})/i";
            $replacement_phone = "8\$2\$3\$4";
            $this->phone = preg_replace($search_phone, $replacement_phone, $this->phone);
        } catch (Exception $e) {
            //
        }
        return true;
    }
}
