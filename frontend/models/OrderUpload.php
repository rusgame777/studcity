<?php

namespace frontend\models;

use common\components\Table;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\validators\FileValidator;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

class OrderUpload extends Model
{
    public $file;
    
    public function formName()
    {
        return '';
    }
    
    public function rules()
    {
        return [
            [
                ['file'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'doc, docx, pdf, rar, zip, jpg, png',
                'maxSize' => (new FileValidator)->getSizeLimit(),
            ],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'Выберите файл',
        ];
    }
    
    public static function mimeType()
    {
        return [
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'pdf' => 'application/pdf',
            'rar' => 'application/x-rar-compressed',
            'zip' => ['application/zip', 'application/x-zip-compressed'],
            'jpg' => ['image/jpeg', 'image/pjpeg'],
            'png' => 'image/png',
        ];
    }
    
    public static function isPhoto()
    {
        return [
            'jpg',
        ];
    }
    
    public static function mimeTypeList()
    {
        $mimeTypeList = [];
        foreach (self::mimeType() as $mimeType) {
            if (is_array($mimeType)) {
                $mimeTypeList = array_merge($mimeTypeList, $mimeType);
            }
            
            if (is_string($mimeType)) {
                $mimeTypeList[] = $mimeType;
            }
        }
        return array_unique($mimeTypeList);
    }
    
    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/orders/');
    }
    
    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/orders/');
    }
    
    public static function filePathDefault()
    {
        return Yii::getAlias('@frontendDocroot/uploads/no_manual.png');
    }
    
    public static function getExtension()
    {
        return Yii::$app->db->createCommand(vsprintf('SELECT * FROM %1$s', [
            1 => Table::get('rb_filter'),
        ]))->queryAll();
    }
    
    public static function getExtensionByMimeType($mime_type)
    {
        $extensions = ArrayHelper::index(self::getExtension(), 'mime_type');
        return $extensions[$mime_type]['name_ext'];
    }
    
    public static function upload($params)
    {
        if (in_array($params['mime_type'], self::mimeTypeList())) {
            $name_ext = self::getExtensionByMimeType($params['mime_type']);
            $data = explode(',', $params['file']);
            $encodedData = str_replace(' ', '+', $data[1]);
            $decodedData = base64_decode($encodedData);
            
            $nameFile = substr_replace(sha1(microtime(true)), '', 32) . '.' . $name_ext;
            file_put_contents(self::filePath() . $nameFile, $decodedData);
            
            return $nameFile;
        }
        return null;
    }
}