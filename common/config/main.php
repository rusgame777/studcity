<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => YII_ENV_PROD ? 'mysql:host=localhost;dbname=studcity' : 'mysql:host=localhost;dbname=stud-city-new',
            'username' => YII_ENV_PROD ? 'stud-city' : 'root',
            'password' => YII_ENV_PROD ? 'F8o3D9o7' : '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
//            'useFileTransport' => true,
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'noreply@stud-city.ru',
                'password' => 'ivan1989',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'formatter' => [
            'dateFormat' => 'd-M-Y',
            'datetimeFormat' => 'd-M-Y H:i:s',
            'timeFormat' => 'H:i:s',
            'locale' => 'ru-RU', //your language locale
            'defaultTimeZone' => 'Asia/Vladivostok',
            'timeZone' => 'Asia/Vladivostok',
        ],
        
    ],
];
