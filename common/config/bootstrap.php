<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::setAlias('@frontendWebroot', YII_ENV_PROD ? 'http://stud-city.ru' : 'http://studcity.rf/');
Yii::setAlias('@frontendDocroot', Yii::getAlias('@frontend/web'));
Yii::setAlias('@backendWebroot', YII_ENV_PROD ? 'http://admin.stud-city.ru' : 'http://admin.studcity.rf/');
Yii::setAlias('@backendDocroot', Yii::getAlias('@backend/web'));