<?php
$conf = [];
$conf['timezone'] = new DateTimeZone("Europe/Moscow");
$conf['DoWNames2'] = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"];
$conf['name_site'] = 'Готовые работы для студентов';
$conf['adminEmail'] = 'rusgame777@example.com';
$conf['supportEmail'] = 'rusgame777@example.com';
$conf['user.passwordResetTokenExpire'] = 3600;

return $conf;
