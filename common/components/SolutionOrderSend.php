<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 15.10.2017
 * Time: 15:59
 */

namespace common\components;

use backend\models\OrdersFiles;
use backend\models\OrdersUpload;
use common\models\SmsClient;
use common\models\SolutionOrderData;
use Yii;
use yii\helpers\ArrayHelper;
use backend\components\Common;
use common\models\Admin;
use common\models\Clients;
use common\models\Downloads;
use common\models\FileInfo;
use common\models\Settings;
use common\models\SettingsEmail;
use common\models\SolutionCat;
use common\models\UserSolution;
use common\models\Orders;
use common\models\OrderDesc;
use common\models\OrderStatus;
use common\models\OrderType;
use frontend\models\OrderUpload;

class SolutionOrderSend
{
    protected $id_order;
    protected $pathArchiveOrder;
    protected $size_archive = false;
    protected $attachments = [];
    protected $fileOrder;
    protected $nameArchiveOrder;
    protected $maxFileWithoutArchive = 5;
    protected $ordersdesc = [];
    
    public function __construct($id_order)
    {
        $this->id_order = $id_order;
    }
    
    public function getAllDataForTable()
    {
        $all = '';
        $results =
            SolutionOrderData::find()->where(['id_order' => $this->id_order])->orderBy(['id' => SORT_ASC])->asArray()
                ->all();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $all .= vsprintf('
                <tr>
               <td align="left">%1$s</td>
               <td align="center">%2$s</td>
                </tr>', [
                    1 => $result['name'],
                    2 => $result['value'],
                ]);
            }
        }
        return $all;
    }
    
    public function removeTempAttachments()
    {
        if ($this->size_archive && ($this->size_archive < 5)) {
            unlink($this->pathArchiveOrder);
            rmdir($this->fileOrder);
        } else {
            rename($this->pathArchiveOrder, OrderUpload::filePath() . $this->nameArchiveOrder);
            rmdir($this->fileOrder);
        }
    }
    
    public function saveAttachmentsAsArchive()
    {
        $this->nameArchiveOrder = "order" . $this->id_order . ".zip";
        $this->fileOrder = OrderUpload::filePath() . "order" . $this->id_order;
        $this->pathArchiveOrder = $this->fileOrder . "/" . $this->nameArchiveOrder;
        
        if (is_file(OrderUpload::filePath() . $this->nameArchiveOrder)) {
            unlink(OrderUpload::filePath() . $this->nameArchiveOrder);
        }
        if (!is_dir($this->fileOrder)) {
            mkdir($this->fileOrder, 0755);
        }
        
        $archiveOrder = new \ZipArchive();
        $isOpenArchiveOrder = $archiveOrder->open($this->pathArchiveOrder, \ZipArchive::CREATE);
        
        foreach ($this->ordersdesc as $orderdesc) {
            if ($isOpenArchiveOrder === true) {
                $nameFile = implode('.', [
                    $orderdesc['file'],
                    FileInfo::getExtension($orderdesc['file']),
                ]);
                $pathFile = OrdersUpload::filePath() . $orderdesc['file'];
                $archiveOrder->addFile($pathFile, Common::getInTranslit($nameFile));
            }
        }
        $archiveOrder->close();
        
        $this->size_archive = round(filesize($this->pathArchiveOrder) / FileInfo::Mb, 3);
        
        if ($this->size_archive < 5) {
            $this->attachments[$this->pathArchiveOrder] = [
                'fileName' => 'order.zip',
            ];
        }
    }
    
    public function saveAttachmentsAsSingle()
    {
        foreach ($this->ordersdesc as $orderdesc) {
//            $nameFile = $orderdesc['file'];
            $nameFile = implode('.', [
                $orderdesc['title'],
                FileInfo::getExtension($orderdesc['file']),
            ]);
            $pathFile = OrdersFiles::filePath() . $orderdesc['file'];
            $this->attachments[$pathFile] = [
                'fileName' => Common::getInTranslit($nameFile),
            ];
        }
    }
    
    public function isSaveAsArchive()
    {
        $countOrdersdesc = OrderDesc::find()->select(['id'])->where(['id_order' => $this->id_order])->count('id');
        
        $sumFileSize = 0;
        foreach ($this->ordersdesc as $orderdesc) {
            $pathFile = OrdersUpload::filePath() . $orderdesc['file'];
            $sumFileSize += is_file($pathFile) ? filesize($pathFile) : 0;
            
        }
        
        $sumFileSize = round($sumFileSize / FileInfo::Mb, 3);
        return ($countOrdersdesc > $this->maxFileWithoutArchive) || ($sumFileSize > 5);
    }
    
    public function addAttachments()
    {
        $this->ordersdesc = OrdersFiles::find()->where(['id_order' => $this->id_order])->distinct()->asArray()->all();
        
        if ($this->isSaveAsArchive()) {
            $this->saveAttachmentsAsArchive();
        } else {
            $this->saveAttachmentsAsSingle();
        }
    }
    
    public function response()
    {
        $order = Orders::findOne($this->id_order);
        if ($order === null) {
            return false;
        }
        
        $id_order = $order->id;
        $order->status = OrderStatus::paid;
        $order->update();
        
//        $date_add = Yii::$app->formatter->asDate($order->date_end, 'php:Y-m-d H:i:s');
//        $date_end = Yii::$app->formatter->asDate($date_add, 'php:Y-m-d');
        $date_add = Common::getDateTime($order->date_end, 'Y-m-d H:i:s');
        $date_end = Common::getDateTime($date_add, 'Y-m-d');
        
        $client = Clients::findOne($order->id_user);
        $settings = Settings::findOne(1);
        $notes = $settings->notes;
        
        $num_order = Orders::find()->where([
            'and',
            ['id_type' => OrderType::solution],
            ['between', 'date_end', $date_end, $date_add],
            ['send' => 1],
            ['status' => [OrderStatus::paid, OrderStatus::send]],
        ])->count();
//        $num_order .= '/' . Yii::$app->formatter->asDate($date_add, 'php:d.m.Y');
        $num_order .= '/' . Common::getDateTime($date_add, 'd.m.Y');
        
        $result = Yii::$app->db->createCommand("
            SELECT 
                orderdesc_solution.id_position,
                orderdesc_solution.cat AS id_cat,
                solution_cat.title AS name_cat,
                solution_position.title AS name_position,
                solution_position.id_author
            FROM orderdesc_solution,
                 solution_cat,
                 solution_position
            WHERE solution_cat.id = orderdesc_solution.cat
                AND solution_position.id = orderdesc_solution.id_position
                AND id_order = :id_order", [
            'id_order' => $id_order,
        ])->queryOne();
        
        $id_cat = array_key_exists('id_cat', $result) ? $result['id_cat'] : 0;
        $name_cat = array_key_exists('name_cat', $result) ? $result['name_cat'] : '';
        $name_position = array_key_exists('name_position', $result) ? $result['name_position'] : '';
        $category = SolutionCat::find()->where($id_cat)->asArray()->one();
        $sms_user = array_key_exists('sms_user', $category) ? $category['sms_user'] : 0;
        
        $all = $this->getAllDataForTable();
        $this->addAttachments();
        
        $params = [];
        $params['subject'] = vsprintf('Новый заказ на  сайте %1$s, номер заказа РМ%2$s', [
            1 => 'stud-city.ru',
//            1 => Yii::$app->request->getHostName(),
            2 => $num_order,
        ]);
        $params['attachments'] = $this->attachments;
        
        $text_attach = '';
        if ($this->isSaveAsArchive()) {
            if ($this->size_archive && ($this->size_archive < 5)) {
                $text_attach = '<b>Ваш заказ находятся во вложении единым архивом</b><br>';
            } else {
                $hash = md5($this->nameArchiveOrder);
                $num_downloads = Downloads::find()->where(['hash' => $hash])->count(0);
                if ($num_downloads == 0) {
//                    $date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                    $date_add = Common::getDateTime('', 'Y-m-d H:i:s');
                    $download = new Downloads();
                    $paymentValues = [
                        'hash' => $hash,
                        'id_order' => $this->id_order,
                        'date_add' => $date_add,
                    ];
                    if ($download->load($paymentValues) && $download->save()) {
                        
                    }
                }
                $url_attach = Yii::getAlias('@frontendWebroot') . '/download/' . $hash . '/';
                $text_attach = vsprintf(implode(' ', [
                    'Архив с вариантами вы можете скачать',
                    '<a href="%1$s"> здесь</a>.',
                    'Ссылка будет доступна в течении 3 дней.<br>',
                ]), [
                    1 => $url_attach,
                ]);
            }
        }
        
        $message = vsprintf('
            <html> 
            <body> 
            <p>            
            Уважаемый клиент! 
            Вы совершили заказ на сайте <a href="%1$s">%2$s</a><br>
            Номер вашего заказа - РМ%3$s; %4$s/%5$s
            %6$s<br>', [
//            1 => Yii::$app->request->getHostInfo(),
//            2 => Yii::$app->request->getHostName(),
            1 => Yii::getAlias('@frontendWebroot'),
            2 => 'stud-city.ru',
            3 => $num_order,
            4 => $name_cat,
            5 => $name_position,
            6 => $text_attach,
        ]);
        $message .= !empty($notes) ? sprintf("%s<br>", $notes) : "";
        
        $message .= vsprintf('
        <br>
        ---------------------------------------------------------------<br>
        При оформлении заказа вы указали следующие данные:<br>
        Номер телефона: %1$s<br>
        Электронная почта: %2$s<br>
        ---------------------------------------------------------------<br><br><br>
       ', [
            1 => $client->phone,
            2 => $client->email,
        ]);
        
        $message .= vsprintf('
        Данные для РГР:<br>
        <table cellpadding="0" cellspacing="0" border="3" width="300">
            <tr>
            <td align="left" width="200" style="padding-right: 10px;"><strong> Переменная</strong></td>
            <td align="center" width="100"><strong>Значение</strong></td>
            </tr>
            
            %1$s
            
        </table>
        ', [
            1 => $all,
        ]);
        
        $message .= vsprintf('
        По всем интересующим Вас вопросам пишите на электронную почту 
        <a href="mailto:admin@stud-city.ru">admin@stud-city.ru</a><br><br>
        Спасибо, что выбрали наш сайт!
        <img src="%1$s/check-user/%2$s/" style="width: 0; height: 0;">
        </p> 
        </body> 
        </html>
        ', [
            1 => Yii::getAlias('@frontendWebroot'),
            2 => $id_order,
        ]);
        
        $params['message'] = $message;
        
        $params['email'] = [];
        $usersArray =
            UserSolution::find()->select(['id_user'])->where(['id_cat' => $id_cat])->distinct()->asArray()->all();
        $usersArray = ArrayHelper::index($usersArray, 'id_user');
        foreach ($usersArray as $id_user => $user) {
            $emailPosition = Admin::findOne($id_user);
            if (null !== $emailPosition) {
                if (!empty($emailPosition->email)) {
                    $params['email'][] = $emailPosition->email;
                }
            }
        }
        
        $params['email'][] = $client->email;
        $params['email'] = array_merge($params['email'], self::getEmail());
        $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
        
        Email::send($params);

        if ($sms_user == 1) {
            $phone = $client->phone;
            $message =
                'Уважаемый клиент! Ваш заказ с сайта stud-city выполнен и отправлен на указанную Вами эл.почту.';
            $phone = "7" . mb_substr($phone, 1, 10, 'UTF-8');
//            $phone = "79175696706"; //TODO потом убрать
            list($num, $sms_cnt, $cost, $balance) = Phone::send_sms($phone, $message, 0);
            if ($sms_cnt > 0) {
                $smsClientValues = [
                    'status' => -1,
                    'message' => $message,
                    'id_user' => $order->id_user,
                    'id_order' => $id_order,
                    'phone' => $phone,
                    'num' => $num,
                ];
                $model = new SmsClient();

                if ($model->load($smsClientValues) && $model->save()) {

                }
            }
        }
        
        if ($this->isSaveAsArchive()) {
            $this->removeTempAttachments();
        }
        
        return true;
    }
    
    public static function getEmail()
    {
        $emailArr = [];
//        $settings = Settings::findOne(1);
//        $type_email = TypeEmail::NoticeOrder;
//        if (!empty($settings->$type_email)) {
//            $emailArr[] = $settings->$type_email;
//        }
        $emailList = SettingsEmail::find()->all();
        foreach ($emailList as $email) {
            if (!empty($email->email)) {
                $emailArr[] = $email->email;
            }
        }
        return $emailArr;
    }
}