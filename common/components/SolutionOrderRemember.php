<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 15.10.2017
 * Time: 15:59
 */

namespace common\components;

use backend\models\OrdersDocs;
use frontend\models\SolutionUpload;
use Yii;
use backend\components\Common;
use backend\models\OrdersFiles;
use backend\models\OrdersUpload;
use backend\models\SolutionPositionUpload;
use common\models\Author;
use common\models\Admin;
use common\models\Clients;
use common\models\FileInfo;
use common\models\Orders;
use common\models\OrderDesc;
use common\models\OrderStatus;
use common\models\OrderType;
use common\models\SettingsEmail;
use common\models\SolutionOrderData;
use common\models\UserSolution;
use frontend\models\OrderUpload;

class SolutionOrderRemember
{
    protected $id_order;
    protected $cat_array;
    protected $pathArchiveOrder;
    protected $size_archive = false;
    protected $attachments = [];
    protected $fileOrder;
    protected $nameArchiveOrder;
    protected $maxFileWithoutArchive = 5;
    protected $ordersdesc = [];
    
    public function __construct($id_order)
    {
        $this->id_order = $id_order;
    }
    
    public function getAllDataForTable()
    {
        $all = '';
        $results =
            SolutionOrderData::find()->where(['id_order' => $this->id_order])->orderBy(['id' => SORT_ASC])->asArray()
                ->all();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $all .= vsprintf('
                <tr>
               <td align="left">%1$s</td>
               <td align="center">%2$s</td>
                </tr>', [
                    1 => $result['name'],
                    2 => $result['value'],
                ]);
            }
        }
        return $all;
    }
    
    public function removeTempAttachments()
    {
        if ($this->size_archive && ($this->size_archive < 5)) {
            unlink($this->pathArchiveOrder);
            rmdir($this->fileOrder);
        } else {
            rename($this->pathArchiveOrder, OrderUpload::filePath() . $this->nameArchiveOrder);
            rmdir($this->fileOrder);
        }
    }
    
    public function saveAttachmentsAsArchive()
    {
        $this->nameArchiveOrder = "order" . $this->id_order . ".zip";
        $this->fileOrder = OrderUpload::filePath() . "order" . $this->id_order;
        $this->pathArchiveOrder = $this->fileOrder . "/" . $this->nameArchiveOrder;
        
        if (is_file(OrderUpload::filePath() . $this->nameArchiveOrder)) {
            unlink(OrderUpload::filePath() . $this->nameArchiveOrder);
        }
        if (!is_dir($this->fileOrder)) {
            mkdir($this->fileOrder, 0755);
        }
        
        $archiveOrder = new \ZipArchive();
        $isOpenArchiveOrder = $archiveOrder->open($this->pathArchiveOrder, \ZipArchive::CREATE);
        
        foreach ($this->ordersdesc as $orderdesc) {
            if ($isOpenArchiveOrder === true) {
                $nameFile = implode('.', [
                    $orderdesc['file'],
                    FileInfo::getExtension($orderdesc['file']),
                ]);
                $pathFile = OrdersUpload::filePath() . $orderdesc['file'];
                $archiveOrder->addFile($pathFile, Common::getInTranslit($nameFile));
            }
        }
        $archiveOrder->close();
        
        $this->size_archive = round(filesize($this->pathArchiveOrder) / FileInfo::Mb, 3);
        
        if ($this->size_archive < 5) {
            $this->attachments[$this->pathArchiveOrder] = [
                'fileName' => 'order.zip',
            ];
        }
    }
    
    public function saveAttachmentsAsSingle()
    {
        foreach ($this->ordersdesc as $orderdesc) {
//            $nameFile = $orderdesc['file'];
            $nameFile = implode('.', [
                $orderdesc['title'],
                FileInfo::getExtension($orderdesc['file']),
            ]);
            $pathFile = OrdersFiles::filePath() . $orderdesc['file'];
            $this->attachments[$pathFile] = [
                'fileName' => Common::getInTranslit($nameFile),
            ];
        }
    }
    
    public function isSaveAsArchive()
    {
        $countOrdersdesc = OrderDesc::find()->select(['id'])->where(['id_order' => $this->id_order])->count('id');
        
        $sumFileSize = 0;
        foreach ($this->ordersdesc as $orderdesc) {
            $pathFile = OrdersUpload::filePath() . $orderdesc['file'];
            $sumFileSize += is_file($pathFile) ? filesize($pathFile) : 0;
            
        }
        
        $sumFileSize = round($sumFileSize / FileInfo::Mb, 3);
        return ($countOrdersdesc > $this->maxFileWithoutArchive) || ($sumFileSize > 5);
    }
    
    public function addAttachments()
    {
        $this->ordersdesc = OrdersFiles::find()->where(['id_order' => $this->id_order])->distinct()->asArray()->all();
        
        if ($this->isSaveAsArchive()) {
            $this->saveAttachmentsAsArchive();
        } else {
            $this->saveAttachmentsAsSingle();
        }
    }
    
    public function response()
    {
        $order = Orders::findOne($this->id_order);
        if ($order === null) {
            return false;
        }
        
        $client = Clients::findOne($order->id_user);
        
        $date_add = Common::getDateTime($order->date_end, 'Y-m-d H:i:s');
        $data_end = Common::getDateTime($date_add, 'd.m.Y');
        $date_end = Common::getDateTime($date_add, 'Y-m-d');
        
        if ($order->id_type == OrderType::solution) {
            $num_order = Orders::find()->where([
                'and',
                ['id_type' => OrderType::solution],
                ['between', "date_end", $date_end, $date_add],
                ['send' => 1],
                ['status' => [OrderStatus::paid, OrderStatus::send]],
            
            ])->count(0);
            $num_order .= " / " . $data_end;
            
            $result = Yii::$app->db->createCommand("
            SELECT 
                orderdesc_solution.id_position,
                orderdesc_solution.cat AS id_cat,
                solution_cat.title AS name_cat,
                solution_position.title AS name_position,
                solution_position.id_author
            FROM orderdesc_solution
            LEFT JOIN solution_cat ON solution_cat.id = orderdesc_solution.cat
            LEFT JOIN solution_position ON  solution_position.id = orderdesc_solution.id_position
            WHERE id_order = :id_order", [
                'id_order' => $this->id_order,
            ])->queryOne();
            
            if (empty($result)) {
                return false;
            }
            
            $id_cat = array_key_exists('id_cat', $result) ? $result['id_cat'] : 0;
            $name_cat = array_key_exists('name_cat', $result) ? $result['name_cat'] : '';
            $name_position = array_key_exists('name_position', $result) ? $result['name_position'] : '';
            $id_author = array_key_exists('id_author', $result) ? $result['id_author'] : 0;
            $author = Author::find()->where($id_author)->asArray()->one();
            $email_author = array_key_exists('email', $author) ? $author['email'] : '';
            
            $params = [];
            $params['subject'] = vsprintf('ПОВТОРНО Новый заказ на сайте  %1$s, номер заказа РМ%2$s', [
                1 => Yii::$app->request->getHostName(),
                2 => $num_order,
            ]);
            $params['attachments'] = [];
            $message = vsprintf('
            <html> 
            <body> 
            <p>
            Вам поступил новый заказ<br>
            Номер заказа - РМ%1$s<br>
            Название РГР - %2$s/%3$s<br><br>
            ---------------------------------------------------------------<br>
            При оформлении заказа покупатель указал следующие данные:<br>
            Фамилия, имя: %4$s<br>
            Номер телефона: %5$s<br>
            Электронная почта: %6$s<br>
            ---------------------------------------------------------------<br>', [
                1 => $num_order,
                2 => $name_cat,
                3 => $name_position,
                4 => $client->name,
                5 => $client->phone,
                6 => $client->email,
            ]);
            
            $all = $this->getAllDataForTable();
            
            $message .= vsprintf('
                Данные для РГР:<br>
                <table cellpadding="0" cellspacing="0" border="3" width="300">
                <tr>
                <td align="left" width="200" style="padding-right: 10px;"><strong> Переменная</strong></td>
                <td align="center" width="100"><strong>Значение</strong></td>
                </tr> 
                
                %1$s
                
                </table>', [
                1 => $all,
            ]);
            
            $message .= vsprintf('
            <br><br>
            <a href="%1$s/solution-orders/send/?id=%2$s">Отправить работу</a>
            <br>Спасибо что пользуетесь нашими услугами, с уважением %3$s
            <img src="%1$s/check-author/%2$s/" style="width: 0; height: 0;">
            </p> 
            </body> 
            </html>', [
                1 => Yii::getAlias('@backendWebroot'),
                2 => $this->id_order,
                3 => Yii::$app->request->getHostInfo(),
            ]);
            
            $params['email'] = [];
            $user_array = [];
            $this->cat_array = UserSolution::find()->where(['id_cat' => $id_cat])->asArray()->all();
            if (count($this->cat_array) != 0) {
                foreach ($this->cat_array as $row) {
                    $id_user = $row['id_user'];
                    if (!in_array($id_user, $user_array)) {
                        $user_array[] = $id_user;
                    }
                }
            }
            foreach ($user_array as $id_user) {
                $emailPosition = Admin::findOne($id_user);
                if (null !== $emailPosition) {
                    if (!empty($emailPosition->email)) {
                        $params['email'][] = $emailPosition->email;
                    }
                }
            }
            $params['email'] = array_merge($params['email'], self::getEmail());
            $params['email'][] = $email_author;
    
            $params['attachments'] = [];
            $filePath = OrdersUpload::filePath();
            $files = OrdersDocs::find()->where(['id_order' => $order->id])->asArray()->all();
            foreach ($files as $file) {
                if (is_file(OrdersUpload::filePath() . $file['file'])) {
                    $nameFile = implode('.', [
                        $file['title'],
                        FileInfo::getExtension($file['file']),
                    ]);
                    $params['attachments'][$filePath . $file['file']] = [
                        'fileName' => $nameFile,
                    ];
                }
            }
            
            $params['message'] = $message;
            $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
            Email::send($params);
            
        }
        
        return true;
    }
    
    public static function getEmail()
    {
        $emailArr = [];
//        $settings = Settings::findOne(1);
//        $type_email = TypeEmail::NoticeOrder;
//        if (!empty($settings->$type_email)) {
//            $emailArr[] = $settings->$type_email;
//        }
        $emailList = SettingsEmail::find()->all();
        foreach ($emailList as $email) {
            if (!empty($email->email)) {
                $emailArr[] = $email->email;
            }
        }
        return $emailArr;
    }
}