<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 14.10.2017
 * Time: 11:24
 */

namespace common\components;

class TypeEmail
{
    const Reverse = 'mail1';
    const Order = 'mail2';
    const Review = 'mail3';
    const NoticeOrder = 'mail4';
}