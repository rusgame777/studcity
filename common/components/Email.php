<?php

namespace common\components;

use Yii;
//use app\models\Curriculum;
//use app\models\CurriculumVitaeContacts;
//use app\models\CurriculumVitaePlace;
use yii\web\ServerErrorHttpException;

class Email
{

    public static function send($params)
    {
        $mailer = Yii::$app->mailer->compose();
        if (array_key_exists('attachments', $params)) {
            foreach ($params['attachments'] as $index => $attachment) {
                $mailer->attach($index, $attachment);
            }
        }
        if (array_key_exists('attachmentsContent', $params)) {
            foreach ($params['attachmentsContent'] as $index => $attachmentFile) {
                $mailer->attachContent($index, $attachmentFile);
            }
        }
        $mailer->setFrom($params['from']);
        $mailer->setTo($params['email']);
        $mailer->setSubject($params['subject']);
        $mailer->setHtmlBody($params['message']);
        $mailer->send();
    }
}