<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 15.10.2017
 * Time: 15:59
 */

namespace common\components;

use backend\models\OrdersDocs;
use backend\models\OrdersUpload;
use common\models\OrdersRemember;
use common\models\SolutionOrderData;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use backend\components\Common;
use backend\models\CatalogPositionUpload;
use backend\models\SolutionPositionUpload;
use common\models\Admin;
use common\models\Author;
use common\models\CatalogPosition;
use common\models\CatalogSubcat;
use common\models\CatalogSubcatCat;
use common\models\Clients;
use common\models\Downloads;
use common\models\FileInfo;
use common\models\Settings;
use common\models\SettingsEmail;
use common\models\SmsAuthor;
use common\models\SolutionCat;
use common\models\University;
use common\models\UserCatalog;
use common\models\UserSolution;
use common\models\Orders;
use common\models\OrderDesc;
use common\models\OrderStatus;
use common\models\OrderType;
use common\models\Payments;
use frontend\models\OrderUpload;

class Order
{
    protected $OutSum;
    protected $InvId;
    protected $SignatureValue;
    protected $mrh_pass2 = 'y5g5vmQhL6Ywg3KOH3Vh';
    protected $cat_array;
    protected $pathArchiveOrder;
    protected $size_archive = false;
    protected $positions;
    protected $subcategories;
    protected $universities;
    protected $attachments = [];
    protected $fileOrder;
    protected $nameArchiveOrder;
    protected $maxFileWithoutArchive = 5;
    protected $ordersdesc = [];
    
    public function __construct($request)
    {
        $this->OutSum = array_key_exists('OutSum', $request) ? $request['OutSum'] : '';
        $this->InvId = array_key_exists('InvId', $request) ? $request['InvId'] : '';
        $this->SignatureValue = array_key_exists('SignatureValue', $request) ? $request['SignatureValue'] : '';
        
        $this->positions = ArrayHelper::index(CatalogPosition::find()->asArray()->all(), 'id');
        $this->subcategories = ArrayHelper::index(CatalogSubcat::find()->asArray()->all(), 'id');
        $this->universities = ArrayHelper::index(University::find()->asArray()->all(), 'id');
    }
    
    public function checkSrc()
    {
        $my_crc = md5(implode(':', [
            $this->OutSum, //x.000000
            $this->InvId,
            $this->mrh_pass2,
        ]));
        
        return (strtoupper($this->SignatureValue) == strtoupper($my_crc));
    }
    
    public function getBadSignResponse()
    {
        return "bad sign" . "\n";
    }
    
    public function getSuccessResponse()
    {
        return "OK" . $this->InvId . "\n";
    }
    
    public function getAllDataForTable()
    {
        $all = '';
        $results =
            SolutionOrderData::find()->where(['id_order' => $this->InvId])->orderBy(['id' => SORT_ASC])->asArray()
                ->all();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $all .= vsprintf('
                        <tr>
                       <td align="left">%1$s</td>
                       <td align="center">%2$s</td>
                        </tr>', [
                    1 => $result['name'],
                    2 => $result['value'],
                ]);
            }
        }
        return $all;
    }
    
    public function removeTempAttachments()
    {
        if ($this->size_archive && ($this->size_archive < 5)) {
            unlink($this->pathArchiveOrder);
            rmdir($this->fileOrder);
        } else {
            rename($this->pathArchiveOrder, OrderUpload::filePath() . $this->nameArchiveOrder);
            rmdir($this->fileOrder);
        }
    }
    
    public function saveAttachmentsAsArchive()
    {
        $this->nameArchiveOrder = "order" . $this->InvId . ".zip";
        $this->fileOrder = OrderUpload::filePath() . "order" . $this->InvId;
        $this->pathArchiveOrder = $this->fileOrder . "/" . $this->nameArchiveOrder;
        
        if (is_file(OrderUpload::filePath() . $this->nameArchiveOrder)) {
            unlink(OrderUpload::filePath() . $this->nameArchiveOrder);
        }
        if (!is_dir($this->fileOrder)) {
            mkdir($this->fileOrder, 0755);
        }
        
        $archiveOrder = new \ZipArchive();
        $isOpenArchiveOrder = $archiveOrder->open($this->pathArchiveOrder, \ZipArchive::CREATE);
        $delete_array = [];
        
        foreach ($this->ordersdesc as $orderdesc) {
            $id_position = $orderdesc['id_position'];
            $position = array_key_exists($id_position, $this->positions) ? $this->positions[$id_position] : [];
            $name_variant = array_key_exists('title', $position) ? $position['title'] : '';
            $file_variant = array_key_exists('file', $position) ? $position['file'] : '';
            $type_option = array_key_exists('type_option', $position) ? $position['type_option'] : '';
            $subcat = array_key_exists('subcat', $position) ? $position['subcat'] : 0;
            $name_variant = str_replace("вар", "", $name_variant);
            
            $subcategory = array_key_exists($subcat, $this->subcategories) ? $this->subcategories[$subcat] : [];
            $name_manual = array_key_exists('title', $subcategory) ? $subcategory['title'] : '';
            
            $nameFile = $name_manual . " - вариант " . $name_variant;
            if ($type_option == 1) {
                $options =
                    Yii::$app->db->createCommand("SELECT id_option, title, file FROM orderdesc, catalog_files WHERE orderdesc.id_option =  catalog_files.id AND id_order = :id_order AND id_position = :id_position ORDER BY CAST(REPLACE(title, '-', '') AS UNSIGNED) ASC", [
                        'id_order' => $this->InvId,
                        'id_position' => $id_position,
                    ])->queryAll();
                
                $num_options = count($options);
                if ($num_options > 0) {
                    $nameArchiveOptionOrder = implode('', [
                        "position",
                        $id_position,
                        ".zip",
                    ]);
                    
                    $pathArchiveOptionOrder = $this->fileOrder . "/" . $nameArchiveOptionOrder;
                    $archiveOptionOrder = new \ZipArchive();
                    $isOpenArchiveOptionOrder = $archiveOptionOrder->open($pathArchiveOptionOrder, \ZipArchive::CREATE);
                    
                    foreach ($options as $option) {
                        $fileOption = $option['file'];
                        $nameOption = implode(".", [
                            $option['title'],
                            FileInfo::getExtension($fileOption),
                        ]);
                        
                        $pathOptionArchiveOrder = CatalogPositionUpload::filePath() . $fileOption;
                        if ($isOpenArchiveOptionOrder === true) {
                            $archiveOptionOrder->addFile($pathOptionArchiveOrder, Common::getInTranslit($nameOption));
                        }
                    }
                    
                    $nameFile .= implode('', [
                        "(",
                        implode(', ', array_map(function($v) {
                            return $v['title'];
                        }, $options)),
                        ")",
                    ]);
                    
                    $archiveOptionOrder->close();
                    if ($isOpenArchiveOrder === true) {
                        $nameFile .= ".zip";
                        $archiveOrder->addFile($pathArchiveOptionOrder, Common::getInTranslit($nameFile));
                        $delete_array[] = $pathArchiveOptionOrder;
                    }
                }
            } elseif ($type_option == 2) {
                if ($isOpenArchiveOrder == true) {
                    $nameFile .= "." . FileInfo::getExtension($file_variant);
                    $pathFile = CatalogPositionUpload::filePath() . $file_variant;
                    $archiveOrder->addFile($pathFile, Common::getInTranslit($nameFile));
                }
            }
        }
        
        $archiveOrder->close();
        foreach ($delete_array as $item) {
            unlink($item);
        }
        
        $this->size_archive = round(filesize($this->pathArchiveOrder) / FileInfo::Mb, 3);
        
        if ($this->size_archive < 5) {
            $this->attachments[$this->pathArchiveOrder] = [
                'fileName' => 'order.zip',
            ];
        }
    }
    
    public function saveAttachmentsAsSingle()
    {
        foreach ($this->ordersdesc as $orderdesc) {
            $id_position = $orderdesc['id_position'];
            $position = array_key_exists($id_position, $this->positions) ? $this->positions[$id_position] : [];
            $name_variant = array_key_exists('title', $position) ? $position['title'] : '';
            $file_variant = array_key_exists('file', $position) ? $position['file'] : '';
            $type_option = array_key_exists('type_option', $position) ? $position['type_option'] : '';
            $subcat = array_key_exists('subcat', $position) ? $position['subcat'] : 0;
            $name_variant = str_replace("вар", "", $name_variant);
            
            $subcategory = array_key_exists($subcat, $this->subcategories) ? $this->subcategories[$subcat] : [];
            $name_manual = array_key_exists('title', $subcategory) ? $subcategory['title'] : '';
            
            $nameFile = $name_manual . " - вариант " . $name_variant;
            if ($type_option == 1) {
                $options =
                    Yii::$app->db->createCommand("SELECT id_option, title, file FROM orderdesc, catalog_files WHERE orderdesc.id_option =  catalog_files.id AND id_order = :id_order AND id_position = :id_position ORDER BY CAST(REPLACE(title, '-', '') AS UNSIGNED) ASC", [
                        'id_order' => $this->InvId,
                        'id_position' => $id_position,
                    ])->queryAll();
                
                $num_options = count($options);
                if ($num_options > 0) {
                    foreach ($options as $option) {
                        $fileOption = $option['file'];
                        $nameOption = implode("", [
                            $nameFile,
                            '(' . $option['title'] . ')',
                            "." . FileInfo::getExtension($fileOption),
                        ]);
                        
                        $pathOptionArchiveOrder = CatalogPositionUpload::filePath() . $fileOption;
                        $this->attachments[$pathOptionArchiveOrder] = [
                            'fileName' => Common::getInTranslit($nameOption),
                        ];
                    }
                }
            } elseif ($type_option == 2) {
                
                $pathFile = CatalogPositionUpload::filePath() . $file_variant;
                $nameFile = implode(".", [
                    $nameFile,
                    FileInfo::getExtension($file_variant),
                ]);
                $this->attachments[$pathFile] = [
                    'fileName' => Common::getInTranslit($nameFile),
                ];
            }
        }
        
    }
    
    public function isSaveAsArchive()
    {
        $countOrdersdesc = OrderDesc::find()->select(['id'])->where(['id_order' => $this->InvId])->count('id');
        
        $sumFileSize = 0;
        foreach ($this->ordersdesc as $orderdesc) {
            $id_position = $orderdesc['id_position'];
            $position = array_key_exists($id_position, $this->positions) ? $this->positions[$id_position] : [];
            $fileVariant = array_key_exists('file', $position) ? $position['file'] : '';
            $type_option = array_key_exists('type_option', $position) ? $position['type_option'] : '';
            
            if ($type_option == 1) {
                $options =
                    Yii::$app->db->createCommand("SELECT id_option, title, file FROM orderdesc, catalog_files WHERE orderdesc.id_option =  catalog_files.id AND id_order = :id_order AND id_position = :id_position ORDER BY CAST(REPLACE(title, '-', '') AS UNSIGNED) ASC", [
                        'id_order' => $this->InvId,
                        'id_position' => $id_position,
                    ])->queryAll();
                
                $num_options = count($options);
                if ($num_options > 0) {
                    foreach ($options as $option) {
                        $pathOption = CatalogPositionUpload::filePath() . $option['file'];
                        $sumFileSize += is_file($pathOption) ? filesize($pathOption) : 0;
                    }
                }
            } elseif ($type_option == 2) {
                $pathVariant = CatalogPositionUpload::filePath() . $fileVariant;
                $sumFileSize += is_file($pathVariant) ? filesize($pathVariant) : 0;
            }
        }
        
        $sumFileSize = round($sumFileSize / FileInfo::Mb, 3);
        return ($countOrdersdesc > $this->maxFileWithoutArchive) || ($sumFileSize > 5);
    }
    
    public function addAttachments()
    {
        $this->ordersdesc =
            OrderDesc::find()->select(['id_position', 'sum'])->where(['id_order' => $this->InvId])->distinct()
                ->asArray()->all();
        
        if ($this->isSaveAsArchive()) {
            $this->saveAttachmentsAsArchive();
        } else {
            $this->saveAttachmentsAsSingle();
        }
    }
    
    public function getAllPositionsForTable()
    {
        $all = '';
        
        $this->positions = ArrayHelper::index(CatalogPosition::find()->asArray()->all(), 'id');
        $this->subcategories = ArrayHelper::index(CatalogSubcat::find()->asArray()->all(), 'id');
        $this->universities = ArrayHelper::index(University::find()->asArray()->all(), 'id');
        
        $cat2subcat = [];
        $subcat2cat = [];
        $rows = CatalogSubcatCat::find()->asArray()->all();
        foreach ($rows as $row) {
            $cat2subcat[$row['id_cat']][$row['id_subcat']] = $row['id_university'];
            $subcat2cat[$row['id_subcat']][] = $row['id_cat'];
        }
        
        $ordersdesc = OrderDesc::find()->select(['id_position', 'sum'])->where(['id_order' => $this->InvId])->distinct()
            ->asArray()->all();
        foreach ($ordersdesc as $orderdesc) {
            $id_position = $orderdesc['id_position'];
            $sum = $orderdesc['sum'];
            $position = array_key_exists($id_position, $this->positions) ? $this->positions[$id_position] : [];
            $name_variant = array_key_exists('title', $position) ? $position['title'] : '';
            $type_option = array_key_exists('type_option', $position) ? $position['type_option'] : '';
            $subcat = array_key_exists('subcat', $position) ? $position['subcat'] : 0;
            $cat =
                array_key_exists($subcat, $subcat2cat) && is_array($subcat2cat[$subcat]) ? current($subcat2cat[$subcat]) : 0;
            $name_variant = str_replace("вар", "", $name_variant);
            if (!in_array($cat, $this->cat_array)) {
                $this->cat_array[] = $cat;
            }
            
            $subcategory = array_key_exists($subcat, $this->subcategories) ? $this->subcategories[$subcat] : [];
            $name_manual = array_key_exists('title', $subcategory) ? $subcategory['title'] : '';
            $author = array_key_exists('author', $subcategory) ? $subcategory['author'] : '';
            $year = array_key_exists('year', $subcategory) ? $subcategory['year'] : '';
            
            $id_university = 0;
            if (array_key_exists($cat, $cat2subcat) && array_key_exists($subcat, $cat2subcat[$cat])) {
                $id_university = $cat2subcat[$cat][$subcat];
            }
            
            $university =
                array_key_exists($id_university, $this->universities) ? $this->universities[$id_university] : [];
            $name_university = array_key_exists('title', $university) ? $university['title'] : '';
            
            $text = implode('', [
                "<span class='recycle_manual'>{$name_manual}",
                !empty($author) ? " / " . $author : '',
                !empty($year) ? " / " . $year . "г . " : '',
                "</span>",
                "<br><span class='recycle_option'>",
                "Вариант №",
                $name_variant,
                "</span>",
            ]);
            
            if ($type_option == 1) {
                $options =
                    Yii::$app->db->createCommand("SELECT id_option, title, file FROM orderdesc, catalog_files WHERE orderdesc.id_option =  catalog_files.id AND id_order = :id_order AND id_position = :id_position ORDER BY CAST(REPLACE(title, '-', '') AS UNSIGNED) ASC", [
                        'id_order' => $this->InvId,
                        'id_position' => $id_position,
                    ])->queryAll();
                $num_options = count($options);
                if ($num_options > 0) {
                    $text .= implode('', [
                        " / Задача ",
                        implode(', ', array_map(function($v) {
                            return $v['title'];
                        }, $options)),
                    ]);
                    $sum *= $num_options;
                }
            }
            $sum .= " руб.";
            $text .= sprintf("<br><span class='recycle_university'>%s</span>", $name_university);
            
            $all .= "
                <tr>
                <td align='left'>{$text}</td>
                <td align='center'>{$sum}</td>
                </tr>";
        }
        return $all;
    }
    
    public function responseError($status)
    {
        
        $order = Orders::findOne($this->InvId);
        if ($order === null) {
            return false;
        }
        
        $client = Clients::findOne($order->id_user);
        
        $id_order = $order->id;
        if ($order->id_type == OrderType::catalog) {
            
            $this->cat_array = [];
            $all = $this->getAllPositionsForTable();
            
            $sum_all = OrderDesc::find()->where(['id_order' => $this->InvId])->sum('sum');
            $sum_all .= " руб. ";
            
            // отправка на почту клиента решения из Каталога заданий
            $params = [];
            $params['subject'] = vsprintf('Новый заказ на сайте  %1$s, номер заказа %2$s', [
                1 => Yii::$app->request->getHostName(),
                2 => $id_order,
            ]);
            $params['attachments'] = $this->attachments;
            
            $message = vsprintf('
            <html> 
            <body> 
            <p>
            Покупатель пытается совершить заказ на сайте <a href="%1$s">%2$s</a><br>
            Номер заказа - %3$s<br>
            ', [
                1 => Yii::$app->request->getHostInfo(),
                2 => Yii::$app->request->getHostName(),
                3 => $id_order,
            ]);
            
            $message .= vsprintf('
            <br>
            ---------------------------------------------------------------<br>
            При оформлении заказа покупатель указал следующие данные:<br>
            Фамилия, имя: %1$s<br>
            Номер телефона: %2$s<br>
            Электронная почта: %3$s<br>
            ---------------------------------------------------------------<br><br><br>
            Заказы:<br>
           ', [
                1 => $client->name,
                2 => $client->phone,
                3 => $client->email,
            ]);
            
            $message .= vsprintf('
            <table cellpadding="0" cellspacing="0" border="3" width="800">
            <tr>
            <td align="left" width="600"><strong>Позиция заказа</strong></td>
            <td align="center" width="100"><strong>Стоимость</strong></td>
            </tr>
            
            %2$s

            <tr>
            <td align="right"><strong>Итоговая стоимость, руб.:</strong></td>
            <td align="center"><strong>%1$s</strong></td>
            </tr>
            </table>
            
            <br><br> По всем интересующим Вас вопросам пишите на электронную почту 
            <a href="mailto:admin@stud-city.ru">admin@stud-city.ru</a><br><br>
            Спасибо, что выбрали наш сайт!<br><br>
            </p>
            </body>
            </html>', [
                1 => $sum_all,
                2 => $all,
            ]);
            
            $params['message'] = $message;
            $params['email'] = [];
            $params['email'] = array_merge($params['email'], self::getEmail());
            $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
            Email::send($params);
        }
        return true;
    }
    
    public function response()
    {
        
        if (!$this->checkSrc()) {
            return $this->getBadSignResponse();
        }
        
        $order = Orders::findOne($this->InvId);
        if ($order === null) {
            return $this->getBadSignResponse();
        }
        
        $payment = Payments::findOne(['InvId' => $this->InvId]);
        if ($payment === null) {
//            $date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
//            $data_end = Yii::$app->formatter->asDate($date_add, 'php:d.m.Y');
//            $date_end = Yii::$app->formatter->asDate($date_add, 'php:Y-m-d');
            $date_add = Common::getDateTime('', 'Y-m-d H:i:s');
            $data_end = Common::getDateTime($date_add, 'd.m.Y');
            $date_end = Common::getDateTime($date_add, 'Y-m-d');
            $paymentValues = [
                'date_add' => $date_add,
                'InvId' => $this->InvId,
                'OutSum' => $this->OutSum,
                'SignatureValue' => $this->SignatureValue,
            ];
            $payment = new Payments();
            if ($payment->load($paymentValues) && $payment->save()) {
                
            }
            
            if ($order->id_type == OrderType::catalog) {
                $order->status = 2;
            } elseif ($order->id_type == OrderType::solution) {
                $order->status = 3;
            }
            $order->date_end = $date_add;
            $order->update();
        } else {
//            $date_add = Yii::$app->formatter->asDate($order->date_end, 'php:Y-m-d H:i:s');
//            $data_end = Yii::$app->formatter->asDate($date_add, 'php:d.m.Y');
//            $date_end = Yii::$app->formatter->asDate($date_add, 'php:Y-m-d');
            $date_add = Common::getDateTime($order->date_end, 'Y-m-d H:i:s');
            $data_end = Common::getDateTime($date_add, 'd.m.Y');
            $date_end = Common::getDateTime($date_add, 'Y-m-d');
        }
        
        $client = Clients::findOne($order->id_user);
        $settings = Settings::findOne(1);
        $notes = $settings->notes;
        
        if ($order->id_type == OrderType::catalog) {
            $num_order = Orders::find()->where([
                'and',
                ['id_type' => OrderType::catalog],
                ['between', 'date_end', $date_end, $date_add],
                ['send' => 1],
                ['status' => OrderStatus::paid],
            
            ])->count(0);
            $num_order .= " / " . $data_end;
            
            $this->cat_array = [];
            $all = $this->getAllPositionsForTable();
            $this->addAttachments();
            
            $sum_all = OrderDesc::find()->where(['id_order' => $this->InvId])->sum('sum');
            $sum_all .= " руб. ";
            
            // отправка на почту клиента решения из Каталога заданий
            $params = [];
            $params['subject'] = vsprintf('Новый заказ на сайте  %1$s, номер заказа %2$s', [
                1 => Yii::$app->request->getHostName(),
                2 => $num_order,
            ]);
            $params['attachments'] = $this->attachments;
            
            $text_attach = '';
            if ($this->isSaveAsArchive()) {
                if ($this->size_archive && ($this->size_archive < 5)) {
                    $text_attach = '<b>Ваш заказ находятся во вложении единым архивом</b><br>';
                } else {
                    $hash = md5($this->nameArchiveOrder);
                    $num_downloads = Downloads::find()->where(['hash' => $hash])->count(0);
                    if ($num_downloads == 0) {
//                        $date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                        $date_add = Common::getDateTime('', 'Y-m-d H:i:s');
                        $download = new Downloads();
                        $paymentValues = [
                            'hash' => $hash,
                            'id_order' => $this->InvId,
                            'date_add' => $date_add,
                        ];
                        if ($download->load($paymentValues) && $download->save()) {
                            
                        }
                    }
                    $url_attach = Yii::getAlias('@frontendWebroot') . '/download/' . $hash . '/';
                    $text_attach = vsprintf(implode(' ', [
                        'Архив с вариантами вы можете скачать',
                        '<a href="%1$s"> здесь</a>.',
                        'Ссылка будет доступна в течении 3 дней.<br>',
                    ]), [
                        1 => $url_attach,
                    ]);
                }
            }
            
            $message = vsprintf('
            <html> 
            <body> 
            <p>
            Вы совершили заказ на сайте <a href="%1$s">%2$s</a><br>
            Номер вашего заказа - %3$s<br>
            %4$s<br>', [
                1 => Yii::$app->request->getHostInfo(),
                2 => Yii::$app->request->getHostName(),
                3 => $num_order,
                4 => $text_attach,
            ]);
            $message .= !empty($notes) ? sprintf("%s<br>", $notes) : "";
            
            $message .= vsprintf('
            <br>
            ---------------------------------------------------------------<br>
            При оформлении заказа вы указали следующие данные:<br>
            Номер телефона: %1$s<br>
            Электронная почта: %2$s<br>
            ---------------------------------------------------------------<br><br><br>
            Заказы:<br>
           ', [
                1 => $client->phone,
                2 => $client->email,
            ]);
            
            $message .= vsprintf('
            <table cellpadding="0" cellspacing="0" border="3" width="800">
            <tr>
            <td align="left" width="600"><strong>Позиция заказа</strong></td>
            <td align="center" width="100"><strong>Стоимость</strong></td>
            </tr>
            
            %2$s

            <tr>
            <td align="right"><strong>Итоговая стоимость, руб.:</strong></td>
            <td align="center"><strong>%1$s</strong></td>
            </tr>
            </table>
            
            <br><br> По всем интересующим Вас вопросам пишите на электронную почту 
            <a href="mailto:admin@stud-city.ru">admin@stud-city.ru</a><br><br>
            Спасибо, что выбрали наш сайт!<br><br>
            </p>
            </body>
            </html>', [
                1 => $sum_all,
                2 => $all,
            ]);
            
            $params['message'] = $message;
            $params['email'] = [];
            $params['email'][] = $client->email;
            $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
            
            $client_array = [];
            foreach ($this->cat_array as $id_cat) {
                $catalogs = UserCatalog::find()->where(['id_cat' => $id_cat])->asArray()->all();
                if (count($catalogs) != 0) {
                    foreach ($catalogs as $catalog) {
                        $id_user = $catalog['id_user'];
                        if (!in_array($id_user, $client_array)) {
                            $client_array[] = $id_user;
                        }
                    }
                }
            }
            foreach ($client_array as $id_user) {
                $emailPosition = Admin::findOne($id_user);
                if (null !== $emailPosition) {
                    if (!empty($emailPosition->email)) {
                        $params['email'][] = $emailPosition->email;
                    }
                }
            }
            Email::send($params);
            
            // отправка на почту администратора решения из Каталога заданий
            $params = [];
            $text_attach = '';
            if ($this->isSaveAsArchive()) {
                if ($this->size_archive && ($this->size_archive < 5)) {
                    $text_attach = '<b>Ваш заказ находятся во вложении единым архивом</b><br>';
                } else {
                    $hash = md5($this->nameArchiveOrder);
                    $num_downloads = Downloads::find()->where(['hash' => $hash])->count(0);
                    if ($num_downloads == 0) {
//                        $date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                        $date_add = Common::getDateTime('', 'Y-m-d H:i:s');
                        $download = new Downloads();
                        $downloadValues = [
                            'hash' => $hash,
                            'id_order' => $this->InvId,
                            'date_add' => $date_add,
                        ];
                        if ($download->load($downloadValues) && $download->save()) {
                            
                        }
                    }
                    $url_attach = Yii::getAlias('@frontendWebroot') . '/download/' . $hash . '/';
                    $text_attach = vsprintf(implode(' ', [
                        'Архив с вариантами вы можете скачать',
                        '<a href="%1$s"> здесь</a>.',
                        'Ссылка будет доступна в течении 3 дней.<br>',
                    ]), [
                        1 => $url_attach,
                    ]);
                }
            }
            
            $params['subject'] = vsprintf('Новый заказ на сайте  %1$s, номер заказа %2$s', [
                1 => Yii::$app->request->getHostName(),
                2 => $num_order,
            ]);
            $params['attachments'] = $this->attachments;
            
            $message = vsprintf('
            <html> 
            <body> 
            <p>            
            На сайте <a href="%1$s">%2$s</a> совершили новый заказ<br>
            Номер заказа - %3$s<br>
            %4$s<br> ', [
                1 => Yii::$app->request->getHostInfo(),
                2 => Yii::$app->request->getHostName(),
                3 => $num_order,
                4 => $text_attach,
            ]);
            $message .= !empty($notes) ? sprintf("%s<br>", $notes) : "";
            
            $message .= vsprintf('
            <br>
            ---------------------------------------------------------------<br>
            При оформлении заказа покупатель указал следующие данные:<br>
            Фамилия, имя: %1$s<br>
            Номер телефона: %2$s<br>
            Электронная почта: %3$s<br>
            ---------------------------------------------------------------<br><br><br>
            Заказы:<br>
           ', [
                1 => $client->name,
                2 => $client->phone,
                3 => $client->email,
            ]);
            
            $message .= vsprintf('
            <table cellpadding="0" cellspacing="0" border="3" width="800">
            <tr>
            <td align="left" width="600"><strong>Позиция заказа</strong></td>
            <td align="center" width="100"><strong>Стоимость</strong></td>
            </tr>
            
            %3$s

            <tr>
            <td align="right"><strong>Итоговая стоимость, руб.:</strong></td>
            <td align="center"><strong>%1$s</strong></td>
            </tr>
            </table>
            
            <br><br> Спасибо что пользуетесь нашими услугами, с уважением %2$s
            </p>
            </body>
            </html>', [
                1 => $sum_all,
                2 => Yii::$app->request->getHostInfo(),
                3 => $all,
            ]);
            $params['message'] = $message;
            $params['email'] = [];
            $params['email'] = array_merge($params['email'], self::getEmail());
            $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
            Email::send($params);
            
            if ($this->isSaveAsArchive()) {
                $this->removeTempAttachments();
            }
            
        } elseif ($order->id_type == OrderType::solution) {
            $num_order = Orders::find()->where([
                'and',
                ['id_type' => OrderType::solution],
                ['between', "date_end", $date_end, $date_add],
                ['send' => 1],
                ['status' => [OrderStatus::paid, OrderStatus::send]],
            
            ])->count(0);
            $num_order .= " / " . $data_end;
            
            $result = Yii::$app->db->createCommand("
            SELECT 
                orderdesc_solution.id_position,
                orderdesc_solution.cat AS id_cat,
                solution_cat.title AS name_cat,
                solution_position.title AS name_position,
                solution_position.id_author
            FROM orderdesc_solution,
                 solution_cat,
                 solution_position
            WHERE solution_cat.id = orderdesc_solution.cat
                AND solution_position.id = orderdesc_solution.id_position
                AND id_order = :id_order", [
                'id_order' => $this->InvId,
            ])->queryOne();
            
            $id_cat = array_key_exists('id_cat', $result) ? $result['id_cat'] : 0;
            $name_cat = array_key_exists('name_cat', $result) ? $result['name_cat'] : '';
            $name_position = array_key_exists('name_position', $result) ? $result['name_position'] : '';
            $id_author = array_key_exists('id_author', $result) ? $result['id_author'] : 0;
            $author = Author::find()->where($id_author)->asArray()->one();
            $email_author = array_key_exists('email', $author) ? $author['email'] : '';
            $phone_author = array_key_exists('phone', $author) ? $author['phone'] : '';
            $category = SolutionCat::find()->where($id_cat)->asArray()->one();
            $sms_author = array_key_exists('sms_author', $category) ? $category['sms_author'] : 0;
            
            if ($sms_author == 1) {
                $phone = $phone_author;
                $message = 'к Вам пришел новый заказ с сайта stud-city';
                $phone = "7" . mb_substr($phone, 1, 10, 'UTF-8');
                $phone = "79175696706"; //TODO потом убрать
                list($num, $sms_cnt, $cost, $balance) = Phone::send_sms($phone, $message, 0);
                if ($sms_cnt > 0) {
                    $smsAuthorValues = [
                        'status' => -1,
                        'message' => $message,
                        'id_author' => $id_author,
                        'id_order' => $this->InvId,
                        'phone' => $phone,
                        'num' => $num,
                    ];
                    $model = new SmsAuthor();
                    if ($model->load($smsAuthorValues) && $model->save()) {
                        
                    }
                }
            }
            
            // отправка на почту администратора решения из Решение за мгновение
            $params = [];
            $params['subject'] = vsprintf('Новый заказ на сайте  %1$s, номер заказа РМ%2$s', [
                1 => Yii::$app->request->getHostName(),
                2 => $num_order,
            ]);
            $params['attachments'] = [];
            $message = vsprintf('
            <html> 
            <body> 
            <p>
            Вам поступил новый заказ<br>
            Номер заказа - РМ%1$s<br>
            Название РГР - %2$s/%3$s<br><br>
            ---------------------------------------------------------------<br>
            При оформлении заказа покупатель указал следующие данные:<br>
            Фамилия, имя: %4$s<br>
            Номер телефона: %5$s<br>
            Электронная почта: %6$s<br>
            ---------------------------------------------------------------<br>', [
                1 => $num_order,
                2 => $name_cat,
                3 => $name_position,
                4 => $client->name,
                5 => $client->phone,
                6 => $client->email,
            ]);
            
            $all = $this->getAllDataForTable();
            
            $message .= vsprintf('
                Данные для РГР:<br>
                <table cellpadding="0" cellspacing="0" border="3" width="300">
                <tr>
                <td align="left" width="200" style="padding-right: 10px;"><strong> Переменная</strong></td>
                <td align="center" width="100"><strong>Значение</strong></td>
                </tr> 
                
                %1$s
                
                </table>', [
                1 => $all,
            ]);
            
            $message .= vsprintf('
            <br><br>
            <a href="%1$s/solution-orders/send/?id=%2$s">Отправить работу</a>
            <br>Спасибо что пользуетесь нашими услугами, с уважением %3$s
            <img src="%1$s/check-author/%2$s/" style="width: 0; height: 0;">
            </p> 
            </body> 
            </html>', [
                1 => Yii::getAlias('@backendWebroot'),
                2 => $this->InvId,                
                3 => Yii::$app->request->getHostInfo(),
            ]);
            
            $params['email'] = [];
            $user_array = [];
            $this->cat_array = UserSolution::find()->where(['id_cat' => $id_cat])->asArray()->all();
            if (count($this->cat_array) != 0) {
                foreach ($this->cat_array as $row) {
                    $id_user = $row['id_user'];
                    if (!in_array($id_user, $user_array)) {
                        $user_array[] = $id_user;
                    }
                }
            }
            foreach ($user_array as $id_user) {
                $emailPosition = Admin::findOne($id_user);
                if (null !== $emailPosition) {
                    if (!empty($emailPosition->email)) {
                        $params['email'][] = $emailPosition->email;
                    }
                }
            }
            $params['email'] = array_merge($params['email'], self::getEmail());
            $params['email'][] = $email_author;
            
            $params['attachments'] = [];
            $filePath = OrdersUpload::filePath();
            $files = OrdersDocs::find()->where(['id_order' => $order->id])->asArray()->all();
            foreach ($files as $file) {
                if (is_file(OrdersUpload::filePath() . $file['file'])) {
                    $nameFile = implode('.', [
                        $file['title'],
                        FileInfo::getExtension($file['file']),
                    ]);
                    $params['attachments'][$filePath . $file['file']] = [
                        'fileName' => $nameFile,
                    ];
                }
            }
            
            $params['message'] = $message;
            $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
            Email::send($params);
            
            // отправка на почту клиента решения из Решение за мгновение
            $params = [];
            $params['email'] = [$client->email];
            $params['subject'] = vsprintf('Новый заказ на сайте  %1$s, номер заказа РМ%2$s', [
                1 => Yii::$app->request->getHostName(),
                2 => $num_order,
            ]);
            
            $message = vsprintf('
            <html> 
            <body> 
            <p>
            Уважаемый клиент!<br>
            На сайте <a href="%1$s">%2$s</a> совершили новый заказ<br>
            Заказ РМ%3$s; %4$s/%5$s -принят в работу<br>
            В ближайшее время будет выслано решение.<br>
            ---------------------------------------------------------------<br>
            При оформлении заказа вы указали следующие данные:<br>
            Номер телефона: %6$s<br>
            Электронная почта: %7$s<br>
            ---------------------------------------------------------------<br>
            ', [
                1 => Yii::getAlias('@frontendWebroot'),
                2 => Yii::$app->request->getHostName(),
                3 => $num_order,
                4 => $name_cat,
                5 => $name_position,
                6 => $client->phone,
                7 => $client->email,
            ]);
    
            $message .= vsprintf('
                Данные для РГР:<br>
                <table cellpadding="0" cellspacing="0" border="3" width="300">
                <tr>
                <td align="left" width="200" style="padding-right: 10px;"><strong> Переменная</strong></td>
                <td align="center" width="100"><strong>Значение</strong></td>
                </tr> 
                
                %1$s
                
                </table>', [
                1 => $all,
            ]);
            
            $message .= '</table> 
            По всем интересующим Вас вопросам пишите на электронную почту 
            <a href="mailto:admin@stud-city.ru">admin@stud-city.ru</a><br><br>
            Спасибо, что выбрали наш сайт!
            </p> 
            </body> 
            </html>
            ';
            
            $params['message'] = $message;
            $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
            Email::send($params);
    
            if (($orderRemember = OrdersRemember::findOne(['id_order' => $this->InvId])) === null) {
                $orderRemember = new OrdersRemember();
            }
            $data = [
                'id_order' => $this->InvId,
                'date_next_send' => Common::getDateTime('', 'Y-m-d H:i:s', '+3 day'),
            ];
    
            if ($orderRemember->load($data) && $orderRemember->save()) {
                //
            }
        }
        return $this->getSuccessResponse();
        
    }
    
    public static function getEmail()
    {
        $emailArr = [];
//        $settings = Settings::findOne(1);
//        $type_email = TypeEmail::NoticeOrder;
//        if (!empty($settings->$type_email)) {
//            $emailArr[] = $settings->$type_email;
//        }
        $emailList = SettingsEmail::find()->all();
        foreach ($emailList as $email) {
            if (!empty($email->email)) {
                $emailArr[] = $email->email;
            }
        }
        return $emailArr;
    }
}