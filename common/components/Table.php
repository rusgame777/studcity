<?php
namespace common\components;

class Table
{
    const prefix_view = '';
    const prefix_executable = '';
    
    public static function get($value, $executable = false)
    {
        
        $array = [

            'page_categories' => 'page_categories',
            'page_content' => 'page_content',

            'catalog_cat' => 'catalog_cat',
            'catalog_files' => 'catalog_files',
            'catalog_foto' => 'catalog_foto',
            'catalog_subcat' => 'catalog_subcat',
            'catalog_subcat_cat' => 'catalog_subcat_cat',
            'catalog_position' => 'catalog_position',
            'orders' => 'orders',
            'orders_errors' => 'orders_errors',
            'orders_remember' => 'orders_remember',
            'orders_check' => 'orders_check',
            'orders_docs' => 'orders_docs',
            'orders_files' => 'orders_files',
            'orderdata_solution' => 'orderdata_solution',
            'orderdesc' => 'orderdesc',
            'orderdesc_solution' => 'orderdesc_solution',
            'payments' => 'payments',
            'downloads' => 'downloads',

            'users_catalog' => 'users_catalog',
            'users_solution' => 'users_solution',
            'sms_authors' => 'sms_authors',
            'sms_users' => 'sms_users',

            'university' => 'university',
            'rb_filter' => 'rb_filter',

            'solution_cat' => 'solution_cat',
            'solution_position' => 'solution_position',
            'solution_data' => 'solution_data',

            'gb' => 'gb',
            'reply' => 'reply',

            'template' => 'template',
            'email' => 'email',
            'site_settings' => 'site_settings',
            
            'regusers' => 'regusers',

            'admin' => 'admin',
//            'users' => 'users',
        
        ];
        
        return (array_key_exists($value, $array) ? implode('.', array_filter([
            ($executable ? self::prefix_executable : self::prefix_view),
            $array[$value],
        ])) : '');
        
    }
    
    public static function getExecutable($value)
    {
        return self::get($value, $executable = true);
    }
}