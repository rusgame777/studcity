<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\components\Table;

/**
 * Settings model
 *
 * @property integer $id
 * @property string $mail1
 * @property string $mail2
 * @property string $mail3
 * @property string $mail4
 * @property string $notes
 * @property string $url_vk
 * @property string $order1
 * @property string $order2
 * @property string $code_map
 * @property string $code_head
 * @property string $code_body
 *
 */
class Settings extends ActiveRecord
{
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('site_settings');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
//            'mail1' => 'E-mail для уведомлений с формы «Обратная связь»',
//            'mail2' => 'E-mail для уведомлений с формы «Заказать работу»',
//            'mail3' => 'E-mail для уведомлений о новых отзывах',
//            'mail4' => 'E-mail для уведомлений о заказах',
            'notes' => 'Заметки к заказу',
            'url_vk' => 'Ссылка на группа в ВКонтакте',
            'order1' => 'Технические предметы на странице "Заказать работу"',
            'order2' => 'Гуманитарные предметы на странице "Заказать работу',
            'code_map' => 'Код вставки карты на страницу Контакты',
            'code_head' => 'Код вставки в <head></head>',
            'code_body' => 'Код вставки перед </body>',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'notes',
                    'url_vk',
                    'order1',
                    'order2',
                    'code_map',
                    'code_head',
                    'code_body',
                ],
                'string',
            ],
//            [
//                [
//                    'mail1',
//                    'mail2',
//                    'mail3',
//                    'mail4',
//                ],
//                'email',
//            ],
//            [
//                [
//                    'mail1',
//                    'mail2',
//                    'mail3',
//                    'mail4',
//                ],
//                'required',
//            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                //
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}