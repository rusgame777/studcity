<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\components\Table;

/**
 * CatalogSubcatCat model
 *
 * @property integer $id_subcat
 * @property integer $id_university
 * @property integer $id_cat
 *
 */
class CatalogSubcatCat extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('catalog_subcat_cat');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id_subcat' => 'Методичка',
            'id_university' => 'Университет',
            'id_cat' => 'Дисциплина',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id_subcat',
                    'id_university',
                    'id_cat',
                ],
                'integer',
            ],
            [
                [
                    'id_university',
                    'id_cat',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }

    public static function primaryKey()
    {
        return ['id_subcat'];
    }
}