<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;
use common\models\CatalogSubcatCat;

/**
 * University model
 *
 * @property integer $id
 * @property string $title
 *
 */
class University extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('university');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID университета',
            'title' => 'Наименование университета',
            'sort' => 'Приоритет',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'title',
                ],
                'string',
            ],
            [
                [
                    'title',
                ],
                'required',
            ],
            [
                'sort',
                'integer',
            ],
        ];
    }

    public function getSubcat()
    {
        return $this->hasMany(CatalogSubcatCat::className(), ['id_university' => 'id']);
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
}