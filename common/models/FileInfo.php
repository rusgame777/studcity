<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 19.01.2018
 * Time: 1:00
 */

namespace common\models;

use Yii;

class FileInfo
{
    const Mb = 1024 * 1204;
    const Kb = 1024;
    const b = 1;
    
    public static function getUrlObject()
    {
        return self::getUrl();
    }
    
    public static function getAddrObject()
    {
        return self::getAddr();
    }
    
    public static function getUrl()
    {
        return Yii::getAlias('@frontendWebroot');
    }
    
    public static function filePathDefault()
    {
        return Yii::getAlias('@frontendDocroot');
    }
    
    public static function getAddr()
    {
        return $_SERVER['DOCUMENT_ROOT'];
    }
    
    public static function getBaseName($fileName)
    {
        return pathinfo($fileName, PATHINFO_BASENAME);
    }
    
    public static function getFileName($fileName)
    {
        return pathinfo($fileName, PATHINFO_BASENAME);
    }
    
    public static function getValidFileName($name)
    {
        return str_replace(explode(' ', '\ / : * ? " < > |'), '', $name);
    }
    
    public static function getContentType($fileAddr)
    {
        return mime_content_type($fileAddr);
    }
    
    public static function getExtension($fileName)
    {
        return pathinfo($fileName, PATHINFO_EXTENSION);
    }
    
    public static function getFileSize($fileName)
    {
        $flsz = filesize(self::getAddrObject() . $fileName);
        if ($flsz > self::Mb) {
            $flsz = round($flsz / self::Mb, 3);
            $flsz .= ' Mb';
        } elseif ($flsz > self::Kb) {
            $flsz = round($flsz / self::Kb, 1);
            $flsz .= ' Кb';
        } else {
            $flsz .= ' b';
        }
        return $flsz;
    }
}