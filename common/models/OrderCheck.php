<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 08.10.2017
 * Time: 17:59
 */

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\components\Table;

/**
 * OrderCheck model
 *
 * @property integer $id
 * @property integer $check_author
 * @property integer $check_user
 * @property integer $id_order
 *
 */
class OrderCheck extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('orders_check');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
        
        ];
    }
    
    public function rules()
    {
        return [

            [
                [
                    'check_author',
                    'check_user',
                ],
                'default',
                'value' => self::STATUS_ACTIVE,
            ],
            [
                [
                    'check_author',
                    'check_user',
                ],
                'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE],
                //                    'skipOnEmty' => false,
            ],
            [
                [
                    'id',
                    'id_order',
                    'check_author',
                    'check_user',
                ],
                'integer',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                //
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}