<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;

/**
 * SolutionData model
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_position
 *
 */
class SolutionData extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    public $value;
    
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('solution_data');
    }
    
    public function formName()
    {
        return 'data';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'id_position' => 'РГР',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'id_position',
                ],
                'integer',
            ],
            [
                [
                    'name',
                    'value',
                ],
                'string',
            ],
            [
                [
                    'name',
                    'value',
                ],
                'required',
            ],
        ];
    }
    
    public function setValue($value)
    {
        $this->value = $value;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
}