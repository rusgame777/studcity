<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\components\Table;

/**
 * UserSolution model
 *
 * @property integer $id
 * @property integer $id_cat
 * @property integer $id_user
 *
 */
class UserSolution extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('users_solution');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'id_cat',
                    'id_user',
                ],
                'integer',
            ],
            [
                [
                    'id_cat',
                    'id_user',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }

//    public static function primaryKey()
//    {
//        return ['id_cat'];
//    }
}