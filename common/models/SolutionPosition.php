<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\components\Table;

/**
 * SolutionPosition model
 *
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $who_create
 * @property string $who_update
 * @property string $title
 * @property string $price
 * @property string $limitation
 * @property integer $id_author
 * @property string $metatitle
 * @property string $description
 * @property string $keywords
 * @property string $content
 * @property string $comments
 * @property string $foto
 * @property string $foto_small
 * @property integer $sort
 * @property integer $cat
 *
 */
class SolutionPosition extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    const OPTION_ACTIVE = 1;
    const OPTION_INACTIVE = 2;
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
                //                'createdAtAttribute' => 'date_create',
                //                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('solution_position');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'title' => 'Наименование',
            'id_author' => 'Автор',
            'cat' => 'Дисциплина',
            'price' => 'Цена',
            'limitation' => 'Срок',
            'metatitle' => 'Заголовок страницы, тег <title>',
            'description' => 'Мета-тег Description',
            'keywords' => 'Мета-тег Keywords',
            'content' => 'Содержание страницы',
            'comments' => 'Примечание',
            'sort' => 'Приоритет',
            'foto' => 'Фотография на главной',
            'foto_small' => 'Миниатюра фотографии',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'metatitle',
                    'description',
                    'keywords',
                    'content',
                    'comments',
                ],
                'string',
            ],
            [
                [
                    'title',
                    'id_author',
                    'cat',
                    'price',
                ],
                'required',
            ],
            [
                [
                    'price',
                    'id_author',
                    'sort',
                    'cat',
                ],
                'integer',
            ],
        ];
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getCat()
    {
        return $this->hasOne(SolutionCat::tableName(), ['id' => 'cat']);
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}