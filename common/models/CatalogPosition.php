<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\components\Table;

/**
 * CatalogPosition model
 *
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $who_create
 * @property string $who_update
 * @property string $title
 * @property integer $type_option
 * @property integer $price
 * @property string $file
 * @property integer $sort
 * @property integer $subcat
 *
 */
class CatalogPosition extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    const OPTION_ACTIVE = 1;
    const OPTION_INACTIVE = 2;
    public $cat, $price1, $price2;
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_create', 'date_update'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date_update'],
                ],
                //                'createdAtAttribute' => 'date_create',
                //                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('catalog_position');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'type_option' => 'Разделение по заданиям',
            'price1' => 'Цена за задание',
            'price2' => 'Цена за вариант',
            'price' => 'Стоимость',
            'sort' => 'Приоритет',
            'file' => 'Файл',
            'cat' => 'Дисциплина',
            'subcat' => 'Методичка',
        ];
    }
    
    public function rules()
    {
        return [
            [
                'type_option',
                'default',
                'value' => self::OPTION_ACTIVE,
            ],
            [
                [
                    'title',
                ],
                'string',
            ],
            [
                [
                    'title',
                    'price',
                    'subcat',
                ],
                'required',
            ],
            [
                [
                    'sort',
                    'type_option',
                    'cat',
                    'price1',
                    'price2',
                ],
                'integer',
            ],
        ];
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getSubcat()
    {
        return $this->hasOne(CatalogSubcat::tableName(), ['id' => 'subcat']);
    }
    
    public function beforeValidate()
    {
        try {
            if ($this->type_option == 1) {
                $this->price = $this->price1;
            } else {
                $this->price = $this->price2;
            }
        } catch (Exception $e) {
            //
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}