<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use common\components\Table;

/**
 * SettingsEmail model
 *
 * @property integer $id
 * @property string $email
 * @property integer $id_user
 *
 */
class SettingsEmail extends ActiveRecord
{
    
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('email');
    }
    
    public function formName()
    {
        return 'email';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail для уведомлений',
            'id_user' => 'Пользователь',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'id_user',
                ],
                'integer',
            ],
            [
                [
                    'email',
                ],
                'email',
            ],
            [
                [
                    'email',
                ],
                'required',
            ],
            [
                'email',
                'uniqueEmail',
            ]
        ];
    }

    public function uniqueEmail($attribute, $params)
    {
        $post = Yii::$app->request->post();
        if (isset($post['email'])) {
            $arrayEmail = ArrayHelper::getColumn($post['email'], 'email');
            $arrayEmailUnique = [];
            foreach ($arrayEmail as $email) {
                if (in_array($email, $arrayEmailUnique)) {
                    $this->addError($attribute, 'Удалите повторяющие email');
                }
                $arrayEmailUnique[] = $email;
            }
        }
        return true;
    }

    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
}