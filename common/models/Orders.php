<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 08.10.2017
 * Time: 17:59
 */

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\components\Table;
use backend\components\Common;

/**
 * Orders model
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $sum
 * @property string $date_add
 * @property string $date_end
 * @property integer $id_type
 * @property integer $send
 * @property integer $status
 *
 */

class Orders extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    public $time_add;
    
    public function behaviors()
    {
        return [];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('orders');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'sum' => 'Сумма',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id_user',
                    'id_type',
                    'send',
                    'status',
                ],
                'integer',
            ],
            [
                'sum',
                'double',
            ],
            [
                [
                    'date_add',
                    'date_end',
                ],
                'datetime',
                'format' => 'php:Y-m-d H:i:s',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                if ($insert) {
//                    $this->date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                    $this->date_add = Common::getDateTime('', 'Y-m-d H:i:s');
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}