<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\components\Table;

/**
 * CatalogCat model
 *
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $who_create
 * @property string $who_update
 * @property string $title
 * @property string $metatitle
 * @property string $description
 * @property string $keywords
 * @property string $foto
 * @property integer $sort
 *
 */
class CatalogCat extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    public $subcat;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('catalog_cat');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'title' => 'Наименование',
            'metatitle' => 'Заголовок страницы, тег <title>',
            'description' => 'Мета-тег Description',
            'keywords' => 'Мета-тег Keywords',
            'sort' => 'Приоритет',
            'foto' => 'Фотография',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'metatitle',
                    'description',
                    'keywords',
                ],
                'string',
            ],
            [
                [
                    'title',
                ],
                'required',
            ],
            [
                'sort',
                'integer',
            ],
        
        ];
    }
    
    public function getSubcat()
    {
        return $this->hasMany(CatalogSubcatCat::className(), ['id_cat' => 'id']);
    }

    public function getSubcatCount()
    {
        return $this->getSubcat()->count('id_subcat');
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}