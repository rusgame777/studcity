<?php
namespace common\models;

use backend\components\Common;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\components\Table;
use yii\web\IdentityInterface;

/**
 * Review model
 *
 * @property integer $id
 * @property string $avtor
 * @property string $phone
 * @property string $title
 * @property string $message
 * @property integer $subm
 * @property string $date_add
 * @property string $time_add
 */
class Review extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('gb');
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avtor' => 'Автор',
            'phone' => 'Телефон',
            'title' => 'Тема',
            'message' => 'Сообщение',
            'subm' => 'Подтверждение',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'subm',
                'default',
                'value' => self::STATUS_ACTIVE,
            ],
            [
                'subm',
                'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE],
            ],
            [
                [
                    'avtor',
                    'phone',
                    'title',
                    'message',
                ],
                'string',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^8\d{10}$/i',
            ],
            [
                [
                    'avtor',
                    'message',
                ],
                'required',
            ],
            [
                'id',
                'integer',
            ]
            //            [
            //                'password',
            //                'validatePass',
            //            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                if ($insert) {
//                    $this->date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d');
//                    $this->time_add = Yii::$app->formatter->asDate('now', 'php:H:i:s');
                    $this->date_add = Common::getDateTime('', 'Y-m-d');
                    $this->time_add = Common::getDateTime('', 'H:i:s');
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}
