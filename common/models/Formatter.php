<?php

namespace common\models;

use Yii;
use yii\helpers\Html;

class Formatter
{
    const pattern_date = "/^(0?[1-9]|[12][0-9]|3[01])[\/\-\.](0?[1-9]|1[012])[\/\-\.]\d{4}$/i";
    const sizeFileBarriers = 1024 * 1024 * 4;

    public static function getDateForDatabase($value, $timezone = '')
    {
        $timezone = !empty($timezone) ? Yii::$app->params['timezone'] : new \DateTimeZone("Europe/Moscow");
        $date = new \DateTime($value, $timezone);
        return $date->format('Ymd');
    }

    public static function getDateStart()
    {
        $session = Yii::$app->session;
        $date_start =
            isset(Yii::$app->request->get()['date_start']) ? Yii::$app->request->get()['date_start'] : (!is_null($session->get('date_start')) ? $session->get('date_start') : null);
        if ($date_start == null) {
            $timezone = Yii::$app->params['timezone'];
            $date = new \DateTime('', $timezone);
            $d = '01';
            $m = $date->format('m');
            $Y = $date->format('Y');
            $date_start = $d . '.' . $m . '.' . $Y;
        }
        if ((!preg_match(self::pattern_date, $date_start)) && ($date_start != '')) {
            $date_start = '';
        }
        $session->set('date_start', $date_start);
        $date_start = Html::encode($date_start);
        return $date_start;
    }

    public static function getDateEnd()
    {
        $session = Yii::$app->session;
        $date_end =
            isset(Yii::$app->request->get()['date_end']) ? Yii::$app->request->get()['date_end'] : (!is_null($session->get('date_end')) ? $session->get('date_end') : null);
        if ($date_end == null) {
            $timezone = Yii::$app->params['timezone'];
            $date = new \DateTime('', $timezone);
            $date_end = $date->format('d.m.Y');
        }
        if ((!preg_match(self::pattern_date, $date_end)) && ($date_end != '')) {
            $date_end = '';
        }
        $session->set('date_end', $date_end);
        $date_end = Html::encode($date_end);
        return $date_end;
    }

    public static function formatHS($value)
    {
        if ($value != '*') {
            $value = rtrim($value, '0');
            return $value . str_repeat('0', strlen($value) % 2);
        } else {
            return $value;
        }
    }

    public static function getSort($value, $range)
    {
        $cell = "
            <div class='sort_value'>" . Html::encode($value) . "</div>
            <div class='sort_actions'>";
        if ($range[1] != $value) {
            $cell .= "<div class='sort_up'><i class='fa fa-toggle-up' aria-hidden='true'></i></div>";
        }
        if ($range[0] != $value) {
            $cell .= "<div class='sort_down'><i class='fa fa-toggle-down' aria-hidden='true'></i></div>";
        }
        $cell .= "</div>";
        return $cell;
    }
}