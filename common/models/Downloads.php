<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 08.10.2017
 * Time: 17:59
 */

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\components\Table;
use backend\components\Common;

/**
 * Downloads model
 *
 * @property integer $id
 * @property string $hash
 * @property integer $id_order
 * @property string $date_add
 * @property integer $bit_delete
 *
 */
class Downloads extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('downloads');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
//            'id' => 'ID',
//            'id_user' => 'Пользователь',
//            'sum' => 'Сумма',
        ];
    }
    
    public function rules()
    {
        return [
            [
                'bit_delete',
                'default',
                'value' => self::STATUS_INACTIVE,
            ],
            [
                'bit_delete',
                'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE],
            ],
            [
                [
                    'id',
                    'id_order',
                ],
                'integer',
            ],
            [
                'date_add',
                'datetime',
                'format' => 'php:Y-m-d H:i:s',
            ],
            [
                'hash',
                'string',
            ],
            [
                [
                    'hash',
                    'id_order',
                    'bit_delete',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                if ($insert) {
//                    $this->date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                    $this->date_add = Common::getDateTime('', 'Y-m-d H:i:s');
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}