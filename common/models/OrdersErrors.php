<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;
use backend\components\Common;

/**
 * OrdersErrors model
 *
 * @property integer $id
 * @property integer $status
 * @property integer $id_order
 * @property string $date_add
 *
 */
class OrdersErrors extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('orders_errors');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
        
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'status',
                    'id_order',
                ],
                'integer',
            ],
            [
                'date_add',
                'datetime',
                'format' => 'php:Y-m-d H:i:s',
            ],
            [
                [
                    'status',
                    'id_order',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                if ($insert) {
//                    $this->date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                    $this->date_add = Common::getDateTime('', 'Y-m-d H:i:s');
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}