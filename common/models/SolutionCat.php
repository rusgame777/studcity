<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\components\Table;

/**
 * SolutionCat model
 *
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $who_create
 * @property string $who_update
 * @property string $title
 * @property string $metatitle
 * @property string $description
 * @property string $keywords
 * @property integer $sms_author
 * @property integer $sms_user
 * @property string $foto
 * @property integer $sort
 *
 */
class SolutionCat extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    const OPTION_INACTIVE = 0;
    const OPTION_ACTIVE = 1;
    public $subcat;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('solution_cat');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'title' => 'Наименование',
            'metatitle' => 'Заголовок страницы, тег <title>',
            'description' => 'Мета-тег Description',
            'keywords' => 'Мета-тег Keywords',
            'sort' => 'Приоритет',
            'sms_author' => 'SMS уведомление для автора',
            'sms_user' => 'SMS уведомление для клиента',
            'foto' => 'Фотография',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'sms_author',
                    'sms_user',
                ],
                'default',
                'value' => self::OPTION_INACTIVE,
            ],
            [
                [
                    'sms_author',
                    'sms_user',
                ],
                'in',
                'range' => [self::OPTION_INACTIVE, self::OPTION_ACTIVE],
            ],
            [
                [
                    'title',
                    'metatitle',
                    'description',
                    'keywords',
                ],
                'string',
            ],
            [
                [
                    'title',
                ],
                'required',
            ],
            [
                'sort',
                'integer',
            ],
        
        ];
    }
    
    public function getPosition()
    {
        return $this->hasMany(SolutionPosition::className(), ['cat' => 'id']);
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                //
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}