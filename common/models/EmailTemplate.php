<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;

/**
 * EmailTemplate model
 *
 * @property integer $id
 * @property string $title
 * @property string $subject
 * @property string $content
 * @property string $foto1
 * @property string $foto2
 * @property string $foto3
 * @property string $foto4
 * @property string $foto5
 * @property string $attachment1
 * @property string $attachment2
 * @property string $attachment3
 * @property string $attachment4
 * @property string $attachment5
 * @property integer $sort
 *
 */
class EmailTemplate extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('template');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название шаблона',
            'subject' => 'Тема письма',
            'content' => 'Содержание письма',
            'foto1' => 'Картинка №1',
            'foto2' => 'Картинка №2',
            'foto3' => 'Картинка №3',
            'foto4' => 'Картинка №4',
            'foto5' => 'Картинка №5',
            'attachment1' => 'Вложение №1',
            'attachment2' => 'Вложение №2',
            'attachment3' => 'Вложение №3',
            'attachment4' => 'Вложение №4',
            'attachment5' => 'Вложение №5',
            'sort' => 'Приоритет',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'subject',
                    'content',
                ],
                'string',
            ],
            [
                [
                    'title',
                ],
                'required',
            ],
            [
                'sort',
                'integer',
            ],

        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}