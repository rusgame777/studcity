<?php
namespace common\models;

use common\components\Email;
use backend\models\EmailTemplateUpload;
use Yii;
use yii\helpers\Html;
use yii\base\Exception;
use yii\base\Model;
use yii\validators\FileValidator;
use yii\validators\EmailValidator;
use common\models\EmailTemplate;

/**
 * EmailDelivery model
 *
 * @property integer $id
 * @property string $title
 * @property string $subject
 * @property string $content
 * @property integer $sort
 *
 */
class chunkReadFilter implements \PHPExcel_Reader_IReadFilter
{
    private $_startRow = 0;
    private $_endRow = 0;
    /**  Set the list of rows that we want to read  */
    /**
     * @param $startRow
     * @param $chunkSize
     */
    public function setRows($startRow, $chunkSize)
    {
        $this->_startRow = $startRow;
        $this->_endRow = $startRow + $chunkSize;
    }
    
    public function readCell($column, $row, $worksheetName = '')
    {
        //  Only read the heading row, and the rows that are configured in $this->_startRow and $this->_endRow
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}

class EmailDelivery extends Model
{
    public $file, $cat;
    
    public function behaviors()
    {
        return [
        ];
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'Файл с реестром электронных почт',
            'cat' => 'Выберите шаблон',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'file',
                ],
                'file',
                'skipOnEmpty' => false,
                'extensions' => 'xls, xlsx',
                'maxSize' => (new FileValidator)->getSizeLimit(),
            ],
            [
                [
                    'cat',
                ],
                'required',
            ],
            [
                'cat',
                'integer',
            ],
        
        ];
    }
    
    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/template/');
    }
    
    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/template/');
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public static function sendEmailEmbed($template)
    {
        if (null !== $template) {
            
            $filePath = EmailTemplateUpload::filePath();
            $fileUrl = EmailTemplateUpload::fileUrl();
            $message = '';
            
            $imgArray = [];
            for ($i = 1; $i <= 5; $i++) {
                $foto = 'foto' . $i;
                $foto = $template->$foto;
                if (is_file($filePath . $foto)) {
                    $imgArray['imageFileName' . $i] = $fileUrl . $foto;
                }
            }
            
            foreach ($imgArray as $index => $image) {
                list($width, $height) = getimagesize(str_replace($fileUrl, $filePath, $image));
                $message .= implode('', [
                    '<img width="' . $width . '" height="' . $height . '" src="',
                    'http://stud-city.ru/uploads/blank.png',
                    '">',
                ]);
//                $message .= implode('', [
//                    '<img width="' . $width . '" height="' . $height . '" src="',
//                    $sendmessage->embed($index),
//                    '">',
//                ]);
            }
        }
    }
    
    /**
     * @param $template EmailTemplate
     */
    public static function generateContent($template, $uploadFile)
    {
        $finish = 0;
        $coord_y = 1;
        $startRow = 1;
        $chunkSize = 100;
        $email_validator = new EmailValidator();
        
        $filePath = EmailTemplateUpload::filePath();
        $fileUrl = EmailTemplateUpload::fileUrl();
        
        $subject_template = Html::encode($template->subject);
        $content_template = stripslashes($template->content);
        
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = ['memoryCacheSize ' => '256MB'];
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
        
        $objReader = \PHPExcel_IOFactory::createReaderForFile($uploadFile);
        $objReader->setReadDataOnly(true);
        
        $chunkFilter = new chunkReadFilter();
        $objReader->setReadFilter($chunkFilter);
        
        while ($finish == 0) {
            $chunkFilter->setRows($startRow, $chunkSize);
            $objPHPExcel = $objReader->load($uploadFile);
            $objPHPExcel->setActiveSheetIndex(0);
            $aSheet = $objPHPExcel->getActiveSheet();
            
            while ($coord_y < ($startRow + $chunkSize)) {
                $cell = "A" . $coord_y;
                $params = [];
                $params['email'] = trim($aSheet->getCell($cell)->getValue());
                if ($params['email'] == '') {
                    $finish = 1;
                    break;
                }
                if ($email_validator->validate($params['email'])) {
                    
                    $params['subject'] = $subject_template;
                    $message = "
                        <html>
                        <body>";
                    if ($content_template != '') {
                        $message .= "{$content_template}<br>";
                    }
                    for ($i = 1; $i <= 5; $i++) {
                        $nameFoto = 'foto' . $i;
                        if (is_file($filePath . $template->$nameFoto)) {
                            list($width, $height) = getimagesize($filePath . $template->$nameFoto);
                            $message .= vsprintf('<img width="%1$s" height="%2$s" src="%3$s"><br>', [
                                1 => $width,
                                2 => $height,
                                3 => $fileUrl . $template->$nameFoto,
                            ]);
                        }
                    }
                    
                    $message .= "
                        </body>
                        </html>";
                    $params['message'] = $message;
                    $params['from'] = ["noreply@stud-city.ru" => "stud-city.ru"];
                    $params['attachments'] = [];
                    $numberAttachment = 0;
                    for ($i = 1; $i <= 5; $i++) {
                        $attachment = 'attachment' . $i;
                        $attachment = $template->$attachment;
                        if (is_file($filePath . $attachment)) {
                            $numberAttachment++;
                            
                            $fileArray = explode('.', $attachment);
                            $extension = end($fileArray);
                            $fileName = implode('', [
                                'Вложение ',
                                $numberAttachment,
                                '.',
                                $extension,
                            ]);
                            $params['attachments'][$filePath . $attachment] = [
                                'fileName' => $fileName,
                            ];
                        }
                    }
                    Email::send($params);
                }
                
                $coord_y++;
            }
            
            $startRow += $chunkSize;
            unset($aSheet);
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
            if ($finish == 1) {
                break;
            }
        }
        unset($objReader);
        
    }
}