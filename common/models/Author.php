<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\SolutionPosition;

/**
 * Author model
 *
 * @property integer $id
 * @property string $login
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $is_disabled
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $id_role
 * @property string $password
 */
class Author extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;
    public $password;
    public $_author;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_author';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }
    
    public function getAuthor()
    {
        if ($this->_author === null) {
            $this->_author = Author::findByUsername($this->login);
        }
        
        return $this->_author;
    }
    
    public function validatePass($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $author = $this->getAuthor();
            if (!$author || !$author->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильный пароль');
            }
        }
    }
    
    public function uniqueLogin($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $author = $this->findModel();
            $id = $author ? $author->id : 0;
            $countAuthors = Author::find()->where([
                'and',
                ['<>', 'id', $id],
                ['login' => $this->login],
            ])->count();
            $countAdmins = Admin::find()->where([
                'and',
                ['login' => $this->login],
            ])->count();
            $count = $countAuthors + $countAdmins;
            if ($count > 0) {
                $this->addError($attribute, 'Данный логин уже существует');
            }
        }
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Логин',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'password' => 'Пароль',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'is_disabled',
                'default',
                'value' => self::STATUS_ACTIVE,
            ],
            [
                'is_disabled',
                'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED],
            ],
            [
                'id_role',
                'default',
                'value' => 2,
            ],
            [
                [
                    'name',
                    'phone',
                    'email',
                    'password',
                ],
                'string',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^8\d{10}$/i',
            ],
            [
                'email',
                'email',
            ],
            [
                [
                    'login',
                    'name',
                    'phone',
                    'email',
                    'id_role',
                ],
                'required',
            ],
            [
                'id',
                'integer',
            ],
            [
                'password',
                'required',
                'when' => function($model) {
                    return $model->id == false;
                },
            ],
            //            [
            //                'password',
            //                'validatePass',
            //            ],
            [
                'login',
                'uniqueLogin',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'is_disabled' => self::STATUS_ACTIVE]);
    }
    
    /**
     * @inheritdoc
     */
    public function findModel()
    {
        return static::findOne(['id' => $this->id]);
    }
    
    /**
     * @inheritdoc
     */
    public static function findByAccessToken($login, $hash)
    {
        $user = static::findOne(['login' => $login]);
        if ($user == null) {
            return false;
        }
        $generate_hash = md5(implode(';', [
            $login,
            $user->password_hash,
            $user->is_disabled,
        ]));
        if ($generate_hash == $hash) {
            return $user;
        } else {
            return false;
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
    /**
     * Finds user by $login
     *
     * @param string $login
     * @return static|null
     */
    public static function findByUsername($login)
    {
        return static::findOne(['login' => $login, 'is_disabled' => self::STATUS_ACTIVE]);
    }
    
    public static function generateHash($id_user)
    {
        $user = static::findOne(['id' => $id_user]);
        $generate_hash = md5(implode(';', [
            $user->login,
            $user->password_hash,
            $user->is_disabled,
        ]));
        return $generate_hash;
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        
        return static::findOne([
            'password_reset_token' => $token,
            'is_disabled' => self::STATUS_ACTIVE,
        ]);
    }
    
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
//        $pass = Yii::$app->security->generatePasswordHash($password);
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                //
                if (!empty($this->password)) {
                    $this->setPassword($this->password);
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function getPosition()
    {
        return $this->hasMany(SolutionPosition::className(), ['id_author' => 'id']);
    }
}
