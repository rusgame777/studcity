<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use common\models\CatalogPosition;
use common\components\Table;

/**
 * CatalogSubcat model
 *
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $who_create
 * @property string $who_update
 * @property string $title
 * @property string $author
 * @property string $year
 * @property string $metatitle
 * @property string $description
 * @property string $keywords
 * @property string $content
 * @property string $foto
 * @property string $foto_small
 * @property integer $sort
 *
 */
class CatalogSubcat extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('catalog_subcat');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'title' => 'Наименование',
            'author' => 'Автор',
            'year' => 'Год',
            'metatitle' => 'Заголовок страницы, тег <title>',
            'description' => 'Мета-тег Description',
            'keywords' => 'Мета-тег Keywords',
            'content' => 'Содержание страницы',
            'sort' => 'Приоритет',
            'foto' => 'Фотография',
            'foto_small' => 'Миниатюра фотографии',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'author',
                    'year',
                    'metatitle',
                    'description',
                    'keywords',
                    'content',
                ],
                'string',
            ],
            [
                [
                    'title',
                ],
                'required',
            ],
            [
                'sort',
                'integer',
            ],

        ];
    }

    public function getPosition()
    {
        return $this->hasMany(CatalogPosition::className(), ['subcat' => 'id']);
    }    

    public function getCat() {
        return $this->hasOne(CatalogSubcatCat::className(), ['id_subcat' => 'id']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}