<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 31.12.2017
 * Time: 0:02
 */

namespace common\models;

class OrderStatus
{
    const newOrder = 1;
    const paid = 2;
    const send = 3;
    
}