<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 08.10.2017
 * Time: 17:59
 */

namespace common\models;

use backend\components\Common;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\Cookie;
use common\components\Table;

/**
 * Clients model
 *
 * @property integer $id
 * @property string $id_avt
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $date_add
 *
 */
class Clients extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    public $time_add;
    
    public function behaviors()
    {
        return [];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('regusers');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Фамилия, имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'time_add' => 'Время оплаты',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'phone',
                    'id_avt',
                ],
                'string',
            ],
            [
                'email',
                'email',
            ],
            [
                [
                    'id_avt',
                ],
                'required',
            ],
        
        ];
    }
    
    public function getPosition()
    {
        return $this->hasMany(CatalogPosition::className(), ['subcat' => 'id']);
    }
    
    public function getCat()
    {
        return $this->hasOne(CatalogSubcatCat::className(), ['id_subcat' => 'id']);
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                $search_phone = "/(\+7) (\d{3}) (\d{3}) (\d{4})/i";
                $replacement_phone = "8\$2\$3\$4";
                $this->phone = preg_replace($search_phone, $replacement_phone, $this->phone);
                if ($insert) {
//                    $this->date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                    $this->date_add = Common::getDateTime('', 'Y-m-d H:i:s');
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
    
    public static function getClientId()
    {
        $id_avt = Yii::$app->request->cookies->getValue('id_avt');
        $client = !empty($id_avt) ? Clients::find()->where(['id_avt' => $id_avt])->asArray()->one() : [];
        $id_client = !empty($client) ? $client['id'] : 0;
        Yii::$app->params['id_client'] = $id_client;
    }
    
    public static function create()
    {
        $id_client = Yii::$app->params['id_client'];
        if ($id_client == 0) {
            $id_avt = md5(uniqid(rand(), 1));
            $params = [
                'id_avt' => $id_avt,
            ];
            $client = new Clients();
            if ($client->load($params) && $client->save()) {
                $id_client = $client->getPrimaryKey();
            }
            
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'id_avt',
                'value' => $id_avt,
                'expire' => time() + 3600 * 24 // 1 day,
            ]));
        }
        return $id_client;
    }
}