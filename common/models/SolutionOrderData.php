<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;

/**
 * SolutionOrderData model
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $id_order
 *
 */
class SolutionOrderData extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    public function behaviors()
    {
        return [
        
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('orderdata_solution');
    }
    
    public function formName()
    {
        return 'data';
    }
    
    public function attributeLabels()
    {
        return [
            'value' => 'Данные',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'id_order',
                ],
                'integer',
            ],
            [
                [
                    'name',
                    'value',
                ],
                'string',
            ],
            [
                [
                    'name',
                    'value',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
}