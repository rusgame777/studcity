<?php
namespace common\models;

use backend\components\Common;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;

/**
 * ReviewReply model
 *
 * @property integer $id
 * @property string $message
 * @property string $date_add
 * @property string $time_add
 * @property integer $comment
 *
 */
class ReviewReply extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('reply');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Сообщение',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'message',
                ],
                'string',
            ],
            [
                [
                    'message',
                ],
                'required',
            ],
            [
                [
                    'comment',
                ],
                'integer',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        
        if (parent::beforeSave($insert)) {
            if ($insert) {
//                $this->date_add = Yii::$app->formatter->asDate('now', 'php:Y-m-d');
//                $this->time_add = Yii::$app->formatter->asDate('now', 'php:H:i:s');
                $this->date_add = Common::getDateTime('', 'Y-m-d');
                $this->time_add = Common::getDateTime('', 'H:i:s');
            }
            return true;
        } else {
            return false;
        }
        
    }
}