<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\models\PageCategory;
use common\components\Table;

/**
 * Page model
 *
 * @property integer $id
 * @property string $date_create
 * @property string $date_update
 * @property string $who_create
 * @property string $who_update
 * @property string $title
 * @property string $nameurl
 * @property string $content
 * @property integer $id_category
 * @property string $metatitle
 * @property string $description
 * @property string $keywords
 * @property integer $sort
 * @property integer $indic
 * @property integer $show_content
 *
 */
class Page extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('page_content');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
            'title' => 'Наименование',
            'nameurl' => 'URL-адрес страницы',
            'content' => 'Содержание страницы',
            'id_category' => 'Раздел',
            'metatitle' => 'Заголовок страницы, тег <title>',
            'description' => 'Мета-тег Description',
            'keywords' => 'Мета-тег Keywords',
            'sort' => 'Приоритет',
            'indic' => 'Нельзя удалить?',
            'show_content' => 'Показать контент?',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'indic',
                    'show_content',
                ],
                'in',
                'range' => [
                    self::STATUS_ACTIVE,
                    self::STATUS_INACTIVE,
                ],
            ],
            [
                [
                    'title',
                    'nameurl',
                    'content',
                    'metatitle',
                    'description',
                    'keywords',
                ],
                'string',
            ],
            [
                [
                    'nameurl',
                    'title',
                ],
                'required',
            ],
            [
                'nameurl',
                'match',
                'pattern' => '/^[A-Z-a-z-0-9\.\/\-\_\#]+$/i',
            ],
            [
                'nameurl',
                'unique',
                'filter' => ['<>', 'id', $this->id],
            ],
            [
                'indic',
                'default',
                'value' => 0,
            ],
            [
                'show_content',
                'default',
                'value' => 1,
            ],
            [
                ['sort', 'id_category'],
                'integer',
            ],
        
        ];
    }
    
    public function getCategories()
    {
        return $this->hasOne(PageCategory::className(), ['id' => 'id_category']);
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function beforeValidate()
    {
        try {
            $this->nameurl = trim($this->nameurl, '/');
            if (!empty($this->nameurl)) {
                $this->nameurl = '/' . $this->nameurl . '/';
            } else {
                $this->nameurl = '/';
            }
        } catch (Exception $e) {
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}