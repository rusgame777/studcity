<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 23.01.2018
 * Time: 0:41
 */

namespace common\models;

class OrderType
{
    const catalog = 1;
    const solution = 2;
}