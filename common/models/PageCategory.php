<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;

/**
 * PageCategory model
 *
 * @property integer $id
 * @property string $title
 *
 */
class PageCategory extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('page_categories');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID категории',
            'title' => 'Наименование категории',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'title',
                ],
                'safe',
            ],
        ];
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
}