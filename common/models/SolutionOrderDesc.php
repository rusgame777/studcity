<?php
/**
 * Created by PhpStorm.
 * User: Rusgame777
 * Date: 08.10.2017
 * Time: 17:59
 */

namespace common\models;

use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\components\Table;

/**
 * SolutionOrderDesc model
 *
 * @property integer $id
 * @property integer $id_position
 * @property integer $cat
 * @property integer $id_order
 *
 */
class SolutionOrderDesc extends ActiveRecord
{
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 0;

    public function behaviors()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('orderdesc_solution');
    }

    public function formName()
    {
        return '';
    }

    public function attributeLabels()
    {
        return [
            'id_position' => 'Методичка',
            'cat' => 'Дисциплина',
        ];
    }

    public function rules()
    {
        return [
            [
                [
                    'id',
                    'id_position',
                    'cat',
                    'id_order',
                ],
                'integer',
            ],

        ];
    }

    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                //
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}