<?php
namespace common\models;

use backend\components\Common;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\components\Table;
use yii\web\IdentityInterface;

/**
 * SmsAuthor model
 *
 * @property integer $id_sms
 * @property integer $status
 * @property string $message
 * @property integer $id_author
 * @property integer $id_order
 * @property string $date_send
 * @property string $phone
 * @property string $num
 */
class SmsAuthor extends ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
    }
    
    public function formName()
    {
        return '';
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }
    
    public function attributeLabels()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'message',
                    'phone',
                    'num',
                ],
                'string',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^7\d{10}$/i',
            ],
            [
                [
                    'status',
                    'message',
                    'id_author',
                    'id_order',
                    'phone',
                    'num',
                ],
                'required',
            ],
            [
                [
                    'id_sms',
                    'status',
                    'id_author',
                    'id_order',
                ],
                'integer',
            ],
            [
                'date_send',
                'datetime',
                'format' => 'php:Y-m-d H:i:s',
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                if ($insert) {
//                    $this->date_send = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
                    $this->date_send = Common::getDateTime('', 'Y-m-d H:i:s');
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}
