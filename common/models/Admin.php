<?php
namespace common\models;

use Yii;
use common\components\Table;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Admin model
 *
 * @property integer $id
 * @property string $login
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $name
 * @property string $email
 * @property string $auth_key
 * @property string $date_entry
 * @property string $date_last_entry
 * @property integer $updated_at
 * @property integer $is_disabled
 * @property integer $id_role
 * @property string $password
 */
class Admin extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 1;
    const STATUS_ACTIVE = 0;
    public $password;
    public $_admin;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('admin');
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'is_disabled',
                'default',
                'value' => self::STATUS_ACTIVE,
            ],
            [
                'is_disabled',
                'in',
                'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED],
            ],
            [
                'id_role',
                'default',
                'value' => 1,
            ],
            [
                [
                    'name',
                    'email',
                    'password',
                ],
                'string',
            ],
            [
                'email',
                'email',
            ],
            [
                [
                    'login',
                    'name',
                    'email',
                    'id_role',
                ],
                'required',
            ],
            [
                'id',
                'integer',
            ],
            [
                'password',
                'required',
                'when' => function($model) {
                    return $model->id == false;
                },
            ],
            [
                'login',
                'uniqueLogin',
            ],
        ];
    }

    public function getAdmin()
    {
        if ($this->_admin === null) {
            $this->_admin = Admin::findByUsername($this->login);
        }

        return $this->_admin;
    }

    public function uniqueLogin($attribute)
    {
        if (!$this->hasErrors()) {
            $author = $this->findModel();
            $id = $author ? $author->id : 0;
            $countAuthors = Author::find()->where([
                'and',
                ['login' => $this->login],
            ])->count();
            $countAdmins = Admin::find()->where([
                'and',
                ['<>', 'id', $id],
                ['login' => $this->login],
            ])->count();
            $count = $countAuthors + $countAdmins;
            if ($count > 0) {
                $this->addError($attribute, 'Данный логин уже существует');
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Логин',
            'email' => 'E-mail',
            'password' => 'Пароль',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'is_disabled' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function findModel()
    {
        return static::findOne(['id' => $this->id]);
    }
    
    /**
     * @inheritdoc
     */
    public static function findByAccessToken($login, $hash)
    {
        $user = static::findOne(['login' => $login]);
        if ($user == null) {
            return false;
        }
        $generate_hash = md5(implode(';', [
            $login,
            $user->password_hash,
            $user->is_disabled,
        ]));
        if ($generate_hash == $hash) {
            return $user;
        } else {
            return false;
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
    /**
     * Finds user by $login
     *
     * @param string $login
     * @return static|null
     */
    public static function findByUsername($login)
    {
        return static::findOne(['login' => $login, 'is_disabled' => self::STATUS_ACTIVE]);
    }
    
    public static function generateHash($id_user)
    {
        $user = static::findOne(['id' => $id_user]);
        $generate_hash = md5(implode(';', [
            $user->login,
            $user->password_hash,
            $user->is_disabled,
        ]));
        return $generate_hash;
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        
        return static::findOne([
            'password_reset_token' => $token,
            'is_disabled' => self::STATUS_ACTIVE,
        ]);
    }
    
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
//        $pass = Yii::$app->security->generatePasswordHash($password);
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                //
                if (!empty($this->password)) {
                    $this->setPassword($this->password);
                }
            } catch (\Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $controller \yii\web\Controller
     * @return bool
     */
    static public function checkAuth($controller)
    {
        $login = Yii::$app->request->cookies->getValue('admin_login');
        $hash = Yii::$app->request->cookies->getValue('admin_hash');
        $admin = null;
        if (!empty($login) && !empty($hash)) {
            $admin = static::findByAccessToken($login, $hash);
            if (!empty($admin)) {
                Yii::$app->view->params['id_user'] = $admin->id;
                Yii::$app->view->params['id_role'] = $admin->id_role;
                Yii::$app->view->params['login'] = $admin->login;
                return true;
            }
        }
        
        $author = null;
        $login = Yii::$app->request->cookies->getValue('author_login');
        $hash = Yii::$app->request->cookies->getValue('author_hash');
        if (!empty($login) && !empty($hash)) {
            $author = Author::findByAccessToken($login, $hash);
            if (!empty($author)) {
                Yii::$app->view->params['id_user'] = $author->id;
                Yii::$app->view->params['id_role'] = $author->id_role;
                Yii::$app->view->params['login'] = $author->login;
                return true;
            }
        }
        
        if (empty($user) && empty($author)) {
            $controller->redirect('/site/login/')->send();
            return false;
        }
        return true;
    }
}
