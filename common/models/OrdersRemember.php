<?php
namespace common\models;

use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\components\Table;

/**
 * OrdersRemember model
 *
 * @property integer $id
 * @property integer $id_order
 * @property string $date_next_send
 *
 */
class OrdersRemember extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Table::get('orders_remember');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
        
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'id_order',
                ],
                'integer',
            ],
            [
                'date_next_send',
                'datetime',
                'format' => 'php:Y-m-d H:i:s',
            ],
            [
                [
                    'id_order',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            //
        } catch (Exception $e) {
            //
        }
        return true;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            try {
                if ($insert) {
                    //
                }
            } catch (Exception $e) {
                //
            }
            return true;
        } else {
            return false;
        }
    }
}