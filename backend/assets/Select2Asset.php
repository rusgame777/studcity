<?php

namespace backend\assets;

use yii\web\AssetBundle;

class Select2Asset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $js = [
        'select2/dist/js/select2.min.js',
    ];
	public $jsOptions = ['position' => \yii\web\View::POS_BEGIN];
    public $css = [
        'select2/dist/css/select2.min.css',
    ];
	public $cssOptions = ['position' => \yii\web\View::POS_BEGIN];
}
