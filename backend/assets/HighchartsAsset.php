<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 22.12.2017
 * Time: 2:05
 */

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class HighchartsAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $js = [
        'highcharts/highcharts.js',
        'highcharts/modules/offline-exporting.js',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $css = [];
    public $cssOptions = ['position' => View::POS_HEAD];
}
