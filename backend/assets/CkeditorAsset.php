<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CkeditorAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $js = [
        'ckeditor/ckeditor.js',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $css = [
    ];
    public $cssOptions = ['position' => View::POS_HEAD];
}
