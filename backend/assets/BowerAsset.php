<?php

namespace backend\assets;

use yii\web\AssetBundle;

class BowerAsset extends AssetBundle
{
    public $sourcePath = '@bower';
    public $js = [
        'html5-history-api/history.min.js',
        'js-cookie/src/js.cookie.js',
        'jquery-ui/jquery-ui.min.js',
        'jquery-ui/ui/i18n/datepicker-ru.js',
        'jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js',
        'jquery.scrollTo/jquery.scrollTo.min.js',
        'chosen/chosen.jquery.js',
        'clockpicker/dist/bootstrap-clockpicker.min.js',
        'bootstrap/dist/js/bootstrap.min.js',
        'moment/moment.js',
        'moment/locale/ru.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
        'jquery-ui/themes/base/jquery-ui.min.css',
        'chosen/chosen.css',
        'clockpicker/dist/bootstrap-clockpicker.min.css',
    ];
    public $cssOptions = ['position' => \yii\web\View::POS_HEAD];
}
