<?php

namespace backend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class NpmAsset extends AssetBundle
{
    public $sourcePath = '@npm';
    public $js = [
        '@fancyapps/fancybox/dist/jquery.fancybox.min.js',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $css = [
        '@fancyapps/fancybox/dist/jquery.fancybox.min.css',
    ];
    public $cssOptions = ['position' => View::POS_HEAD];
}