<?php

namespace backend\assets;

use yii\web\AssetBundle;

class LibAsset extends AssetBundle
{
    public $css = [
    ];
	public $cssOptions = ['position' => \yii\web\View::POS_HEAD];
    public $js = [
        'lib/md5.js',
        'js/functions.js',
    ];
	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
