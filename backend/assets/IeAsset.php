<?php

namespace backend\assets;

use yii\web\AssetBundle;

class IeAsset extends AssetBundle
{
    public $css = [
    ];
    
    public $js = [
        'lib/ie/html5.js',
        'lib/ie/respond.js',
        'lib/ie/selectivizr.js',
    ];
	public $jsOptions = ['condition' => 'lte IE9', 'position' => \yii\web\View::POS_HEAD];
}
