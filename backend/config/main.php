<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'assetManager' => [
            'linkAssets' => false,
            'appendTimestamp' => true,
        ],
        'user' => [
            'identityClass' => 'common\models\Admin',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'trace', 'error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<id>' => 'site/<id>',
                'catalog-position/position-file/<id:[\d]+>' => 'catalog-position/position-file',
                'catalog-position/file/<id:[\d]+>' => 'catalog-position/file',
                'catalog-position/foto/<id:[\d]+>' => 'catalog-position/foto',
                'solution-orders/doc/<id:[\d]+>' => 'solution-orders/doc',
                'solution-orders/file/<id:[\d]+>' => 'solution-orders/file',
                'barrier/doc/<action:[\d]*>/' => 'barrier/doc',
                'barrier/filedoc/<action:[\w]*>/' => 'barrier/filedoc',
                '<dir1:[\w\-\_\d]+>' => 'site/login',
                //'<controller:(users)>/<id>' => 'users/<controller>/<id>',
            ],
            'suffix' => '/',
        ],
        'formatter' => [
            'sizeFormatBase' => 1024,
        ],
    ],
    'defaultRoute' => 'site/login',
    'language' => 'ru',
    'params' => $params,
];