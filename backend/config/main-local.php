<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'v94Z57P42XHyNHAFMicq0RSA9BxWF_tY',
        ],
    ],
];

$ip = '';
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

$debug_show = in_array($ip, ['127.0.0.1', '79.98.142.2', '178.140.62.221']);

if ((YII_ENV_DEV) || ($debug_show)) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        //        'allowedIPs' => ['127.0.0.1', '192.168.0.155', '79.98.142.2', '176.97.0.147'],
    ];
    
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        //        'allowedIPs' => ['127.0.0.1', '192.168.0.155', '79.98.142.2', '176.97.0.147'],
    ];
}

return $config;