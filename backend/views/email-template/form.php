<?php

use yii\helpers\Url;
use common\models\EmailTemplate;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\EmailTemplate;
 *
 */

$request = Yii::$app->request;
$edit = Yii::$app->request->get('edit');
$get = Yii::$app->request->get();
$id = isset($get['id']) ? intval($get['id']) : 0;

$url_validate = Url::to(['email-template/validate']);
$url_show = Url::to(['email-template/show']);

$url_return = 'Вернуться к списку';
if ($edit == null) {
    $model->loadDefaultValues();
    $url_action = Url::to(['email-template/create']);
    $submit = "Добавить";
    $actions = 'Добавить новый шаблон';
} else {
    $url_action = Url::to(['email-template/update']);
    $submit = "Внести правки";
    $actions = "Изменить шаблон";
}

$result = [
    'model' => $model,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
    'id' => $id,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_show ?>' class='btn btn-primary section_button'><?= $url_return ?></a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]);