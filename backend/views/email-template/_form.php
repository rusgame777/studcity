<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\EmailTemplate
 *
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\validators\FileValidator;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\Select2Asset;
use backend\assets\CkeditorAsset;
use backend\models\EmailTemplateUpload;
use backend\components\Common;
use backend\components\Notice;
use common\models\EmailTemplate;
use kartik\select2\Select2;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
Select2Asset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$fileUrl = EmailTemplateUpload::fileUrl();
$filePath = EmailTemplateUpload::filePath();

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];

$modelUpload = new EmailTemplateUpload();
?>
    <div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            //         'enableClientValidation' => true,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <?php Notice::init(); ?>

                <?= $form->field($model, 'title')->textInput(); ?>
                <?= $form->field($model, 'subject')->textInput(); ?>
                <div class="section_title"><?= $model->getAttributeLabel('content') ?></div>
                <div class="section_input">
                <?= $form->field($model, 'content', [
                    'options' => [
                        'class' => 'form-group form-wysiwyg',
                    ],
                ])->textarea([])->label(false); ?>
                </div>
                <script>
                    $(function () {
                        var ckeditor = CKEDITOR.replace('content', {
                            customConfig: '/lib/ckeditor/config.js'
                        });
                        AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor});
                    });
                </script>

                <div class='section_title'>
                    <h3>Картинки</h3>
                    <span>в формате jpg, png или gif,
                    размер не более
                        <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>
                        </span>
                </div>
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    $foto = 'foto' . $i;
                    ?>
                    <div class='section_title'><? echo $model->getAttributeLabel($foto); ?></div>
                    <?php
                    if (is_file($filePath . $model->$foto)) {
                        ?>
                        <div class="section_doc">
                        <div class="icon">
                            <? echo Html::a(Html::img($fileUrl . $model->$foto, ['width' => 100]), Url::to($fileUrl . $model->$foto), [
                                'data' => ['fancybox' => 'gallery'],
                                'target' => '_blank',
                            ]); ?>
                        </div>
                        <div class="title" title="<?= $model->$foto ?>">
                            <?
                            echo implode('', [
                                Common::formatLongName($model->$foto),
                                '<br><span>',
                                Common::formatFileSize(filesize($filePath . $model->$foto)),
                                '</span>',
                            ]);
                            ?>
                        </div>
                        <div class="delete">
                            <?php
                            echo $form->field($modelUpload, '[' . $i . ']delete_foto', [
                                'options' => ['class' => 'form-group form-checkbox',],
                            ])->checkbox(['class' => 'span-1']); ?>
                        </div>
                    </div>
                        <?php
                    }
                    ?>


                    <div class="section_checkbox">
                    <?php
                    echo $form->field($modelUpload, '[' . $i . ']foto', [
                        'options' => ['class' => 'form-group form-file',],
                    ])->fileInput(['accept' => '.jpg, .png, .gif'])->label(false); ?>
                    </div>
                    <?
                }
                ?>

                <div class='section_title'>
                    <h3>Вложения</h3>
                    <span>в формате pdf, jpg, png или gif,
                    размер не более
                        <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>
                        </span>
                </div>
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    $attachment = 'attachment' . $i;
                    ?>
                    <div class='section_title'><? echo $model->getAttributeLabel($attachment); ?></div>
                    <?php
                    if (is_file($filePath . $model->$attachment)) {
                        ?>
                        <div class="section_doc">
                        <div class="icon">
                            <? echo Html::a('<i class="fa fa-file" aria-hidden="true"></i>',
                                Url::to($fileUrl . $model->$attachment), [
                                    'target' => '_blank'
                                ]); ?>
                        </div>
                        <div class="title" title="<?= $model->$attachment ?>">
                            <?
                            echo implode('', [
                                Common::formatLongName($model->$attachment),
                                '<br><span>',
                                Common::formatFileSize(filesize($filePath . $model->$attachment)),
                                '</span>',
                            ]);
                            ?>
                        </div>
                        <div class="delete">
                            <?php
                            echo $form->field($modelUpload, '[' . $i . ']delete_attachment', [
                                'options' => ['class' => 'form-group form-checkbox',],
                            ])->checkbox(['class' => 'span-1']); ?>
                        </div>
                    </div>
                        <?php
                    }
                    ?>


                    <div class="section_checkbox">
                    <?php
                    echo $form->field($modelUpload, '[' . $i . ']attachment', [
                        'options' => ['class' => 'form-group form-file',],
                    ])->fileInput(['accept' => '.jpg, .png, .gif'])->label(false); ?>
                    </div>
                    <?
                }
                ?>

            </div>

            <div class='section_stripe2'></div>
            <?php
            echo Html::submitButton($submit, [
                'id' => 'submiting',
                'class' => 'btn btn-primary section_submit',
            ]);
            ?>
        </article>
        <?php ActiveForm::end(); ?>
    </div>
<?
$this->registerJsFile('/js/template_add.js', ['position' => yii\web\View::POS_END]);