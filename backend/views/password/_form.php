<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model backend\models\AdminPassword
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\CkeditorAsset;
use backend\components\Notice;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$fieldConfig = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];

?>
<div class="post-form">
    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'action' => [
            $url_action,
            'id' => $model->id,
        ],
        'fieldConfig' => $fieldConfig,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validationUrl' => $url_validate,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <article>
        <div class='section_stripe'></div>
        <div class='section_info'>
            <?php Notice::init(); ?>

            <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'login')->hiddenInput()->label(false); ?>
            <?= $form->field($model, 'password_old')->passwordInput(); ?>
            <?= $form->field($model, 'password_new')->passwordInput(); ?>
            <?= $form->field($model, 'password_repeat')->passwordInput(); ?>

        </div>

        <div class='section_stripe2'></div>
        <?php
        echo Html::submitButton($submit, [
            'id' => 'submiting',
            'class' => 'btn btn-primary section_submit',
        ]);
        ?>
    </article>
    <?php ActiveForm::end(); ?>
</div>