<?php

use yii\helpers\Url;

/**
 *
 * @var $this yii\web\View
 * @var $model backend\models\AdminPassword;
 *
 */

$url_validate = Url::to(['password/validate']);
$url_action = Url::to(['password/update']);

$url_return = Url::to(['settings/form']);
$name_return = 'Настройки';

$actions = "Пароль администратора";
$submit = "Изменить пароль";

$result = [
    'model' => $model,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_return ?>' class='btn btn-primary section_button'><?= $name_return ?></a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]) ?>