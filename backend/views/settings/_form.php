<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\Settings
 * @var $modelCat common\models\SettingsEmail
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\CkeditorAsset;
use backend\components\Notice;
use common\models\SettingsEmail;
use wbraganca\dynamicform\DynamicFormWidget;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$fieldConfig = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];

if ($model->id != 0) {
    $modelItems = SettingsEmail::find()->where(['id_user' => Yii::$app->view->params['id_user']])->orderBy('id ASC')->all();
}
if (empty($modelItems)) {
    $modelItems = [new SettingsEmail()];
}

?>
<div class="post-form">
    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'action' => [
            $url_action,
            'id' => $model->id,
        ],
        'fieldConfig' => $fieldConfig,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validationUrl' => $url_validate,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <article>
        <div class='section_stripe'></div>
        <div class='section_info'>
            <?php Notice::init(); ?>

            <? DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper',
                'widgetBody' => '#goods_value_list',
                'widgetItem' => '.goods_value_list',
                'limit' => 1000,
                'min' => 1,
                'insertButton' => '.goods_add',
                'deleteButton' => '.goods_delete',
                'model' => new SettingsEmail(),
                'formId' => 'dynamic-form',
                'formFields' => [
                    'id',
                    'email',
                ],
            ]); ?>
            <div class='section_title' style="margin-top: 0;"><h3>Email для уведомления</h3></div>
            <div id="goods_value_list">
                <?php
                if (!empty($modelItems)) {
                    foreach ($modelItems as $indexItem => $modelItem) {
                        ?>
                        <div class="goods_value_list">
                            <div class='goods_values'>
                                <?= Html::activeHiddenInput($modelItem, "[{$indexItem}]id"); ?>
                                <div class="goods_value">
                                    <?= $form->field($modelItem, '[' . $indexItem . ']email')->textInput([
                                        'placeholder' => $modelItem->getAttributeLabel('email'),
                                        'title' => $modelItem->getAttributeLabel('email'),
                                    ])->label(false); ?>
                                </div>
                            </div>
                            <div class="goods_actions">
                                <div class='goods_delete'><i class='fa fa-minus-square' aria-hidden='true'></i>
                                </div>
                                <div class='goods_add'><i class='fa fa-plus-square' aria-hidden='true'></i>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <? DynamicFormWidget::end(); ?>

            <?#= $form->field($model, 'mail1')->textInput(); ?>
            <?#= $form->field($model, 'mail2')->textInput(); ?>
            <?#= $form->field($model, 'mail3')->textInput(); ?>
            <?#= $form->field($model, 'mail4')->textInput(); ?>

            <div class="section_title"><?= $model->getAttributeLabel('content') ?></div>
            <div class="section_input">
            <?= $form->field($model, 'notes', [
                'options' => [
                    'class' => 'form-group form-wysiwyg',
                ],
            ])->textarea([])->label(false); ?>
            </div>
            <script>
                $(function () {
                    var ckeditor = CKEDITOR.replace('notes', {
                        customConfig: '/lib/ckeditor/config.js'
                    });
                    AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor});
                });
            </script>

            <?= $form   ->field($model, 'url_vk')->textarea([
                'rows' => '4',
            ]); ?>

            <div class="section_title"><?= $model->getAttributeLabel('order1') ?></div>
            <div class="section_input">
            <?= $form->field($model, 'order1', [
                'options' => [
                    'class' => 'form-group form-wysiwyg',
                ],
            ])->textarea([])->label(false); ?>
            </div>
            <script>
                $(function () {
                    var ckeditor = CKEDITOR.replace('order1', {
                        customConfig: '/lib/ckeditor/config.js'
                    });
                    AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor});
                });
            </script>

            <div class="section_title"><?= $model->getAttributeLabel('order2') ?></div>
            <div class="section_input">
            <?= $form->field($model, 'order2', [
                'options' => [
                    'class' => 'form-group form-wysiwyg',
                ],
            ])->textarea([])->label(false); ?>
            </div>
            <script>
                $(function () {
                    var ckeditor = CKEDITOR.replace('order2', {
                        customConfig: '/lib/ckeditor/config.js'
                    });
                    AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor});
                });
            </script>

            <?= $form->field($model, 'code_map')->textarea([
                'rows' => '4',
            ]); ?>
            <?= $form->field($model, 'code_head')->textarea([
                'rows' => '4',
            ]); ?>
            <?= $form->field($model, 'code_body')->textarea([
                'rows' => '4',
            ]); ?>

        </div>

        <div class='section_stripe2'></div>
        <?php
        echo Html::submitButton($submit, [
            'id' => 'submiting',
            'class' => 'btn btn-primary section_submit',
        ]);
        ?>
    </article>
    <?php ActiveForm::end(); ?>
</div>