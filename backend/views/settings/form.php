<?php

use yii\helpers\Url;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\Settings;
 *
 */

$url_validate = Url::to(['settings/validate']);
$url_action = Url::to(['settings/update']);

$url_return = Url::to(['password/form']);
$name_return = 'Пароль администратора';

$actions = "Настройки";
$submit = "Внести изменения";

$result = [
    'model' => $model,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_return ?>' class='btn btn-primary section_button'><?= $name_return ?></a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]) ?>