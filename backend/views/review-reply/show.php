<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\ReviewReplySearch
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use yii\widgets\Breadcrumbs;
use common\models\Review;
use backend\components\Common;
use backend\components\Notice;

$cat = Yii::$app->request->get('cat');
$url_add = Url::to(array_filter([
    'review-reply/form',
    'cat' => !empty($cat) ? $cat : null,
]));
$url_catalog = Url::to(['review/show']);

$this->params['breadcrumbs'] = [];
if (!empty($cat)) {
    $catArr = Review::findOne($cat);
    if (!empty($catArr)) {
        $this->params['breadcrumbs'][] = [
            'label' => 'Ответы на отзыв автора "' . $catArr->avtor . '"',
            'url' => Url::current(),
        ];
    }
}

?>
    <header>
        <div id='section_header'>
            <div id='section_title'>
                   <?
                   echo Breadcrumbs::widget([
                       'homeLink' => [
                           'label' => Yii::t('yii', 'Отзывы'),
                           'url' => $url_catalog,
                       ],
                       'links' => $this->params['breadcrumbs'],
                   ]);
                   ?>
                
            </div>
            <div id='section_url'></div>
        </div>
    </header>
    <article>
        <?= Html::a('<i class="fa fa-plus-circle" aria-hidden="true"></i>', $url_add, [
            'class' => 'add-user',
            'title' => 'Добавить ответ на отзыв',
        ]) ?>
        
        <?= $this->render('_search', ['model' => $searchModel]) ?>
        <?php Notice::init(); ?>
        <div class='kartiktable'>
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => implode(',', [
                    "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                    "input[name='" . $dataProvider->getPagination()->pageParam . "']",
                ]),
                'pager' => [
                    'class' => LinkPager::className(),
                    'template' => '{pageButtons} {customPage} {pageSize}',
                    'pageSizeList' => [10, 20, 50, 100],
                    'customPageWidth' => 50,
                    'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                    'customPageAfter' => "&nbsp;Показывать по&nbsp;",
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'notes',
                    ],
                    'afterGrid' => implode("\r\n", [
                        "<div id='section_actions'>",
                        "<div id='section_delete_all'>",
                        "Удалить выделенные элементы?",
                        "</div>",
                        "</div>",
                    ]),
                ],
                'striped' => false,
                'hover' => true,
                'resizableColumns' => false,
                'rowOptions' => function($model, $key, $index, $grid) {
                    $class = $index % 2 ? '' : 'odd_tr';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class,
                        'id' => $model->id,
                    ];
                },
                'tableOptions' => [
                    'class' => 'kartik_table',
                    'id' => 'table_barrier_block',
                    'style' => 'width: 100%;',
                ],
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'position' => 'fixed',
                ],
                'layout' => "{items}\n{pager}\n{summary}",
                'panel' => [
                    'type' => 'primary',
                ],
                'toolbar' => [
                    '{toggleData}',
                ],
                'toggleDataOptions' => ['minCount' => 0],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'filterInputOptions' => ['class' => 'form-control form-id'],
                        'contentOptions' => [
                            'class' => 'first_td right_td',
                            'width' => 30,
                        ],
                        'headerOptions' => ['width' => 30],
                    ],
                    [
                        'attribute' => 'date_publication',
                        'contentOptions' => [
                            'class' => 'middle_td',
                            'width' => 150,
                        ],
                        'headerOptions' => ['width' => 150],
                        'format' => 'raw',
                        'value' => function($model) {
//                            return Yii::$app->formatter->format(implode(' ', [
//                                $model->date_add,
//                                $model->time_add,
//                            ]), ['date', 'php:d.m.Y<\b\r>H:i:s']);
                            return Common::getDateTime(implode(' ', [
                                $model->date_add,
                                $model->time_add,
                            ]), 'd.m.Y<\b\r>H:i:s');
                        },
                    ],
                    [
                        'attribute' => 'message',
                        'format' => 'ntext',
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'header' => 'Действия',
                        'width' => '100px',
                        'headerOptions' => [
                            'class' => 'last_th',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td middle_td',
                        ],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-pencil-square-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'review-reply/form',
                                    'id' => $model->id,
                                    'edit' => 'ok',
                                ]), [
                                    'class' => 'icon_td',
                                    'data-pjax' => 0,
                                    'title' => 'Редактирование ответа на отзыв',
                                ]);
                            },
                            'delete' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-trash user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'review-reply/remove',
                                    'id' => $model->id,
                                ]), [
                                    'class' => 'icon_td',
                                    'data-pjax' => 0,
                                    'title' => 'Удаление ответа на отзыв',
                                    'onclick' => "return confirm('Вы действительно хотите удалить этот ответ на отзыв?');",
                                ]);
                            },
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => "40px",
                        'headerOptions' => [
                            'class' => 'delete-all',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td delete',
                        ],
                    ],
                ],
            ]);
            ?>
        </div>

    </article>
<?php
$this->registerJsFile('/js/review_reply_show.js', ['position' => yii\web\View::POS_END]);