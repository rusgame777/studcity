<?php

use yii\helpers\Url;
use common\models\Review;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\ReviewReply;
 *
 */

$request = Yii::$app->request;
$edit = $request->get('edit');
$get = $request->get();
$id = isset($get['id']) ? intval($get['id']) : 0;
$cat = Yii::$app->request->get('cat');

if (empty($cat) && !empty($edit)) {
    $cat = $model->comment;
}

$url_module = '/' . $request->getPathInfo();
$url_validate = str_replace('form', 'validate', $url_module);

if (!empty($cat)) {
    $url_show = Url::to(['review-reply/show', 'cat' => $cat]);
} else {
    $url_show = Url::to(['review/show']);
}

$url_return = 'Вернуться к списку';
if ($edit == null) {
    $model->loadDefaultValues();
    $model->comment = $cat;
    $url_action = str_replace('form', 'create', $url_module);
    $submit = "Добавить";
    $actions = 'Добавить новый ответ на отзыв';
} else {
    $url_action = str_replace('form', 'update', $url_module);
    $submit = "Внести правки";
    $actions = "Изменить ответ на отзыв";
}

$result = [
    'model' => $model,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
    'id' => $id,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_show ?>' class='btn btn-primary section_button'><?= $url_return ?></a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]) ?>