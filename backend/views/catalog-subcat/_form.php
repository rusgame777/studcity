<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\CatalogSubcat
 * @var $modelSubcatCat common\models\CatalogSubcatCat
 *
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\validators\FileValidator;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\Select2Asset;
use backend\assets\CkeditorAsset;
use backend\models\CatalogSubcatUpload;
use backend\components\Common;
use backend\components\Notice;
use common\models\CatalogCat;
use common\models\University;
use kartik\select2\Select2;
use common\models\UserCatalog;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
Select2Asset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$fileUrl = CatalogSubcatUpload::fileUrl();
$filePath = CatalogSubcatUpload::filePath();

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];

$filterCat = [];
$filterCat[] = 'and';
if (Yii::$app->view->params['id_role'] == 1) {
    $filterCat[] = [
        'in',
        'id',
        UserCatalog::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
    ];
}

$categories = CatalogCat::find()->where($filterCat)->orderBy(['title' => SORT_ASC])->asArray()->all();
$categories = ArrayHelper::map($categories, 'id', 'title');
$universities = University::find()->orderBy(['title' => SORT_ASC])->asArray()->all();
$universities = ArrayHelper::map($universities, 'id', 'title');
$modelUpload = new CatalogSubcatUpload();
?>
    <div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            //         'enableClientValidation' => true,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <?php Notice::init(); ?>
    
                <?= $form->field($model, 'title')->textInput(); ?>
    
                <?= $form->field($modelSubcatCat, 'id_cat')->widget(Select2::className(), [
                    'data' => $categories,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите дисциплину',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($modelSubcatCat->attributeLabels()['id_cat'], ['class' => 'form_title form_select']); ?>
    
                <?= $form->field($modelSubcatCat, 'id_university')->widget(Select2::className(), [
                    'data' => $universities,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите университет',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($modelSubcatCat->attributeLabels()['id_university'], ['class' => 'form_title form_select']); ?>
    
                <?= $form->field($model, 'year')->textInput(); ?>
                <?= $form->field($model, 'author')->textInput(); ?>
                <?= $form->field($model, 'metatitle')->textInput(); ?>
                <?= $form->field($model, 'description')->textarea([
                    'rows' => '4',
                ]); ?>
                <?= $form->field($model, 'keywords')->textarea([
                    'rows' => '4',
                ]); ?>
                <div class="section_title"><?= $model->attributeLabels()['content'] ?></div>
                <div class="section_input">
                <?= $form->field($model, 'content', [
                    'options' => [
                        'class' => 'form-group form-wysiwyg',
                    ],
                ])->textarea([])->label(false); ?>
                </div>
                <script>
                    $(function () {
                        var ckeditor = CKEDITOR.replace('content', {
                            customConfig: '/lib/ckeditor/config.js'
                        });
                        AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor});
                    });
                </script>
                
                <div class='section_title' style="margin-top: 0;">
                    Прикрепляемый файл (в формате jpg, png или gif,
                    размер не более
                    <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>)
                </div>
                <?php
                if (is_file($filePath . $model->foto) && is_file($filePath . $model->foto_small)) {
                    ?>
                    <div class="section_doc">
                        <div class="icon">
                            <? echo Html::a(Html::img($fileUrl .
                                $model->foto_small, ['width' => 100]), Url::to($fileUrl . $model->foto), [
                                'data' => ['fancybox' => 'gallery'],
                                'target' => '_blank',
                            ]); ?>
                        </div>
                        <div class="title" title="<?= $model->foto ?>">
                            <?
                            echo implode('', [
                                Common::formatLongName($model->foto),
                                '<br><span>',
                                Common::formatFileSize(filesize($filePath . $model->foto)),
                                '</span>',
                            ]);
                            ?>
                        </div>
                        <div class="delete">
                            <? echo $form->field($modelUpload, 'delete_foto', [
                                'options' => ['class' => 'form-group form-checkbox',],
                            ])->checkbox(['class' => 'span-1']); ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="section_checkbox">
                    <? echo $form->field($modelUpload, 'foto', [
                        'options' => ['class' => 'form-group form-file',],
                    ])->fileInput(['accept' => '.jpg, .png, .gif'])->label(false); ?>
                </div>

            </div>

            <div class='section_stripe2'></div>
            <?php
            echo Html::submitButton($submit, [
                'id' => 'submiting',
                'class' => 'btn btn-primary section_submit',
            ]);
            ?>
        </article>
        <?php ActiveForm::end(); ?>
    </div>
<?
$this->registerJsFile('/js/catalog_subcat_add.js', ['position' => yii\web\View::POS_END]);