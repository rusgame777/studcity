<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\CatalogSubcatSearch
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use yii\widgets\Breadcrumbs;
use common\models\CatalogCat;
use common\models\CatalogSubcat;
use backend\models\CatalogSubcatUpload;
use backend\components\Notice;

$minSort = CatalogSubcat::find()->min('sort');
$maxSort = CatalogSubcat::find()->max('sort');
$rangeSort = [$minSort, $maxSort];

$cat = Yii::$app->request->get('cat');
$fileUrl = CatalogSubcatUpload::fileUrl();
$filePath = CatalogSubcatUpload::filePath();

$url_add = Url::to(array_filter([
    'catalog-subcat/form',
    'cat' => !empty($cat) ? $cat : null,
]));
$url_catalog = Url::to(['catalog-cat/show']);

$this->params['breadcrumbs'] = [];
if (!empty($cat)) {
    $catArr = CatalogCat::findOne($cat);
    $this->params['breadcrumbs'][] = [
        'label' => $catArr->title,
        'url' => Url::current(),
    ];
}

?>
    <header>
        <div id='section_header'>
            <div id='section_title'>
                <?
                echo Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('yii', 'Готовые работы'),
                        'url' => $url_catalog,
                    ],
                    'links' => $this->params['breadcrumbs'],
                ]);
                ?>
            </div>
            <div id='section_url'></div>
        </div>
    </header>
    <article>
        <?= Html::a('<i class="fa fa-plus-circle" aria-hidden="true"></i>', $url_add, [
            'class' => 'add-user',
            'title' => 'Добавить методичку',
        ]) ?>
        
        <?= $this->render('_search', ['model' => $searchModel]) ?>
        <?php Notice::init(); ?>
        <div class='kartiktable'>
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => implode(',', [
                    "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                    "input[name='" . $dataProvider->getPagination()->pageParam . "']",
                ]),
                'pager' => [
                    'class' => LinkPager::className(),
                    'template' => '{pageButtons} {customPage} {pageSize}',
                    'pageSizeList' => [10, 20, 50, 100],
                    'customPageWidth' => 50,
                    'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                    'customPageAfter' => "&nbsp;Показывать по&nbsp;",
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'notes',
                    ],
                    'afterGrid' => implode("\r\n", [
                        "<div id='section_actions'>",
                        "<div id='section_delete_all'>",
                        "Удалить выделенные элементы?",
                        "</div>",
                        "</div>",
                    ]),
                ],
                'striped' => false,
                'hover' => true,
                'resizableColumns' => false,
                'rowOptions' => function($model, $key, $index, $grid) {
                    $class = $index % 2 ? '' : 'odd_tr';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class,
                        'id' => $model->id,
                    ];
                },
                'tableOptions' => [
                    'class' => 'kartik_table',
                    'id' => 'table_barrier_block',
                    'style' => 'width: 100%;',
                ],
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'position' => 'fixed',
                ],
                'layout' => "{items}\n{pager}\n{summary}",
                'panel' => [
                    'type' => 'primary',
                ],
                'toolbar' => [
                    '{toggleData}',
                ],
                'toggleDataOptions' => ['minCount' => 0],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'filterInputOptions' => ['class' => 'form-control form-id'],
                        'contentOptions' => [
                            'class' => 'first_td right_td',
                            'width' => 30,
                        ],
                        'headerOptions' => ['width' => 30],
                    ],
                    [
                        'attribute' => 'foto',
                        'format' => 'raw',
                        'headerOptions' => [
                            'width' => 80,
                        ],
                        'contentOptions' => [
                            'class' => 'foto_td',
                        ],
                        'value' => function($model) use ($fileUrl, $filePath, $cat) {
                            if (is_file($filePath . $model->foto)) {
                                return Html::a(Html::img($fileUrl . $model->foto, ['width' => '100']), Url::to([
                                    'catalog-position/show',
                                    'cat' => $cat,
                                    'subcat' => $model->id,
                                ]), [
                                    'data-pjax' => 0,
                                ]);
                            }
                            return '-';
                        },
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'value' => function($model) use ($cat) {
                            return Html::a($model->name, Url::to([
                                'catalog-position/show',
                                'cat' => $cat,
                                'subcat' => $model->id,
                            ]), [
                                'data-pjax' => 0,
                                'title' => 'Открыть список',
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'name_university',
                        'format' => 'ntext',
                        'headerOptions' => [
                            'width' => 100,
                        ],
                        'contentOptions' => [
                            'width' => 100,
                        ],
                    ],
                    [
                        'attribute' => 'year',
                        'format' => 'ntext',
                        'headerOptions' => [
                            'width' => 40,
                        ],
                        'contentOptions' => [
                            'width' => 40,
                            'class' => 'middle_td',
                        ],
                    ],
                    [
                        'attribute' => 'author',
                        'format' => 'ntext',
                        'headerOptions' => [
                            'width' => 100,
                        ],
                        'contentOptions' => [
                            'width' => 100,
                        ],
                    ],
                    [
                        'attribute' => 'sort',
                        'headerOptions' => [
                            'width' => 40,
                        ],
                        'contentOptions' => [
                            'width' => 40,
                        ],
                        'format' => 'raw',
                        'value' => function($model) {
                            return Html::tag('div', Html::input('text', 'sort', $model->sort), [
                                'class' => 'edit_div',
                            ]);
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'header' => 'Действия',
                        'width' => '70px',
                        'headerOptions' => [
                            'class' => 'last_th',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td middle_td',
                        ],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-pencil-square-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'catalog-subcat/form',
                                    'id' => $model->id,
                                    'edit' => 'ok',
                                ]), [
                                    'class' => 'edit_td',
                                    'data-pjax' => 0,
                                    'title' => 'Редактирование методички',
                                ]);
                            },
                            'delete' => function($url, $model, $key) {
                                if ($model->count == 0) {
                                    return Html::a('<i class="fa fa-trash user-table--actions" aria-hidden="true"></i>', Url::to([
                                        'catalog-subcat/remove',
                                        'id' => $model->id,
                                    ]), [
                                        'class' => 'delete_td',
                                        'data-pjax' => 0,
                                        'title' => 'Удаление методички',
                                        'onclick' => "return confirm('Вы действительно хотите удалить методичку?');",
                                    ]);
                                } else {
                                    return null;
                                }
                            },
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => "40px",
                        'headerOptions' => [
                            'class' => 'delete-all',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td delete',
                        ],
                        'checkboxOptions' => function($model) {
                            if($model->count > 0){
                                return ['disabled' => true];
                            }else{
                                return [];
                            }
                        },
                    ],
                ],
            ]);
            ?>
        </div>

    </article>
<?php
$this->registerJsFile('/js/catalog_subcat_show.js', ['position' => yii\web\View::POS_END]);