<?php

use yii\helpers\Url;
use common\models\CatalogSubcatCat;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\CatalogSubcat;
 * @var $modelCat common\models\CatalogSubcatCat;
 *
 */

$request = Yii::$app->request;
$edit = $request->get('edit');
$get = $request->get();
$id = isset($get['id']) ? intval($get['id']) : 0;
$cat = Yii::$app->request->get('cat');
$modelSubcatCat = null;
if (empty($cat) && ($id != 0)) {
    $modelSubcatCat = CatalogSubcatCat::findOne(['id_subcat' => $id]);
    if ($modelSubcatCat != null) {
        $cat = $modelSubcatCat->id_cat;
    }
}
if (is_null($modelSubcatCat)) {
    $modelSubcatCat = new CatalogSubcatCat();
}

$url_module = '/' . $request->getPathInfo();
$url_validate = str_replace('form', 'validate', $url_module);

if (!empty($cat)) {
    $url_show = Url::to(['catalog-subcat/show', 'cat' => $cat]);
} else {
    $url_show = Url::to(['catalog-cat/show']);
}

$url_return = 'Вернуться к списку';
if ($edit == null) {
    $model->loadDefaultValues();
    $modelCat->loadDefaultValues();
    if (!empty($cat)) {
        $modelCat->id_cat = $cat;
    }
    $url_action = str_replace('form', 'create', $url_module);
    $submit = "Добавить";
    $actions = 'Добавить новую методичку';
} else {
    $url_action = str_replace('form', 'update', $url_module);
    $submit = "Внести правки";
    $actions = "Изменить методичку";
}

$result = [
    'model' => $model,
    'modelSubcatCat' => $modelSubcatCat,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
    'id' => $id,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_show ?>' class='btn btn-primary section_button'><?= $url_return ?></a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]) ?>