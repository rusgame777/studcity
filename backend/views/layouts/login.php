<?php

/**
 *
 * @var $this \yii\web\View
 * @var $content string
 *
 */

use yii\helpers\Html;
use backend\assets\AppAsset;

$this->registerJsFile('/js/login.js?1', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/css/login.css', ['position' => yii\web\View::POS_BEGIN]);
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::decode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>