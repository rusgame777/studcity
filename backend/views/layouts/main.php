<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\AppAsset;

$this->registerJsFile('/js/main.js?1', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/css/forms.css', ['position' => yii\web\View::POS_BEGIN]);
$this->title = 'Система управления содержанием сайта "' . Yii::$app->params['name_site'] . '"';
AppAsset::register($this);
$this->registerCssFile('/css/site.css?3', ['position' => yii\web\View::POS_END]);

$params = Yii::$app->view->params;
$auth = Yii::$app->authManager;
$id_role = isset($params['id_role']) ? $params['id_role'] : '';
$email_user = isset($params['email']) ? $params['email'] : '';

$this->params['metaDescription'] = '';
$this->params['metaKeywords'] = '';

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <? $this->registerMetaTag([
            'name' => 'description',
            'content' => $this->params['metaDescription'],
        ]); ?>
        <? $this->registerMetaTag([
            'name' => 'keywords',
            'content' => $this->params['metaKeywords'],
        ]); ?>
        <title><?= Html::decode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <div class='show_window'></div>

    <div id='container'>
        <header id='page_header'>
            <div id='page_overheader'></div>
            <div id="page_logo">
            <div id="logo_text"><span>
                <a href="/admin/" title="<?= Yii::$app->params['name_site'] ?>">
                    «<?= Yii::$app->params['name_site'] ?>»
                </a>.
                <span id="logo_cms">Система управления сайтом</span>
            </span></div>
            <a href="http://itc27.ru/" title="Центр Информационных Технологий"><div id="logo_foto"></div></a>
            </div>
        </header>

        <div id='bottom'>
            <aside id='page_aside' unselectable="on" onselectstart="return false;">
                <div id='aside'>

                    <?php
                    if ($auth->checkAccess($id_role, 'managementAdmins')) {
                        ?>
                        <div class='menu'>
                            <div class='menu_stripe'></div>
                            <div class='menu_content'>
                                <div class='menu_icon' id='menu_pages'></div>
                                <div class='menu_title menu_close'><span>Рабочая группа</span></div>
                            </div>
                        </div>
                        <div class='submenu'>
                            <div class='submenu_stripe'></div>
                            <div class='submenu_content'>
                                <div class='submenu_title'>- <a href='<?= Url::toRoute('admin/show'); ?>'>Список</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::toRoute('admin/form'); ?>'>Добавление</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementPages')) {
                        ?>
                        <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content'>
                            <div class='menu_icon' id='menu_pages'></div>
                            <div class='menu_title menu_close'>
                                <span>Текстовые страницы</span>
                            </div>
                        </div>
                        </div>
                        <div class='submenu'>
                        <div class='submenu_stripe'></div>
                        <div class='submenu_content'>
                            <div class='submenu_title'>- <a href='<?= Url::to(['page/show']); ?>'>Список</a>
                            </div>
                            <div class='submenu_title'>- <a href='<?= Url::to(['page/form']); ?>'>Добавление</a>
                            </div>
                        </div>
                        </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementCatalog')) {
                        ?>
                        <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content'>
                            <div class='menu_icon' id='menu_portfolio'></div>
                            <div class='menu_title menu_close'>
                                <span>Каталог "Готовые решения"</span>
                            </div>
                        </div>
                        </div>
                        <div class='submenu'>
                        <div class='submenu_stripe'></div>
                        <div class='submenu_content'>
                            <div class='submenu_title'>- <a href='<?= Url::to(['catalog-cat/show']); ?>'>Готовые работы</a>
                            </div>
                            <div class='submenu_title'>- <a href='<?= Url::to(['catalog-cat/form']); ?>'>Добавить дисциплину</a>
                            </div>
                            <div class='submenu_title'>- <a href='<?= Url::to(['catalog-subcat/form']); ?>'>Добавить методичку</a>
                            </div>
                            <div class='submenu_title'>- <a href='<?= Url::to(['catalog-position/form']); ?>'>Добавить вариант</a>
                            </div>
                            <div class='submenu_title'>- <a href='<?= Url::to(['university/show']); ?>'>Университеты</a>
                            </div>
                            <div class='submenu_title'>- <a href='<?= Url::to(['university/form']); ?>'>Добавить университет</a>
                            </div>
                        </div>
                        </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementSolution')) {
                        ?>
                        <div class='menu'>
                            <div class='menu_stripe'></div>
                            <div class='menu_content'>
                                <div class='menu_icon' id='menu_portfolio'></div>
                                <div class='menu_title menu_close'>
                                    <span>Каталог "Решение за мгновение"</span>
                                </div>
                            </div>
                        </div>
                        <div class='submenu'>
                            <div class='submenu_stripe'></div>
                            <div class='submenu_content'>
                                <div class='submenu_title'>- <a href='<?= Url::to(['solution-cat/show']); ?>'>Решение за мгновение</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['solution-cat/form']); ?>'>Добавить дисциплину</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['solution-position/form']); ?>'>Добавить РГР</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['author/show']); ?>'>Авторы</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['author/form']); ?>'>Добавить автора</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementCatalogStatistics')) {
                        ?>
                        <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content'>
                            <div class='menu_icon' id='menu_portfolio'></div>
                            <div class='menu_title menu_close'>
                                <span>Статистика "Готовые решения"</span>
                            </div>
                        </div>
                        </div>
                        <div class='submenu'>
                            <div class='submenu_stripe'></div>
                            <div class='submenu_content'>
                                <div class='submenu_title'>- <a href='<?= Url::to(['orders/show']); ?>'>Реестр заказов</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['orders/statistics']); ?>'>Аналитика</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementSolutionStatistics')) {
                        ?>
                        <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content'>
                            <div class='menu_icon' id='menu_portfolio'></div>
                            <div class='menu_title menu_close'>
                                <span>Статистика "Решение за мгновение"</span>
                            </div>
                        </div>
                        </div>
                        <div class='submenu'>
                            <div class='submenu_stripe'></div>
                            <div class='submenu_content'>
                                <div class='submenu_title'>- <a href='<?= Url::to(['solution-orders/show']); ?>'>Реестр заказов</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['solution-orders/statistics']); ?>'>Аналитика</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementSolutionOrders')) {
                        ?>
                        <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content'>
                            <div class='menu_icon' id='menu_portfolio'></div>
                            <div class='menu_title menu_close'>
                                <span>Управление заказами "Решение за мгновение"</span>
                            </div>
                        </div>
                        </div>
                        <div class='submenu'>
                            <div class='submenu_stripe'></div>
                            <div class='submenu_content'>
                                <div class='submenu_title'>- <a href='<?= Url::to(['solution-orders/management']); ?>'>Список</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['solution-orders/form']); ?>'>Добавление</a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementTemplate')) {
                        ?>
                        <div class='menu'>
                            <div class='menu_stripe'></div>
                            <div class='menu_content'>
                                <div class='menu_icon' id='menu_pages'></div>
                                <div class='menu_title menu_close'>
                                    <span>Рассылка по электронной почте</span>
                                </div>
                            </div>
                        </div>
                        <div class='submenu'>
                            <div class='submenu_stripe'></div>
                            <div class='submenu_content'>
                                <div class='submenu_title'>- <a href='<?= Url::to(['email-template/show']); ?>'>Список шаблонов</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['email-template/form']); ?>'>Добавить шаблон</a>
                                </div>
                                <div class='submenu_title'>- <a href='<?= Url::to(['email-delivery/form']); ?>'>Рассылка</a>
                                </div>
                            </div>
                         </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementReviews')) {
                        ?>
                        <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content'>
                            <div class='menu_icon' id='menu_portfolio'></div>
                            <div class='menu_title menu_close'>
                                <span>Отзывы</span>
                            </div>
                        </div>
                    </div>
                        <div class='submenu'>
                        <div class='submenu_stripe'></div>
                        <div class='submenu_content'>
                            <div class='submenu_title'>- <a href='<?= Url::to(['review/show']); ?>'>Список</a>
                            </div>
                            <div class='submenu_title'>- <a href='<?= Url::to(['review/form']); ?>'>Добавление</a>
                            </div>
                        </div>
                    </div>
                        <?php
                    }

                    if ($auth->checkAccess($id_role, 'managementSettings')) {
                        ?>
                        <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content'>
                        <div class='menu_icon' id='menu_settings'></div>
                        <div class='menu_title menu_close'><span>Настройки сайта</span></div>
                        </div>
                        </div>
                        <div class='submenu' id='submenu_6'>
                        <div class='submenu_stripe'></div>
                        <div class='submenu_content'>
                            <div class='submenu_title'>- <a href='<?= Url::toRoute('settings/form'); ?>'>Общие настройки</a></div>
                            <div class='submenu_title'>- <a href='<?= Url::toRoute('password/form'); ?>'>Пароль администратора</a></div>
                        </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class='menu'>
                        <div class='menu_stripe'></div>
                        <div class='menu_content' id='menuexit'>
                            <a href='<?= Url::toRoute('site/logout'); ?>'>
                                <div class='menu_icon' id='menu_exit'></div>
                            </a>
                            <a href='<?= Url::toRoute('site/logout'); ?>'>
                                <div class='menu_title'><span>Выход</span></div>
                            </a>
                        </div>
                    </div>

                </div>
            </aside>

            <section id='page_section'>
                <div>
                    <div id='section'>

                        <?php $this->beginBody() ?>
                        <?= $content ?>
                        <?php $this->endBody() ?>

                    </div>

                    <footer></footer>
                </div>
            </section>

        </div>
    </div>

    <div id='status'></div>
    <div class='show_box'></div>

    </body>
    </html>
<?php
$this->registerCssFile('/css/select2-krajee.min.css', ['position' => yii\web\View::POS_END]);
$this->registerCssFile('/css/select2-addl.min.css', ['position' => yii\web\View::POS_END]);
$this->endPage();