<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use app\components\DeviceDetect;

$this->registerJsFile('/js/main.js?2', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/css/site.css', ['position' => yii\web\View::POS_BEGIN]);
$this->registerCssFile('/css/forms.css', ['position' => yii\web\View::POS_BEGIN]);

$this->title = 'Система управления содержанием сайта "' . Yii::$app->params['name_site'] . '"';
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::decode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<div id='container'>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</div>

</body>
</html>
<?php $this->endPage() ?>