<?
use yii\helpers\Json;
use yii\web\Cookie;
use common\models\Admin;
use common\models\Author;

/**
 *
 * @var $data string
 */

$status = 0;
$array = [];
$dataArray = Json::decode($data, true);

$login = $dataArray['login'];
$password = $dataArray['password'];

if ($login == '') {
    $array['status'] = $status;
    $array['message'] = 'Введите логин';
    echo Json::encode($array);
    return true;
}

if ($password == '') {
    $array['status'] = $status;
    $array['message'] = 'Введите пароль';
    echo Json::encode($array);
    return true;
}

$validateAdmin = false;
$id_admin = 0;
$admin = Admin::findByUsername($login);
if (!empty($admin)) {
    $id_admin = $admin['id'];
    $validateAdmin = $admin->validatePassword($password);
}

$validateAuthor = false;
$id_author = 0;
$author = Author::findByUsername($login);
if (!empty($author)) {
    $id_author = $author['id'];
    $validateAuthor = $author->validatePassword($password);
}

if (empty(array_filter([
    $validateAdmin,
    $validateAuthor,
]))
) {
    
    if (!$validateAdmin) {
        Yii::$app->response->cookies->remove('admin_login');
        Yii::$app->response->cookies->remove('admin_hash');
        $array['status'] = $status;
        $array['message'] = 'Введите правильный пароль и/или логин!';
        echo Json::encode($array);
        return true;
    }
    
    if (!$validateAuthor) {
        Yii::$app->response->cookies->remove('author_login');
        Yii::$app->response->cookies->remove('author_hash');
        $array['status'] = $status;
        $array['message'] = 'Введите правильный пароль и/или логин!';
        echo Json::encode($array);
        return true;
    }
}

if ($id_admin > 0) {
    if ($admin->is_disabled == 1) {
        $array['status'] = $status;
        $array['message'] = 'Доступ запрещен!';
        echo Json::encode($array);
        return true;
    }
    
    $assignments = Yii::$app->authManager->getAssignments($admin->id_role);
    if (empty($assignments)) {
        $array['status'] = $status;
        $array['message'] = 'Данного пользователя не существует';
        echo Json::encode($array);
        return true;
    }
    
    $timezone = Yii::$app->params['timezone'];
    $date = new DateTime('', $timezone);
    $date_entry = $date->format('Y-m-d H:i:s');
    Yii::$app->response->cookies->add(new Cookie([
        'name' => 'admin_login',
        'value' => $login,
        'expire' => time() + 3600 * 24 * 30,
    ]));
    Yii::$app->response->cookies->add(new Cookie([
        'name' => 'admin_hash',
        'value' => $admin->generateHash($id_admin),
        'expire' => time() + 3600 * 24 * 30,
    ]));
    
    $status = 1;
    $array['url'] = $admin->id_role == -1 ? '/page/show/' : '/catalog-cat/show/';
}

if ($id_author > 0) {
    if ($author->is_disabled == 1) {
        
        $array['status'] = $status;
        $array['message'] = 'Доступ запрещен!';
        echo Json::encode($array);
        return true;
    }
    
    $assignments = Yii::$app->authManager->getAssignments($author->id_role);
    if (empty($assignments)) {
        $array['status'] = $status;
        $array['message'] = 'Данного пользователя не существует';
        echo Json::encode($array);
        return true;
    }
    
    $timezone = Yii::$app->params['timezone'];
    $date = new DateTime('', $timezone);
    $date_entry = $date->format('Y-m-d H:i:s');
    Yii::$app->response->cookies->add(new Cookie([
        'name' => 'author_login',
        'value' => $login,
        'expire' => time() + 3600 * 24 * 30,
    ]));
    Yii::$app->response->cookies->add(new Cookie([
        'name' => 'author_hash',
        'value' => $author->generateHash($id_author),
        'expire' => time() + 3600 * 24 * 30,
    ]));
    
    $status = 1;
    $array['url'] = '/solution-orders/management/';
}

$array['status'] = $status;
echo Json::encode($array);
return true;