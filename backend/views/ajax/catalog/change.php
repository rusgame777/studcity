<?php
use yii\helpers\ArrayHelper;

$sqlQuery = 'SELECT id_subcat, id_university, id_cat FROM catalog_subcat_cat2';
$catalogSubcatCat2 = ArrayHelper::index(Yii::$app->db->createCommand($sqlQuery)->queryAll(), 'id_subcat');

$sqlQuery = 'SELECT id_subcat, id_university, id_cat FROM catalog_subcat_cat';
$catalogSubcatCat = ArrayHelper::index(Yii::$app->db->createCommand($sqlQuery)->queryAll(), 'id_subcat');

foreach ($catalogSubcatCat2 as $id_subcat2 => $itemSubcatCat2) {
    if (in_array($id_subcat2, array_keys($catalogSubcatCat))) {
        Yii::$app->db->createCommand()->update('catalog_subcat_cat', [
            'id_university' => $itemSubcatCat2['id_university']
        ], [
            'id_subcat' => $itemSubcatCat2['id_subcat']
        ])->execute();
    }
}
