<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 24.12.2017
 * Time: 14:33
 */

/**
 * @var $data array
 */

use yii\helpers\Json;
use backend\components\Common;

//$dateStart =
//    Yii::$app->formatter->asDate(!empty($data['dateStart']) ? $data['dateStart'] : strtotime("-1 year"), 'php:Y-m-d');
//$dateEnd = Yii::$app->formatter->asDate(!empty($data['dateEnd']) ? $data['dateEnd'] : 'now', 'php:Y-m-d');
$dateStart =
    !empty($data['dateStart']) ? Common::getDateTime($data['dateStart'], 'Y-m-d') : Common::getDateTime('', 'Y-m-d', "-1 year");
$dateEnd =
    !empty($data['dateEnd']) ? Common::getDateTime($data['dateEnd'], 'Y-m-d') : Common::getDateTime('', 'Y-m-d');
$id_university =
    !empty($data['id_university']) ? implode(',', !is_array($data['id_university']) ? [$data['id_university']] : $data['id_university']) : '';

$data = [];
$sqlQuery = '
    SELECT group_concat(id) id_university
    FROM university
    ORDER BY sort ASC
';
$university = Yii::$app->db->createCommand($sqlQuery)->queryOne()['id_university'];

$filters = [];

$filters[] = vsprintf('(DATE_FORMAT(date_end, \'%%Y-%%m-%%d\') BETWEEN \'%1$s\' AND \'%2$s\')', [
    1 => $dateStart,
    2 => $dateEnd,
]);

if (Yii::$app->view->params['id_role'] == 1) {
    $filters[] = vsprintf('(catalog_subcat_cat.id_cat IN (SELECT id_cat FROM users_catalog WHERE id_user=%1$s))', [
        1 => Yii::$app->view->params['id_user'],
    ]);
}

if (empty($id_university) || ($id_university == $university)) {
    $sqlQuery = vsprintf('
        SELECT 
            SUM(sum) sum,
            id_university,
            name_university
        FROM (
            SELECT DISTINCT
                orderdesc.id,
                orderdesc.sum,
                catalog_subcat_cat.id_university,
                university.title name_university
            FROM orders
            LEFT JOIN orderdesc
            ON orderdesc.id_order = orders.id
            LEFT JOIN regusers
            ON regusers.id = orders.id_user
            LEFT JOIN catalog_subcat
            ON catalog_subcat.id = orderdesc.subcat
            INNER JOIN catalog_subcat_cat
            ON catalog_subcat_cat.id_subcat = catalog_subcat.id
            LEFT JOIN university            
            ON university.id = catalog_subcat_cat.id_university
            WHERE orders.id_type=1
            AND send=1
            AND status=2
            %1$s
        ) list
        GROUP BY id_university
        ', [
        1 => !empty($filters) ? ' AND ' . implode(' AND ', $filters) : '',
    ]);
    $sumUniversityResults = Yii::$app->db->createCommand($sqlQuery)->queryAll();
    
    foreach ($sumUniversityResults as $result) {
        $data['data'][0][$result['id_university']] = [
            'name' => $result['name_university'],
            'y' => intval($result['sum']),
        ];
    }
}

$sqlQuery = vsprintf('
    SELECT SUM(sum) sum,
        name_cat,
        id_cat,
        id_university,
        name_university
    FROM (
         SELECT DISTINCT
            orderdesc.id,
            orderdesc.sum,
            catalog_cat.title name_cat,
            catalog_subcat_cat.id_cat,
            catalog_subcat_cat.id_university,
            university.title name_university
        FROM orders
        LEFT JOIN orderdesc         
        ON orderdesc.id_order = orders.id
        LEFT JOIN regusers
        ON regusers.id = orders.id_user
        LEFT JOIN catalog_cat
        ON catalog_cat.id = orderdesc.cat
        LEFT JOIN catalog_subcat
        ON catalog_subcat.id = orderdesc.subcat
        INNER JOIN catalog_subcat_cat
        ON catalog_subcat_cat.id_subcat = catalog_subcat.id
        LEFT JOIN university            
        ON university.id = catalog_subcat_cat.id_university
        WHERE orders.id_type=1
        AND send=1
        AND status=2
        %1$s
        %2$s
    ) list
    GROUP BY id_cat, id_university
', [
    1 => !empty($filters) ? ' AND ' . implode(' AND ', $filters) : '',
    2 => !empty($id_university) ? sprintf('AND catalog_subcat_cat.id_university IN (%s)', $id_university) : '',
]);

$results = Yii::$app->db->createCommand($sqlQuery)->queryAll();
foreach ($results as $result) {
    $data['university'][$result['id_university']] = $result['name_university'];
    $data['cat'][$result['id_cat']] = $result['name_cat'];
    $data['data'][$result['id_university']][$result['id_cat']] = [
        'name' => $result['name_cat'],
        'y' => intval($result['sum']),
    ];
}

ob_start();

$count = count(!empty($data['data']) ? $data['data'] : []);
$ind = 0;
if ($count > 0) {
    foreach ($data['data'] as $index => $value) {
        $ind++;
        $name_university = !empty($data['university'][$index]) ? $data['university'][$index] : 'Вуз';
        if ($ind % 3 == 1) {
            ?>
            <div class="row">
            <?php
        }
        ?>
        <div class="col-lg-4">
            <div class="panel panel-default">
               <div class="panel-heading">
                    <i class="fa fa-pie-chart fa-fw"></i>
                   <?= $name_university ?>
                   <div class="pull-right"></div>
               </div>
                <div class="panel-body">
                    <div id="section_chart<?= $index ?>"></div>
                </div>
            </div>
        </div>
        <?php
        
        if ($ind % 3 == 0 || ($ind == $count)) {
            ?>
            </div>
            <?php
        }
        ?>
        <?php
    }
}

$html = ob_get_clean();
$data['html'] = $html;

echo Json::encode($data);