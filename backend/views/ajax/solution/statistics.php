<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 24.12.2017
 * Time: 14:33
 */

/**
 * @var $data array
 */

use yii\helpers\Json;
use backend\components\Common;

//$dateStart =
//    Yii::$app->formatter->asDate(!empty($data['dateStart']) ? $data['dateStart'] : strtotime("-1 year"), 'php:Y-m-d');
//$dateEnd = Yii::$app->formatter->asDate(!empty($data['dateEnd']) ? $data['dateEnd'] : 'now', 'php:Y-m-d');
$dateStart =
    !empty($data['dateStart']) ? Common::getDateTime($data['dateStart'], 'Y-m-d') : Common::getDateTime('', 'Y-m-d', "-1 year");
$dateEnd =
    !empty($data['dateEnd']) ? Common::getDateTime($data['dateEnd'], 'Y-m-d') : Common::getDateTime('', 'Y-m-d');
$id_author =
    !empty($data['id_author']) ? implode(',', !is_array($data['id_author']) ? [$data['id_author']] : $data['id_author']) : '';

$data = [];
$sqlQuery = '
    SELECT group_concat(id) id_author
    FROM admin_author
    ORDER BY name ASC
';
$author = Yii::$app->db->createCommand($sqlQuery)->queryOne()['id_author'];

$filters = [];

$filters[] = vsprintf('(DATE_FORMAT(date_end, \'%%Y-%%m-%%d\') BETWEEN \'%1$s\' AND \'%2$s\')', [
    1 => $dateStart,
    2 => $dateEnd,
]);

if (Yii::$app->view->params['id_role'] == 1) {
    $filters[] = vsprintf('(solution_position.cat IN (SELECT id_cat FROM users_solution WHERE id_user=%1$s))', [
        1 => Yii::$app->view->params['id_user'],
    ]);
}

if (empty($id_author) || ($id_author == $author)) {
    $sqlQuery = vsprintf('
        SELECT SUM(sum) sum,
               id_author,
               name_author
        FROM (
            SELECT DISTINCT
                orders.id,
                orders.sum,
                IFNULL(solution_position.id_author, 10000000) id_author,
                IFNULL(admin_author.name, \'Без автора\') name_author
            FROM orders
            LEFT JOIN orderdesc_solution ON orderdesc_solution.id_order = orders.id
            LEFT JOIN regusers ON regusers.id = orders.id_user
            LEFT JOIN solution_position ON solution_position.id = orderdesc_solution.id_position
            LEFT JOIN admin_author ON admin_author.id = solution_position.id_author
            WHERE orders.id_type=2
            AND send=1
            AND status!=1
            %1$s
        ) list
        GROUP BY id_author
        ', [
        1 => !empty($filters) ? ' AND ' . implode(' AND ', $filters) : '',
    ]);
    $sumAuthorResults = Yii::$app->db->createCommand($sqlQuery)->queryAll();
    
    foreach ($sumAuthorResults as $result) {
        $data['data'][0][$result['id_author']] = [
            'name' => $result['name_author'],
            'y' => intval($result['sum']),
        ];
    }
}

$sqlQuery = vsprintf('
    SELECT SUM(sum) sum,
        id_cat,
        name_cat,
        id_author,
        name_author
    FROM (
        SELECT DISTINCT
                orders.id,
                orders.sum,
            IFNULL(solution_cat.id, 10000000) id_cat,
            IFNULL(solution_cat.title, \'Без названия\') name_cat,
            IFNULL(solution_position.id_author, 10000000) id_author,
            IFNULL(admin_author.name, \'Без автора\') name_author
        FROM orders
        LEFT JOIN orderdesc_solution ON orderdesc_solution.id_order = orders.id
        LEFT JOIN regusers ON regusers.id = orders.id_user
        LEFT JOIN solution_cat ON solution_cat.id = orderdesc_solution.cat
        LEFT JOIN solution_position ON solution_position.id = orderdesc_solution.id_position
        LEFT JOIN admin_author ON admin_author.id = solution_position.id_author
        WHERE orders.id_type=2
        AND send=1
        AND status!=1
        %1$s
        %2$s
    ) list
    GROUP BY id_cat, id_author
', [
    1 => !empty($filters) ? ' AND ' . implode(' AND ', $filters) : '',
    2 => !empty($id_author) ? sprintf('AND solution_position.id_author IN (%s)', $id_author) : '',
]);

$results = Yii::$app->db->createCommand($sqlQuery)->queryAll();
if (!empty($results)) {
    foreach ($results as $result) {
        $data['author'][$result['id_author']] = $result['name_author'];
        $data['cat'][$result['id_cat']] = $result['name_cat'];
        $data['data'][$result['id_author']][$result['id_cat']] = [
            'name' => $result['name_cat'],
            'y' => intval($result['sum']),
        ];
    }
}

ob_start();

$count = count(!empty($data['data']) ? $data['data'] : []);
$ind = 0;
if ($count > 0) {
    foreach ($data['data'] as $index => $value) {
        $ind++;
        $name_author = !empty($data['author'][$index]) ? $data['author'][$index] : 'Автор';
        if ($ind % 3 == 1) {
            ?>
            <div class="row">
            <?php
        }
        ?>
        <div class="col-lg-4">
        <div class="panel panel-default">
           <div class="panel-heading">
                <i class="fa fa-pie-chart fa-fw"></i>
               <?= $name_author ?>
               <div class="pull-right"></div>
           </div>
            <div class="panel-body">
                <div id="section_chart<?= $index ?>"></div>
            </div>
        </div>
    </div>
        <?php
        
        if ($ind % 3 == 0 || ($ind == $count)) {
            ?>
            </div>
            <?php
        }
        ?>
        <?php
    }
}

$html = ob_get_clean();
$data['html'] = $html;

echo Json::encode($data);