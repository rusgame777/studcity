<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\ReviewSearch
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use common\models\Review;
use backend\components\Common;
use backend\components\Notice;

$request = Yii::$app->request;
$url_module = '/' . $request->getPathInfo();
$url_add = str_replace('show', 'form', $url_module);

?>
    <header>
        <div id='section_header'>
            <div id='section_title'>Отзывы</div>
            <div id='section_url'></div>
        </div>
    </header>
    <article>
        <?= Html::a('<i class="fa fa-plus-circle" aria-hidden="true"></i>', $url_add, [
            'class' => 'add-user',
            'title' => 'Добавить отзыв',
        ]) ?>
        
        <?= $this->render('_search', ['model' => $searchModel]) ?>
        <?php Notice::init(); ?>
        <div class='kartiktable'>
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => implode(',', [
                    "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                    "input[name='" . $dataProvider->getPagination()->pageParam . "']",
                ]),
                'pager' => [
                    'class' => LinkPager::className(),
                    'template' => '{pageButtons} {customPage} {pageSize}',
                    'pageSizeList' => [10, 20, 50, 100],
                    'customPageWidth' => 50,
                    'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                    'customPageAfter' => "&nbsp;Показывать по&nbsp;",
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'notes',
                    ],
                    'afterGrid' => implode("\r\n", [
                        "<div id='section_actions'>",
                        "<div id='section_delete_all'>",
                        "Удалить выделенные элементы?",
                        "</div>",
                        "</div>",
                    ]),
                ],
                'striped' => false,
                'hover' => true,
                'resizableColumns' => false,
                'rowOptions' => function($model, $key, $index, $grid) {
                    $class = $index % 2 ? '' : 'odd_tr';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class,
                        'id' => $model->id,
                    ];
                },
                'tableOptions' => [
                    'class' => 'kartik_table',
                    'id' => 'table_barrier_block',
                    'style' => 'width: 100%;',
                ],
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'position' => 'fixed',
                ],
                'layout' => "{items}\n{pager}\n{summary}",
                'panel' => [
                    'type' => 'primary',
                ],
                'toolbar' => [
                    '{toggleData}',
                ],
                'toggleDataOptions' => ['minCount' => 0],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'filterInputOptions' => ['class' => 'form-control form-id'],
                        'contentOptions' => [
                            'class' => 'first_td right_td',
                            'width' => 30,
                        ],
                        'headerOptions' => ['width' => 30],
                    ],
                    [
                        'attribute' => 'date_publication',
                        'contentOptions' => [
                            'class' => 'middle_td',
                            'width' => 150,
                        ],
                        'headerOptions' => ['width' => 150],
                        'format' => 'raw',
                        'value' => function($model) {
//                            return Yii::$app->formatter->format(implode(' ', [
//                                $model->date_add,
//                                $model->time_add,
//                            ]), ['date', 'php:d.m.Y<\b\r>H:i:s']);
                            return Common::getDateTime(implode(' ', [
                                $model->date_add,
                                $model->time_add,
                            ]), 'd.m.Y<\b\r>H:i:s');
                        },
                    ],
                    [
                        'attribute' => 'avtor',
                    ],
                    [
                        'attribute' => 'message',
                        'format' => 'ntext',
                    ],
                    [
                        'attribute' => 'phone',
                        'contentOptions' => [
                            'width' => 100,
                        ],
                        'headerOptions' => ['width' => 100],
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'header' => 'Действия',
                        'width' => '130px',
                        'headerOptions' => [
                            'class' => 'last_th',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td middle_td',
                        ],
                        'template' => '{submit} {cancel} {reply} {update} {delete}',
                        'buttons' => [
                            'submit' => function($url, $model, $key) {
                                if ($model->subm == Review::STATUS_INACTIVE) {
                                    return Html::a('<i class="fa fa-check-circle user-table--actions" aria-hidden="true"></i>', Url::to([
                                        'review/submit',
                                        'id' => $model->id,
                                    ]), [
                                        'class' => 'icon_td',
                                        'data-pjax' => 0,
                                        'title' => 'Опубликовать',
                                    ]);
                                } else {
                                    return null;
                                }
                            },
                            'cancel' => function($url, $model, $key) {
                                if ($model->subm == Review::STATUS_ACTIVE) {
                                    return Html::a('<i class="fa fa-ban user-table--actions" aria-hidden="true"></i>', Url::to([
                                        'review/cancel',
                                        'id' => $model->id,
                                    ]), [
                                        'class' => 'icon_td',
                                        'data-pjax' => 0,
                                        'title' => 'Убрать из публикации',
                                    ]);
                                } else {
                                    return null;
                                }
                            },
                            'reply' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-reply user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'review-reply/show',
                                    'cat' => $model->id,
                                ]), [
                                    'class' => 'icon_td',
                                    'data-pjax' => 0,
                                    'title' => 'Ответить на отзыв',
                                ]);
                            },
                            'update' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-pencil-square-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'review/form',
                                    'id' => $model->id,
                                    'edit' => 'ok',
                                ]), [
                                    'class' => 'icon_td',
                                    'data-pjax' => 0,
                                    'title' => 'Редактирование отзыва',
                                ]);
                            },
                            'delete' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-trash user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'review/remove',
                                    'id' => $model->id,
                                ]), [
                                    'class' => 'icon_td',
                                    'data-pjax' => 0,
                                    'title' => 'Удаление отзыва',
                                    'onclick' => "return confirm('Вы действительно хотите удалить отзыв?');",
                                ]);
                            },
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => "40px",
                        'headerOptions' => [
                            'class' => 'delete-all',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td delete',
                        ],
                    ],
                ],
            ]);
            ?>
        </div>

    </article>
<?php
$this->registerJsFile('/js/review_show.js', ['position' => yii\web\View::POS_END]);