<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\CatalogSubcat
 *
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\validators\FileValidator;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\Select2Asset;
use backend\assets\CkeditorAsset;
use backend\models\SolutionData;
use backend\models\SolutionPositionUpload;
use backend\components\Common;
use backend\components\Notice;
use common\models\SolutionCat;
use common\models\Author;
use kartik\select2\Select2;
use wbraganca\dynamicform\DynamicFormWidget;
use common\models\UserSolution;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
Select2Asset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$fileUrl = SolutionPositionUpload::fileUrl();
$filePath = SolutionPositionUpload::filePath();

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];

$filterCat = [];
$filterCat[] = 'and';
if (Yii::$app->view->params['id_role'] == 1) {
    $filterCat[] = [
        'in',
        'id',
        UserSolution::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
    ];
}

$categories = SolutionCat::find()->where($filterCat)->orderBy(['title' => SORT_ASC])->asArray()->all();
$categories = ArrayHelper::map($categories, 'id', 'title');
$authors = Author::find()->orderBy(['name' => SORT_ASC])->asArray()->all();
$authors = ArrayHelper::map($authors, 'id', 'name');

if ($model->id != 0) {
    $modelItems = SolutionData::find()->where(['id_position' => $model->id])->orderBy('name ASC')->all();
}
if (empty($modelItems)) {
    $modelItems = [new SolutionData()];
}

//$modelItems = [];
//$dataArray =
//    $model->id != 0 ? ArrayHelper::index(SolutionData::find()->where(['id_position' => $model->id])->orderBy('name ASC')
//        ->asArray()->all(), 'id') : [];
//if (!empty($dataArray)) {
//    foreach ($dataArray as $id_data => $array) {
//        $modelItem = SolutionData::find()->where(['id' => $id_data])->one();
//        if (!empty($modelItem)) {
//            $modelItems[] = $modelItem;
//        }
//    }
//}
//if (empty($modelItems)) {
//    $modelItem = new SolutionData();
//    $modelItems[] = $modelItem;
//}

$modelUpload = new SolutionPositionUpload();
?>
    <div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            //         'enableClientValidation' => true,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <?php Notice::init(); ?>
    
                <?= $form->field($model, 'title')->textInput(); ?>
                
                <?= $form->field($model, 'id_author')->widget(Select2::className(), [
                    'data' => $authors,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите автора',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($model->attributeLabels()['id_author'], ['class' => 'form_title form_select']); ?>

                <?= $form->field($model, 'cat')->widget(Select2::className(), [
                    'data' => $categories,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите дисциплину',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($model->attributeLabels()['cat'], ['class' => 'form_title form_select']); ?>

                <?= $form->field($model, 'price')->textInput(); ?>
                <?= $form->field($model, 'limitation')->textInput(); ?>
                <?= $form->field($model, 'comments')->textarea([
                    'rows' => '4',
                ]); ?>

                <? DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper',
                    'widgetBody' => '#goods_value_list',
                    'widgetItem' => '.goods_value_list',
                    'limit' => 1000,
                    'min' => 1,
                    'insertButton' => '.goods_add',
                    'deleteButton' => '.goods_delete',
                    'model' => new SolutionData(),
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'id',
                        'name',
                    ],
                ]); ?>
                <div class='section_title' style="margin-top: 0;"><h3>Данные</h3></div>
                <div id="goods_value_list">
                    <?php
                    if (!empty($modelItems)) {
                        foreach ($modelItems as $indexItem => $modelItem) {
                            ?>
                            <div class="goods_value_list">
                                <div class='goods_values'>
                                    <?= Html::activeHiddenInput($modelItem, "[{$indexItem}]id"); ?>
                                    <div class="goods_value">
                                        <?= $form->field($modelItem, '[' . $indexItem . ']name')->textInput([
                                            'placeholder' => $modelItem->attributeLabels()['name'],
                                            'title' => $modelItem->attributeLabels()['name'],
                                        ])->label(false); ?>
                                    </div>
                                </div>
                                <div class="goods_actions">
                                    <div class='goods_delete'><i class='fa fa-minus-square' aria-hidden='true'></i>
                                    </div>
                                    <div class='goods_add'><i class='fa fa-plus-square' aria-hidden='true'></i>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <? DynamicFormWidget::end(); ?>

                <?= $form->field($model, 'metatitle')->textInput(); ?>
                <?= $form->field($model, 'description')->textarea([
                    'rows' => '4',
                ]); ?>
                <?= $form->field($model, 'keywords')->textarea([
                    'rows' => '4',
                ]); ?>
                <div class="section_title"><?= $model->attributeLabels()['content'] ?></div>
                <div class="section_input">
                <?= $form->field($model, 'content', [
                    'options' => [
                        'class' => 'form-group form-wysiwyg',
                    ],
                ])->textarea([])->label(false); ?>
                </div>
                <script>
                    $(function () {
                        var ckeditor = CKEDITOR.replace('content', {
                            customConfig: '/lib/ckeditor/config.js'
                        });
                        AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor});
                    });
                </script>
                
                <div class='section_title' style="margin-top: 0;">
                    Прикрепляемый файл (в формате jpg, png или gif при соотношении сторон - 3:4,
                    размер не более
                    <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>)
                </div>
                <?php
                if (is_file($filePath . $model->foto) && is_file($filePath . $model->foto_small)) {
                    ?>
                    <div class="section_doc">
                        <div class="icon">
                            <? echo Html::a(Html::img($fileUrl . $model->foto_small, ['width' => 100]), Url::to($fileUrl . $model->foto), [
                                'data' => ['fancybox' => 'gallery'],
                                'target' => '_blank',
                            ]); ?>
                        </div>
                        <div class="title" title="<?= $model->foto ?>">
                            <?
                            echo implode('', [
                                Common::formatLongName($model->foto),
                                '<br><span>',
                                Common::formatFileSize(filesize($filePath . $model->foto)),
                                '</span>',
                            ]);
                            ?>
                        </div>
                        <div class="delete">
                            <? echo $form->field($modelUpload, 'delete_foto', [
                                'options' => ['class' => 'form-group form-checkbox',],
                            ])->checkbox(['class' => 'span-1']); ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="section_checkbox">
                    <? echo $form->field($modelUpload, 'foto', [
                        'options' => ['class' => 'form-group form-file',],
                    ])->fileInput(['accept' => '.jpg, .png, .gif'])->label(false); ?>
                </div>

            </div>

            <div class='section_stripe2'></div>
            <?php
            echo Html::submitButton($submit, [
                'id' => 'submiting',
                'class' => 'btn btn-primary section_submit',
            ]);
            ?>
        </article>
        <?php ActiveForm::end(); ?>
    </div>
<?
$this->registerJsFile('/js/solution_position_add.js', ['position' => yii\web\View::POS_END]);