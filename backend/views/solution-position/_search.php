<?php
/**
 * @var $this yii\web\View
 * @var $model backend\models\SolutionPositionSearch
 * @var $form yii\widgets\ActiveForm
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$id_type = $model->getId_type();
$search_word = $model->getSearch_word();
$filter_arr = [
    1 => 'Частичное совпадение с любым из слов',
    2 => 'Частичное совпадение с каждым из слов',
    3 => 'Точное совпадение',
];
$filter2_arr = [
    1 => 'Любое из слов',
    2 => 'Все слова',
    3 => 'Как в запросе',
];
?>
<div class="post-search">
    <?php Pjax::begin(['id' => 'search']) ?>
    <?php $form = ActiveForm::begin([
        'action' => ['show'],
        'method' => 'get',
        'options' => ['data-pjax' => true, 'id' => 'search_form'],
    ]); ?>
    <div class="filter">
        <div class="row">
            <div class="col-xs-4">
                <div class="input-group" id="search_word">
                    <input type="text" class="form-control" value="<?= $search_word ?>" aria-label="...">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="value"><?= $filter2_arr[$id_type]; ?></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <? foreach ($filter_arr as $key => $value) { ?>
                                <li data-id="<?= $key ?>"><a href="#"><?= $value ?></a></li>
                            <? } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-1">
                <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
            </div>
            <div class="col-xs-7"></div>
        </div>
    </div>
    <?= $form->field($model, 'cat')->hiddenInput()->hint(false)->label(false); ?>
    <?= $form->field($model, 'id_type')->hiddenInput()->hint(false)->label(false); ?>
    <?= $form->field($model, 'search_word')->hiddenInput()->hint(false)->label(false); ?>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>