<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\OrdersSearch
 * @var $model backend\models\OrdersSearch
 */

use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use backend\models\OrdersSearch;
use backend\components\Notice;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\components\Common;

$request = Yii::$app->request;
$url_module = '/' . $request->getPathInfo();
$url_add = str_replace('show', 'form', $url_module);
$url_excel = Url::to(['excel', Yii::$app->request->queryParams]);
//$numberList = OrdersSearch::getNumberList();
?>
<header>
    <div id='section_header'>
        <div id='section_title'>Реестр заказов</div>
        <div id='section_url'></div>
    </div>
</header>
<article>
    <?= Html::a('<i class="fa fa-file-excel-o excel" aria-hidden="true"></i>', $url_excel, [
        'class' => 'add-user',
        'title' => 'Выгрузка',
    ]) ?>
    <?= $this->render('_search', ['model' => $searchModel]) ?>
    <?php Notice::init(); ?>
    <div class='kartiktable'>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => implode(',', [
                "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                "input[name='" . $dataProvider->getPagination()->pageParam . "']",
            ]),
            'pager' => [
                'class' => LinkPager::className(),
                'template' => '{pageButtons} {customPage} {pageSize}',
                'pageSizeList' => [10, 20, 50, 100],
                'customPageWidth' => 50,
                'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                'customPageAfter' => "&nbsp;Показывать по&nbsp;",
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'id' => 'notes',
                ],
            ],
            'striped' => false,
            'hover' => true,
            'resizableColumns' => false,
            'rowOptions' => function($model, $key, $index, $grid) {
                $class = $index % 2 ? '' : 'odd_tr';
                return [
                    'key' => $key,
                    'index' => $index,
                    'class' => $class,
                    'id' => $model->id,
                ];
            },
            'tableOptions' => [
                'class' => 'kartik_table',
                'id' => 'table_barrier_block',
                'style' => 'width: 100%;',
            ],
            'floatHeader' => true,
            'floatHeaderOptions' => [
                'position' => 'fixed',
            ],
            'layout' => "{items}\n{pager}\n{summary}",
            'panel' => [
                'type' => 'primary',
            ],
            'toggleDataOptions' => ['minCount' => 0],
            'columns' => [
                [
                    'attribute' => 'id',
                    'filterInputOptions' => ['class' => 'form-control form-id'],
                    'contentOptions' => [
                        'class' => 'first_td right_td',
                        'width' => 100,
                    ],
                    'headerOptions' => [
                        'width' => 100,
                    ],
//                    'value' => function($model) use ($numberList) {
                    'value' => function($model) {
                        /**
                         * @var $model backend\models\OrdersSearch
                         */
//                        $numberItem = array_key_exists($model->id, $numberList) ? $numberList[$model->id] : [];
//                        return !empty($numberItem) ? implode('/', [
//                            $numberItem['count'],
//                            $numberItem['date_end'] . '(' . $numberItem['id'] . ')',
//                        ]) : null;
                        return $model->getOrderId();
                    },
                ],
                [
                    'attribute' => 'date_end',
                    'format' => 'raw',
                    'value' => function($model) {
//                        return Yii::$app->formatter->asDate($model->date_end, 'php:H:i');
                        return Common::getDateTime($model->date_end, 'H:i');
                    },
                    'headerOptions' => [
                        'width' => 110,
                    ],
                    'contentOptions' => [
                        'width' => 110,
                    ],
                ],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'headerOptions' => [
                        'width' => 110,
                    ],
                    'contentOptions' => [
                        'width' => 110,
                    ],
                ],
                [
                    'attribute' => 'contacts',
                    'format' => 'raw',
                    'value' => function($model) {
                        return implode("<br>", array_filter([
                            $model->phone,
                            !empty($model->email) ? "(" . $model->email . ")" : '',
                        ]));
                    },
                ],
                [
                    'attribute' => 'orderList',
                    'format' => 'ntext',
                    'value' => function($model) {
                        /**
                         * @var $model backend\models\OrdersSearch
                         */
                        return $model->getOrderList();
                    },
                ],
                [
                    'attribute' => 'sum',
                    'headerOptions' => [
                        'width' => 100,
                    ],
                    'contentOptions' => [
                        'width' => 100,
                    ],
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->sum . " руб.";
                    },
                ],
            ],
        ]);
        ?>
    </div>

</article>
<?php
$this->registerJsFile('/js/ordersShow.js', ['position' => yii\web\View::POS_END]);
?>
