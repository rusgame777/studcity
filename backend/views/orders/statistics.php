<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 22.12.2017
 * Time: 1:53
 */

/**
 * @var $searchModel backend\models\Statistics
 *
 */

use backend\assets\HighchartsAsset;

$bundle = HighchartsAsset::register(Yii::$app->view);

$this->registerCssFile('/css/sb-admin-2.css');
$this->registerCssFile('/css/timeline.css');
$this->registerJsFile('/js/ordersStatistics.js?2');
?>
<header>
    <div id='section_header'>
        <div id='section_title'>Аналитика</div>
        <div id='section_url'></div>
    </div>
</header>

<article>
    <?= $this->render('_search_statistics', ['model' => $searchModel]) ?>
    <div id="charts">
    </div>
</article>
