<?php
/**
 * @var $this yii\web\View
 * @var $model backend\models\OrdersSearch
 * @var $form yii\widgets\ActiveForm
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use yii\db\Expression;
use backend\assets\Select2Asset;
use common\models\University;
use common\models\CatalogCat;
use common\models\UserCatalog;
use kartik\select2\Select2;

Select2Asset::register(Yii::$app->view);
$universities = University::find()->orderBy(['sort' => SORT_ASC, 'title' => SORT_ASC])->asArray()->all();
$universities = ArrayHelper::map($universities, 'id', 'title');

$filterCat[] = 'and';
if (Yii::$app->view->params['id_role'] == 1) {
    $filterCat[] = [
        'in',
        'id',
        UserCatalog::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
    ];
}
$categories =
    CatalogCat::find()->where($filterCat)->orderBy(['sort' => SORT_ASC, 'title' => SORT_ASC])->asArray()->all();
$categories = ArrayHelper::map($categories, 'id', 'title');
?>

<div class="post-search">
    <?php Pjax::begin(['id' => 'search']) ?>
    <?php $form = ActiveForm::begin([
        'action' => ['show'],
        'method' => 'get',
        'options' => ['data-pjax' => true, 'id' => 'search_form'],
    ]); ?>
    <div class="filter">

        <div class="row">
            <div class="col-xs-4">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите дату:</div>
                <div class="statistic_filter_date1">
                    <?= $form->field($model, 'dateStart')->hint('')->label(false)->widget(DatePicker::classname(), [
                        'language' => 'ru',
                        'dateFormat' => 'dd.MM.yyyy',
                        'options' => ['placeholder' => 'от'],
                    ]) ?>
                </div>
                <div class="statistic_filter_date2">
                    <?= $form->field($model, 'dateEnd')->hint('')->label(false)->widget(DatePicker::classname(), [
                        'language' => 'ru',
                        'dateFormat' => 'dd.MM.yyyy',
                        'options' => ['placeholder' => 'до'],
                    ]) ?>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите вуз:</div>
                <div class="statistic_filter_select">
                    <?= $form->field($model, 'id_university')->widget(Select2::className(), [
                        'data' => $universities,
                        'language' => 'ru',
                        'options' => [
                            'placeholder' => 'Выберите вуз',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label(false); ?>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите предмет:</div>
                <div class="statistic_filter_select">
                    <?= $form->field($model, 'cat')->widget(Select2::className(), [
                        'data' => $categories,
                        'language' => 'ru',
                        'options' => [
                            'placeholder' => 'Выберите предмет',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label(false); ?>
                </div>
                </div>
            </div>
            <div class="col-xs-1">
                <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Сумма:</div>
                <div class="statistic_filter_value"><?= $model->getSum() ?> руб.</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Количество клиентов:</div>
                <div class="statistic_filter_value"><?= $model->getUniqueEmail() ?></div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>