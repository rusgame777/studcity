<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 22.12.2017
 * Time: 1:53
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerCssFile('/css/sb-admin-2.css', ['position' => yii\web\View::POS_HEAD]);
$this->registerCssFile('/css/timeline.css', ['position' => yii\web\View::POS_HEAD]);
?>
<header>
    <div id='section_header'>
        <div id='section_title'>Статистика</div>
        <div id='section_url'></div>
    </div>
</header>

<article>
<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><? #= InnerDashboard::generateUser(); ?></div>
                        <div>Всего<br>пользователей<br><br></div>
                    </div>
                </div>
            </div>
            <? $html = "<div class=\"panel-footer\">
                    <span class=\"pull-left\">Подробнее</span>
                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                    <div class=\"clearfix\"></div>
                </div>";
            echo Html::a($html, Url::to(['inner-user/show']), ['title' => 'Подробнее']) ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-cogs fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><? #= InnerDashboard::generateActiveUser(); ?></div>
                        <div>
                            Авторизовано<br>
                            пользователей<br>
                            <span class="small"> <? #= $period ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <? $html = "<div class=\"panel-footer\">
                    <span class=\"pull-left\">Подробнее</span>
                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                    <div class=\"clearfix\"></div>
                </div>";
            echo Html::a($html, Url::to([
                'inner-user/show',
                'sort' => '-statistic_date_create',
            ]), ['title' => 'Подробнее']) ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user-plus fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><? #= InnerDashboard::generateNewUser(); ?></div>
                        <div>
                            Новых<br>
                            пользователей<br>
                            <span class="small"> <? #= $period ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <? $html = "<div class=\"panel-footer\">
                    <span class=\"pull-left\">Подробнее</span>
                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                    <div class=\"clearfix\"></div>
                </div>";
            echo Html::a($html, Url::to(['inner-user/show', 'sort' => '-date_create']), ['title' => 'Подробнее']) ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red" id="panel_kind_sum">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-cube fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><? #= InnerStatisticKind::generateKindSum() ?></div>
                        <div>
                            Обработано<br>
                            запросов к КД<br>
                            <span class="small"> <? #= $period ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <? $html = "<div class=\"panel-footer\">
                    <span class=\"pull-left\">Подробнее</span>
                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                    <div class=\"clearfix\"></div>
                </div>";
            echo Html::a($html, Url::to([
                'inner-statistic/kind',
                'date_start' => '',
                'date_end' => '',
            ]), ['title' => 'Подробнее']) ?>
        </div>
        <div class="panel panel-red" id="panel_kind_save" style='display: none;'>
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-floppy-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge"><? #= InnerStatisticSave::generateSaveSum() ?></div>
                        <div>
                            Всего отчётов<br>
                            сохранено<br>пользователями<br>
                        </div>
                    </div>
                </div>
            </div>
            <? $html = "<div class=\"panel-footer\">
                    <span class=\"pull-left\">Подробнее</span>
                    <span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>
                    <div class=\"clearfix\"></div>
                </div>";
            echo Html::tag('span', $html) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> Запросы к кубам данных
                <div class="pull-right">
                    <? /* <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Action</a>
                            </li>
                            <li><a href="#">Another action</a>
                            </li>
                            <li><a href="#">Something else here</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>*/ ?>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div id="section_chart1"></div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>

    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-pie-chart fa-fw"></i> Запросы к кубам данных
                <div class="pull-right">
                    <? /* <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Action</a>
                            </li>
                            <li><a href="#">Another action</a>
                            </li>
                            <li><a href="#">Something else here</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>*/ ?>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div id="section_chart2"></div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <?php
    # $resultsReport = InnerStatisticReport::popular();
    #if (!empty($resultsReport)) {
    ?>
    <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-list fa-fw"></i> Популярные отчеты
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="list-group">
                        <?php
                        //                        foreach ($resultsReport as $array) {
                        //                            ?>
                        <!--                            <span class="list-group-item">-->
                        <!--                            --><? //= $array['type'] ?>
                        <!--                                --><?php
                        //                                if (!empty($array['params'])) {
                        //                                    echo "<br>" . $array['params'];
                        //                                }
                        //                                ?>
                        <!--                                <span class="pull-right text-muted small"><em>-->
                        <? //= $array['qty'] ?><!--</em></span>-->
                        <!--                            </span>-->
                        <!--                            --><?php
                        //                        }
                        ?>
                    </div>
                    <!-- /.list-group -->
                    <?= Html::a('Показать все', Url::to([
                        'inner-statistic/report',
                        'date_start' => '',
                        'date_end' => '',
                    ]), ['class' => 'btn btn-default btn-block', 'title' => 'Показать все']) ?>
                </div>
                <!-- /.panel-body -->
            </div>
        <!-- /.panel -->
        </div>
    <?php
    #}
    ?>
</div>
