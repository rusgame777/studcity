<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\SolutionCat
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\validators\FileValidator;
use backend\components\Common;
use backend\components\Notice;
use backend\models\SolutionCatUpload;

$fileUrl = SolutionCatUpload::fileUrl();
$filePath = SolutionCatUpload::filePath();

$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];

$modelUpload = new SolutionCatUpload();
?>
    <div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            //         'enableClientValidation' => true,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <?php Notice::init(); ?>
    
                <?= $form->field($model, 'title')->textInput(); ?>

                <?= $form->field($model, 'metatitle')->textInput(); ?>
    
                <?= $form->field($model, 'description')->textarea([
                    'rows' => '4',
                ]); ?>
                <?= $form->field($model, 'keywords')->textarea([
                    'rows' => '4',
                ]); ?>
                <?= $form->field($model, 'sms_author')->radioList([
                    0 => 'отключить',
                    1 => 'включить',
                ], ['class' => 'form_radio']); ?>
                <?= $form->field($model, 'sms_user')->radioList([
                    0 => 'отключить',
                    1 => 'включить',
                ], ['class' => 'form_radio']); ?>

                <div class='section_title' style="margin-top: 0;">
                    Прикрепляемый файл (в формате jpg, png или gif,
                    размер не более
                    <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>)
                </div>
                <?php
                if (is_file($filePath . $model->foto)) {
                    ?>
                    <div class="section_doc">
                        <div class="icon">
                            <? echo Html::a(Html::img($fileUrl . $model->foto, ['width' => 100]), Url::to($fileUrl . $model->foto), [
                                'data' => ['fancybox' => 'gallery'],
                                'target' => '_blank',
                            ]); ?>
                        </div>
                        <div class="title" title="<?= $model->foto ?>">
                            <?
                            echo implode('', [
                                Common::formatLongName($model->foto),
                                '<br><span>',
                                Common::formatFileSize(filesize($filePath . $model->foto)),
                                '</span>',
                            ]);
                            ?>
                        </div>
                        <div class="delete">
                            <? echo $form->field($modelUpload, 'delete_foto', [
                                'options' => ['class' => 'form-group form-checkbox',],
                            ])->checkbox(['class' => 'span-1']); ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="section_checkbox">
                    <? echo $form->field($modelUpload, 'foto', [
                        'options' => ['class' => 'form-group form-file',],
                    ])->fileInput(['accept' => '.jpg, .png, .gif'])->label(false); ?>
                </div>

            </div>

            <div class='section_stripe2'></div>
            <?php
            echo Html::submitButton($submit, [
                'id' => 'submiting',
                'class' => 'btn btn-primary section_submit',
            ]);
            ?>
        </article>
        <?php ActiveForm::end(); ?>
    </div>
<?
$this->registerJsFile('/js/solution_cat_add.js', ['position' => yii\web\View::POS_END]);