<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\CatalogSubcatSearch
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use yii\widgets\Breadcrumbs;
use common\models\CatalogCat;
use common\models\CatalogSubcat;
use common\models\CatalogPosition;
use backend\components\Notice;

$cat = Yii::$app->request->get('cat');
$subcat = Yii::$app->request->get('subcat');

$url_add = Url::to(array_filter([
    'catalog-position/form',
    'cat' => !empty($cat) ? $cat : null,
    'subcat' => !empty($subcat) ? $subcat : null,
]));
$url_catalog = Url::to(['catalog-cat/show']);

$minSort = CatalogPosition::find()->where(['subcat' => $subcat])->min('sort');
$maxSort = CatalogPosition::find()->where(['subcat' => $subcat])->max('sort');
$rangeSort = [$minSort, $maxSort];

$this->params['breadcrumbs'] = [];
if (!empty($cat)) {
    $catArr = CatalogCat::findOne($cat);
    $this->params['breadcrumbs'][] = [
        'label' => $catArr->title,
        'url' => Url::to([
            'catalog-subcat/show',
            'cat' => $cat,
        ]),
    ];
}
if (!empty($subcat)) {
    $subcatArr = CatalogSubcat::findOne($subcat);
    $this->params['breadcrumbs'][] = [
        'label' => $subcatArr->title,
        'url' => Url::current(),
    ];
}

?>
    <header>
        <div id='section_header'>
            <div id='section_title'>
                <?
                echo Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => Yii::t('yii', 'Готовые работы'),
                        'url' => $url_catalog,
                    ],
                    'links' => $this->params['breadcrumbs'],
                ]);
                ?>
            </div>
            <div id='section_url'></div>
        </div>
    </header>
    <article>
        <?= Html::a('<i class="fa fa-plus-circle" aria-hidden="true"></i>', $url_add, [
            'class' => 'add-user',
            'title' => 'Добавить вариант',
        ]) ?>
        <?= $this->render('_search', ['model' => $searchModel]) ?>
        <?php Notice::init(); ?>
        <div class='kartiktable'>
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => implode(',', [
                    "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                    "input[name='" . $dataProvider->getPagination()->pageParam . "']",
                ]),
                'pager' => [
                    'class' => LinkPager::className(),
                    'template' => '{pageButtons} {customPage} {pageSize}',
                    'pageSizeList' => [10, 20, 50, 100],
                    'customPageWidth' => 50,
                    'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                    'customPageAfter' => "&nbsp;Показывать по&nbsp;",
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'notes',
                    ],
                    'afterGrid' => implode("\r\n", [
                        "<div class='container-fluid' id='section_actions'>",
                        "<div class='row'>",
                        "<div class='pull-right col-xs-12'>",
                        "<div id='section_delete_all'>Удалить выделенные элементы?</div>",
                        "<div id='section_set_value'>Задать единую стоимость</div>",
                        "<div id='section_set_input' class='input-group pull-right'>",
                        "<span class='input-group-btn'>",
                        "<button class='btn btn-default' type='button'>Применить</button>",
                        "</span>",
                        "<input type='text' name='set_value_price' class='form-control'>",
                        "</div>",
                        "</div>",
                        "</div>",
                        "</div>",
                    ]),
                ],
                'striped' => false,
                'hover' => true,
                'resizableColumns' => false,
                'rowOptions' => function($model, $key, $index, $grid) {
                    $class = $index % 2 ? '' : 'odd_tr';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class,
                        'id' => $model->id,
                    ];
                },
                'tableOptions' => [
                    'class' => 'kartik_table',
                    'id' => 'table_barrier_block',
                    'style' => 'width: 100%;',
                ],
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'position' => 'fixed',
                ],
                'layout' => "{items}\n{pager}\n{summary}",
                'panel' => [
                    'type' => 'primary',
                ],
                'toolbar' => [
                    '{toggleData}',
                ],
                'toggleDataOptions' => ['minCount' => 0],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'filterInputOptions' => ['class' => 'form-control form-id'],
                        'contentOptions' => [
                            'class' => 'first_td right_td',
                            'width' => 30,
                        ],
                        'headerOptions' => ['width' => 30],
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'value' => function($model) {
                            return Html::a($model->name, Url::to([
                                'catalog-position/form',
                                'id' => $model->id,
                                'edit' => 'ok',
                            ]), [
                                'data-pjax' => 0,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'price',
                    ],
                    [
                        'attribute' => 'sort',
                        'headerOptions' => [
                            'width' => 40,
                        ],
                        'contentOptions' => [
                            'width' => 40,
                        ],
                        'format' => 'raw',
                        'value' => function($model) {
                            return Html::tag('div', Html::input('text', 'sort', $model->sort), [
                                'class' => 'edit_div',
                            ]);
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'header' => 'Действия',
                        'width' => '70px',
                        'headerOptions' => [
                            'class' => 'last_th',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td middle_td',
                        ],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-pencil-square-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'catalog-position/form',
                                    'id' => $model->id,
                                    'edit' => 'ok',
                                ]), [
                                    'class' => 'edit_td',
                                    'data-pjax' => 0,
                                    'title' => 'Редактирование РГР',
                                ]);
                            },
                            'delete' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-trash user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'catalog-position/remove',
                                    'id' => $model->id,
                                ]), [
                                    'class' => 'delete_td',
                                    'data-pjax' => 0,
                                    'title' => 'Удаление РГР',
                                    'onclick' => "return confirm('Вы действительно хотите удалить РГР?');",
                                ]);
                            },
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => "40px",
                        'headerOptions' => [
                            'class' => 'delete-all',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td delete',
                        ],
                    ],
                ],
            ]);
            ?>
        </div>

    </article>
<?php
$this->registerJsFile('/js/catalog_position_show.js?2', ['position' => yii\web\View::POS_END]);