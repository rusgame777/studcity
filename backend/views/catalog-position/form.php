<?php

use yii\helpers\Url;
use common\models\CatalogSubcatCat;
use common\models\CatalogSubcat;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\CatalogPosition;
 *
 */

$request = Yii::$app->request;
$edit = $request->get('edit');
$get = $request->get();
$id = isset($get['id']) ? intval($get['id']) : 0;
$cat = Yii::$app->request->get('cat');
$subcat = Yii::$app->request->get('subcat');

if (empty($subcat) && ($id != 0)) {
    $subcat = $model->subcat;
}

if (empty($cat) && ($id != 0)) {
    $modelSubcatCat = CatalogSubcatCat::findOne(['id_subcat' => $subcat]);
    if ($modelSubcatCat != null) {
        $cat = $modelSubcatCat->id_cat;
    }
}

$url_module = '/' . $request->getPathInfo();
$url_validate = str_replace('form', 'validate', $url_module);

if (!empty($subcat) && !empty($cat)) {
    $url_show = Url::to(['catalog-position/show', 'cat' => $cat, 'subcat' => $subcat]);
} else {
    $url_show = Url::to(['catalog-cat/show']);
}

$url_return = 'Вернуться к списку';
if ($edit == null) {
    $model->loadDefaultValues();
    $model->type_option = 1;
    $url_action = str_replace('form', 'create', $url_module);
    $submit = "Добавить";
    $actions = 'Добавить новый вариант';
} else {
    $url_action = str_replace('form', 'update', $url_module);
    $submit = "Внести правки";
    $actions = "Изменить вариант";
}

$model->cat = $cat;
$model->subcat = $subcat;
if ($model->type_option == 1) {
    $model->price1 = $model->price;
} else {
    $model->price2 = $model->price;
}

$result = [
    'model' => $model,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
    'id' => $id,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_show ?>' class='btn btn-primary section_button'><?= $url_return ?></a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]) ?>