<?
/**
 * @var $data array
 */

use backend\models\CatalogFoto;

$nameFile = CatalogFoto::upload($data);

if (!empty($nameFile)) {
    $model = new CatalogFoto();
    $model->file = $nameFile;
    $titleFile = str_replace("." . CatalogFoto::getExtensionByMimeType($data['mime_type']), '', $data['title']);
    $model->title = $titleFile;
    $model->position = $data['id'];
    
    if ($model->save()) {
        $id_file = $model->id;
        $url_ext = CatalogFoto::getUrlExtensionByMimeType($data['mime_type']);
        
        ob_start();
        ?>
        <div class='section_task' data-id='<?= $id_file ?>'>
            
        <a href="<?= CatalogFoto::fileSafetyUrl() . $id_file ?>" class='section_task_photo' data-fancybox="gallery">
            <img src="<?= CatalogFoto::fileSafetyUrl() . $id_file ?>">
        </a>
        <div class='section_task_actions'>
        <div class='section_task_delete'></div>
        </div>
        <div class='section_task_title'><?= $titleFile ?></div>
        </div>
        <?php
        $content = ob_get_clean();
        echo $content;
    }
}