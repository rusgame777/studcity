<?
/**
 * @var $data array
 */

use backend\models\CatalogFiles;

$nameFile = CatalogFiles::upload($data);

if (!empty($nameFile)) {
    $model = new CatalogFiles();
    $model->file = $nameFile;
    $titleFile = str_replace("." . CatalogFiles::getExtensionByMimeType($data['mime_type']), '', $data['title']);
    $model->title = $titleFile;
    $model->position = $data['id'];
    
    if ($model->save()) {
        $id_file = $model->id;
        $url_ext = CatalogFiles::getUrlExtensionByMimeType($data['mime_type']);
        
        ob_start();
        ?>
        <div class='section_task' data-id='<?= $id_file ?>'>
        <a href="javascript:void(0);" class='section_task_photo'>
            <img src="<?= $url_ext ?>">
        </a>
        <div class='section_task_actions'>
        <div class='section_task_delete'></div>
        </div>
        <div class='section_task_title'><?= $titleFile ?></div>
        </div>
        <?php
        $content = ob_get_clean();
        echo $content;
    }
}