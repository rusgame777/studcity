<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\CatalogPosition
 *
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\validators\FileValidator;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\Select2Asset;
use backend\assets\CkeditorAsset;
use backend\models\CatalogFiles;
use backend\models\CatalogFoto;
use backend\models\CatalogPositionUpload;
use backend\components\Common;
use backend\components\Notice;
use common\components\Table;
use common\models\CatalogCat;
use common\models\CatalogSubcat;
use common\models\CatalogSubcatCat;
use kartik\select2\Select2;
use common\models\FileInfo;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
Select2Asset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];

$categories = CatalogCat::find()->orderBy(['title' => SORT_ASC])->asArray()->all();
$categories = ArrayHelper::map($categories, 'id', 'title');
$subcatCategories = CatalogSubcat::find()->select([
    CatalogSubcat::tableName() . '.id',
    CatalogSubcat::tableName() . '.title',
])->joinWith('cat')->where([CatalogSubcatCat::tableName() . '.id_cat' => $model->cat])->orderBy(['title' => SORT_ASC])
    ->asArray()->all();
$subcatCategories = ArrayHelper::map($subcatCategories, 'id', 'title');
$modelUpload = new CatalogPositionUpload();

$files = CatalogFiles::find()->where(['position' => $id])->asArray()->all();
$fotos = CatalogFoto::find()->where(['position' => $id])->asArray()->all();
$extensions = Yii::$app->db->createCommand(vsprintf('SELECT * FROM %1$s', [
    1 => Table::get('rb_filter'),
]))->queryAll();
$extensions = ArrayHelper::index($extensions, 'name_ext');
?>
    <input type="hidden" name='id' value="<?= $model->id ?>">
    <div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validateOnBlur' => true,
            'validateOnChange' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <?php Notice::init(); ?>
    
                <?= $form->field($model, 'title')->textInput(); ?>
    
                <?= $form->field($model, 'cat')->widget(Select2::className(), [
                    'data' => $categories,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите дисциплину',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($model->attributeLabels()['cat'], ['class' => 'form_title form_select']); ?>
    
                <?= $form->field($model, 'subcat')->widget(Select2::className(), [
                    'data' => $subcatCategories,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите методичку',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($model->attributeLabels()['subcat'], ['class' => 'form_title form_select']); ?>
    
                <?= $form->field($model, 'type_option')->radioList([
                    1 => 'да',
                    2 => 'нет',
                ], ['class' => 'form_radio']); ?>
                <div id="section_block1" <?php if ($model->type_option == 1) echo "style='display: block;'" ?>>
                    <?= $form->field($model, 'price1')->textInput(); ?>
                    <div class='section_title'>Мультизагрузка файлов с заданиями</div>
                    <div class="drop-files" data-id="file" ondragover="return false">
                        <p>Перетащите файл сюда</p>
                        <div class="section_file2">
                            <button type="button">Выберите файл</button>
                            <div><span>Файл не выбран</span></div>
                            <?= Html::fileInput('file1', '', [
                                'class' => 'uploadbtn',
                                'data' => [
                                    'id' => 'file',
                                ],
                                'multiple' => true,
                                'accept' => implode(', ', array_map(function($v) {
                                    return '.' . $v;
                                }, array_keys(CatalogFiles::mimeType()))),
                            ]) ?>
                        </div>
                    </div>
                    <div class='section_task_block' data-id="file">
                        <?php
                        foreach ($files as $file) {
                            $id_file = $file['id'];
                            $name_ext = FileInfo::getExtension($file['file']);
                            $url_ext = $extensions[$name_ext]['url_ext'];
                            ?>
                            <div class='section_task' data-id='<?= $id_file ?>'>
                                <a href="javascript:void(0);" class='section_task_photo'>
                                    <img src="<?= $url_ext ?>">
                                </a>
                                <div class='section_task_actions'>
                                    <div class='section_task_delete'></div>
                                </div>
                                <div class='section_task_title'><?= $file['title'] ?></div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    
                    <div class='section_title'>Мультизагрузка фотографий с заданиями</div>
                    <div class="drop-files" data-id="foto" ondragover="return false">
                        <p>Перетащите файл сюда</p>
                        <div class="section_file2">
                            <button type="button">Выберите файл</button>
                            <div><span>Файл не выбран</span></div>
                            <?= Html::fileInput('file2', '', [
                                'class' => 'uploadbtn',
                                'data' => [
                                    'id' => 'foto',
                                ],
                                'multiple' => true,
                                'accept' => implode(', ', array_map(function($v) {
                                    return '.' . $v;
                                }, array_keys(CatalogFoto::mimeType()))),
                            ]) ?>
                        </div>
                    </div>
                    <div class='section_task_block' data-id="foto">
                        <?php
                        foreach ($fotos as $file) {
                            $id_file = $file['id'];
                            ?>
                            <div class='section_task' data-id='<?= $id_file ?>'>
                                <a href="<?= CatalogFoto::fileSafetyUrl() .
                                $id_file ?>" class='section_task_photo' data-fancybox="gallery">
                                    <img src="<?= CatalogFoto::fileSafetyUrl() . $id_file ?>">
                                </a>
                                <div class='section_task_actions'>
                                    <div class='section_task_delete'></div>
                                </div>
                                <div class='section_task_title'><?= $file['title'] ?></div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    
                </div>
                <div id="section_block2" <?php if ($model->type_option == 2) echo "style='display: block;'" ?>>
                <?= $form->field($model, 'price2')->textInput(); ?>
                    <div class='section_title' style="margin-top: 0;">
                    Файл с вариантом (в формате <?= implode(', ', array_keys(CatalogPositionUpload::mimeType())) ?>,
                    размер не более
                        <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>
                        )
                     </div>
                    <?php
                    if (is_file(CatalogPositionUpload::filePath() . $model->file)) {
                        ?>
                        <div class="section_doc">
                        <div class="icon">
                            <? echo Html::a('<i class="fa fa-file" aria-hidden="true"></i>', "javascript:void(0);"); ?>
                        </div>
                        <div class="title" title="<?= $model->file ?>">
                            <?
                            echo implode('', [
                                Common::formatLongName($model->file),
                                '<br><span>',
                                Common::formatFileSize(filesize(CatalogPositionUpload::filePath() . $model->file)),
                                '</span>',
                            ]);
                            ?>
                        </div>
                        <div class="delete">
                            <? echo $form->field($modelUpload, 'delete_file', [
                                'options' => ['class' => 'form-group form-checkbox',],
                            ])->checkbox(['class' => 'span-1']); ?>
                        </div>
                    </div>
                        <?php
                    }
                    ?>
                    <div class="section_checkbox">
                    <? echo $form->field($modelUpload, 'file', [
                        'options' => ['class' => 'form-group form-file',],
                    ])->fileInput([
                        'accept' => implode(', ', array_map(function($v) {
                            return '.' . $v;
                        }, array_keys(CatalogPositionUpload::mimeType()))),
                    ])->label(false); ?>
                </div>
                </div>
            </div>

            <div class='section_stripe2'></div>
            <?php
            echo Html::submitButton($submit, [
                'id' => 'submiting',
                'class' => 'btn btn-primary section_submit',
            ]);
            ?>
        </article>
        <?php ActiveForm::end(); ?>
    </div>
<?
$this->registerJsFile('/js/catalog_position_add.js?22', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/js/catalog_position_multiupload.js?22', ['position' => yii\web\View::POS_END]);