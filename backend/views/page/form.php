<?php

use yii\helpers\Url;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\Page;
 *
 */

$request = Yii::$app->request;
$edit = $request->get('edit');
$get = $request->get();
$id = isset($get['id']) ? intval($get['id']) : 0;

$url_module = '/' . $request->getPathInfo();
$url_show = str_replace('form', 'show', $url_module);
$url_validate =  str_replace('form', 'validate', $url_module);
$url_duplicate =  str_replace('form', 'duplicate', $url_module);

$url_return = 'Вернуться к списку';
if ($edit == null) {
    $url_action = str_replace('form', 'create', $url_module);
    $submit = "Добавить";
    $duplicate = '';
    $actions = 'Добавить новую страницу';
} else {
    $url_action = str_replace('form', 'update', $url_module);
    $submit = "Внести правки";
    $duplicate = "Дублировать";
    $actions = "Изменить страницу";
}


$result = [
    'model' => $model,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'url_duplicate' => $url_duplicate,
    'submit' => $submit,
    'duplicate' => $duplicate,
    'id' => $id,
    ];
?>

<header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_show ?>' class='btn btn-primary section_button'><?=$url_return?></a>
        </div>
    </div>
</header>

<?= $this->render('_form', ['result' => $result]) ?>