<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\Page
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\View;
use yii\validators\FileValidator;
use backend\assets\Select2Asset;
use backend\assets\CkeditorAsset;
use backend\components\Common;
use common\components\Table;
use common\models\Formatter;
use common\models\PageCategory;
use backend\components\Notice;
use kartik\select2\Select2;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
//$bundle = Select2Asset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];

$categories = PageCategory::find()->orderBy(['title' => SORT_ASC])->asArray()->all();
$categories = ArrayHelper::map($categories, 'id', 'title');
$indic = $model->indic;
if (($model->nameurl == '') && ($id > 0)) {
    $model->nameurl = '/';
}
?>
    <div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            //         'enableClientValidation' => true,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <? Notice::init(); ?>
    
                <?= $form->field($model, 'title')->textInput(); ?>

                <?
                if ($indic == 0) {
                    echo $form->field($model, 'nameurl')->textInput();
                } else {
                    echo $form->field($model, 'nameurl', ['options' => ['class' => 'form-hidden']])->hiddenInput()
                        ->hint(false)->label(false);
                }
                ?>

                <?= $form->field($model, 'id_category')->widget(Select2::className(), [
                    'data' => $categories,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите раздел',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($model->attributeLabels()['id_category'], ['class' => 'form_title form_select']); ?>
                <?= $form->field($model, 'metatitle')->textInput(); ?>
    
                <?= $form->field($model, 'description')->textarea([
                    'rows' => '4',
                ]); ?>
    
                <?= $form->field($model, 'keywords')->textarea([
                    'rows' => '4',
                ]); ?>
                <div class="section_title"><?= $model->attributeLabels()['content'] ?></div>
                <div class="section_input">
                <?= $form->field($model, 'content', [
                    'options' => [
                        'class' => 'form-group form-wysiwyg',
                    ],
                ])->textarea([])->label(false); ?>
                </div>
                <script>
                    $(function () {
                        var ckeditor = CKEDITOR.replace('content', {
                            customConfig: '/lib/ckeditor/config.js'
                        });
                        AjexFileManager.init({returnTo: 'ckeditor', editor: ckeditor});
                    });
                </script>

            </div>

            <div class='section_stripe2'></div>
            <?php
            echo Html::submitButton($submit, [
                'id' => 'submiting',
                'class' => 'btn btn-primary section_submit',
            ]);

//            if ($id != 0) {
//                echo Html::submitButton($duplicate, [
//                    'id' => 'duplicate',
//                    'class' => 'btn btn-danger section_duplicate',
//                ]);
//            }
            ?>
        </article>
        <?php ActiveForm::end(); ?>
    </div>
<?
$this->registerJsFile('/js/page_add.js', ['position' => yii\web\View::POS_END]);