<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\PageSearch
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use common\models\Page;
use common\models\PageCategory;
use backend\components\Notice;

//$minMaxSortArray = Page::find()->select([
//        'MAX(sort) max_sort',
//        'MIN(sort) min_sort'
//])->asArray()->all();
//$minSort = $minMaxSortArray[0]['min_sort'];
//$maxSort = $minMaxSortArray[0]['max_sort'];
$minSort = Page::find()->min('sort');
$maxSort = Page::find()->max('sort');
$rangeSort = [$minSort, $maxSort];

$request = Yii::$app->request;
$url_module = '/' . $request->getPathInfo();
$url_add = str_replace('show', 'form', $url_module);
?>
    <header>
        <div id='section_header'>
            <div id='section_title'>Текстовые страницы</div>
            <div id='section_url'></div>
        </div>
    </header>
    <article>
        <?= Html::a('<i class="fa fa-plus-circle" aria-hidden="true"></i>', $url_add, [
            'class' => 'add-user',
            'title' => 'Добавить страницу',
        ]) ?>
        
        <?= $this->render('_search', ['model' => $searchModel]) ?>
        <? Notice::init(); ?>
        <div class='kartiktable'>
            <?php
            //            $login = 'itc27';
            //            $identity = Admin::findOne(['login' => $login]);
            //            if (Yii::$app->user->login($identity)) {
            //            var_export(Yii::$app->user->identity, true);
            //            var_export(Yii::$app->user->can('statisticUsers'), true);
            //            }

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => implode(',', [
                    "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                    "input[name='" . $dataProvider->getPagination()->pageParam . "']",
                ]),
                'pager' => [
                    'class' => LinkPager::className(),
                    'template' => '{pageButtons} {customPage} {pageSize}',
                    'pageSizeList' => [10, 20, 50, 100],
                    'customPageWidth' => 50,
                    'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                    'customPageAfter' => "&nbsp;Показывать по&nbsp;",
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'notes',
                    ],
                    'afterGrid' => implode("\r\n", [
                        "<div id='section_actions'>",
                        "<div id='section_delete_all'>",
                        "Удалить выделенные элементы?",
                        "</div>",
                        "</div>",
                    ]),
                ],
                'striped' => false,
                'hover' => true,
                'resizableColumns' => false,
                'rowOptions' => function($model, $key, $index, $grid) {
                    $class = $index % 2 ? '' : 'odd_tr';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class,
                        'id' => $model->id,
                    ];
                },
                'tableOptions' => [
                    'class' => 'kartik_table',
                    'id' => 'table_barrier_block',
                    'style' => 'width: 100%;',
                ],
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'position' => 'fixed',
                ],
                'layout' => "{items}\n{pager}\n{summary}",
                'panel' => [
                    'type' => 'primary',
                ],
                'toolbar' => [
                    '{toggleData}',
                ],
                'toggleDataOptions' => ['minCount' => 0],
                'columns' => [
                    [
                        'attribute' => 'id_category',
                        'width' => '310px',
                        'value' => function($model, $key, $index, $widget) {
                            return $model->categories->title;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(PageCategory::find()->orderBy('title')->asArray()
                            ->all(), 'id', 'title'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => 'выберите категорию'],
                        'group' => true,
                        'groupedRow' => true,
                    ],
                    [
                        'attribute' => 'id',
                        'filterInputOptions' => ['class' => 'form-control form-id'],
                        'contentOptions' => [
                            'class' => 'first_td right_td',
                            'width' => 30,
                        ],
                        'headerOptions' => ['width' => 30],
                    ],
                    [
                        'attribute' => 'name',
                    ],
                    [
                        'attribute' => 'nameurl',
                        'format' => 'text',
                    ],
                    [
                        'attribute' => 'sort',
                        'headerOptions' => [
                            'width' => 40,
                        ],
                        'contentOptions' => [
                            'width' => 40,
                        ],
                        'format' => 'raw',
                        'value' => function($model) {
                            return Html::tag('div', Html::input('text', 'sort', $model->sort), [
                                'class' => 'edit_div',
                            ]);
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'header' => 'Действия',
                        'headerOptions' => [
                            'width' => 70,
                            'class' => 'last_th',
                        ],
                        'contentOptions' => [
                            'width' => 70,
                            'class' => 'last_td middle_td',
                        ],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-pencil-square-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'page/form',
                                    'id' => $model->id,
                                    'edit' => 'ok',
                                ]), [
                                    'class' => 'edit_td',
                                    'data-pjax' => 0,
                                    'title' => 'Редактирование страницы',
                                ]);
                            },
                            'delete' => function($url, $model, $key) {
                                if ($model->indic == 1) {
                                    return null;
                                }
                                return Html::a('<i class="fa fa-trash user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'page/remove',
                                    'id' => $model->id,
                                ]), [
                                    'class' => 'delete_td',
                                    'data-pjax' => 0,
                                    'title' => 'Удаление страницы',
                                    'onclick' => "return confirm('Вы действительно хотите удалить страницу?');",
                                ]);
                            },
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => "40px",
                        'headerOptions' => [
                            'class' => 'delete-all',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td delete',
                        ],
                        'checkboxOptions' => function($model) {
                            if ($model->indic == 1) {
                                return ['disabled' => true];
                            } else {
                                return [];
                            }
                        },
                    ],
                ],
            ]);
            ?>
        </div>

    </article>
<?php
$this->registerJsFile('/js/page_show.js?1', ['position' => yii\web\View::POS_END]);
?>