<?php

$db = Yii::$app->db;
$this->title = 'Система управления содержанием сайта "' . Yii::$app->params['name_site'] . '"';
?>

<div class='auth_fon'>
<div class='auth_panel'>

<div class='auth_info'>
<div class='auth_login'><input placeholder='Логин' class='auth_input' type='text' name='auth_login' autocomplete="off" value=''></div>
<div class='auth_pass'><input placeholder='Пароль' class='auth_input' type='password' name='auth_password' autocomplete="off" value=''></div>
<div class='auth_submit'><div>Авторизация</div></div>
</div>

</div>
</div>