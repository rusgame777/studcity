<?php

/* @var $this yii\web\View */

use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = 'My Yii Application';

$db = Yii::$app->db;
$timezone = Yii::$app->params['timezone'];
$conpos = $db->createCommand("SELECT * FROM users ORDER BY id ASC")->queryAll();

$provider = new ArrayDataProvider([
    'allModels' => $conpos,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => ['id', 'first_name'],
    ],
]);
?>

<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        
        'id',
        'email:ntext',
        'first_name:ntext',
        'last_name:ntext',
        'middle_name:ntext',
        'phone:ntext',
        'country:ntext',
        'region:ntext',
        'city:ntext',
        'organization_type:ntext',
        'organization:ntext',
        'position:ntext',
        
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Действия',
            'headerOptions' => ['width' => '100'],
            'template' => '{view} {update} {delete}{link}',
        ],
    ],
]); ?>