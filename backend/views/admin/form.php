<?php

use yii\helpers\Url;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\Admin;
 * @var $modelCat backend\models\UserCat
 * @var $catalogCat array
 * @var $solutionCat array
 *
 */
$edit = Yii::$app->request->get('edit');
$id = !empty(Yii::$app->request->get('id')) ? intval(Yii::$app->request->get('id')) : 0;

$url_module = '/' . Yii::$app->request->getPathInfo();
$url_show = str_replace('form', 'show', $url_module);
$url_validate = str_replace('form', 'validate', $url_module);

$url_return = 'Вернуться к списку';
if ($edit == null) {
    $model->loadDefaultValues();
    $url_action = str_replace('form', 'create', $url_module);
    $submit = "Добавить";
    $actions = 'Добавить нового пользователя';
} else {
    $url_action = str_replace('form', 'update', $url_module);
    $submit = "Внести правки";
    $actions = "Изменить пользователя";
}

$result = [
    'model' => $model,
    'modelCat' => $modelCat,
    'catalogCat' => $catalogCat,
    'solutionCat' => $solutionCat,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
    'id' => $id,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_show ?>' class='btn btn-primary section_button'><?= $url_return ?></a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]) ?>