<?php

use common\components\Table;

$db = Yii::$app->db;

class chunkReadFilter implements PHPExcel_Reader_IReadFilter
{
    private $_startRow = 0;
    private $_endRow = 0;

    public function setRows($startRow, $chunkSize) {
        $this->_startRow = $startRow;
        $this->_endRow = $startRow + $chunkSize;
    }

    public function readCell($column, $row, $worksheetName = '') {
        //  Only read the heading row, and the rows that are configured in $this->_startRow and $this->_endRow
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}

function RemoveDir($path) {
    if (file_exists($path) && is_dir($path)) {
        $dirHandle = opendir($path);
        while (false !== ($file = readdir($dirHandle))) {
            if ($file != '.' && $file != '..') {
                $tmpPath = $path . '/' . $file;
                chmod($tmpPath, 0644);
                if (is_dir($tmpPath)) RemoveDir($tmpPath); elseif (file_exists($tmpPath)) unlink($tmpPath);
            }
        }
        closedir($dirHandle);
        if (file_exists($path)) rmdir($path);
    }
}

$uploaddir = Yii::getAlias('@backendDocroot/uploads/barriers/');
$array_file = array();
if (file_exists($uploaddir) && is_dir($uploaddir)) {
    $dirHandle = opendir($uploaddir);
    while (false !== ($file = readdir($dirHandle))) {
        if ($file != '.' && $file != '..') {
            $tmpPath = $uploaddir . $file;
            if ((!is_dir($tmpPath)) && (preg_match("/^[\w]+.xlsx$/", $file))) {
                $array_file[] = $file;
            }
        }
    }
    closedir($dirHandle);
}

asort($array_file);

$name_file = $array_file[0];
if ($name_file == null) return true;
$filename = $uploaddir . $name_file;

if (file_exists($filename)) {
    $finish = 0;
    $coord_y = 2;
    $startRow = 2;
    $chunkSize = 100;

    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
    $cacheSettings = array('memoryCacheSize' => '32MB');
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
    $objReader = PHPExcel_IOFactory::createReaderForFile($filename);
    /**
     * @var $objReader PHPExcel_Reader_Abstract
     */
    $chunkFilter = new chunkReadFilter();
    $format = 'd.m.Y';
    $pattern_date = "/^(0?[1-9]|[12][0-9]|3[01])[\/\-\.](0?[1-9]|1[012])[\/\-\.]\d{4}$/i";

    while ($finish == 0) {
        $chunkFilter->setRows($startRow, $chunkSize);
        $objReader->setReadFilter($chunkFilter);
        $objReader->setReadDataOnly(false);
        $objPHPExcel = $objReader->load($filename);
        $objPHPExcel->setActiveSheetIndex(0);
        $aSheet = $objPHPExcel->getActiveSheet();

        while ($coord_y < ($startRow + $chunkSize)) {
            $cell = "A" . $coord_y;
            $data['id_reporter'] =  trim($aSheet->getCell($cell)->getValue());
            if ($data['id_reporter'] == '') {
                $finish = 1;
                break;
            }
            $cell = "D" . $coord_y;
            $data['id'] = trim($aSheet->getCell($cell)->getValue());
            $position = $db->createCommand(vsprintf('SELECT TOP 1 id FROM %1$s WHERE id=:id', [
                1 => Table::get('[site].[barriers]'),
            ]))->bindValues([':id' => $data['id']])->queryOne();
            if (empty($position)) {
                $cell = "B" . $coord_y;
                $data['id_barriers_types'] = trim($aSheet->getCell($cell)->getValue());
                $cell = "C" . $coord_y;
                $data['id_barriers_types_wits'] = trim($aSheet->getCell($cell)->getValue());
                $cell = "G" . $coord_y;
                $data['text'] = trim($aSheet->getCell($cell)->getValue());
                $cell = "H" . $coord_y;
                $data['regulation_title'] = trim($aSheet->getCell($cell)->getValue());

                $cell = "I" . $coord_y;
                $data['date_investigation'] = trim($aSheet->getCell($cell)->getValue());
                if (PHPExcel_Shared_Date::isDateTime($aSheet->getCell($cell))) {
                    $data['date_investigation'] = date($format,
                        PHPExcel_Shared_Date::ExcelToPHP($data['date_investigation']));
                }
                $date = new DateTime($data['date_investigation']);
                if (($data['date_investigation'] != '') && preg_match($pattern_date,
                        $data['date_investigation']) && checkdate($date->format('n'), $date->format('j'),
                        $date->format('Y'))
                ) {
                    $data['date_investigation'] = $date->format('Y-m-d');
                } else {
                    $data['date_investigation'] = '1900-01-01';
                }

                $cell = "J" . $coord_y;
                $data['date_start'] = intval($aSheet->getCell($cell)->getValue());
                if (PHPExcel_Shared_Date::isDateTime($aSheet->getCell($cell))) {
                    $data['date_start'] = date($format, PHPExcel_Shared_Date::ExcelToPHP($data['date_start']));
                    $date = new DateTime($data['date_start']);
                    if (($data['date_start'] != '') && preg_match($pattern_date,
                            $data['date_start']) && checkdate($date->format('n'), $date->format('j'),
                            $date->format('Y'))
                    ) {
                        $data['date_start'] = $date->format('Y-m-d');
                    } else {

                        $data['date_start'] = '1900-01-01';
                    }
                } else {
                    $data['date_start'] = '1900-01-01';
                }

                $cell = "K" . $coord_y;
                $data['date_end'] = $aSheet->getCell($cell)->getValue();
                $date = new DateTime($data['date_end']);
                if (($data['date_end'] != '') && preg_match($pattern_date,
                        $data['date_end']) && checkdate($date->format('n'), $date->format('j'), $date->format('Y'))
                ) {
                    $data['date_end'] = $date->format('Y-m-d');
                } else {
                    $data['date_end'] = '1900-01-01';
                }
                $array = [
                    'id_reporter',
                    'id_barriers_types',
                    'id_barriers_types_wits',
                    'text',
                    'regulation_title',
                    'id_barriers_types',
                    'id_barriers_types',
                ];
                $insertValues = [];
//                $db->createCommand();
            }
            $hs_fts = trim($aSheet->getCell($cell)->getValue());
            //$price = trim($aSheet->getCell($cell)->getValue());
//            $position = $db->createCommand("SELECT id FROM catalog_position WHERE article=:article LIMIT 1")->bindValues(['article' => $article])->queryOne();
//            if (empty($position) == false)
//            {
//                $id = $position['id'];
//                echo $price."<br>";
//                $db->createCommand()->update('catalog_position', ['price' => $price], 'id=:id')->bindValues(['id' => $id])->execute();
//            }
            $coord_y++;
        }

        $startRow += $chunkSize;
        unset($aSheet);
        $objPHPExcel->disconnectWorksheets();
        unset($objPHPExcel);
        if ($finish == 1) break;
    }
    unset($objReader);
}

#RemoveDir($filename);