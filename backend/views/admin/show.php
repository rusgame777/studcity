<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\AdminSearch
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use backend\components\Notice;

$request = Yii::$app->request;
$url_module = '/' . $request->getPathInfo();
$url_add = str_replace('show', 'form', $url_module);

?>
    <header>
        <div id='section_header'>
            <div id='section_title'>Рабочая группа</div>
            <div id='section_url'></div>
        </div>
    </header>
    <article>
        <?= Html::a('<i class="fa fa-plus-circle" aria-hidden="true"></i>', $url_add, [
            'class' => 'add-user',
            'title' => 'Добавить пользователя',
        ]) ?>
        
        <?= $this->render('_search', ['model' => $searchModel]) ?>
        <?php Notice::init(); ?>
        <div class='kartiktable'>
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'filterSelector' => implode(',', [
                    "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                    "input[name='" . $dataProvider->getPagination()->pageParam . "']",
                ]),
                'pager' => [
                    'class' => LinkPager::className(),
                    'template' => '{pageButtons} {customPage} {pageSize}',
                    'pageSizeList' => [10, 20, 50, 100],
                    'customPageWidth' => 50,
                    'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                    'customPageAfter' => "&nbsp;Показывать по&nbsp;",
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'id' => 'notes',
                    ],
                    'afterGrid' => implode("\r\n", [
                        "<div id='section_actions'>",
                        "<div id='section_delete_all'>",
                        "Удалить выделенные элементы?",
                        "</div>",
                        "</div>",
                    ]),
                ],
                'striped' => false,
                'hover' => true,
                'resizableColumns' => false,
                'rowOptions' => function($model, $key, $index, $grid) {
                    $class = $index % 2 ? '' : 'odd_tr';
                    return [
                        'key' => $key,
                        'index' => $index,
                        'class' => $class,
                        'id' => $model->id,
                    ];
                },
                'tableOptions' => [
                    'class' => 'kartik_table',
                    'id' => 'table_barrier_block',
                    'style' => 'width: 100%;',
                ],
                'floatHeader' => true,
                'floatHeaderOptions' => [
                    'position' => 'fixed',
                ],
                'layout' => "{items}\n{pager}\n{summary}",
                'panel' => [
                    'type' => 'primary',
                ],
                'toolbar' => [
                    '{toggleData}',
                ],
                'toggleDataOptions' => ['minCount' => 0],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'filterInputOptions' => ['class' => 'form-control form-id'],
                        'contentOptions' => [
                            'class' => 'first_td right_td',
                            'width' => 30,
                        ],
                        'headerOptions' => ['width' => 30],
                    ],
                    [
                        'attribute' => 'name',
                        'value' => function($model) {
                            return $model->name;
                        },
                    ],
                    [
                        'attribute' => 'login',
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'html',
                        'contentOptions' => [
                            'class' => 'email_td',
                            'width' => 150,
                        ],
                        'value' => function($model) {
                            return "<a href='mailto:" . $model->email . "' title='" . $model->email . "'>" . $model->email . "<span></span></a>";
                        },
                        'headerOptions' => ['width' => 150],
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'header' => 'Действия',
                        'width' => '70px',
                        'headerOptions' => [
                            'class' => 'last_th',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td middle_td',
                        ],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-pencil-square-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                    'admin/form',
                                    'id' => $model->id,
                                    'edit' => 'ok',
                                ]), [
                                    'class' => 'edit_td',
                                    'data-pjax' => 0,
                                    'title' => 'Редактирование пользователя',
                                ]);
                            },
                            'delete' => function($url, $model, $key) {
                                if ($model->id > 2) {
                                    return Html::a('<i class="fa fa-trash user-table--actions" aria-hidden="true"></i>', Url::to([
                                        'admin/remove',
                                        'id' => $model->id,
                                    ]), [
                                        'class' => 'delete_td',
                                        'data-pjax' => 0,
                                        'title' => 'Удаление пользователя',
                                        'onclick' => "return confirm('Вы действительно хотите удалить пользователя?');",
                                    ]);
                                } else {
                                    return null;
                                }
                            },
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\CheckboxColumn',
                        'width' => "40px",
                        'headerOptions' => [
                            'class' => 'delete-all',
                        ],
                        'contentOptions' => [
                            'class' => 'last_td delete',
                        ],
                        'checkboxOptions' => function($model) {
                            if ($model->id <= 2) {
                                return ['disabled' => true];
                            }else{
                                return [];
                            }
                        },
                    ],
                ],
            ]);
            ?>
        </div>

    </article>
<?php
$this->registerJsFile('/js/admin_show.js', ['position' => yii\web\View::POS_END]);