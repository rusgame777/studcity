<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\Admin
 * @var $modelCat backend\models\UserCat
 * @var $catalogCat array
 * @var $solutionCat array
 *
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\components\Notice;

$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];
?>
    <div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
        <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <?php Notice::init(); ?>

                <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                <?= $form->field($model, 'login')->textInput(); ?>
                <?= $form->field($model, 'name')->textInput(); ?>
                <?= $form->field($model, 'email')->textInput(); ?>
                <?= $form->field($model, 'password')->textInput(); ?>
                <?php
                if ($id == 0 || $id > 2) {
                    echo $form->field($modelCat, 'catalog_cat')
                        ->checkboxList($catalogCat, ['class' => 'form_checkList']);
                    echo $form->field($modelCat, 'solution_cat')
                        ->checkboxList($solutionCat, ['class' => 'form_checkList']);
                }
                ?>
            </div>

            <div class='section_stripe2'></div>
            <?php
            echo Html::submitButton($submit, [
                'id' => 'submiting',
                'class' => 'btn btn-primary section_submit',
            ]);
            ?>
        </article>
        <?php ActiveForm::end(); ?>
    </div>
<?
$this->registerJsFile('/js/admin_add.js', ['position' => yii\web\View::POS_END]);