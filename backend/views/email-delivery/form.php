<?php

use yii\helpers\Url;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\EmailDelivery;
 *
 */

$url_action = Url::to(['email-delivery/create']);
$url_validate = Url::to(['email-delivery/validate']);

$submit = "Загрузить";
$actions = 'Рассылка';

$result = [
    'model' => $model,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'></div>
    </div>
    </header>
<?= $this->render('_form', ['result' => $result]);