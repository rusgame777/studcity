<?php

/**
 *
 * @var $this yii\web\View
 * @var $result
 * @var $submit
 * @var $duplicate
 * @var $model common\models\EmailDelivery
 *
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\validators\FileValidator;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\Select2Asset;
use backend\components\Notice;
use common\models\EmailDelivery;
use common\models\EmailTemplate;
use kartik\select2\Select2;

Select2Asset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);

$templates = EmailTemplate::find()->asArray()->all();
$templates = ArrayHelper::map($templates, 'id', 'title');

$fileUrl = EmailDelivery::fileUrl();
$filePath = EmailDelivery::filePath();

$db = Yii::$app->db;
extract($result, EXTR_OVERWRITE);
/**
 * @var $submit string
 * @var $id integer
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];
$radio_arr = [
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_radio'],
];


//$cat = 2;
//$template = EmailTemplate::findOne($cat);
//$uploadFile = $filePath . '1.xls';
//EmailDelivery::generateContent($template, $uploadFile);
?>
<div class="post-form">
    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'action' => [
            $url_action,
        ],
        'fieldConfig' => $form_arr,
                 'enableClientValidation' => true,
//        'enableClientValidation' => false,
//        'enableAjaxValidation' => true,
        'validationUrl' => $url_validate,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <article>
        <div class='section_stripe'></div>
        <div class='section_info'>
            <?php Notice::init(); ?>

            <?= $form->field($model, 'cat')->widget(Select2::className(), [
                'data' => $templates,
                'language' => 'ru',
                'options' => [
                    'placeholder' => 'Выберите шаблон',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])->label($model->getAttributeLabel('cat'), ['class' => 'form_title form_select']); ?>
            <div class='section_title' style="margin-top: 0;">
                    <? echo $model->getAttributeLabel('file'); ?>
                &nbsp;в формате xls, xlsx;
                размер не более
                <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>
                )
                </div>

                <div class="section_checkbox">
                <?php
                echo $form->field($model, 'file', [
                    'options' => ['class' => 'form-group form-file',],
                ])->fileInput(['accept' => '.xls, .xlsx'])->label(false); ?>
                </div>

        </div>

        <div class='section_stripe2'></div>
        <?php
        echo Html::submitButton($submit, [
            'id' => 'submiting',
            'class' => 'btn btn-primary section_submit',
        ]);
        ?>
    </article>
    <?php ActiveForm::end(); ?>
</div>