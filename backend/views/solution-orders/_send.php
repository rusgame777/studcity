<?php

/**
 * @var $this yii\web\View
 * @var $result array
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\components\Notice;
use backend\models\OrdersUpload;
use backend\models\OrderSendForm;
use common\components\Table;
use common\models\FileInfo;

$fileUrlFile = OrdersUpload::fileSafetyUrl();
$filePathFile = OrdersUpload::filePath();

$extensions = Yii::$app->db->createCommand(vsprintf('SELECT * FROM %1$s', [
    1 => Table::get('rb_filter'),
]))->queryAll();
$extensions = ArrayHelper::index($extensions, 'name_ext');

extract($result, EXTR_OVERWRITE);
/**
 * @var $model common\models\Orders
 * @var $files array
 * @var $submit string
 * @var $id integer
 */

$orderSendForm = new OrderSendForm();
$orderSendForm->number = count($files);

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];


?>
<div class="post-form">
    <?php $form = ActiveForm::begin([
        'id' => 'dynamic-form',
        'action' => [
            $url_action,
            'id' => $model->id,
        ],
        'fieldConfig' => $form_arr,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validationUrl' => $url_validate,
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <article>
        <div class='section_stripe'></div>
        <div class='section_info'>
            <?php Notice::init(); ?>
            <input type="hidden" id="id" name='id' value="<?= $model->id ?>">
            <div class='section_title'>Мультизагрузка файлов с заданиями</div>
            <div id="drop-files" ondragover="return false">
                <p>Перетащите файл сюда</p>
                <form id="frm">
                    <div class="section_file2">
                        <button type="button">Выберите файл</button>
                        <div><span>Файл не выбран</span></div>
                        <?= Html::fileInput('file1', '', [
                            'id' => 'uploadbtn',
                            'multiple' => true,
                            'accept' => implode(', ', array_map(function($v) {
                                return '.' . $v;
                            }, array_keys(OrdersUpload::mimeType()))),
                        ]) ?>
                    </div>
                </form>
            </div>

            <div id='section_task'>
                <?php
                foreach ($files as $file) {
                    $id_file = $file['id'];
                    $name_ext = FileInfo::getExtension($file['file']);
                    $url_ext = $extensions[$name_ext]['url_ext'];
                    ?>
                    <div class='section_task' data-id='<?= $id_file ?>'>
                        <a href="javascript:void(0);<?#= $fileUrlFile . $id_file ?>" target="_blank" class='section_task_photo'>
                            <img src="<?= $url_ext ?>">
                        </a>
                        <div class='section_task_actions'>
                            <div class='section_task_delete'></div>
                        </div>
                        <div class='section_task_title'><?= $file['title'] ?></div>
                    </div>
                    <?php
                }
                ?>
            </div>
            
            <?=$form->field($orderSendForm, 'number')->hiddenInput()->hint(false)->label(false); ?>
            <div
        </div>
        <div class='section_stripe2'></div>
        <?php
        echo Html::submitButton($submit, [
            'id' => 'submiting',
            'class' => 'btn btn-primary section_submit',
        ]);
        ?>
    </article>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJsFile('/js/solution_order_send.js', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/js/solution_order_multiupload.js?4', ['position' => yii\web\View::POS_END]);
?>
