<?php
/**
 * @var $this yii\web\View
 * @var $model backend\models\OrdersSearch
 * @var $form yii\widgets\ActiveForm
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use common\models\Author;

$authors = Author::find()->orderBy(['name' => SORT_ASC])->asArray()->all();
$authors = ArrayHelper::map($authors, 'id', 'name');

?>
<div class="post-search">
    <?php Pjax::begin(['id' => 'search']) ?>
    <?php $form = ActiveForm::begin([
        'action' => ['statistics'],
        'method' => 'get',
        'options' => ['data-pjax' => true, 'id' => 'search_form'],
    ]); ?>
    <div class="filter">

        <div class="row">
            <div class="col-xs-4">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите дату:</div>
                <div class="statistic_filter_date1">
                    <?= $form->field($model, 'dateStart')->hint('')->label(false)->widget(DatePicker::classname(), [
                        'language' => 'ru',
                        'dateFormat' => 'dd.MM.yyyy',
                        'options' => ['placeholder' => 'от'],
                    ]) ?>
                </div>
                <div class="statistic_filter_date2">
                    <?= $form->field($model, 'dateEnd')->hint('')->label(false)->widget(DatePicker::classname(), [
                        'language' => 'ru',
                        'dateFormat' => 'dd.MM.yyyy',
                        'options' => ['placeholder' => 'до'],
                    ]) ?>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите автора:</div>
                <div class="statistic_filter_list">
                <?= $form->field($model, 'id_author')->checkboxList($authors, ['class' => 'form_checkList'])
                    ->label(false); ?>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-1">
                <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>