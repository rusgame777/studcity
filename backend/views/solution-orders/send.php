<?php

use yii\helpers\Url;
use common\models\SolutionCat;
use common\models\SolutionPosition;
use common\models\SolutionOrderDesc;
use common\models\Orders;
use backend\models\OrdersFiles;
use backend\components\Common;
use common\models\OrderType;
use common\models\OrderStatus;

/**
 *
 * @var $this yii\web\View
 * @var $model common\models\Orders;
 *
 */

$get = Yii::$app->request->get();
$id = isset($get['id']) ? intval($get['id']) : 0;

$url_validate = Url::to(['solution-orders/send-validate']);
$url_return = Url::to(['solution-orders/management']);
$url_action = Url::to(['solution-orders/send-submit']);

//$date_add = Yii::$app->formatter->asDate($model->date_end, 'php:Y-m-d H:i:s');
//$date_end = Yii::$app->formatter->asDate($date_add, 'php:Y-m-d');
$date_add = Common::getDateTime($model->date_end, 'Y-m-d H:i:s');
$date_end = Common::getDateTime($date_add, 'Y-m-d');
$num_order = Orders::find()->where([
    'and',
    ['id_type' => OrderType::solution],
    ['between', 'date_end', $date_end, $date_add],
    ['send' => 1],
    ['status' => [OrderStatus::paid, OrderStatus::send]],
])->count();
//$num_order .= '/' . Yii::$app->formatter->asDate($date_add, 'php:d.m.Y');
$num_order .= '/' . Common::getDateTime($date_add, 'd.m.Y');

if (($orderdesc = SolutionOrderDesc::findOne(['id_order' => $id])) === null) {
    $orderdesc = new SolutionOrderDesc();
}
$id_position = $orderdesc->id_position;
$cat = $orderdesc->cat;

if (($position = SolutionPosition::findOne($id_position)) === null) {
    $position = new SolutionPosition();
}
$name_position = !empty($position->title) ? $position->title : '';

if (($category = SolutionCat::findOne($cat)) === null) {
    $category = new SolutionCat();
}
$name_cat = !empty($category->title) ? $category->title : '';
$files = OrdersFiles::find()->where(['id_order' => $id])->asArray()->all();

$submit = "Отправить РГР";
$actions = implode('', [
    "Отправка РГР \"",
    $name_cat,
    !empty($name_cat) ? "/" : "",
    $name_position,
    "\", заказ РМ",
    $num_order,
]);

$result = [
    'model' => $model,
    'files' => $files,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
    'id' => $id,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_return ?>' class='btn btn-primary section_button'>Вернуться к списку</a>
        </div>
    </div>
</header>
<?= $this->render('_send', ['result' => $result]) ?>