<?php
/**
 * @var $this yii\web\View
 * @var $model backend\models\SolutionOrdersManagement
 * @var $form yii\widgets\ActiveForm
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use yii\db\Expression;
use backend\assets\Select2Asset;
use common\models\Author;
use common\models\SolutionCat;
use common\models\UserSolution;
use kartik\select2\Select2;

Select2Asset::register(Yii::$app->view);
$authors = Author::find()->orderBy(['name' => SORT_ASC])->asArray()->all();
$authors = ArrayHelper::map($authors, 'id', 'name');

$filterCat[] = 'and';
if (Yii::$app->view->params['id_role'] == 1) {
    $filterCat[] = [
        'in',
        'id',
        UserSolution::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
    ];
}
$categories =
    SolutionCat::find()->where($filterCat)->orderBy(['sort' => SORT_ASC, 'title' => SORT_ASC])->asArray()->all();
$categories = ArrayHelper::map($categories, 'id', 'title');

?>
<div class="post-search">
    <?php Pjax::begin(['id' => 'search']) ?>
    <?php $form = ActiveForm::begin([
        'action' => ['management'],
        'method' => 'get',
        'options' => ['data-pjax' => true, 'id' => 'search_form'],
    ]); ?>
    <div class="filter">
        <div class="row">
            <div class="col-xs-4">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите дату:</div>
                <div class="statistic_filter_date1">
                    <?= $form->field($model, 'dateStart')->hint('')->label(false)->widget(DatePicker::classname(), [
                        'language' => 'ru',
                        'dateFormat' => 'dd.MM.yyyy',
                        'options' => ['placeholder' => 'от'],
                    ]) ?>
                </div>
                <div class="statistic_filter_date2">
                    <?= $form->field($model, 'dateEnd')->hint('')->label(false)->widget(DatePicker::classname(), [
                        'language' => 'ru',
                        'dateFormat' => 'dd.MM.yyyy',
                        'options' => ['placeholder' => 'до'],
                    ]) ?>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите автора:</div>
                <div class="statistic_filter_select">
                    <?= $form->field($model, 'id_author')->widget(Select2::className(), [
                        'data' => $authors,
                        'language' => 'ru',
                        'options' => [
                            'placeholder' => 'Выберите автора',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label(false); ?>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <div class="statistic_filter">
                <div class="statistic_filter_title">Выберите предмет:</div>
                <div class="statistic_filter_select">
                    <?= $form->field($model, 'cat')->widget(Select2::className(), [
                        'data' => $categories,
                        'language' => 'ru',
                        'options' => [
                            'placeholder' => 'Выберите предмет',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label(false); ?>
                </div>
                </div>
            </div>
            <div class="col-xs-1">
                <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>