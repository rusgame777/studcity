<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\SolutionOrdersManagement
 * @var $model backend\models\SolutionOrdersManagement
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use backend\models\SolutionOrdersManagement;
use backend\components\Notice;
use backend\components\Common;

$request = Yii::$app->request;
$url_module = '/' . $request->getPathInfo();
$url_add = str_replace('show', 'form', $url_module);
$numberList = SolutionOrdersManagement::getNumberList();
?>
<header>
    <div id='section_header'>
        <div id='section_title'>Список</div>
        <div id='section_url'></div>
    </div>
</header>
<article>
    <?= $this->render('_search_management', ['model' => $searchModel]) ?>
    <?php Notice::init(); ?>
    <div class='kartiktable'>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => implode(',', [
                "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                "input[name='" . $dataProvider->getPagination()->pageParam . "']",
            ]),
            'pager' => [
                'class' => LinkPager::className(),
                'template' => '{pageButtons} {customPage} {pageSize}',
                'pageSizeList' => [10, 20, 50, 100],
                'customPageWidth' => 50,
                'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                'customPageAfter' => "&nbsp;Показывать по&nbsp;",
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'id' => 'notes',
                ],
                'afterGrid' => implode("\r\n", [
                    "<div id='section_actions'>",
                    "<div id='section_delete_all'>",
                    "Удалить выделенные элементы?",
                    "</div>",
                    "</div>",
                ]),
            ],
            'striped' => false,
            'hover' => true,
            'resizableColumns' => false,
            'rowOptions' => function($model, $key, $index, $grid) {
                $class = $index % 2 ? '' : 'odd_tr';
                return [
                    'key' => $key,
                    'index' => $index,
                    'class' => $class,
                    'id' => $model->id,
                ];
            },
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => "&mdash;",
            ],
            'tableOptions' => [
                'class' => 'kartik_table',
                'id' => 'table_barrier_block',
                'style' => 'width: 100%;',
            ],
            'floatHeader' => true,
            'floatHeaderOptions' => [
                'position' => 'fixed',
            ],
            'layout' => "{items}\n{pager}\n{summary}",
            'panel' => [
                'type' => 'primary',
            ],
            'toolbar' => [
                '{toggleData}',
            ],
            'toggleDataOptions' => ['minCount' => 0],
            'columns' => [
                [
                    'attribute' => 'id',
                    'filterInputOptions' => ['class' => 'form-control form-id'],
                    'contentOptions' => [
                        'class' => 'first_td right_td',
                        'width' => 100,
                    ],
                    'headerOptions' => [
                        'width' => 100,
                    ],
                    'value' => function($model) use ($numberList) {
                        $numberItem = array_key_exists($model->id, $numberList) ? $numberList[$model->id] : [];
                        return !empty($numberItem) ? implode('/', [
                            $numberItem['count'],
                            $numberItem['date_end'] . '(' . $numberItem['id'] . ')',
                        ]) : null;
                    },
                ],
                [
                    'attribute' => 'date_end',
                    'format' => 'raw',
                    'value' => function($model) {
//                        return Yii::$app->formatter->asDate($model->date_end, 'php:H:i');
                        return Common::getDateTime($model->date_end, 'H:i');
                    },
                    'headerOptions' => [
                        'width' => 110,
                        'class' => 'middle_th',
                    ],
                    'contentOptions' => [
                        'width' => 110,
                        'class' => 'middle_td',
                    ],
                ],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'headerOptions' => [
                        'width' => 110,
                    ],
                    'contentOptions' => [
                        'width' => 110,
                    ],
                ],
                [
                    'attribute' => 'contacts',
                    'format' => 'raw',
                    'value' => function($model) {
                        return implode("<br>", array_filter([
                            $model->phone,
                            !empty($model->email) ? "(" . $model->email . ")" : '',
                        ]));
                    },
                    'headerOptions' => [
                        'width' => 250,
                    ],
                    'contentOptions' => [
                        'width' => 250,
                    ],
                ],
                [
                    'attribute' => 'name_position',
                    'format' => 'ntext',
                ],
                [
                    'attribute' => 'name_author',
                    'format' => 'ntext',
                    'headerOptions' => [
                        'width' => 110,
                    ],
                    'contentOptions' => [
                        'width' => 110,
                    ],
                ],
                [
                    'attribute' => 'sum',
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'contentOptions' => [
                        'width' => 50,
                    ],
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->sum . " руб.";
                    },
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'header' => 'Действия',
                    'width' => '100px',
                    'headerOptions' => [
                        'class' => 'last_th',
                    ],
                    'contentOptions' => [
                        'class' => 'last_td middle_td',
                    ],
                    'template' => '{update}{envelope}{delete}',
                    'buttons' => [
                        'update' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil-square-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                'solution-orders/form',
                                'id' => $model->id,
                                'edit' => 'ok',
                            ]), [
                                'class' => 'edit_td',
                                'data-pjax' => 0,
                                'title' => 'Редактирование заказа',
                            ]);
                        },
                        'envelope' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-envelope-o user-table--actions" aria-hidden="true"></i>', Url::to([
                                'solution-orders/send',
                                'id' => $model->id,
                            ]), [
                                'class' => 'edit_td',
                                'data-pjax' => 0,
                                'title' => 'Отправить РГР',
                            ]);
                        },
                        'delete' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash user-table--actions" aria-hidden="true"></i>', Url::to([
                                'solution-orders/remove',
                                'id' => $model->id,
                            ]), [
                                'class' => 'delete_td',
                                'data-pjax' => 0,
                                'title' => 'Удаление заказа',
                                'onclick' => "return confirm('Вы действительно хотите удалить заказ?');",
                            ]);
                        },
                    ],
                ],
                [
                    'class' => 'kartik\grid\CheckboxColumn',
                    'width' => "40px",
                    'headerOptions' => [
                        'class' => 'delete-all',
                    ],
                    'contentOptions' => [
                        'class' => 'last_td delete',
                    ],
                ],
            ],
        ]);
        ?>
    </div>

</article>
<?php
$this->registerJsFile('/js/ordersSolutionManagement.js?2', ['position' => yii\web\View::POS_END]);
?>
