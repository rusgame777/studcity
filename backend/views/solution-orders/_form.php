<?php

/**
 * @var $this yii\web\View
 * @var $result
 */

use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\validators\FileValidator;
use yii\widgets\ActiveForm;
use yii\web\View;
use backend\assets\Select2Asset;
use backend\assets\CkeditorAsset;
use backend\components\Common;
use backend\components\Notice;
use backend\models\OrdersUpload;
use common\models\Clients;
use common\models\FileInfo;
use common\models\SolutionCat;
use common\models\SolutionData;
use common\models\SolutionOrderData;
use common\models\SolutionPosition;
use common\models\SolutionOrderDesc;
use common\components\Table;
use kartik\select2\Select2;

$bundle_ckeditor = CkeditorAsset::register(Yii::$app->view);
Select2Asset::register(Yii::$app->view);
$this->registerJsFile('/lib/ajexfilemanager/ajex.js', ['position' => View::POS_END]);


$extensions = Yii::$app->db->createCommand(vsprintf('SELECT * FROM %1$s', [
    1 => Table::get('rb_filter'),
]))->queryAll();
$extensions = ArrayHelper::index($extensions, 'name_ext');

$fileUrlDoc = OrdersUpload::docSafetyUrl();
$fileUrl = OrdersUpload::fileUrl();
$filePath = OrdersUpload::filePath();

extract($result, EXTR_OVERWRITE);
/**
 * @var $submit
 * @var $duplicated
 * @var $files
 * @var $id
 * @var $model common\models\Orders
 */

$form_arr = [
    'options' => ['class' => 'form-group'],
    'errorOptions' => ['class' => 'form_error'],
    'labelOptions' => ['class' => 'form_title'],
    'inputOptions' => ['class' => 'form_input'],
];

if (($modelOrderDesc = SolutionOrderDesc::findOne(['id_order' => $model->id])) === null) {
    $modelOrderDesc = new SolutionOrderDesc();
}
$cat = $modelOrderDesc->cat;
$id_position = $modelOrderDesc->id_position;
if (($modelClient = Clients::findOne($model->id_user)) === null) {
    $modelClient = new Clients();
}
$modelUpload = new OrdersUpload();

$categories = SolutionCat::find()->orderBy(['title' => SORT_ASC])->asArray()->all();
$categories = ArrayHelper::map($categories, 'id', 'title');

$positions = SolutionPosition::find()->where(['cat' => $cat])->orderBy(['title' => SORT_ASC])->asArray()->all();
$positions = ArrayHelper::map($positions, 'id', 'title');

$solutionParams = [];
if ($id != 0) {
    $solutionParams = SolutionOrderData::find()->where(['id_order' => $id])->orderBy(['id' => SORT_ASC])->all();
}
if (empty($solutionParams)) {
    $solutionParams =
        SolutionData::find()->select([new Expression("'' AS `value`"), new Expression("'0' AS `id`"), 'name'])
            ->where(['id_position' => $id_position])->orderBy(['id' => SORT_ASC])->all();
}

?>
<div class="post-form">
        <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'action' => [
                $url_action,
                'id' => $model->id,
            ],
            'fieldConfig' => $form_arr,
            'enableClientValidation' => true,
            'enableAjaxValidation' => true,
            'validationUrl' => $url_validate,
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>
    <article>
            <div class='section_stripe'></div>
            <div class='section_info'>
                <?php Notice::init(); ?>
                <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>
                <?= $form->field($modelClient, 'phone')->textInput(); ?>
                <?= $form->field($modelClient, 'name')->textInput(); ?>
                <?= $form->field($modelClient, 'email')->textInput(); ?>
    
                <?= $form->field($modelOrderDesc, 'cat')->widget(Select2::className(), [
                    'data' => $categories,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите дисциплину',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($modelOrderDesc->getAttributeLabel('cat'), ['class' => 'form_title form_select']); ?>
    
                <?= $form->field($modelOrderDesc, 'id_position')->widget(Select2::className(), [
                    'data' => $positions,
                    'language' => 'ru',
                    'options' => [
                        'placeholder' => 'Выберите методичку',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])->label($modelOrderDesc->getAttributeLabel('id_position'), ['class' => 'form_title form_select']); ?>
                <div id="goods_value_list">
                <?php
                if (count($solutionParams) > 0) {
                    ?>
                    <div class='section_title' style="margin-top: 0;"><h3>Данные</h3></div>
                    <?php
                    foreach ($solutionParams as $indexItem => $solutionParam) {
                        ?>
                        <div class="goods_value_list">
                            <div class='goods_values'>
                                <?= Html::activeHiddenInput($solutionParam, '[' . $indexItem . ']id'); ?>
                                <?= Html::activeHiddenInput($solutionParam, '[' . $indexItem . ']name'); ?>
                                <div class="goods_value">
                                    <?php
                                    echo $form->field($solutionParam, '[' . $indexItem . ']value')
                                        ->textInput(['placeholder' => $solutionParam->name])->label(false);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                }
                ?>
                </div>
                
                 <div class='section_title'>Мультизагрузка файлов с заданиями</div>
                <div id="drop-files" ondragover="return false">
                    <p>Перетащите файл сюда</p>
                    <form id="frm">
                        <div class="section_file2">
                            <button type="button">Выберите файл</button>
                            <div><span>Файл не выбран</span></div>
                            <?= Html::fileInput('file1', '', [
                                'id' => 'uploadbtn',
                                'multiple' => true,
                                'accept' => implode(', ', array_map(function($v) {
                                    return '.' . $v;
                                }, array_keys(OrdersUpload::mimeType()))),
                            ]) ?>
                        </div>
                    </form>
                </div>
    
                <div id='section_task'>
                    <?php
                    foreach ($files as $file) {
                        $id_file = $file['id'];
                        $name_ext = FileInfo::getExtension($file['file']);
                        $url_ext = $extensions[$name_ext]['url_ext'];
                        ?>
                        <div class='section_task' data-id='<?= $id_file ?>'>
                            <a href="<?= $fileUrlDoc . $id_file ?>" target="_blank" class='section_task_photo'>
                                <img src="<?= $url_ext ?>">
                            </a>
                            <div class='section_task_actions'>
                                <div class='section_task_delete'></div>
                            </div>
                            <div class='section_task_title'><?= $file['title'] ?></div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                
                <?php
                /*
                <div class='section_title' style="margin-top: 0;">
                    Прикрепляемый файл (в формате <?= implode(', ', array_keys(OrdersUpload::mimeType())) ?>,
                    размер не более
                    <?= str_replace('и', '', Yii::$app->formatter->asShortSize((new FileValidator)->getSizeLimit())) ?>)
                </div>
                <?php
                if (is_file($filePath . $modelOrderDesc->doc)) {
                    ?>
                    <div class="section_doc">
                        <div class="icon">
                            <? echo Html::a('<i class="fa fa-file" aria-hidden="true"></i>', Url::to($fileUrl . $modelOrderDesc->doc), [
                                'target' => '_blank',
                            ]); ?>
                        </div>
                        <div class="title" title="<?= $modelOrderDesc->doc ?>">
                            <?
                            echo implode('', [
                                Common::formatLongName($modelOrderDesc->doc),
                                '<br><span>',
                                Common::formatFileSize(filesize($filePath . $modelOrderDesc->doc)),
                                '</span>',
                            ]);
                            ?>
                        </div>
                        <div class="delete">
                            <?php
                            echo $form->field($modelUpload, 'delete_doc', [
                                'options' => ['class' => 'form-group form-checkbox'],
                            ])->checkbox(['class' => 'span-1']); ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="section_checkbox">
                <?php
                echo $form->field($modelUpload, 'doc', [
                    'options' => ['class' => 'form-group form-file',],
                ])->fileInput([
                    'accept' => implode(', ', array_map(function($v) {
                        return '.' . $v;
                    }, array_keys(OrdersUpload::mimeType()))),
                ])->label(false); ?>
                </div>
                */
                ?>

            </div>

            <div class='section_stripe2'></div>
        <?php
        echo Html::submitButton($submit, [
            'id' => 'submiting',
            'class' => 'btn btn-primary section_submit',
        ]);
        ?>
        </article>
    <?php ActiveForm::end(); ?>
    </div>
<?php
$this->registerJsFile('/js/solution_order_add.js?22', ['position' => yii\web\View::POS_END]);
$this->registerJsFile('/js/solution_order_add_multiupload.js?22', ['position' => yii\web\View::POS_END]);
?>
