<?
/**
 * @var $data array
 */

use backend\models\OrdersDocs;
use backend\models\OrdersUpload;

$nameFile = OrdersDocs::upload($data);

if (!empty($nameFile)) {
    $model = new OrdersDocs();
    $model->file = $nameFile;
    $titleFile = str_replace("." . OrdersDocs::getExtensionByMimeType($data['mime_type']), '', $data['title']);
    $model->title = $titleFile;
    $model->id_order = $data['id'];
    
    if ($model->save()) {
        $id_file = $model->id;
        $url_ext = OrdersDocs::getUrlExtensionByMimeType($data['mime_type']);
        
        ob_start();
        ?>
        <div class='section_task' data-id='<?= $id_file ?>'>
        <a href="<?= OrdersUpload::docSafetyUrl() . $model->id ?>" class='section_task_photo'>
            <img src="<?= $url_ext ?>">
        </a>
        <div class='section_task_actions'>
        <div class='section_task_delete'></div>
        </div>
        <div class='section_task_title'><?= $titleFile ?></div>
        </div>
        <?php
        $content = ob_get_clean();
        echo $content;
    } else {
        
    }
}