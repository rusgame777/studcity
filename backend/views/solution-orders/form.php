<?php

use yii\helpers\Url;
use backend\models\OrdersDocs;
/**
 *
 * @var $this yii\web\View
 * @var $model common\models\Orders;
 *
 */

$edit = Yii::$app->request->get('edit');
$get = Yii::$app->request->get();
$id = isset($get['id']) ? intval($get['id']) : 0;
$files = OrdersDocs::find()->where(['id_order' => $id])->asArray()->all();

$url_validate = Url::to(['solution-orders/validate']);
$url_return = Url::to(['solution-orders/management']);

if ($edit == null) {
    $url_action = Url::to(['solution-orders/create']);
    $submit = "Добавить";
    $actions = 'Добавить новый заказ';
} else {
    $url_action = Url::to(['solution-orders/update']);
    $submit = "Внести правки";
    $actions = "Изменить заказ";
}

$result = [
    'model' => $model,
    'files' => $files,
    'url_action' => $url_action,
    'url_validate' => $url_validate,
    'submit' => $submit,
    'id' => $id,
];
?>
    <header>
    <div id='section_header'>
        <div id='section_title'><?= $actions ?></div>
        <div id='section_url'><a href='<?= $url_return ?>' class='btn btn-primary section_button'>Вернуться к списку</a>
        </div>
    </div>
</header>
<?= $this->render('_form', ['result' => $result]) ?>