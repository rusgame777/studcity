<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel backend\models\SolutionOrdersSearch
 * @var $model backend\models\SolutionOrdersSearch
 */

use kartik\grid\GridView;
use liyunfang\pager\LinkPager;
use backend\models\SolutionOrdersSearch;
use backend\components\Common;
use backend\components\Notice;
use yii\helpers\Html;
use yii\helpers\Url;

$request = Yii::$app->request;
$url_module = '/' . $request->getPathInfo();
$url_add = str_replace('show', 'form', $url_module);
$url_excel = Url::to(['excel', Yii::$app->request->queryParams]);
$numberList = SolutionOrdersSearch::getNumberList();
?>
<header>
    <div id='section_header'>
        <div id='section_title'>Реестр заказов</div>
        <div id='section_url'></div>
    </div>
</header>
<article>
    <?= Html::a('<i class="fa fa-file-excel-o excel" aria-hidden="true"></i>', $url_excel, [
        'class' => 'add-user',
        'title' => 'Выгрузка',
    ]) ?>
    <?= $this->render('_search', ['model' => $searchModel]) ?>
    <?php Notice::init(); ?>
    <div class='kartiktable'>
        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'filterSelector' => implode(',', [
                "select[name='" . $dataProvider->getPagination()->pageSizeParam . "']",
                "input[name='" . $dataProvider->getPagination()->pageParam . "']",
            ]),
            'pager' => [
                'class' => LinkPager::className(),
                'template' => '{pageButtons} {customPage} {pageSize}',
                'pageSizeList' => [10, 20, 50, 100],
                'customPageWidth' => 50,
                'customPageBefore' => "&nbsp;Перейти на&nbsp;",
                'customPageAfter' => "&nbsp;Показывать по&nbsp;",
            ],
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'id' => 'notes',
                ],
            ],
            'striped' => false,
            'hover' => true,
            'resizableColumns' => false,
            'rowOptions' => function($model, $key, $index, $grid) {
                $class = $index % 2 ? '' : 'odd_tr';
                return [
                    'key' => $key,
                    'index' => $index,
                    'class' => $class,
                    'id' => $model->id,
                ];
            },
            'formatter' => [
                'class' => 'yii\i18n\Formatter',
                'nullDisplay' => "&mdash;",
            ],
            'tableOptions' => [
                'class' => 'kartik_table',
                'id' => 'table_barrier_block',
                'style' => 'width: 100%;',
            ],
            'floatHeader' => true,
            'floatHeaderOptions' => [
                'position' => 'fixed',
            ],
            'layout' => "{items}\n{pager}\n{summary}",
            'panel' => [
                'type' => 'primary',
            ],
            'toolbar' => [
                '{toggleData}',
            ],
            'toggleDataOptions' => ['minCount' => 0],
            'columns' => [
                [
                    'attribute' => 'id',
                    'filterInputOptions' => ['class' => 'form-control form-id'],
                    'contentOptions' => [
                        'class' => 'first_td right_td',
                        'width' => 100,
                    ],
                    'headerOptions' => [
                        'width' => 100,
                    ],
                    'value' => function($model) use ($numberList) {
                        /**
                         * @var $model backend\models\SolutionOrdersSearch
                         */
                        $numberItem = array_key_exists($model->id, $numberList) ? $numberList[$model->id] : [];
                        return !empty($numberItem) ? implode('/', [
                            $numberItem['count'],
                            $numberItem['date_end'] . '(' . $numberItem['id'] . ')',
                        ]) : null;
                    },
                ],
                [
                    'attribute' => 'date_end',
                    'format' => 'raw',
                    'value' => function($model) {
//                        return Yii::$app->formatter->asDate($model->date_end, 'php:H:i');
                        return Common::getDateTime($model->date_end, 'H:i');
                    },
                    'headerOptions' => [
                        'width' => 110,
                        'class' => 'middle_th',
                    ],
                    'contentOptions' => [
                        'width' => 110,
                        'class' => 'middle_td',
                    ],
                ],
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'headerOptions' => [
                        'width' => 110,
                    ],
                    'contentOptions' => [
                        'width' => 110,
                    ],
                ],
                [
                    'attribute' => 'contacts',
                    'format' => 'raw',
                    'value' => function($model) {
                        return implode("<br>", array_filter([
                            $model->phone,
                            !empty($model->email) ? "(" . $model->email . ")" : '',
                        ]));
                    },
                    'headerOptions' => [
                        'width' => 250,
                    ],
                    'contentOptions' => [
                        'width' => 250,
                    ],
                ],
                [
                    'attribute' => 'name_position',
                    'format' => 'ntext',
                    //                    'value' => function($model) {
                    //                        return !empty($model->name_position) ? $model->name_position : '';
                    //                    },
                ],
                [
                    'attribute' => 'name_author',
                    'format' => 'ntext',
                    //                    'value' => function($model) {
                    //                        return !empty($model->name_author) ? $model->name_author : '';
                    //                    },
                    'headerOptions' => [
                        'width' => 110,
                    ],
                    'contentOptions' => [
                        'width' => 110,
                    ],
                ],
                [
                    'attribute' => 'sum',
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'contentOptions' => [
                        'width' => 50,
                    ],
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->sum . " руб.";
                    },
                ],
            ],
        ]);
        ?>
    </div>

</article>
<?php
$this->registerJsFile('/js/ordersSolutionShow.js?2', ['position' => yii\web\View::POS_END]);
?>
