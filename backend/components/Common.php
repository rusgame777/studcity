<?php
namespace backend\components;

use Yii;
use yii\db\Exception;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

class Common
{
    static function select2_select($id, $placeholder = 'выберите значение')
    {
        $name = "span[aria-labelledby='select2-{$id}-container']";
        $select = "
		<style>
		{$name} { 
			width: 100%;
			}
//		.select2-container--default {$name}.select2-selection--single {
//			-webkit-border-radius: 0;
//			-moz-border-radius: 0;
//			-ms-border-radius: 0;
//			-o-border-radius: 0;
//			border-radius: 0;
//			}
//		.select2-container {$name}.select2-selection--single {
//			outline: 0;
//			height: 25px;
//			border: 0;
//			-webkit-box-shadow: inset 0 0 3px rgba(0,0,0,0.29);
//			-moz-box-shadow: inset 0 0 3px rgba(0,0,0,0.29);
//			box-shadow: inset 0 0 3px rgba(0,0,0,0.29);
//			}
//			
//		.select2-container--default {$name}.select2-selection--single .select2-selection__arrow {
//			width: 23px;
//			height: 23px;
//			-webkit-background-size: 100% 100%;
//			-moz-background-size: 100% 100%;
//			-ms-background-size: 100% 100%;
//			-o-background-size: 100% 100%;
//			background-size: 100% 100%;
//			}
//			
//		.select2-container--default {$name}.select2-selection--single .select2-selection__rendered {
//			font: 14px/25px 'Exo 2', sans-serif;
//			color: #000;
		}
		</style>
		<script>
        $(document).ready(function() {            
            $('#{$id}').select2({
                allowClear: true,
                width: 'resolve',
                placeholder: '{$placeholder}',
                dropdownAutoWidth: true,
                templateSelection: function (selection) {
                   return selection.text.trim();
                },
            });
            $('span[aria-labelledby=\"select2-{$id}-container\"]').parents('.select2').removeClass('select2-container--default').addClass('select2-container--krajee');
            $(window).on('resize', function() {
                $('#{$id}').select2({
                    allowClear: false,
                    width: 'resolve',
                    placeholder: '{$placeholder}',
                    dropdownAutoWidth: true,
                    templateSelection: function (selection) {
                       return selection.text.trim();
                    },
                });
           $('span[aria-labelledby=\"select2 -{$id}-container\"]').parents('.select2').removeClass('select2-container--default').addClass('select2-container--krajee');
            });
        });
		</script>";
        return $select;
    }
    
    static function sort_param($url_query, $param, $value, $sort_default = 0)
    {
        $sort_array = [
            'asc',
            'desc',
        ];
        $pos = mb_strpos($url_query, $param . '=', 0, 'UTF-8');
        if ($pos == false) {
            $position = 0;
        } else {
            $position = 1;
        }
        if ($position == 1) {
            $type = $param . '=' . $value;
            $url_query = str_replace($type . "&", "", $url_query);
            $url_query = str_replace($type, "", $url_query);
        }
        
        $sort_param = '';
        $type_param = '';
        if ($value == null) {
            $sort_param = $param . '=' . $sort_array[$sort_default];
            $type_param = 'sort_' . $sort_array[$sort_default];
        } else {
            foreach ($sort_array as $key => $val) {
                if ($value != $val) {
                    $sort_param = $param . '=' . $val;
                    $type_param = 'sort_' . $val;
                }
            }
        }
        return [
            $sort_param,
            $type_param,
            $url_query,
        ];
    }
    
    static function pagination($itemscount, $cpage, $itemsperpage = 20, $pagedispleftrange = 2, $pagedisprightrange = 2)
    {
        $pagescount = ceil($itemscount / $itemsperpage);
        $offset_page = ($cpage - 1) * $itemsperpage;
        
        $kolpage = $pagedispleftrange + $pagedisprightrange;
        if ($cpage <= $kolpage) {
            $pagedispleftrange = $cpage - 1;
            if ($kolpage - $pagedispleftrange > $pagedisprightrange) {
                $pagedisprightrange = $kolpage - $pagedispleftrange;
            }
        }
        
        if (($cpage > $pagescount - $kolpage) && ($cpage <= $pagescount)) {
            $pagedisprightrange = $pagescount - $cpage;
            if ($kolpage - $pagedisprightrange > $pagedispleftrange) {
                $pagedispleftrange = $kolpage - $pagedisprightrange;
            }
        }
        
        $stpage = $cpage - $pagedispleftrange;
        if ($stpage < 1) {
            $stpage = 1;
        }
        $endpage = $cpage + $pagedisprightrange;
        if ($endpage > $pagescount) {
            $endpage = $pagescount;
        }
        
        return [
            'offset_page' => $offset_page,
            'pagescount' => $pagescount,
            'stpage' => $stpage,
            'endpage' => $endpage,
        ];
    }
    
    static function pagination_show($query_array, $pagescount, $cpage, $stpage, $endpage, $param = 'page')
    {
        $query_arr = $query_array;
        $content = "";
        $url_module = '/' . \Yii::$app->request->getPathInfo();
        if ($cpage > 2) {
            $query_arr[$param] = $cpage - 1;
        } else {
            unset($query_arr[$param]);
        }
        if (count($query_arr) != 0) {
            $url_query = "?" . http_build_query($query_arr);
        } else {
            $url_query = '';
        }
        if ($cpage > 1) {
            $content .= "<a href='" . $url_module . $url_query . "' class='pg_prev_on'><span><i class='fa fa-angle-left'></i></span></a>";
        } else {
            $content .= "<div class='pg_prev_off'><span><i class='fa fa-angle-left'></i></span></div>";
        }
        for ($i = $stpage; $i <= $endpage; $i++) {
            $query_arr = $query_array;
            if ($i > 1) {
                $query_arr[$param] = $i;
            } else {
                unset($query_arr[$param]);
            }
            if (count($query_arr) != 0) {
                $url_query = "?" . http_build_query($query_arr);
            } else {
                $url_query = '';
            }
            if ($i == $cpage) {
                $content .= "<div class='cur_pg'><span>{$i}</span></div>";
            } else {
                $content .= "<a href='" . $url_module . $url_query . "' class='nocur_pg'><span>{$i}</span></a>";
            }
        }
        $query_arr = $query_array;
        $query_arr[$param] = $cpage + 1;
        if (count($query_arr) != 0) {
            $url_query = "?" . http_build_query($query_arr);
        } else {
            $url_query = '';
        }
        if ($cpage < $pagescount) {
            $content .= "<a href='" . $url_module . $url_query . "' class='pg_next_on'><span><i class='fa fa-angle-right'></i></span></a>";
        } else {
            $content .= "<div class='pg_next_off'><span><i class='fa fa-angle-right'></i></span></div>";
        }
        return $content;
    }
    
    static public function translit($string)
    {
        $converter = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'e',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ь' => '\'',
            'ы' => 'y',
            'ъ' => '\'',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'E',
            'Ж' => 'Zh',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'Y',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'Ch',
            'Ш' => 'Sh',
            'Щ' => 'Sch',
            'Ь' => '\'',
            'Ы' => 'Y',
            'Ъ' => '\'',
            'Э' => 'E',
            'Ю' => 'Yu',
            'Я' => 'Ya',
        ];
        return strtr($string, $converter);
    }
    
    public static function getInTranslit($string)
    {
        $disabled_symbols = ["/", "\\", "|", ":", "\"", "?", "*", ">", "<", "№"];
        $replace = [
            "'" => "",
            "`" => "",
            "а" => "a",
            "А" => "a",
            "б" => "b",
            "Б" => "b",
            "в" => "v",
            "В" => "v",
            "г" => "g",
            "Г" => "g",
            "д" => "d",
            "Д" => "d",
            "е" => "e",
            "Е" => "e",
            "ж" => "zh",
            "Ж" => "zh",
            "з" => "z",
            "З" => "z",
            "и" => "i",
            "И" => "i",
            "й" => "y",
            "Й" => "y",
            "к" => "k",
            "К" => "k",
            "л" => "l",
            "Л" => "l",
            "м" => "m",
            "М" => "m",
            "н" => "n",
            "Н" => "n",
            "о" => "o",
            "О" => "o",
            "п" => "p",
            "П" => "p",
            "р" => "r",
            "Р" => "r",
            "с" => "s",
            "С" => "s",
            "т" => "t",
            "Т" => "t",
            "у" => "u",
            "У" => "u",
            "ф" => "f",
            "Ф" => "f",
            "х" => "h",
            "Х" => "h",
            "ц" => "c",
            "Ц" => "c",
            "ч" => "ch",
            "Ч" => "ch",
            "ш" => "sh",
            "Ш" => "sh",
            "щ" => "sch",
            "Щ" => "sch",
            "ъ" => "",
            "Ъ" => "",
            "ы" => "y",
            "Ы" => "y",
            "ь" => "",
            "Ь" => "",
            "э" => "e",
            "Э" => "e",
            "ю" => "yu",
            "Ю" => "yu",
            "я" => "ya",
            "Я" => "ya",
            "і" => "i",
            "І" => "i",
            "ї" => "yi",
            "Ї" => "yi",
            "є" => "e",
            "Є" => "e",
        ];
        $string = str_replace($disabled_symbols, "", $string);
        return iconv("UTF-8", "UTF-8//IGNORE", strtr($string, $replace));
    }
    
    static public function makeUrlCode($str)
    {
        return trim(preg_replace('~[^-a-z0-9_]+~u', '-', strtolower(static::translit($str))), "-");
    }
    
    public static function printTerms(
        $goods, $terms = [
        1 => "товар",
        2 => "товара",
        5 => "товаров",
    ])
    {
        $cgoods = (($x = ($goods % 100)) > 19) ? $x % 10 : $x;
        if ($cgoods == 1) {
            // 1, 21, 31, ....
            $_goods = $terms[1];
        } elseif ($cgoods > 0 && $cgoods < 5) {
            // 2, 3, 4, 22, 23, 24, ....
            $_goods = $terms[2];
        } else {
            // 0, 5-20, 25-30, 35-40, ....
            $_goods = $terms[5];
        }
        return $_goods;
    }
    
    public static function formatFileSize($size)
    {
        $unitsArray = [
            "B",
            "KB",
            "MB",
            "GB",
            "TB",
            "PB",
        ];
        $pos = 0;
        while ($size >= 1024) {
            $size /= 1024;
            $pos++;
        }
        return round($size, 2) . " " . $unitsArray[$pos];
    }
    
    public static function formatLongName($value, $size = 60)
    {
        $sizeValue = mb_strlen($value, 'UTF-8');
        $returnValue = mb_substr($value, 0, $size, 'UTF-8');
        if ($sizeValue > $size) {
            $returnValue .= '...';
        }
        return $returnValue;
    }
    
    public static function insertIntoBarrier($array)
    {
        $arrayNameFieldsInsert = [
            'id_reporter',
            'id_barriers_types',
            'id_barriers_types_wits',
            'id',
            'regulation_url',
            'name',
            'text',
            'regulation_title',
            'date_investigation',
            'date_start',
            'date_end',
            'sort',
            'is_last',
            'is_disabled',
            'who_create',
            'who_update',
        ];
        
        $arrayNameFieldsInsertNew = [
            'new_id_reporter',
            'new_id_barriers_types',
            'new_id_barriers_types_wits',
            'new_id',
            'new_regulation_url',
            'new_name',
            'new_text',
            'new_regulation_title',
            'new_date_investigation',
            'new_date_start',
            'new_date_end',
            'new_sort',
            'new_is_last',
            'new_is_disabled',
            'new_who_create',
            'new_who_update',
        ];
        
        $sqlQuery = vsprintf("
            SET IDENTITY_INSERT %1\$s ON; 
            MERGE %1\$s AS target
            USING (SELECT %4\$s 
            FROM (SELECT *,row_number() over(partition by new_id order by new_id) r
            FROM (VALUES %2\$s) AS source (%4\$s)) AS source
            WHERE r=1) source
            ON (target.id = source.new_id)
            WHEN MATCHED THEN
                UPDATE SET target.id_reporter = source.new_id_reporter, target.id_barriers_types = source.new_id_barriers_types, target.id_barriers_types_wits=source.new_id_barriers_types_wits, target.name=source.new_name, target.text=source.new_text, target.regulation_title=source.new_regulation_title, target.regulation_url=source.new_regulation_url, target.date_investigation=source.new_date_investigation, target.date_start=source.new_date_start, target.date_end=source.new_date_end, target.date_update=GETDATE()
            WHEN NOT MATCHED BY TARGET THEN
                INSERT (%3\$s) VALUES (%4\$s);
                
            SET IDENTITY_INSERT %1\$s OFF; 
            ", [
            1 => Tables::get('[site].[barriers]'),
            2 => implode(",\n", $array),
            3 => implode(', ', $arrayNameFieldsInsert),
            4 => implode(', ', $arrayNameFieldsInsertNew),
        ]);
        
        try {
            $db = Yii::$app->db;
            $rows = $db->createCommand($sqlQuery)->execute();
        } catch (Exception $e) {
            var_dump($e);
            file_put_contents('errors.txt', (string)$e . PHP_EOL, FILE_APPEND);
        }
    }
    
    public static function insertIntoBarrierGoods($array)
    {
        $arrayNameFieldsInsert = [
            'id_barriers',
            'hs_original',
            'hs_fts',
            'is_disabled',
        ];
        $arrayNameFieldsInsertNew = [
            'new_id_barriers',
            'new_hs_original',
            'new_hs_fts',
            'new_is_disabled',
        ];
        
        $sqlQuery = vsprintf("
            MERGE %1\$s AS target
            USING (SELECT DISTINCT * FROM (VALUES %2\$s) AS source (%4\$s)) AS source
            ON (target.hs_original = source.new_hs_original AND target.id_barriers = source.new_id_barriers)
            WHEN MATCHED THEN
                UPDATE SET target.hs_fts = source.new_hs_fts
            WHEN NOT MATCHED BY TARGET THEN
                INSERT (%3\$s) VALUES (%4\$s);
            ", [
            1 => Tables::get('[site].[barriers2goods]'),
            2 => implode(",\n", $array),
            3 => implode(', ', $arrayNameFieldsInsert),
            4 => implode(', ', $arrayNameFieldsInsertNew),
        ]);
        
        try {
            $db = Yii::$app->db;
            $rows = $db->createCommand($sqlQuery)->execute();
        } catch (Exception $e) {
            var_dump($e);
            file_put_contents('errors.txt', (string)$e . PHP_EOL, FILE_APPEND);
        }
    }
    
    public static function escape($value)
    {
        return Yii::$app->db->quoteValue($value);
    }
    
    public static function replaceEnSymToRu($value)
    {
        $arr = [
            'А' => 'A',
            'В' => 'B',
            'С' => 'C',
            'Е' => 'E',
            'К' => 'K',
            'О' => 'O',
            'М' => 'M',
            'Р' => 'P',
            'T' => 'Т',
            'Х' => 'X',
        ];
        foreach ($arr as $search => $replace) {
            $value = str_replace($search, $replace, $value);
        }
        return $value;
    }
    
    static function getFotoStyle($filename, $newSize)
    {
        if (!file_exists($filename)) {
            return [];
        }
        
        $array = [];
        $imagine = new Imagine();
        $image = $imagine->open($filename);
        $size = $image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();
        $resize = $width / $height;
        $newResize = $newSize[0] / $newSize[1];
        if ($newResize > $resize) {
            $array['width'] = '100%';
            $array['left'] = 0;
        } else {
            $array['width'] = round($newSize[1] / $height * $width);
            $array['left'] = round(($newSize[0] - $array['width']) / 2);
            $array['left'] .= 'px';
            $array['width'] .= 'px';
        }
        if ($newResize < $resize) {
            $array['height'] = '100%';
            $array['top'] = 0;
        } else {
            $array['height'] = round($newSize[0] / $width * $height);
            $array['top'] = round(($newSize[1] - $array['height']) / 3);
            $array['top'] .= 'px';
            $array['height'] .= 'px';
        }
        
        $styleArr = [];
        foreach ($array as $k => $v) {
            $styleArr[] = $k . ":" . $v;
        }
        $style = "style='" . implode('; ', $styleArr) . "'";
        return $style;
    }
    
    static function getDateTime($value = '', $format = 'Y-m-d H:i:s', $modify = false)
    {
        $date = new \DateTime($value, new \DateTimeZone(Yii::$app->formatter->defaultTimeZone));
        if ($modify) {
            $date->modify($modify);
        }
        return $date->format($format);
        
    }
}