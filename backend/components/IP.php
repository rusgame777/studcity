<?php
namespace backend\components;

use Yii;
use common\models\Admin;
use common\models\Author;

class IP
{
    const Unknown = 'Unknown IP';
    
    public static function get()
    {
        if (getenv('HTTP_CLIENT_IP')) {
            return getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            return getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            return getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            return getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            return getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            return getenv('REMOTE_ADDR');
        }
        
        return self::Unknown;
    }
    
    public static function who()
    {
        $admin_login = Yii::$app->request->cookies->getValue('admin_login');
        $admin_hash = Yii::$app->request->cookies->getValue('admin_hash');
        if (!empty($admin_login) && !empty($admin_hash)) {
            $admin = Admin::findByAccessToken($admin_login, $admin_hash);
            if ($admin !== false) {
                return $admin->login;
            }
        }
        
        $author_login = Yii::$app->request->cookies->getValue('author_login');
        $author_hash = Yii::$app->request->cookies->getValue('author_hash');
        if (!empty($author_login) && !empty($author_hash)) {
            $author = Admin::findByAccessToken($author_login, $author_hash);
            if ($author !== false) {
                return $author->login;
            }
        }
        return null;
    }
    
    public static function whoCreate()
    {
        return implode('@', array_filter([self::who(), self::get()]));
    }
}