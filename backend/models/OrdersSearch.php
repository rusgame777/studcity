<?php

namespace backend\models;

use backend\components\Common;
use common\models\CatalogPosition;
use common\models\CatalogSubcat;
use common\models\CatalogSubcatCat;
use common\models\Clients;
use common\models\Formatter;
use common\models\Orders;
use common\models\OrderDesc;
use common\models\OrderStatus;
use common\models\OrderType;
use common\models\UserCatalog;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class OrdersSearch extends Orders
{
    public $dateStart, $dateEnd;
    public $phone, $email, $contacts;
    public $id_university, $cat, $name;
    
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'date_end' => 'Время оплаты',
            'id_university' => 'Выберите вуз',
            'contacts' => 'Телефон(E-mail)',
            'name' => 'Фамилия, имя',
            'sum' => 'Сумма',
            'orderList' => 'Состав',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'date_end',
                    'contacts',
                    'name',
                    'sum',
                ],
                'safe',
            ],
            [
                [
                    'cat',
                    'id_university',
                    'dateStart',
                    'dateEnd',
                ],
                'safe',
            ],
            [
                [
                    'dateStart',
                ],
                'default',
                'value' => function() {
                    return self::getMinDate();
                },
            
            ],
            [
                [
                    'dateEnd',
                ],
                'default',
                'value' => function() {
                    return self::getMaxDate();
                },
            ]
            //            [
            //                ['sort'],
            //                'integer',
            //            ],
        ];
    }
    
    public function attributeInt()
    {
        return [
            'id',
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function formName()
    {
        return '';
    }
    
    public function getDate_start()
    {
        return Formatter::getDateStart();
    }
    
    public function getDate_end()
    {
        return Formatter::getDateEnd();
    }
    
    public static function getMaxDate()
    {
        return Orders::find()->where([
            'id_type' => OrderType::catalog,
            'send' => 1,
            'status' => OrderStatus::paid,
        ])->max('DATE_FORMAT(date_end, \'%Y-%m-%d\')');
    }
    
    public static function getMinDate()
    {
        return Orders::find()->where([
            'id_type' => OrderType::catalog,
            'send' => 1,
            'status' => OrderStatus::paid,
        ])->min('DATE_FORMAT(date_end, \'%Y-%m-%d\')');
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function search_filter($query)
    {

//        $dateStart = Yii::$app->formatter->asDate($this->dateStart, 'php:Y-m-d');
//        $dateEnd = Yii::$app->formatter->asDate($this->dateEnd, 'php:Y-m-d');
        $dateStart = Common::getDateTime($this->dateStart, 'Y-m-d');
        $dateEnd = Common::getDateTime($this->dateEnd, 'Y-m-d');
        
        $query->andFilterWhere(['between', new Expression("DATE_FORMAT(date_end, '%Y-%m-%d')"), $dateStart, $dateEnd]);
        
        if (intval($this->cat) > 0) {
            $query->andFilterWhere(['orderdesc.cat' => $this->cat]);
        }
        
        if (intval($this->id_university) > 0) {
            $query->andFilterWhere(['catalog_subcat_cat.id_university' => $this->id_university]);
        }
        
        return $query;
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function field_filter($query)
    {
        $filterFieldArray = [
            'sum' => self::tableName() . '.sum',
            'date_end' => self::tableName() . '.date_end',
            'name' => 'client.name',
            'phone' => 'client.phone',
            'email' => 'client.email',
        ];
        foreach ($filterFieldArray as $key => $field) {
            $name = is_integer($key) ? $this->{$field} : $this->{$key};
            if (!empty($name)) {
                $arr = [];
                $wordArray = explode(' ', $name);
                foreach ($wordArray as $word) {
                    if (empty($arr)) {
                        $arr[] = 'or';
                    }
                    $arr[] = [
                        'like',
                        $field,
                        '%' . $word . '%',
                        false,
                    ];
                }
                if (!empty($arr)) {
                    $query->andFilterWhere($arr);
                }
            }
        }
        return $query;
    }
    
    public function search($params)
    {
        $this->load($params);
        $query = $this->find();
        
        $showAllParam = '_tog' . hash('crc32', 'w0');
        $showAll = array_key_exists($showAllParam, $params) ? $params[$showAllParam] : 'page';
        $perPage = array_key_exists('per-page', $params) ? $params['per-page'] : 20;
        $count = $showAll == 'page' ? $perPage : PHP_INT_MAX;
        
        $query->select([
            self::tableName() . '.id',
            self::tableName() . '.sum',
            self::tableName() . '.date_end',
            'client.name',
            'client.phone',
            'client.email',
        ])->joinWith('user as client', $eagerLoading = true, $joinType =
            'LEFT JOIN')//            ->leftJoin(Clients::tableName(). ' as client', 'client.id = ' . self::tableName() . '.id_user')
        ->leftJoin(OrderDesc::tableName(), OrderDesc::tableName() . '.id_order = ' . self::tableName() . '.id')
            ->leftJoin(CatalogSubcat::tableName(), CatalogSubcat::tableName() . '.id = ' . OrderDesc::tableName() .
                '.subcat')->leftJoin(CatalogSubcatCat::tableName(), CatalogSubcat::tableName() . '.id = ' .
                CatalogSubcatCat::tableName() . '.id_subcat')->where([
                self::tableName() . '.id_type' => OrderType::catalog,
                self::tableName() . '.send' => 1,
                self::tableName() . '.status' => OrderStatus::paid,
            ]);
        
        if (Yii::$app->view->params['id_role'] == 1) {
            $query->andFilterWhere([
                'in',
                'catalog_subcat_cat.id_cat',
                UserCatalog::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
            ]);
        }
        
        $query->distinct()->limit($count)->all();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ],
        ]);
        
        $dataProvider->setSort([
            'attributes' => [
                'date_end' => [
                    'asc' => [self::tableName() . '.date_end' => SORT_ASC],
                    'desc' => [self::tableName() . '.date_end' => SORT_DESC],
                ],
                'sum' => [
                    'asc' => [self::tableName() . '.sum' => SORT_ASC],
                    'desc' => [self::tableName() . '.sum' => SORT_DESC],
                ],
                'name' => [
                    'asc' => ['client.name' => SORT_ASC],
                    'desc' => ['client.name' => SORT_DESC],
                ],
            ],
            'defaultOrder' => [
                'date_end' => SORT_DESC,
            ],
            'sortParam' => 'prior',
        ]);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query = $this->search_filter($query);
        $query = $this->field_filter($query);
        
        return $dataProvider;
    }
    
    /**
     * @param $params
     * @return \yii\db\ActiveQuery
     */
    public function searchQuery($params)
    {
        $this->load($params);
        $query = $this->find();
        
        $count = PHP_INT_MAX;
        
        $query->select([
            'MAX(' . self::tableName() . '.date_end) date_end',
            'client.name',
            'client.phone',
            'client.email',
        ])->joinWith('user as client', $eagerLoading = true, $joinType = 'LEFT JOIN')
            ->leftJoin(OrderDesc::tableName(), OrderDesc::tableName() . '.id_order = ' . self::tableName() . '.id')
            ->leftJoin(CatalogSubcat::tableName(), CatalogSubcat::tableName() . '.id = ' . OrderDesc::tableName() .
                '.subcat')->leftJoin(CatalogSubcatCat::tableName(), CatalogSubcat::tableName() . '.id = ' .
                CatalogSubcatCat::tableName() . '.id_subcat')->where([
                self::tableName() . '.id_type' => OrderType::catalog,
                self::tableName() . '.send' => 1,
                self::tableName() . '.status' => OrderStatus::paid,
            ])->groupBy([
                'client.name',
                'client.phone',
                'client.email',
            ]);
        
        if (Yii::$app->view->params['id_role'] == 1) {
            $query->andFilterWhere([
                'in',
                'catalog_subcat_cat.id_cat',
                UserCatalog::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
            ]);
        }
        
        $query->distinct()->limit($count);
        
        $this->dateStart = empty($this->dateStart) ? self::getMinDate() : $this->dateStart;
        $this->dateEnd = empty($this->dateEnd) ? self::getMaxDate() : $this->dateEnd;
        
        $query = $this->search_filter($query);
        $query = $this->field_filter($query);
        
        return $query;
    }
    
    public function getUser()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_user']);
    }
    
    public function getOrderList()
    {
        $orderList = [];
        $ordersdesc = OrderDesc::find()->select(['id_position'])->where(['id_order' => $this->id])->distinct()->all();
        if (!empty($ordersdesc)) {
            foreach ($ordersdesc as $index => $orderdesc) {
                $position = CatalogPosition::findOne($orderdesc->id_position);
                if ($position === null) {
                    continue;
                }
                $name_variant = str_replace('вар', '', $position->title);
                $subcategory = CatalogSubcat::findOne($position->subcat);
                $name_manual = !empty($subcategory->title) ? $subcategory->title : '';
                
                $orderList[$index] = implode('-', [
                    1 => $name_manual,
                    2 => !empty($name_variant) ? 'вариант ' . $name_variant : '',
                ]);
                if ($position->type_option == 1) {
                    $options = OrderDesc::find()->select([
                        "GROUP_CONCAT(" . CatalogFiles::tableName() . ".title SEPARATOR ', ') title",
                    ])->leftJoin(CatalogFiles::tableName(), CatalogFiles::tableName() . '.id = ' .
                        OrderDesc::tableName() . '.id_option')->where([
                        'id_order' => $this->id,
                        'id_position' => $orderdesc->id_position,
                    ])->orderBy(['title' => SORT_ASC])->asArray()->all();
                    if (!empty($options) && array_key_exists(0, $options)) {
                        $orderList[$index] .= "(" . $options[0]['title'] . ")";
                    }
                }
            }
        }
        return implode(";\n", $orderList);
    }
    
    public function getOrderId()
    {
//        $date_end = Yii::$app->formatter->asDate($this->date_end, 'php:Y-m-d');
        $date_end = Common::getDateTime($this->date_end, 'Y-m-d');
        $num_orders = Orders::find()->where([
            'and',
            ['id_type' => OrderType::catalog],
            ['send' => 1],
            ['status' => OrderStatus::paid],
            [
                'between',
                'date_end',
                //                Yii::$app->formatter->asDate($date_end, 'php:Y-m-d'),
                //                Yii::$app->formatter->asDate($this->date_end, 'php:Y-m-d H:i:s'),
                Common::getDateTime($date_end, 'Y-m-d'),
                Common::getDateTime($this->date_end, 'Y-m-d H:i:s'),
            ],
        ])->count(1);
        return implode('/', [
            $num_orders,
            Common::getDateTime($this->date_end, 'd.m.Y') . '(' . $this->id . ')',
            //            Yii::$app->formatter->asDate($this->date_end, 'php:d.m.Y') . '(' . $this->id . ')',
        ]);
    }
    
    public function getSum()
    {
        $query = $this->find()//            ->select(['SUM('.self::tableName() . '.sum) sum'])
        ->select([
            self::tableName() . '.id',
            self::tableName() . '.sum',
        ])->joinWith('user as client', $eagerLoading = true, $joinType = 'LEFT JOIN')
            ->leftJoin(OrderDesc::tableName(), OrderDesc::tableName() . '.id_order = ' . self::tableName() . '.id')
            ->leftJoin(CatalogSubcat::tableName(), CatalogSubcat::tableName() . '.id = ' . OrderDesc::tableName() .
                '.subcat')->leftJoin(CatalogSubcatCat::tableName(), CatalogSubcat::tableName() . '.id = ' .
            CatalogSubcatCat::tableName() . '.id_subcat')->where([
            self::tableName() . '.id_type' => OrderType::catalog,
            self::tableName() . '.send' => 1,
            self::tableName() . '.status' => OrderStatus::paid,
        ]);
        
        if (Yii::$app->view->params['id_role'] == 1) {
            $query->andFilterWhere([
                'in',
                'catalog_subcat_cat.id_cat',
                UserCatalog::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
            ]);
        }
        
        $query = $this->field_filter($query);
        $query = $this->search_filter($query);
//        $sum = $query->scalar();
        $query->distinct();
        $sum = $query->sum('sum');
        $sum = !empty($sum) ? $sum : 0;
        
        return $sum;
        
    }
    
    public function getUniqueEMail()
    {
        $query =
            $this->find()->select(['client.email email'])->joinWith('user as client', $eagerLoading = true, $joinType =
                'LEFT JOIN')->leftJoin(OrderDesc::tableName(), OrderDesc::tableName() . '.id_order = ' .
                self::tableName() . '.id')->leftJoin(CatalogSubcat::tableName(), CatalogSubcat::tableName() . '.id = ' .
                OrderDesc::tableName() . '.subcat')
                ->leftJoin(CatalogSubcatCat::tableName(), CatalogSubcat::tableName() . '.id = ' .
                    CatalogSubcatCat::tableName() . '.id_subcat')->where([
                    self::tableName() . '.id_type' => OrderType::catalog,
                    self::tableName() . '.send' => 1,
                    self::tableName() . '.status' => OrderStatus::paid,
                
                ]);
        
        if (Yii::$app->view->params['id_role'] == 1) {
            $query->andFilterWhere([
                'in',
                'catalog_subcat_cat.id_cat',
                UserCatalog::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
            ]);
        }
        $query = $this->field_filter($query);
        $query = $this->search_filter($query);
        $query->distinct();
        $uniqueEmail = $query->count();
        return $uniqueEmail;
    }
    
    public static function getNumberList()
    {
        $tableOrders1 = '
            (SELECT  
               id, date_end
              FROM orders
            WHERE (id_type = 1)
            AND (send = 1)
            AND (status = 2)) orders1
        ';
        
        $tableOrders2 = '
            (SELECT  
               id, date_end
              FROM orders
            WHERE (id_type = 1)
            AND (send = 1)
            AND (status = 2)) orders2
        ';
        
        $results = self::find()->select([
            'orders1.id',
            'DATE_FORMAT(orders1.date_end, \'%d.%m.%Y\') date_end',
            'COUNT(orders2.id) count',
        ])->from([$tableOrders1])->innerJoin($tableOrders2, 'orders2.date_end BETWEEN CONCAT_WS(\' \', DATE_FORMAT(orders1.date_end, \'%Y-%m-%d\'), \'00:00:00\')
AND DATE_FORMAT(orders1.date_end, \'%Y-%m-%d %H:%i:%s\')')->groupBy(['orders1.id'])
            ->orderBy(['orders1.date_end' => SORT_DESC])->asArray()->all();
        
        foreach ($results as $index => $result) {
            $results[$result['id']] = $result;
            unset($results[$index]);
        }
        return $results;
    }
}