<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\validators\FileValidator;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

class SolutionPositionUpload extends Model
{
    public $foto;
    public $foto_small;
    public $delete_foto;

    public function formName()
    {
        return '';
    }

    public function rules()
    {
        return [
            [
                ['foto', 'foto_small'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'jpg, png, gif',
                'maxSize' => (new FileValidator)->getSizeLimit(),
            ],
            [
                ['delete_foto'],
                'boolean',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'foto' => 'Выберите файл',
            'delete_foto' => 'удалить',
        ];
    }

    public static function mimeType()
    {
        return [
            'jpg' => ['image/jpeg', 'image/pjpeg'],
            'png' => 'image/png',
            'gif' => 'image/gif',
        ];
    }

    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/solutions/');
    }

    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/solutions/');
    }

    public static function size()
    {
        return [
            'foto' => [
                800,
                600,
            ],
            'foto_small' => [
                320,
                240,
            ],
        ];
    }

    public static function changeSize($foto, $nameAttr)
    {
        $imagine = new Imagine();
        $fileName = self::filePath() . $foto;
        $image = $imagine->open($fileName);
        $curSize = $image->getSize();
        $curWidth = $curSize->getWidth();
        $curHeight = $curSize->getHeight();
        $size = self::size()[$nameAttr];
        if ($size[0] > $size[1]) {
            if ($curWidth > $size[0]) {
                $newwidth = $size[0];
            } else {
                $newwidth = $curWidth;
            }
            $newheight = $curHeight * $newwidth / $curWidth;
        } else {
            if ($curHeight > $size[1]) {
                $newheight = $size[1];
            } else {
                $newheight = $curHeight;
            }
            $newwidth = $curWidth * $newheight / $curHeight;
        }
        $image->resize(new Box($newwidth, $newheight))->save($fileName);
    }
}