<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 25.01.2018
 * Time: 1:35
 */

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * OrderSendForm model
 *
 * @property string $number
 */
class OrderSendForm extends Model
{
    public $number;
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [];
    }
    
    public function checkFiles($attribute, $param)
    {
        
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'number',
                ],
                'integer',
                'min' => 1,
                'tooSmall' => 'Загрузите файлы для отправки РГР',
            ],
            [
                [
                    'number',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        return true;
    }
}