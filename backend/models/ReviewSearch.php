<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use common\models\Review;

class ReviewSearch extends Review
{
    public $id_type, $search_word;
    public $date_publication;
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avtor' => 'Автор',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
            'date_publication' => 'Дата публикации',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'avtor',
                    'phone',
                ],
                'safe',
            ],
            [
                [
                    'search_word',
                    'id_type',
                ],
                'safe',
            ],
        ];
    }
    
    public function attributeInt()
    {
        return [
            'id',
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function formName()
    {
        return '';
    }
    
    public function getSearch_word()
    {
        return Html::encode(!empty(Yii::$app->request->get('search_word')) ? Yii::$app->request->get('search_word') : '');
    }
    
    public function getSearchWordText()
    {
        return (isset(Yii::$app->request->get()['search_word']) ? Yii::$app->request->get()['search_word'] : '');
    }
    
    public function getId_type()
    {
        return intval(!empty(Yii::$app->request->get('id_type')) ? Yii::$app->request->get('id_type') : 1);
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function search_filter($query)
    {
        $id_type = $this->getId_type();
        $search_word = $this->getSearchWordText();
        
        $filterFieldArray = [
            'id' => self::tableName() . '.id',
            'avtor' => self::tableName() . '.avtor',
            'phone' => self::tableName() . '.phone',
        ];
        $searchFieldArray = [
            'id' => self::tableName() . '.id',
            'avtor' => self::tableName() . '.avtor',
            'phone' => self::tableName() . '.phone',
        ];
        
        $fields = [];
        
        foreach ($filterFieldArray as $keyField => $nameField) {
            if (!in_array($keyField, $this->attributeInt())) {
                $nameField = "IFNULL(" . $nameField . ", '')";
            }
            if (in_array($keyField, $this->attributeInt())) {
                $nameField = 'CONVERT(' . $nameField . ', CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci';
            }
            $fields[] = $nameField;
        }
        $fieldSqlString = implode('', [
            'CONCAT(',
            implode(", ' ',  ", $fields),
            ')',
        ]);
        
        $arr = [];
        if ($id_type != 3) {
            $wordArray = explode(' ', $search_word);
            foreach ($wordArray as $word) {
                if (empty($arr)) {
                    $arr[] = ($id_type == 1) ? 'or' : 'and';
                }
                $arr[] = [
                    'like',
                    $fieldSqlString,
                    '%' . $word . '%',
                    false,
                ];
            }
        } else {
            foreach ($searchFieldArray as $keyField => $nameField) {
                if (empty($arr)) {
                    $arr[] = 'or';
                }
                if (!in_array($keyField, $this->attributeInt())) {
                    $nameField = "IFNULL(" . $nameField . ", '')";
                }
                $arr[] = [
                    'like',
                    $nameField,
                    $search_word,
                    false,
                ];
            }
        }
        
        if (!empty($arr)) {
            $query->andFilterWhere($arr);
        }
        return $query;
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function field_filter($query)
    {
        $filterFieldArray = [
            'id' => self::tableName() . '.id',
            'avtor' => self::tableName() . '.avtor',
            'phone' => self::tableName() . '.phone',
        ];
        foreach ($filterFieldArray as $key => $field) {
            $name = is_integer($key) ? $this->{$field} : $this->{$key};
            if (!empty($name)) {
                $arr = [];
                $wordArray = explode(' ', $name);
                #$field = ''.self::tableName().'.[' . $field . ']';
                foreach ($wordArray as $word) {
                    if (empty($arr)) {
                        $arr[] = 'or';
                    }
                    $arr[] = [
                        'like',
                        $field,
                        '%' . $word . '%',
                        false,
                    ];
                }
                if (!empty($arr)) {
                    $query->andFilterWhere($arr);
                }
            }
        }
        return $query;
    }
    
    public function search($params)
    {
        $this->load($params);
        $query = $this->find();
//        $count = $this::find()->count('1');
    
        $showAllParam = '_tog' . hash('crc32', 'w0');
        $showAll = array_key_exists($showAllParam, $params) ? $params[$showAllParam] : 'page';
        $perPage = array_key_exists('per-page', $params) ? $params['per-page'] : 20;
        $count = $showAll == 'page' ? $perPage : PHP_INT_MAX;

        $query->select([
            self::tableName() . '.id',
            self::tableName() . '.avtor',
            self::tableName() . '.phone',
            self::tableName() . '.message',
            self::tableName() . '.date_add',
            self::tableName() . '.time_add',
            self::tableName() . '.subm',
        ])->limit($count)->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ],
        ]);
        
        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => [self::tableName() . '.id' => SORT_ASC],
                    'desc' => [self::tableName() . '.id' => SORT_DESC],
                ],
                'avtor' => [
                    'asc' => [self::tableName() . '.avtor' => SORT_ASC],
                    'desc' => [self::tableName() . '.avtor' => SORT_DESC],
                ],
                'phone' => [
                    'asc' => [self::tableName() . '.phone' => SORT_ASC],
                    'desc' => [self::tableName() . '.phone' => SORT_DESC],
                ],
                'date_publication' => [
                    'asc' => [
                        self::tableName() . '.date_add' => SORT_ASC,
                        self::tableName() . '.time_add' => SORT_ASC,
                    ],
                    'desc' => [
                        self::tableName() . '.date_add' => SORT_DESC,
                        self::tableName() . '.time_add' => SORT_DESC,
                    ],
                ],
            ],
            'defaultOrder' => [
                'date_publication' => SORT_DESC,
            ],
            'sortParam' => 'prior',
        ]);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $search_word = $this->getSearchWordText();
        if ($search_word != '') {
            $query = $this->search_filter($query);
        }
        $query = $this->field_filter($query);
        
        return $dataProvider;
    }
}