<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 28.01.2018
 * Time: 17:41
 */

namespace backend\models;

use backend\components\Common;
use Yii;

class chunkReadFilter implements \PHPExcel_Reader_IReadFilter
{
    private $_startRow = 0;
    private $_endRow = 0;
    
    public function setRows($startRow, $chunkSize)
    {
        $this->_startRow = $startRow;
        $this->_endRow = $startRow + $chunkSize;
    }
    
    public function readCell($column, $row, $worksheetName = '')
    {
        //  Only read the heading row, and the rows that are configured in $this->_startRow and $this->_endRow
        if (($row == 1) || ($row >= $this->_startRow && $row < $this->_endRow)) {
            return true;
        }
        return false;
    }
}

class OrdersExcel
{
    public static $coord_column = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    public static function fileTemplate()
    {
        return Yii::getAlias('@frontendDocroot/img/report.xlsx');
    }
    
    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/report/');
    }
    
    public static function unload($rows)
    {
        $nameReport = implode('', [
            "otchet",
//            Yii::$app->formatter->asDate('now', 'php:YmdHis'),
            Common::getDateTime('', 'YmdHis'),
        ]);
        
        \PHPExcel_Cell::setValueBinder(new \PHPExcel_Cell_AdvancedValueBinder());
        $objPHPExcel = \PHPExcel_IOFactory::load(self::fileTemplate());
        $offset_y = 2;
        
        foreach ($rows as $index => $row) {
//            $date_end = !empty($row->date_end) ? Yii::$app->formatter->asDate($row->date_end, 'php:d.m.Y H:i:s') : '';
            $date_end = !empty($row->date_end) ? Common::getDateTime($row->date_end, 'd.m.Y H:i:s') : '';
            $name = !empty($row->name) ? $row->name : '';
            $email = !empty($row->email) ? $row->email : '';
            $phone = !empty($row->phone) ? $row->phone : '';
            $coord_y = $offset_y + $index;
            $cell = self::$coord_column[0] . $coord_y;
            $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($date_end);
            $cell = self::$coord_column[1] . $coord_y;
            $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($name);
            $cell = self::$coord_column[2] . $coord_y;
            $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($email);
            $cell = self::$coord_column[3] . $coord_y;
            $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($phone);
        }
        $pathFile = implode('', [
            self::filePath(),
            $nameReport,
            '.xlsx',
        ]);
        
        if (file_exists($pathFile)) {
            unlink($pathFile);
        }
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($pathFile);
        
        return $pathFile;
    }
}