<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\validators\FileValidator;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

class EmailTemplateUpload extends Model
{
    public $foto;
    public $delete_foto;
    public $attachment;
    public $delete_attachment;

    public function formName()
    {
        return 'files';
    }

    public function rules()
    {
        return [
            [
                ['foto'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'jpg, png, gif',
                'maxSize' => (new FileValidator)->getSizeLimit(),
            ],
            [
                ['attachment'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'pdf, jpg, png, gif',
                'maxSize' => (new FileValidator)->getSizeLimit(),
            ],
            [
                [
                    'delete_foto',
                    'delete_attachment',
                ],
                'boolean',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'foto' => 'Выберите файл',
            'delete_foto' => 'удалить',
            'attachment' => 'Выберите файл',
            'delete_attachment' => 'удалить',
        ];
    }

    public static function mimeType()
    {
        return [
            'pdf' => 'application/pdf',
            'jpg' => ['image/jpeg', 'image/pjpeg'],
            'png' => 'image/png',
            'gif' => 'image/gif',
        ];
    }

    public static function mimeTypeImage()
    {
        return [
            'jpg' => ['image/jpeg', 'image/pjpeg'],
            'png' => 'image/png',
            'gif' => 'image/gif',
        ];
    }

    public static function mimeTypeList($mimeTypes)
    {
        $mimeTypeList = [];
        foreach ($mimeTypes as $mimeType) {
            if (is_array($mimeType)) {
                $mimeTypeList = array_merge($mimeTypeList, $mimeType);
            }

            if (is_string($mimeType)) {
                $mimeTypeList[] = $mimeType;
            }
        }
        return array_unique($mimeTypeList);
    }

    public static function isImage($mimeType)
    {
        if (in_array($mimeType, self::mimeTypeList(self::mimeTypeImage()))) {
            return true;
        }
        return false;
    }

    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/template/');
    }

    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/template/');
    }

    public static function size()
    {
        return [
            'foto' => [
                2000,
                1000,
            ],
            'attachment' => [
                2000,
                2000,
            ],
        ];
    }

    public static function changeSize($foto, $nameAttr)
    {
        $fileName = self::filePath() . $foto;
        if (self::isImage(mime_content_type($fileName))) {
            $imagine = new Imagine();
            $image = $imagine->open($fileName);
            $curSize = $image->getSize();
            $curWidth = $curSize->getWidth();
            $curHeight = $curSize->getHeight();
            $size = self::size()[$nameAttr];
            if ($size[0] > $size[1]) {
                if ($curWidth > $size[0]) {
                    $newwidth = $size[0];
                } else {
                    $newwidth = $curWidth;
                }
                $newheight = $curHeight * $newwidth / $curWidth;
            } else {
                if ($curHeight > $size[1]) {
                    $newheight = $size[1];
                } else {
                    $newheight = $curHeight;
                }
                $newwidth = $curWidth * $newheight / $curHeight;
            }
            $image->resize(new Box($newwidth, $newheight))->save($fileName);
        }
    }
}