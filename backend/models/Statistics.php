<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 24.12.2017
 * Time: 21:47
 */

namespace backend\models;

use yii\base\Model;

class Statistics extends Model
{
    public $dateStart, $dateEnd;
    public $id_university;
    
    public function attributeLabels()
    {
        return [
            'dateStart' => 'от',
            'dateEnd' => 'до',
            'id_university' => 'Выберите вуз',
        ];
    }
    
    public function formName()
    {
        return '';
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id_university',
                    'dateStart',
                    'dateEnd',
                ],
                'safe',
            ],
        ];
    }
}