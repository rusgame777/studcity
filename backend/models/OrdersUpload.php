<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\validators\FileValidator;
use Imagine\Image\Box;
use Imagine\Imagick\Imagine;

class OrdersUpload extends Model
{
    public $doc;
    public $delete_doc;

    public function formName()
    {
        return '';
    }

    public function rules()
    {
        return [
            [
                ['doc'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'doc, docx, pdf, rar, zip, jpg, png',
                'maxSize' => (new FileValidator)->getSizeLimit(),
            ],
            [
                ['delete_doc'],
                'boolean',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'doc' => 'Выберите файл',
            'delete_doc' => 'удалить',
        ];
    }

    public static function mimeType()
    {
        return [
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'pdf' => 'application/pdf',
            'rar' => 'application/x-rar-compressed',
            'zip' => ['application/zip', 'application/x-zip-compressed'],
            'jpg' => ['image/jpeg', 'image/pjpeg'],
        ];
    }

    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/solution/');
    }
    
    public static function fileSafetyUrl()
    {
        return Yii::getAlias('@backendWebroot/solution-orders/file/');
    }
    
    public static function docSafetyUrl()
    {
        return Yii::getAlias('@backendWebroot/solution-orders/doc/');
    }

    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/solution/');
    }

    public static function size()
    {
        return [
            'doc' => [
                800,
                600,
            ],
        ];
    }

    public static function changeSize($foto, $nameAttr)
    {
        $imagine = new Imagine();
        $fileName = self::filePath() . $foto;
        $image = $imagine->open($fileName);
        $curSize = $image->getSize();
        $curWidth = $curSize->getWidth();
        $curHeight = $curSize->getHeight();
        $size = self::size()[$nameAttr];
        if ($size[0] > $size[1]) {
            if ($curWidth > $size[0]) {
                $newwidth = $size[0];
            } else {
                $newwidth = $curWidth;
            }
            $newheight = $curHeight * $newwidth / $curWidth;
        } else {
            if ($curHeight > $size[1]) {
                $newheight = $size[1];
            } else {
                $newheight = $curHeight;
            }
            $newwidth = $curWidth * $newheight / $curHeight;
        }
        $image->resize(new Box($newwidth, $newheight))->save($fileName);
    }
}