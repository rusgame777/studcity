<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 31.12.2017
 * Time: 0:46
 */

namespace backend\models;

use yii\base\Model;

class UserCat extends Model
{
    public $catalog_cat, $solution_cat;
    
    public function attributeLabels()
    {
        return [
            'catalog_cat' => 'Готовые решения',
            'solution_cat' => 'Решение за мгновение',
        ];
    }
    
    public function formName()
    {
        return '';
    }
    
    public function rules()
    {
        return [
            [
                [
                    'catalog_cat',
                    'solution_cat',
                ],
                'safe',
            ],
            [
                [
                    'catalog_cat',
                    'solution_cat',
                ],
                'each',
                'rule' => ['integer'],
            ],
        ];
    }
}