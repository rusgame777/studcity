<?php

namespace backend\models;

use yii\base\Model;

class SolutionStatistics extends Model
{
    public $dateStart, $dateEnd;
    public $id_author;
    
    public function attributeLabels()
    {
        return [
            'dateStart' => 'от',
            'dateEnd' => 'до',
            'id_author' => 'Выберите автора',
        ];
    }
    
    public function formName()
    {
        return '';
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id_author',
                    'dateStart',
                    'dateEnd',
                ],
                'safe',
            ],
        ];
    }
}