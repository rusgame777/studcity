<?php

namespace backend\models;

use backend\components\Common;
use common\models\Author;
use common\models\Clients;
use common\models\Formatter;
use common\models\Orders;
use common\models\SolutionOrderDesc;
use common\models\OrderStatus;
use common\models\OrderType;
use common\models\SolutionPosition;
use common\models\UserSolution;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class SolutionOrdersManagement extends Orders
{
    public $dateStart, $dateEnd;
    public $name, $phone, $email, $contacts, $name_position, $name_author;
    public $id_author, $cat;
    
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'date_end' => 'Время оплаты',
            'name' => 'Фамилия, имя',
            'contacts' => 'Телефон(E-mail)',
            'name_position' => 'Название РГР',
            'name_author' => 'Автор',
            'sum' => 'Сумма',
            'id_author' => 'Выберите автора',
            'cat' => 'Выберите предмет',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'date_end',
                    'contacts',
                    'name',
                    'sum',
                ],
                'safe',
            ],
            [
                [
                    'cat',
                    'id_author',
                    'name_position',
                    'name_author',
                    'dateStart',
                    'dateEnd',
                ],
                'safe',
            ],
            [
                [
                    'dateStart',
                ],
                'default',
                'value' => function() {
                    return self::getMinDate();
                },
            
            ],
            [
                [
                    'dateEnd',
                ],
                'default',
                'value' => function() {
                    return self::getMaxDate();
                },
            ],
        ];
    }
    
    public function attributeInt()
    {
        return [
            'id',
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function formName()
    {
        return '';
    }
    
    public function getDate_start()
    {
        return Formatter::getDateStart();
    }
    
    public function getDate_end()
    {
        return Formatter::getDateEnd();
    }
    
    public static function getMaxDate()
    {
        return Orders::find()->where([
            'and',
            ['id_type' => OrderType::solution],
            ['send' => 1],
            ['status' => OrderStatus::send],
        ])->max('DATE_FORMAT(date_end, \'%Y-%m-%d\')');
    }
    
    public static function getMinDate()
    {
        return Orders::find()->where([
            'and',
            ['id_type' => OrderType::solution],
            ['send' => 1],
            ['status' => OrderStatus::send],
        ])->min('DATE_FORMAT(date_end, \'%Y-%m-%d\')');
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function search_filter($query)
    {

//        $dateStart = Yii::$app->formatter->asDate($this->dateStart, 'php:Y-m-d');
//        $dateEnd = Yii::$app->formatter->asDate($this->dateEnd, 'php:Y-m-d');
        $dateStart = Common::getDateTime($this->dateStart, 'Y-m-d');
        $dateEnd = Common::getDateTime($this->dateEnd, 'Y-m-d');
        
        $query->andFilterWhere(['between', new Expression("DATE_FORMAT(date_end, '%Y-%m-%d')"), $dateStart, $dateEnd]);
        if (intval($this->cat) > 0) {
            $query->andFilterWhere(['orderdesc_solution.cat' => $this->cat]);
        }
        if (intval($this->id_author) > 0) {
            $query->andFilterWhere(['solution_position.id_author' => $this->id_author]);
        }
        if (Yii::$app->view->params['id_role'] == 1) {
            $query->andFilterWhere([
                'in',
                'solution_position.cat',
                UserSolution::find()->select(['id_cat'])->where(['id_user' => Yii::$app->view->params['id_user']]),
            ]);
        }
        
        return $query;
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function field_filter($query)
    {
        $filterFieldArray = [
            'sum' => self::tableName() . '.sum',
            'date_end' => self::tableName() . '.date_end',
            'name' => 'client.name',
            'phone' => 'client.phone',
            'email' => 'client.email',
            'name_author' => Author::tableName() . '.name',
            'name_position' => SolutionPosition::tableName() . '.title',
        ];
        foreach ($filterFieldArray as $key => $field) {
            $name = is_integer($key) ? $this->{$field} : $this->{$key};
            if (!empty($name)) {
                $arr = [];
                $wordArray = explode(' ', $name);
                foreach ($wordArray as $word) {
                    if (empty($arr)) {
                        $arr[] = 'or';
                    }
                    $arr[] = [
                        'like',
                        $field,
                        '%' . $word . '%',
                        false,
                    ];
                }
                if (!empty($arr)) {
                    $query->andFilterWhere($arr);
                }
            }
        }
        return $query;
    }
    
    public function search($params)
    {
        $this->load($params);
        $query = $this->find();
        
        $showAllParam = '_tog' . hash('crc32', 'w0');
        $showAll = array_key_exists($showAllParam, $params) ? $params[$showAllParam] : 'page';
        $perPage = array_key_exists('per-page', $params) ? $params['per-page'] : 20;
        $count = $showAll == 'page' ? $perPage : PHP_INT_MAX;
        
        $query->select([
            self::tableName() . '.id',
            self::tableName() . '.sum',
            self::tableName() . '.date_end',
            'client.name',
            'client.phone',
            'client.email',
            Author::tableName() . '.name AS name_author',
            SolutionPosition::tableName() . '.title AS name_position',
        ])->joinWith('user as client', $eagerLoading = true, $joinType = 'LEFT JOIN')
            ->leftJoin(SolutionOrderDesc::tableName(), self::tableName() . '.id = ' . SolutionOrderDesc::tableName() . '.id_order')
            ->leftJoin(SolutionPosition::tableName(), SolutionPosition::tableName() . '.id = ' . SolutionOrderDesc::tableName() . '.id_position')
            ->leftJoin(Author::tableName(), Author::tableName() . '.id = ' . SolutionPosition::tableName() . '.id_author')
            ->where([
                'and',
                [self::tableName() . '.id_type' => OrderType::solution],
                [self::tableName() . '.send' => 1],
                ['status' => OrderStatus::send],
            ])->distinct()->limit($count)->all();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ],
        ]);
        
        $dataProvider->setSort([
            'attributes' => [
                'date_end' => [
                    'asc' => [self::tableName() . '.date_end' => SORT_ASC],
                    'desc' => [self::tableName() . '.date_end' => SORT_DESC],
                ],
                'sum' => [
                    'asc' => [self::tableName() . '.sum' => SORT_ASC],
                    'desc' => [self::tableName() . '.sum' => SORT_DESC],
                ],
                'name' => [
                    'asc' => ['client.name' => SORT_ASC],
                    'desc' => ['client.name' => SORT_DESC],
                ],
                'name_position' => [
                    'asc' => [SolutionPosition::tableName() . '.title' => SORT_ASC],
                    'desc' => [SolutionPosition::tableName() . '.title' => SORT_DESC],
                ],
                'name_author' => [
                    'asc' => [Author::tableName() . '.name' => SORT_ASC],
                    'desc' => [Author::tableName() . '.name' => SORT_DESC],
                ],
            ],
            'defaultOrder' => [
                'date_end' => SORT_DESC,
            ],
            'sortParam' => 'prior',
        ]);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query = $this->search_filter($query);
        $query = $this->field_filter($query);
        
        return $dataProvider;
    }
    
    public function getUser()
    {
        return $this->hasOne(Clients::className(), ['id' => 'id_user']);
    }
    
    public function getOrderId()
    {
//        $date_end = Yii::$app->formatter->asDate($this->date_end, 'php:Y-m-d');
        $date_end = Common::getDateTime($this->date_end, 'Y-m-d');
        $num_orders = Orders::find()->where([
            'and',
            ['id_type' => OrderType::solution],
            ['send' => 1],
            ['status' => OrderStatus::send],
            [
                'between',
                'date_end',
                //                Yii::$app->formatter->asDate($date_end, 'php:Y-m-d'),
                //                Yii::$app->formatter->asDate($this->date_end, 'php:Y-m-d H:i:s'),
                Common::getDateTime($date_end, 'Y-m-d'),
                Common::getDateTime($this->date_end, 'Y-m-d H:i:s'),
            ],
        ])->count(1);
        return implode('/', [
            $num_orders,
            //            Yii::$app->formatter->asDate($this->date_end, 'php:d.m.Y') . '(' . $this->id . ')',
            Common::getDateTime($this->date_end, 'd.m.Y') . '(' . $this->id . ')',
        ]);
    }
    
    public static function getNumberList()
    {
        $tableOrders1 = vsprintf('
            (SELECT  
               id, date_end
              FROM orders
            WHERE (id_type = %1$s)
            AND (send = 1)
            AND (status IN (%2$s))) orders1
        ', [
            1 => OrderType::solution,
            2 => OrderStatus::send,
        ]);
        
        $tableOrders2 = vsprintf('
            (SELECT  
               id, date_end
              FROM orders
            WHERE (id_type = %1$s)
            AND (send = 1)
            AND (status IN (%2$s))) orders2
        ', [
            1 => OrderType::solution,
            2 => OrderStatus::send,
        ]);
        
        $results = self::find()->select([
            'orders1.id',
            'DATE_FORMAT(orders1.date_end, \'%d.%m.%Y\') date_end',
            'COUNT(orders2.id) count',
        ])->from([$tableOrders1])->innerJoin($tableOrders2, 'orders2.date_end BETWEEN CONCAT_WS(\' \', DATE_FORMAT(orders1.date_end, \'%Y-%m-%d\'), \'00:00:00\')
AND DATE_FORMAT(orders1.date_end, \'%Y-%m-%d %H:%i:%s\')')->groupBy(['orders1.id'])
            ->orderBy(['orders1.date_end' => SORT_DESC])->asArray()->all();
        
        foreach ($results as $index => $result) {
            $results[$result['id']] = $result;
            unset($results[$index]);
        }
        return $results;
    }
}