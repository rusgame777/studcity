<?php

namespace backend\models;

use common\models\Page;
use common\models\PageCategory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use common\components\Table;

class PageSearch extends Page
{
    public $id_type, $search_word;
    public $name;
    
    public function attributeLabels()
    {
        return [
            'sort' => 'Приоритет',
            'name' => 'Наименование',
            'nameurl' => 'URL-адрес страницы',
            'id_category' => 'Категория',
            'indic' => 'Нельзя удалить?',
            'show_content' => 'Показать контент?',
        ];
    }
    
    public function rules()
    {
        return [
            [
                'indic',
                'in',
                'range' => [
                    self::STATUS_ACTIVE,
                    self::STATUS_INACTIVE,
                ],
            ],
            [
                [
                    'id',
                    'name',
                    'nameurl',
                    'indic',
                    'show_content',
                ],
                'safe',
            ],
            [
                [
                    'search_word',
                    'id_type',
                ],
                'safe',
            ],
            [
                ['sort', 'id_category'],
                'integer',
            ],
        ];
    }
    
    public function attributeInt()
    {
        return [
            'id_category',
            'id',
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function formName()
    {
        return '';
    }
    
    public function getSearch_word()
    {
        return Html::encode(!empty(Yii::$app->request->get('search_word')) ? Yii::$app->request->get('search_word') : '');
    }
    
    public function getSearchWordText()
    {
        return (isset(Yii::$app->request->get()['search_word']) ? Yii::$app->request->get()['search_word'] : '');
    }
    
    public function getId_type()
    {
        return intval(!empty(Yii::$app->request->get('id_type')) ? Yii::$app->request->get('id_type') : 1);
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function search_filter($query)
    {
        $id_type = $this->getId_type();
        $search_word = $this->getSearchWordText();
        
        $filterFieldArray = [
            'id' => self::tableName() . '.id',
            'name' => self::tableName() . '.title',
            'title' => PageCategory::tableName() . '.title',
        ];
        $searchFieldArray = [
            'id' => self::tableName() . '.id',
            'name' => self::tableName() . '.title',
            'title' => PageCategory::tableName() . '.title',
        ];
        
        $fields = [];
        
        foreach ($filterFieldArray as $keyField => $nameField) {
            if (!in_array($keyField, $this->attributeInt())) {
                $nameField = "IFNULL(" . $nameField . ", '')";
            }
            if (in_array($keyField, $this->attributeInt())) {
                $nameField = 'CONVERT(' . $nameField . ', CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci';
            }
            $fields[] = $nameField;
        }
        $fieldSqlString = implode('', [
            'CONCAT(',
            implode(", ' ',  ", $fields),
            ')',
        ]);
        
        $arr = [];
        if ($id_type != 3) {
            $wordArray = explode(' ', $search_word);
            foreach ($wordArray as $word) {
                if (empty($arr)) {
                    $arr[] = ($id_type == 1) ? 'or' : 'and';
                }
                $arr[] = [
                    'like',
                    $fieldSqlString,
                    '%' . $word . '%',
                    false,
                ];
            }
        } else {
            foreach ($searchFieldArray as $keyField => $nameField) {
                if (empty($arr)) {
                    $arr[] = 'or';
                }
                if (!in_array($keyField, $this->attributeInt())) {
                    $nameField = "IFNULL(" . $nameField . ", '')";
                }
                $arr[] = [
                    'like',
                    $nameField,
                    $search_word,
                    false,
                ];
            }
        }
        
        if (!empty($arr)) {
            $query->andFilterWhere($arr);
        }
        return $query;
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function field_filter($query)
    {
        $filterFieldArray = [
            'id' => self::tableName() . '.id',
            'name' => self::tableName() . '.title',
            'title' => PageCategory::tableName() . '.title',
        ];
        foreach ($filterFieldArray as $key => $field) {
            $name = is_integer($key) ? $this->{$field} : $this->{$key};
            if (!empty($name)) {
                $arr = [];
                $wordArray = explode(' ', $name);
                #$field = ''.self::tableName().'.[' . $field . ']';
                foreach ($wordArray as $word) {
                    if (empty($arr)) {
                        $arr[] = 'or';
                    }
                    $arr[] = [
                        'like',
                        $field,
                        '%' . $word . '%',
                        false,
                    ];
                }
                if (!empty($arr)) {
                    $query->andFilterWhere($arr);
                }
            }
        }
        return $query;
    }
    
    public function search($params)
    {
        $this->load($params);
        $query = $this->find();
//        $count = $this::find()->count('1');
    
        $showAllParam = '_tog' . hash('crc32', 'w0');
        $showAll = array_key_exists($showAllParam, $params) ? $params[$showAllParam] : 'page';
        $perPage = array_key_exists('per-page', $params) ? $params['per-page'] : 20;
        $count = $showAll == 'page' ? $perPage : PHP_INT_MAX;
        
        $query->select([
            self::tableName() . '.id',
            self::tableName() . '.sort',
            self::tableName() . '.title name',
            self::tableName() . '.nameurl',
            self::tableName() . '.indic',
            self::tableName() . '.show_content',
            self::tableName() . '.id_category id_category',
        ])->joinWith('categories')->limit($count)->all();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ],
        ]);
        
        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => [self::tableName() . '.id' => SORT_ASC],
                    'desc' => [self::tableName() . '.id' => SORT_DESC],
                ],
                'sort' => [
                    'asc' => [self::tableName() . '.sort' => SORT_ASC],
                    'desc' => [self::tableName() . '.sort' => SORT_DESC],
                ],
                'name' => [
                    'asc' => [self::tableName() . '.title' => SORT_ASC],
                    'desc' => [self::tableName() . '.title' => SORT_DESC],
                ],
                'nameurl' => [
                    'asc' => [self::tableName() . '.nameurl' => SORT_ASC],
                    'desc' => [self::tableName() . '.nameurl' => SORT_DESC],
                ],
            ],
            'defaultOrder' => [
                'name' => SORT_DESC,
            ],
            'sortParam' => 'prior',
        ]);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $search_word = $this->getSearchWordText();
        if ($search_word != '') {
            $query = $this->search_filter($query);
        }
        $query = $this->field_filter($query);
        
        return $dataProvider;
    }
    
    public static function changeSort($params)
    {
        $cur_sort = -1;
        $id_type = $params['id_type'];
        $cur_id = $params['cur_id'];
        $results = Page::find()->orderBy(['sort' => SORT_DESC])->asArray()->all();
        $resultsSortArray = ArrayHelper::map($results, 'id', 'sort');
        foreach ($resultsSortArray as $id => $sort) {
            if ($cur_id == $id) {
                $cur_sort = $sort;
                break;
            }
        }
        if ($cur_sort == -1) {
            return true;
        }
        
        if (!current($resultsSortArray)) {
            end($resultsSortArray);
        } else {
            prev($resultsSortArray);
        }
        
        if ($id_type == 'up') {
            prev($resultsSortArray);
            $prev_sort = current($resultsSortArray);
            $prev_id = key($resultsSortArray);
            Yii::$app->db->createCommand()->update(Page::tableName(), ['sort' => $prev_sort], "id = {$cur_id}")
                ->execute();
            Yii::$app->db->createCommand()->update(Page::tableName(), ['sort' => $cur_sort], "id = {$prev_id}")
                ->execute();
        } elseif ($id_type == 'down') {
            next($resultsSortArray);
            $next_sort = current($resultsSortArray);
            $next_id = key($resultsSortArray);
            Yii::$app->db->createCommand()->update(Page::tableName(), ['sort' => $next_sort], "id = {$cur_id}")
                ->execute();
            Yii::$app->db->createCommand()->update(Page::tableName(), ['sort' => $cur_sort], "id = {$next_id}")
                ->execute();
        }
        return true;
    }
    
    public function beforeValidate()
    {
        return true;
    }
}