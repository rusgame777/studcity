<?php
/**
 * Created by PhpStorm.
 * User: Evgenii
 * Date: 25.01.2018
 * Time: 1:35
 */

namespace backend\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * OrderForm model
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $cat
 * @property integer $id_position
 */
class OrderForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $cat;
    public $id_position;
    
    public function formName()
    {
        return '';
    }
    
    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия, Имя',
            'phone' => 'Номер телефона',
            'email' => 'Электронная почта',
            'cat' => 'Дисциплина',
            'id_position' => 'Методичка',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'phone',
                    'email',
                ],
                'string',
            ],
            [
                [
                    'id_position',
                    'cat',
                ],
                'integer',
            ],
            [
                'phone',
                'match',
                'pattern' => '/^8\d{10}$/i',
            ],
            [
                'email',
                'email',
                'checkDNS' => true,
            ],
            [
                [
                    'name',
                    'phone',
                    'email',
                    'id_position',
                    'cat',
                ],
                'required',
            ],
        ];
    }
    
    public function beforeValidate()
    {
        try {
            $search_phone = "/(\+7) (\d{3}) (\d{3}) (\d{4})/i";
            $replacement_phone = "8\$2\$3\$4";
            $this->phone = preg_replace($search_phone, $replacement_phone, $this->phone);
        } catch (Exception $e) {
            //
        }
        return true;
    }
}