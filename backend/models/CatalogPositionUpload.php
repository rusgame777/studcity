<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\validators\FileValidator;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;

class CatalogPositionUpload extends Model
{
    public $file;
    public $delete_file;
    
    public function formName()
    {
        return '';
    }
    
    public function rules()
    {
        return [
            [
                ['file'],
                'file',
                'skipOnEmpty' => true,
                'extensions' => 'doc, docx, pdf, rar, zip, jpg, png',
                'maxSize' => (new FileValidator)->getSizeLimit(),
            ],
            [
                ['delete_file'],
                'boolean',
            ],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'Выберите файл',
            'delete_file' => 'удалить',
        ];
    }
    
    public static function mimeType()
    {
        return [
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'rar' => 'application/x-rar-compressed',
            'zip' => ['application/zip', 'application/x-zip-compressed'],
            'pdf' => 'application/pdf',
            'jpg' => ['image/jpeg', 'image/jpg'],
        ];
    }
    
    public static function isPhoto()
    {
        return [
            'jpg',
        ];
    }
    
    public static function fileUrlDefault() {
        return Yii::getAlias('@frontendWebroot/uploads/no_manual.png');
    }
    
    public static function filePathDefault()
    {
        return Yii::getAlias('@frontendDocroot/uploads/no_manual.png');
    }
    
    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/work/');
    }
    
    public static function fileSafetyUrl()
    {
        return Yii::getAlias('@backendWebroot/catalog-position/position-file/');
    }
    
    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/work/');
    }
    
    public static function size()
    {
        return [
            'file' => [
                2000,
                1000,
            ],
        ];
    }
    
    public static function changeSize($file, $nameAttr)
    {
        $imagine = new Imagine();
        $fileName = self::filePath() . $file;
        $image = $imagine->open($fileName);
        $curSize = $image->getSize();
        $curWidth = $curSize->getWidth();
        $curHeight = $curSize->getHeight();
        $size = self::size()[$nameAttr];
        if ($size[0] > $size[1]) {
            if ($curWidth > $size[0]) {
                $newwidth = $size[0];
            } else {
                $newwidth = $curWidth;
            }
            $newheight = $curHeight * $newwidth / $curWidth;
        } else {
            if ($curHeight > $size[1]) {
                $newheight = $size[1];
            } else {
                $newheight = $curHeight;
            }
            $newwidth = $curWidth * $newheight / $curHeight;
        }
        $image->resize(new Box($newwidth, $newheight))->save($fileName);
    }
}