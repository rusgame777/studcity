<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\validators\FileValidator;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Imagick\Imagine;
use yii\helpers\ArrayHelper;
use common\components\Table;

/**
 * CatalogFiles model
 *
 * @property integer $id
 * @property string $title
 * @property string $file
 * @property integer $position
 *
 */
class CatalogFiles extends ActiveRecord
{
    public $delete_file;
    
    public static function tableName()
    {
        return Table::get('catalog_files');
    }
    
    public function formName()
    {
        return '';
    }
    
    public function rules()
    {
        return [
            [
                [
                    'file',
                    'title',
                ],
                'string',
            ],
            [
                ['delete_file'],
                'boolean',
            ],
            [
                'position',
                'integer',
            ],
            [
                [
                    'file',
                    'position',
                    'title',
                ],
                'required',
            ]
            //            [
            //                ['file'],
            //                'file',
            //                'skipOnEmpty' => false,
            //                'extensions' => 'doc, docx, rar, zip, pdf, jpg',
            //                'maxSize' => (new FileValidator)->getSizeLimit(),
            //            ],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'file' => 'Выберите файл',
            'delete_file' => 'удалить',
        ];
    }
    
    public static function mimeType()
    {
        return [
            'doc' => 'application/msword',
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'rar' => 'application/x-rar-compressed',
            'zip' => ['application/zip', 'application/x-zip-compressed'],
            'pdf' => 'application/pdf',
            'jpg' => ['image/jpeg', 'image/jpg'],
        ];
    }
    
    public static function isPhoto()
    {
        return [
            'jpg',
        ];
    }
    
    public static function mimeTypeList()
    {
        $mimeTypeList = [];
        foreach (self::mimeType() as $mimeType) {
            if (is_array($mimeType)) {
                $mimeTypeList = array_merge($mimeTypeList, $mimeType);
            }
            
            if (is_string($mimeType)) {
                $mimeTypeList[] = $mimeType;
            }
        }
        return array_unique($mimeTypeList);
    }
    
    public static function fileUrlDefault()
    {
        return Yii::getAlias('@frontendWebroot/uploads/no_manual.png');
    }
    
    public static function filePathDefault()
    {
        return Yii::getAlias('@frontendDocroot/uploads/no_manual.png');
    }
    
    public static function fileUrl()
    {
        return Yii::getAlias('@frontendWebroot/uploads/work/');
    }
    
    public static function fileSafetyUrl()
    {
        return Yii::getAlias('@backendWebroot/catalog-position/file/');
    }
    
    public static function filePath()
    {
        return Yii::getAlias('@frontendDocroot/uploads/work/');
    }
    
    public static function size()
    {
        return [
            'file' => [
                2000,
                1000,
            ],
        ];
    }
    
    public static function upload($params)
    {
        if (in_array($params['mime_type'], CatalogFiles::mimeTypeList())) {
            $name_ext = self::getExtensionByMimeType($params['mime_type']);
            $data = explode(',', $params['file']);
            $encodedData = str_replace(' ', '+', $data[1]);
            $decodedData = base64_decode($encodedData);
            
            $nameFile = substr_replace(sha1(microtime(true)), '', 32) . '.' . $name_ext;
            file_put_contents(self::filePath() . $nameFile, $decodedData);
            
            if (in_array($name_ext, self::isPhoto())) {
                self::changeSize($nameFile, 'file');
            }
            return $nameFile;
        }
        return null;
    }
    
    public static function getExtension()
    {
        return Yii::$app->db->createCommand(vsprintf('SELECT * FROM %1$s', [
            1 => Table::get('rb_filter'),
        ]))->queryAll();
    }
    
    public static function getExtensionByMimeType($mime_type)
    {
        $extensions = ArrayHelper::index(self::getExtension(), 'mime_type');
        return $extensions[$mime_type]['name_ext'];
    }
    
    public static function getUrlExtensionByMimeType($mime_type)
    {
        $extensions = ArrayHelper::index(self::getExtension(), 'mime_type');
        return $extensions[$mime_type]['url_ext'];
    }
    
    public static function changeSize($file, $nameAttr)
    {
        $imagine = new Imagine();
        $fileName = self::filePath() . $file;
        $image = $imagine->open($fileName);
        $curSize = $image->getSize();
        $curWidth = $curSize->getWidth();
        $curHeight = $curSize->getHeight();
        $size = self::size()[$nameAttr];
        if ($size[0] > $size[1]) {
            if ($curWidth > $size[0]) {
                $newwidth = $size[0];
            } else {
                $newwidth = $curWidth;
            }
            $newheight = $curHeight * $newwidth / $curWidth;
        } else {
            if ($curHeight > $size[1]) {
                $newheight = $size[1];
            } else {
                $newheight = $curHeight;
            }
            $newwidth = $curWidth * $newheight / $curHeight;
        }
        $image->resize(new Box($newwidth, $newheight))->save($fileName);
    }
}