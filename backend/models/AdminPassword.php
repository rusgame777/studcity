<?php
namespace backend\models;

use Yii;
use common\models\Admin;

/**
 * Password model
 *
 * @property integer $id
 * @property string $password_old
 * @property string $password_new
 * @property string $password_repeat
 *
 */
class AdminPassword extends Admin
{
    public $password_old;
    public $password_new;
    public $password_repeat;
    
    public function attributeLabels()
    {
        return [
            'password_old' => 'Текущий пароль',
            'password_new' => 'Новый пароль',
            'password_repeat' => 'Повторите новый пароль',
        ];
    }
    
    public function validatePass($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $admin = $this->getAdmin();
            if (!$admin || !$admin->validatePassword($this->password_old)) {
                $this->addError($attribute, 'Неправильный пароль');
            }
        }
    }
    
    public function coincidence($attribute, $params)
    {
        
        if ($this->$attribute != $this->password_new) {
            $this->addError($attribute, 'Подтверждение не совпадает с паролем');
        }
        return true;
    }
    
    public function rules()
    {
        return [
            [
                'password_old',
                'validatePass',
            ],
            [
                'password_repeat',
                'coincidence',
            ],
            [
                [
                    'password_old',
                    'password_new',
                    'password_repeat',
                ],
                'string',
            ],
            [
                [
                    'password_old',
                    'password_new',
                    'password_repeat',
                ],
                'required',
            ],
            [
                'id',
                'integer',
            ],
            [
                [
                    'id',
                    'login',
                    'password_hash',
                ],
                'safe',
            ],
        ];
    }
    
    public function beforeSave($insert)
    {
        
        if (parent::beforeSave($insert)) {
//            $password = isset(Yii::$app->request->post()['password']) ? Yii::$app->request->post()['password'] : '';
//            if ($password != '') {
//                $this->password = md5($password);
//            }
            return true;
        }
        return false;
    }
}