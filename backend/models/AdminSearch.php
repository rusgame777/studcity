<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use common\models\Admin;

class AdminSearch extends Admin
{
    public $id_type, $search_word;
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'login' => 'Логин',
            'email' => 'E-mail',
        ];
    }
    
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'name',
                    'login',
                    'email',
                ],
                'safe',
            ],
            [
                [
                    'search_word',
                    'id_type',
                ],
                'safe',
            ],
        ];
    }
    
    public function attributeInt()
    {
        return [
            'id',
        ];
    }
    
    public function scenarios()
    {
        return Model::scenarios();
    }
    
    public function formName()
    {
        return '';
    }
    
    public function getSearch_word()
    {
        return Html::encode(!empty(Yii::$app->request->get('search_word')) ? Yii::$app->request->get('search_word') : '');
    }
    
    public function getSearchWordText()
    {
        return (isset(Yii::$app->request->get()['search_word']) ? Yii::$app->request->get()['search_word'] : '');
    }
    
    public function getId_type()
    {
        return intval(!empty(Yii::$app->request->get('id_type')) ? Yii::$app->request->get('id_type') : 1);
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function search_filter($query)
    {
        $id_type = $this->getId_type();
        $search_word = $this->getSearchWordText();
        
        $filterFieldArray = [
            'id' => self::tableName() . '.id',
            'name' => self::tableName() . '.name',
            'login' => self::tableName() . '.login',
            'email' => self::tableName() . '.email',
        ];
        $searchFieldArray = [
            'id' => self::tableName() . '.id',
            'name' => self::tableName() . '.name',
            'login' => self::tableName() . '.login',
            'email' => self::tableName() . '.email',
        ];
        
        $fields = [];
        
        foreach ($filterFieldArray as $keyField => $nameField) {
            if (!in_array($keyField, $this->attributeInt())) {
                $nameField = "IFNULL(" . $nameField . ", '')";
            }
            if (in_array($keyField, $this->attributeInt())) {
                $nameField = 'CONVERT(' . $nameField . ', CHAR CHARACTER SET utf8) COLLATE utf8_unicode_ci';
            }
            $fields[] = $nameField;
        }
        $fieldSqlString = implode('', [
            'CONCAT(',
            implode(", ' ',  ", $fields),
            ')',
        ]);
        
        $arr = [];
        if ($id_type != 3) {
            $wordArray = explode(' ', $search_word);
            foreach ($wordArray as $word) {
                if (empty($arr)) {
                    $arr[] = ($id_type == 1) ? 'or' : 'and';
                }
                $arr[] = [
                    'like',
                    $fieldSqlString,
                    '%' . $word . '%',
                    false,
                ];
            }
        } else {
            foreach ($searchFieldArray as $keyField => $nameField) {
                if (empty($arr)) {
                    $arr[] = 'or';
                }
                if (!in_array($keyField, $this->attributeInt())) {
                    $nameField = "IFNULL(" . $nameField . ", '')";
                }
                $arr[] = [
                    'like',
                    $nameField,
                    $search_word,
                    false,
                ];
            }
        }
        
        if (!empty($arr)) {
            $query->andFilterWhere($arr);
        }
        return $query;
    }
    
    /**
     * @param $query \yii\db\ActiveQuery
     * @return \yii\db\ActiveQuery
     */
    public function field_filter($query)
    {
        $filterFieldArray = [
            'id' => self::tableName() . '.id',
            'name' => self::tableName() . '.name',
            'login' => self::tableName() . '.login',
            'email' => self::tableName() . '.email',
        ];
        foreach ($filterFieldArray as $key => $field) {
            $name = is_integer($key) ? $this->{$field} : $this->{$key};
            if (!empty($name)) {
                $arr = [];
                $wordArray = explode(' ', $name);
                foreach ($wordArray as $word) {
                    if (empty($arr)) {
                        $arr[] = 'or';
                    }
                    $arr[] = [
                        'like',
                        $field,
                        '%' . $word . '%',
                        false,
                    ];
                }
                if (!empty($arr)) {
                    $query->andFilterWhere($arr);
                }
            }
        }
        return $query;
    }
    
    public function search($params)
    {
        $this->load($params);
        $query = $this->find();
//        $count = $this::find()->count(1);
    
        $showAllParam = '_tog' . hash('crc32', 'w0');
        $showAll = array_key_exists($showAllParam, $params) ? $params[$showAllParam] : 'page';
        $perPage = array_key_exists('per-page', $params) ? $params['per-page'] : 20;
        $count = $showAll == 'page' ? $perPage : PHP_INT_MAX;
        
        $where = [];
        if (Yii::$app->view->params['id_role'] == -1) {
            $where = ['>', 'id', 1];
        } elseif (Yii::$app->view->params['id_role'] == 1) {
            $where = ['>', 'id', 2];
        }
        $query->select([
            self::tableName() . '.id',
            self::tableName() . '.name',
            self::tableName() . '.login',
            self::tableName() . '.email',
        ])->where($where)->limit($count)->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $count,
            ],
        ]);
        
        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => [self::tableName() . '.id' => SORT_ASC],
                    'desc' => [self::tableName() . '.id' => SORT_DESC],
                ],
                'name' => [
                    'asc' => [self::tableName() . '.name' => SORT_ASC],
                    'desc' => [self::tableName() . '.name' => SORT_DESC],
                ],
                'login' => [
                    'asc' => [self::tableName() . '.login' => SORT_ASC],
                    'desc' => [self::tableName() . '.login' => SORT_DESC],
                ],
                'email' => [
                    'asc' => [self::tableName() . '.email' => SORT_ASC],
                    'desc' => [self::tableName() . '.email' => SORT_DESC],
                ],
            ],
            'defaultOrder' => [
                'name' => SORT_DESC,
            ],
            'sortParam' => 'prior',
        ]);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $search_word = $this->getSearchWordText();
        if ($search_word != '') {
            $query = $this->search_filter($query);
        }
        $query = $this->field_filter($query);
        
        return $dataProvider;
    }
}