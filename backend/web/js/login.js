function auth_enter() {
    var data = {};
    data['login'] = $('input[name=auth_login]').val();
    data['password'] = $('input[name=auth_password]').val();
    console.log(data);
    
    $.ajax({
        type: "POST",
        url: "/ajax/login/",
        dataType: "json",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        traditional: true,
        cache: false,
        success: function (response, textStatus, jqXHR) {
            var responseArray = JSON.parse(jqXHR.responseText),
                status = parseInt(responseArray.status);
            switch (status) {
                case 0:
                    var message = responseArray.message;
                    alert(message);
                    break;

                case 1:
                    var url = responseArray.url;
                    document.location = url;
                    break;

                default:
            }
        }
    });
}

$(function () {

    $('.auth_info').on('click', '.auth_submit', function (e) {
        auth_enter();
    });

    $('.auth_info').on('keydown', 'input', function (e) {
        if (e.keyCode == 13) {
            auth_enter();
        }
    });

});