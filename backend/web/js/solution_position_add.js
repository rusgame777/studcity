$(function () {

    var page_section = $('#page_section'),
        dynamic_form = $('#dynamic-form'),
        url_submit = dynamic_form.attr('action'),
        url_duplicate = url_submit.replace('update', 'duplicate');

    $(dynamic_form).on('click', '#submiting', function () {
        dynamic_form.attr('action', url_submit);
    });
    $(dynamic_form).on('click', '#duplicate', function () {
        dynamic_form.attr('action', url_duplicate);
    });
    $(dynamic_form).on('submit', function () {
    });

    page_section.on("change", "input[type=file]", function () {
        if (!$(this).attr('multiple')) {
            var name, checkbox;
            if ($(this).parents('.file_list').length > 0) {
                name = $(this).attr('name').replace('name_file', 'delete_name_file');
                checkbox = $('input[name="' + name + '"]');
            } else {
                name = 'delete_' + $(this).attr('name');
                checkbox = $('input[name=' + name + ']');
            }
            if (checkbox.length > 0) {
                checkbox.attr("checked", "checked");
            }
        }
    });

    $("input[name=title]").on('input', function () {
        var title = $("input[name=title]").val();
        $("input[name=metatitle]").val(title);
    });

});