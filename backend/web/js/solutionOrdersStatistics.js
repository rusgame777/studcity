/**
 * Created by Evgenii on 22.12.2017.
 */

$(function () {


    function getOrdersStatistics() {
        var data = {
            dateStart: $.getUrlParam('dateStart'),
            dateEnd: $.getUrlParam('dateEnd'),
            id_author: $.getUrlParam('id_author')
        };

        $.ajax({
            type: "POST",
            url: "/ajax/solution-statistics/",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var data = JSON.parse(jqXHR.responseText);
                $('#charts').html(data.html);
                if (data.data) {
                    for (var index in data.data) {
                        if (data.data.hasOwnProperty(index)) {
                            var array = data.data[index];
                            array = $.map(array, function (value, key) {
                                return value;
                            });
                            if (array.length > 0) {
                                getHighChartStatistics(array, index);
                            } else {
                                $('#section_chart' + index).html('нет данных');
                            }
                        }
                    }
                }
            },
            fail: function (jqXHR, textStatus) {
            }
        });
    }


    function getHighChartStatistics(array, index) {

        Highcharts.chart('section_chart' + index, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: null
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                },
                series: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return Math.round(this.percentage * 100) / 100 + ' %';
                        },
                        distance: -30,
                        color: '#fff',
                        style: {
                            color: "contrast",
                            fontSize: "11px",
                            fontWeight: "normal",
                            textOutline: "0"
                        }
                    }
                }
            },
            subtitle: {
                text: null
            },
            credits: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: true,
                labelFormat: "{name} ({percentage:.1f}%)"
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} из {point.total} руб.</b>'
            },
            series: [{
                name: 'Сумма',
                colorByPoint: true,
                data: array
            }]
        });
    }

    getOrdersStatistics();
    $('#container').on("pjax:end", '#search', function () {
        getOrdersStatistics();
    });

});