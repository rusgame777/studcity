/**
 * Created by Evgenii on 22.12.2017.
 */

$(function () {


    function getOrdersStatistics() {
        var data = {
            dateStart: $.getUrlParam('dateStart'),
            dateEnd: $.getUrlParam('dateEnd'),
            id_university: $.getUrlParam('id_university')
        };

        $.ajax({
            type: "POST",
            url: "/ajax/catalog-statistics/",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var data = JSON.parse(jqXHR.responseText);
                $('#charts').html(data.html);
                if (data.data) {
                    for (var index in data.data) {
                        if (data.data.hasOwnProperty(index)) {
                            var array = data.data[index];
                            array = $.map(array, function (value, key) {
                                return value;
                            });
                            if (array.length > 0) {
                                getHighChartStatistics(array, index);
                            } else {
                                $('#section_chart' + index).html('нет данных');
                            }
                        }
                    }
                }
            },
            fail: function (jqXHR, textStatus) {
            }
        });
    }


    function getHighChartStatistics(array, index) {

        Highcharts.chart('section_chart' + index, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: null
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                },
                series: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return Math.round(this.percentage * 100) / 100 + ' %';
                        },
                        distance: -30,
                        color: '#fff',
                        style: {
                            color: "contrast",
                            fontSize: "11px",
                            fontWeight: "normal",
                            textOutline: "0"
                        }
                    }
                }
            },
            subtitle: {
                text: null
            },
            credits: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: true,
                labelFormat: "{name} ({percentage:.1f}%)"
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} из {point.total} руб.</b>'
            },
            series: [{
                name: 'Сумма',
                colorByPoint: true,
                data: array
            }]
        });
    }


    function getDataChartViews() {
        var data = {};
        var array = {};
        var categories = [];
        var series = [];
        $.ajax({
            type: "POST",
            url: "/ajax/chart-views",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var json_text = jqXHR.responseText;
                var obj = JSON.parse(json_text);
                var status = parseInt(obj.status);
                switch (status) {
                    case 0:
                        break;

                    case 1:
                        array['categories'] = obj.categories;
                        array['series'] = obj.series;
                        if (array['series'].length > 0) {
                            getHighChartViews(array);
                        } else {
                            $('#section_char11').html('нет данных');
                        }
                        break;

                    default:
                }
            },
            fail: function (jqXHR, textStatus) {
            }
        });
    }

    function getHighChartViews(array) {
        Highcharts.chart('section_chart1', {
            chart: {
                type: 'spline'
            },
            title: null,
            subtitle: null,
            credits: {
                enabled: false
            },
            xAxis: {
                categories: array['categories']
            },
            yAxis: {
                title: {
                    text: null
                },
                plotLines: null
            },
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 1000
                    },
                    chartOptions: {
                        legend: {
                            enabled: false
                        }
                    }
                }]
            },
            plotOptions: {
                spline: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            tooltip: {
                valueSuffix: ' просмотров',
                backgroundColor: 'none',
                borderWidth: 0,
                shadow: false,
                useHTML: true,
                padding: 0
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'Количество',
                data: array['series'][0]['data']
            }]
        });
    }

    function getDataChartHs() {
        var data = {};
        var array = {};
        var series = [];
        var categories = [];
        $.ajax({
            type: "POST",
            url: "/ajax/chart-hs",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var json_text = jqXHR.responseText;
                var obj = JSON.parse(json_text);
                var status = parseInt(obj.status);
                switch (status) {
                    case 0:
                        break;

                    case 1:
                        array['categories'] = obj.categories;
                        array['series'] = obj.series;
                        if (array['series'].length > 0) {
                            getHighChartHs(array);
                        } else {
                            $('#section_char12').html('нет данных');
                        }
                        break;

                    default:
                }
            },
            fail: function (jqXHR, textStatus) {
            }
        });
    }

    function getHighChartHs(array) {

        Highcharts.chart('section_chart2', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'bar'
            },
            title: {
                text: null
            },

            subtitle: {
                text: null
            },

            credits: {
                enabled: false
            },
            xAxis: {
                categories: array['categories'],
                title: {
                    text: '<b>Количество знаков</b>'
                }
            },
            yAxis: {
                title: {
                    text: '<b>Просмотры</b>'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                headerFormat: null,
                pointFormat: null
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            /*plotOptions: {
             pie: {
             allowPointSelect: true,
             cursor: 'pointer',
             dataLabels: {
             enabled: true,
             format: '<b>{point.name}</b>: {point.percentage:.1f} %',
             style: {
             color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
             }
             }
             }
             },*/
            series: [{
                name: 'Количество',
                data: array['series'][0]['data']
            }]
        });
    }

    function getDataChartUnitsMoney() {
        var data = {};
        var array = {};
        var series = [];
        $.ajax({
            type: "POST",
            url: "/ajax/chart-units-money",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            async: true,
            success: function (response, textStatus, jqXHR) {
                var json_text = jqXHR.responseText;
                var obj = JSON.parse(json_text);
                var status = parseInt(obj.status);
                switch (status) {
                    case 0:
                        break;

                    case 1:
                        series = obj.series;
                        array['series'] = series;
                        if (array['series'].length > 0) {
                            getHighChartUnitsMoney(array);
                        } else {
                            $('#section_char3').html('нет данных');
                        }
                        break;

                    default:
                }
            },
            fail: function (jqXHR, textStatus) {
            }
        });
    }

    function getHighChartUnitsMoney(array) {
        Highcharts.chart('section_chart3', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: null
            },

            subtitle: {
                text: null
            },

            credits: {
                enabled: false
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} из {point.total}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Просмотров',
                data: array['series'][0]['data']
            }]
        });
    }


    getOrdersStatistics();
    $('#container').on("pjax:end", '#search', function () {
        getOrdersStatistics();
    });

    //
    // getDataChartViews();
    // getDataChartHs();
    // getDataChartUnitsMoney();
});