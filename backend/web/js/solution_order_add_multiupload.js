var $ = jQuery.noConflict();
var file_api = window.File && window.FileReader && window.FileList && window.Blob;
window.fileReload = false;
window.filesMax = 0;
window.files = 0;
var id = parseInt($('input[name=id]').val()) || 0;

$(document).ready(function () {
    jQuery.event.props.push('dataTransfer');
    var maxFiles = 100,
        errMessage = 0,
        defaultUploadBtn = $('#uploadbtn'),
        dataArray = [],
        dropFiles = $('#drop-files'),
        dropFilesBox = dropFiles.find('p'),
        dynamic_form = $('#dynamic-form');
    dropFiles.on('drop', function (e) {
        var files = e.dataTransfer.files;
        if (files.length <= maxFiles) {
            loadInView(files);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
            files.length = 0;
            return false;
        }
    });

    defaultUploadBtn.on('change', function () {
        var files = $(this)[0].files;
        if (files.length <= maxFiles) {
            loadInView(files);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
            files.length = 0;
        }

    });

    function loadInView(files) {
        id = parseInt($('input[name=id]').val()) || 0;
        var allowUpload = true;
        window.filesMax = files.length;
        window.files = 0;

        $.each(files, function (index, file) {
            if (!files[index].type.match('application/msword|application/vnd.openxmlformats-officedocument.wordprocessingml.document|image/jpeg|image/pjpeg|application/x-rar-compressed|application/x-zip-compressed|application/zip|application/pdf')) {
                if (errMessage == 0) {
                    dropFilesBox.html('Эй! только doc/docx, pdf, zip, rar, jpg!');
                    ++errMessage
                } else if (errMessage == 1) {
                    dropFilesBox.html('Стоп! Загружаются только doc/docx, pdf, zip, rar, jpg!');
                    ++errMessage
                } else if (errMessage == 2) {
                    dropFilesBox.html("Не умеешь читать? Только doc/docx, pdf, zip, rar, jpg!");
                    ++errMessage
                } else if (errMessage == 3) {
                    dropFilesBox.html("Хорошо! Продолжай в том же духе");
                    errMessage = 0;
                }
                allowUpload = false;
                return false;
            }

            dynamic_form.yiiActiveForm('validate');
            if ((dynamic_form.find('.has-error').length > 0) && (id == 0)) {
                alert('Перед загрузкой файлов, заполните другие данные корректно!');
                allowUpload = false;
                return false;
            }

            if ((dataArray.length + files.length) > maxFiles) {
                alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
                allowUpload = false;
                return false;
            }
        });

        if (!allowUpload) {
            return false;
        }

        if (id == 0) {
            var data = dynamic_form.serialize();

            $.ajax({
                url: "/solution-orders/add-position/",
                type: "POST",
                data: data,
                cache: false,
                success: function (response) {
                    id = parseInt(response);
                    window.fileReload = true;
                    $("input[name=id]").val(id);

                    $.each(files, function (index, file) {
                        var fileReader = new FileReader();
                        fileReader.onload = (function (file) {
                            return function () {
                                dataArray.push({title: file.name, mime_type: file.type, file: this.result, id: id});
                            };
                        })(files[index]);
                        fileReader.readAsDataURL(file);
                    });
                },
                fail: function () {
                    console.log(arguments);
                    alert('Перед загрузкой файлов, заполните другие данные корректно!');
                },
                error: function () {
                    console.log(arguments);
                    alert('Перед загрузкой файлов, заполните другие данные корректно!');
                }
            });
        } else {
            $.each(files, function (index, file) {
                var fileReader = new FileReader();
                fileReader.onload = (function (file) {
                    return function () {
                        dataArray.push({title: file.name, mime_type: file.type, file: this.result, id: id});
                    };
                })(files[index]);
                fileReader.readAsDataURL(file);

            });
        }

        setTimeout(function () {
            uploadFiles();
        }, 1000);
    }

    function uploadFiles() {
        $.each(dataArray, function (index, file) {
            $.ajax({
                url: "/solution-orders/add-upload-file/",
                type: "POST",
                data: dataArray[index],
                cache: false,
                success: function (data) {
                    $("#section_task").append(data);
                    $('input[name=number]').val($('.section_task').length);
                    window.files++;
                    defaultUploadBtn.wrap('<form>').closest('form').get(0).reset();
                    defaultUploadBtn.unwrap();

                    if (window.fileReload && (window.files == window.filesMax)) {
                        document.location = '/solution-orders/form/?id=' + id + '&edit=ok';
                    }

                },
                fail: function () {
                    console.log(arguments);
                },
                error: function () {
                    console.log(arguments);
                }
            });

        });
        dataArray.length = 0;
    }

    dropFiles.on('dragenter', function () {
        $(this).css({'background': '#ffbaba'});
        return false;
    });

    dropFiles.on('dragleave', function () {
        $(this).css({'background': '#d9e9ff'});
        return false;
    });

    dropFiles.on('drop', function () {
        $(this).css({'background': '#ffbaba'});
        return false;
    });

    $(".section_file2 button, .section_file2 div span").on('click', function () {
        $(this).parents('.section_file2').find('input').trigger('click');
    });

    $('input[name^=file]').on('change', function () {
        var file_name,
            inp = $(this),
            fileBox = inp.parents('.section_file2'),
            lbl = fileBox.find('div span'),
            btn = fileBox.find('button');

        if (file_api && inp[0].files[0]) {
            file_name = inp[0].files[0].name;
        } else {
            file_name = inp.val().replace("C:\\fakepath\\", '');
        }

        if (!file_name.length) {
            return;
        }
        if (lbl.is(":visible")) {
            lbl.text(file_name);
            btn.text("Выберите файл");
        } else {
            btn.text(file_name);
        }
    });

    $(window).resize(function () {
        $(".section_file2 input").triggerHandler("change");
    });

    $('#section_task').on("click", '.section_task_delete', function () {
        var task = $(this).parents('.section_task'),
            id = task.attr('data-id');
        if (!confirm('Вы действительно хотите удалить этот файл?')) {
            return false;
        }
        task.remove();
        $.ajax({
            url: "/solution-orders/add-remove-file/",
            type: "GET",
            data: {"id": id},
            cache: false,
            success: function (response) {
                $("body").append(response);
                $('input[name=number]').val($('.section_task').length);
            },
            fail: function () {
                console.log(arguments);
            },
            error: function () {
                console.log(arguments);
            }
        });
    });

});