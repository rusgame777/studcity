var $ = jQuery.noConflict(),
    file_api = window.File && window.FileReader && window.FileList && window.Blob;
window.fileReload = {
    "foto": false,
    "file": false
};
window.filesMax = [];
window.files = [];
var id = parseInt($('input[name=id]').val()) || 0;

$(document).ready(function () {
    jQuery.event.props.push('dataTransfer');
    var maxFiles = 100,
        errMessage = [],
        defaultUploadBtn = $('.uploadbtn'),
        dataArray = {
            "foto": [],
            "file": []
        },
        dropFiles = $('.drop-files'),
        dropFilesBox = dropFiles.find('p'),
        dynamic_form = $('#dynamic-form');

    dropFiles.on('drop', function (e) {
        var files = e.dataTransfer.files;
        var attribute = $(this).data('id');
        if (files.length <= maxFiles) {
            loadInView(files, attribute);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
            files.length = 0;
            return false;
        }
    });

    defaultUploadBtn.on('change', function () {
        var files = $(this)[0].files;
        var attribute = $(this).data('id');
        if (files.length <= maxFiles) {
            loadInView(files, attribute);
        } else {
            alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
            files.length = 0;
        }

    });

    function loadInView(files, attribute) {
        id = parseInt($('input[name=id]').val()) || 0;
        var allowUpload = [];
        allowUpload[attribute] = true;
        window.filesMax[attribute] = files.length;
        window.files[attribute] = 0;

        var dropFilesBoxSingle = dropFilesBox.filter('[data-id="' + attribute + '"]');
        if (isNaN(errMessage[[attribute]])) {
            errMessage[attribute] = 0;
        }

        $.each(files, function (index, file) {
            if (attribute == 'file') {

                if (!files[index].type.match('application/msword|application/vnd.openxmlformats-officedocument.wordprocessingml.document|image/jpeg|image/pjpeg|application/x-rar-compressed|application/x-zip-compressed|application/zip|application/pdf')) {
                    if (errMessage[attribute] == 0) {
                        dropFilesBoxSingle.html('Эй! только doc/docx, pdf, zip, rar, jpg!');
                        ++errMessage[attribute];
                    } else if (errMessage[attribute] == 1) {
                        dropFilesBoxSingle.html('Стоп! Загружаются только doc/docx, pdf, zip, rar, jpg!');
                        ++errMessage[attribute]
                    } else if (errMessage[attribute] == 2) {
                        dropFilesBoxSingle.html("Не умеешь читать? Только doc/docx, pdf, zip, rar, jpg!");
                        ++errMessage[attribute]
                    } else if (errMessage[attribute] == 3) {
                        dropFilesBoxSingle.html("Хорошо! Продолжай в том же духе");
                        errMessage[attribute] = 0;
                    }
                    allowUpload[attribute] = false;
                    return false;
                }
            } else if (!files[index].type.match('image/png|image/jpeg|image/pjpeg')) {
                if (errMessage[attribute] == 0) {
                    dropFilesBoxSingle.html('Эй! только png и jpg!');
                    ++errMessage[attribute];
                } else if (errMessage[attribute] == 1) {
                    dropFilesBoxSingle.html('Стоп! Загружаются только png и jpg!');
                    ++errMessage[attribute]
                } else if (errMessage[attribute] == 2) {
                    dropFilesBoxSingle.html("Не умеешь читать? Только png и jpg!");
                    ++errMessage[attribute]
                } else if (errMessage[attribute] == 3) {
                    dropFilesBoxSingle.html("Хорошо! Продолжай в том же духе");
                    errMessage[attribute] = 0;
                }
                allowUpload[attribute] = false;
                return false;
            }

            dynamic_form.yiiActiveForm('validate');
            if ((dynamic_form.find('.has-error').length > 0) && (id == 0)) {
                alert('Перед загрузкой файлов, заполните другие данные корректно!');
                allowUpload[attribute] = false;
                return false;
            }

            console.log(attribute, dataArray[attribute]);
            if ((dataArray[attribute].length + files.length) > maxFiles) {
                alert('Вы не можете загружать больше ' + maxFiles + ' файлов!');
                allowUpload[attribute] = false;
                return false;
            }
        });

        if (!allowUpload[attribute]) {
            return false;
        }

        if (id == 0) {
            var type_option = parseInt($('input[name=type_option]:checked').val()),
                price_name = 'price' + type_option,
                price = parseInt($('input[name=' + price_name + ']').val()) || 0;
            if (isNaN(price) == true) {
                price = 0;
            }
            var data = dynamic_form.serialize();

            $.ajax({
                url: "/catalog-position/position/",
                type: "POST",
                data: data,
                cache: false,
                success: function (response) {
                    id = parseInt(response);
                    window.fileReload[attribute] = true;
                    $("input[name=id]").val(id);
                    if (id == 0) {
                        allowUpload[attribute] = false;
                        alert('Перед загрузкой файлов, заполните другие данные корректно!');
                        return false;
                    }

                    $.each(files, function (index, file) {
                        var fileReader = new FileReader();
                        fileReader.onload = (function (file) {
                            return function () {
                                dataArray[attribute].push({
                                    title: file.name,
                                    mime_type: file.type,
                                    file: this.result,
                                    id: id
                                });
                            };
                        })(files[index]);
                        fileReader.readAsDataURL(file);
                    });
                },
                fail: function () {
                    console.log(arguments);
                    alert('Перед загрузкой файлов, заполните другие данные корректно!');
                },
                error: function () {
                    console.log(arguments);
                    alert('Перед загрузкой файлов, заполните другие данные корректно!');
                }
            });
        } else {
            $.each(files, function (index, file) {
                var fileReader = new FileReader();
                fileReader.onload = (function (file) {
                    return function () {
                        dataArray[attribute].push({title: file.name, mime_type: file.type, file: this.result, id: id});
                    };
                })(files[index]);
                fileReader.readAsDataURL(file);
            });
        }

        if (!allowUpload[attribute]) {
            return false;
        }

        setTimeout(function () {
            uploadFiles(attribute);
        }, 1000);
    }

    function uploadFiles(attribute) {
        $.each(dataArray[attribute], function (index, file) {
            $.ajax({
                url: "/catalog-position/upload-" + attribute + "/",
                type: "POST",
                data: dataArray[attribute][index],
                cache: false,
                success: function (data) {
                    $(".section_task_block").filter('[data-id="' + attribute + '"]').append(data);
                    $('input[name=number]').val($('.section_task').length);
                    window.files[attribute]++;
                    var defaultUploadBtnSingle = defaultUploadBtn.filter('[data-id="' + attribute + '"]');
                    defaultUploadBtnSingle.wrap('<form>').closest('form').get(0).reset();
                    defaultUploadBtnSingle.unwrap();

                    if (window.fileReload[attribute] && (window.files[attribute] == window.filesMax[attribute])) {
                        document.location = '/catalog-position/form/?id=' + id + '&edit=ok';
                    }

                },
                fail: function () {
                    console.log(arguments);
                },
                error: function () {
                    console.log(arguments);
                }
            });

        });
        dataArray[attribute].length = 0;
    }

    dropFiles.on('dragenter', function () {
        $(this).css({'background': '#ffbaba'});
        return false;
    });

    dropFiles.on('dragleave', function () {
        $(this).css({'background': '#d9e9ff'});
        return false;
    });

    dropFiles.on('drop', function () {
        $(this).css({'background': '#ffbaba'});
        return false;
    });

    $(".section_file2 button, .section_file2 div span").on('click', function () {
        $(this).parents('.section_file2').find('input').trigger('click');
    });

    $('input[name^=file]').on('change', function () {
        var file_name,
            inp = $(this),
            fileBox = inp.parents('.section_file2'),
            lbl = fileBox.find('div span'),
            btn = fileBox.find('button');

        if (file_api && inp[0].files[0]) {
            file_name = inp[0].files[0].name;
        } else {
            file_name = inp.val().replace("C:\\fakepath\\", '');
        }

        if (!file_name.length) {
            return;
        }
        if (lbl.is(":visible")) {
            lbl.text(file_name);
            btn.text("Выберите файл");
        } else {
            btn.text(file_name);
        }
    });

    $(window).resize(function () {
        $(".section_file2 input").triggerHandler("change");
    });

    $('.section_task_block').on("click", '.section_task_delete', function () {
        var task = $(this).parents('.section_task'),
            id = task.data('id'),
            attribute = $(this).parents('.section_task_block').data('id');
        if (!confirm('Вы действительно хотите удалить этот файл?')) {
            return false;
        }
        task.remove();
        $.ajax({
            url: "/catalog-position/remove-" + attribute + "/",
            type: "GET",
            data: {"id": id},
            cache: false,
            success: function (response) {
                $("body").append(response);
            },
            fail: function () {
                console.log(arguments);
            },
            error: function () {
                console.log(arguments);
            }
        });
    });

});