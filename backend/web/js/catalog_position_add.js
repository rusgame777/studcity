$(function () {

    var page_section = $('#page_section'),
        dynamic_form = $('#dynamic-form'),
        url_submit = dynamic_form.attr('action');

    $(dynamic_form).on('click', '#submiting', function () {
        dynamic_form.attr('action', url_submit);
    });
    $(dynamic_form).on('submit', function () {
    });

    page_section.on("change", "input[type=file]", function () {
        if (!$(this).attr('multiple')) {
            var name, checkbox;
            if ($(this).parents('.file_list').length > 0) {
                name = $(this).attr('name').replace('name_file', 'delete_name_file');
                checkbox = $('input[name="' + name + '"]');
            } else {
                name = 'delete_' + $(this).attr('name');
                checkbox = $('input[name=' + name + ']');
            }
            if (checkbox.length > 0) {
                checkbox.attr("checked", "checked");
            }
        }
    });

    $("select[name=cat]").on('change', function () {
        var cat = $(this).val();
        var subcat_object = $('select[name=subcat]');
        subcat_object.empty();

        $.ajax({
            url: "/catalog-position/subcat/",
            type: "GET",
            data: {"cat": cat},
            cache: false,
            success: function (response) {
                subcat_object.html(response);
                subcat_object.trigger('change');
            },
            fail: function () {
                console.log(arguments);
            },
            error: function () {
                console.log(arguments);
            }
        });
    });

    $('input[name=type_option]').on('change', function () {
        var type_option = parseInt($(this).val());
        if (type_option == 1) {
            $('#section_block1').show();
            $('#section_block2').hide();
        } else {
            $('#section_block2').show();
            $('#section_block1').hide();
        }
    });

    setTimeout(function () {
        if (dynamic_form.data('yiiActiveForm') == undefined) {
            dynamic_form.data('yiiActiveForm').submitting = true;
            dynamic_form.yiiActiveForm('validate');
        }
    }, 200);

    dynamic_form.on('submit', function () {
        if (dynamic_form.data('yiiActiveForm') == undefined) {
            dynamic_form.data('yiiActiveForm').submitting = true;
            dynamic_form.yiiActiveForm('validate');
        }
        console.log(dynamic_form.find('.has-error').length);
    });


});