$(function () {
    var post_search = $('.post-search');
    var section = $('#section');

    $.pjax.defaults.timeout = 30000;

    $('#container').on("pjax:end", '#search', function () {
        $.pjax.reload({container: "#notes"});
    });

    var filter2_arr = ['', 'Любое из слов', 'Все слова', 'Как в запросе'];
    $('.dropdown-toggle').dropdown();

    post_search.on('input', '#search_word input', function () {
        $('input[name="search_word"]').val($(this).val());
    });

    post_search.on('click', '#search_word a', function (e) {
        var id = $(this).parent().data('id');
        var value = $(this).text();
        $(this).parents('.input-group').find('.value').text(filter2_arr[id]);
        $('input[name="id_type"]').val(id);
        e.preventDefault();
    });

    section.on('submit', '#search_form', function () {
        var sort = ($.getUrlVar('sort') != undefined && typeof $.getUrlVar('sort') != "function") ? $.getUrlVar('sort') : '';
        $('input[name=sort]').val(sort);
    });

    moment().format();
    function update_date() {
        $('#page_section').find('.date_update').each(function () {
            var $this = $(this);
            var date = $this.find('.date').data('value');
            if (date != '') {
                date = Math.floor((new Date(date) - new Date()) / 1000);
                var minute = Math.abs(Math.floor(date / 60));
                var time = moment.duration(date, 'second').humanize(true);
                var time_object = $this.find('.time');
                time_object.css({'color': 'rgb(' + getGreenToRedGradientRGBColorValue(minute) + ')'});
                time_object.text(time);
            }
        });

        $('#page_section').find('.date_create').each(function () {
            var $this = $(this);
            var date = $this.find('.date').data('value');
            if (date != '') {
                date = Math.floor((new Date(date) - new Date()) / 1000);
                var minute = Math.abs(Math.floor(date / 60));
                var time = moment.duration(date, 'second').humanize(true);
                var time_object = $this.find('.time');
                time_object.css({'color': 'rgb(' + getGreenToRedGradientRGBColorValue(minute) + ')'});
                time_object.text(time);
            }
        });
    }

    update_date();
    setInterval(function () {
        update_date();
    }, 20000);

    $('#container').on("pjax:end", '#notes', function () {
        update_date();
    });

    var article = section.find('article');
    article.on('click', '.excel', function (e) {
        $(this).attr('href', '/solution-orders/excel/' + document.location.search);
        document.location = $(this).attr('href');
        e.preventDefault();
    });

});