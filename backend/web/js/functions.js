var pattern_float = new RegExp(/^[\d]+(\.[\d]+)?$/);
var pattern_email = new RegExp(/^((\"[\w-\s]+\")|([\w-]+(?:\.[\w-]+)*)|(\"[\w-\s]+\")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
var pattern_phone = new RegExp(/^8\d{10}$/);

function getType(input) {
    var m = pattern_float.exec(input);
    if (m) {
        if (m[1]) {
            return 'float';
        }
        else {
            return 'int';
        }
    }
    return 'string';
}

function parse_url(url) {
    var parser = document.createElement('a');
    parser.href = url;
    return parser;
}

function ltrim(str, charlist) {
    charlist = !charlist ? ' \\s\u00A0' : (charlist + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1')
    var re = new RegExp('^[' + charlist + ']+', 'g')
    return (str + '').replace(re, '')
}

function strpos(data) {
    var haystack = data.str, needle = data.find, offset = 0;
    for (var i = 0; i < haystack.split(needle).length; i++) {
        var index = haystack.indexOf(needle, offset + (data.index != 1 ? 1 : 0));
        if (i == data.index - 1) {
            return (index != -1 ? index : null);
        } else {
            offset = index;
        }
    }
}

function in_array(what, where) {
    for (var i = 0; i < where.length; i++)
        if (what == where[i]) {
            return true;
        }
    return false;
}

function array_map(callback) {
    var argc = arguments.length, argv = arguments;
    var j = argv[1].length, i = 0, k = 1, m = 0;
    var tmp = [], tmp_ar = [];

    while (i < j) {
        while (k < argc) {
            tmp[m++] = argv[k++][i];
        }
        m = 0;
        k = 1;
        if (callback) {
            tmp_ar[i++] = callback.apply(null, tmp);
        } else {
            tmp_ar[i++] = tmp;
        }
        tmp = [];
    }
    return tmp_ar;
}


function getGreenToRedGradientRGBColorValue(value, as_string, min, max) {
    if (as_string == undefined) {
        as_string = true;
    }
    if (min == undefined) {
        min = 0;
    }
    if (max == undefined) {
        max = 60 * 24 * 21;
    }
    var array = array_map(function (a) {
        return parseInt(a);
    }, [
        (R = (255.00 * Math.max(min, Math.min(max, value)) / (max - min))),
        (G = (200.00 * ((max - min) - Math.max(min, Math.min(max, value)))) / (max - min)),
        (B = 0.00)
    ]);
    return (as_string ? array.join(",") : array);
}


$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.search.slice(1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});

$.extend({

    getUrlParams: function (url) {

        // get query string from url (optional) or window
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

        queryString = decodeURIComponent(queryString);
        // we'll store the parameters here
        var obj = {};

        // if query string exists
        if (queryString) {

            // stuff after # is not part of query string, so get rid of it
            queryString = queryString.split('#')[0];

            // split our query string into its component parts
            var arr = queryString.split('&');

            for (var i = 0; i < arr.length; i++) {
                // separate the keys and the values
                var a = arr[i].split('=');

                // in case params look like: list[]=thing1&list[]=thing2
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function (v) {
                    paramNum = v.slice(1, -1);
                    return '';
                });

                // set parameter value (use 'true' if empty)
                var paramValue = typeof(a[1]) === 'undefined' ? true : a[1];

                // (optional) keep case consistent
                // paramName = paramName.toLowerCase();
                // paramValue = paramValue.toLowerCase();

                // if parameter name already exists
                if (obj[paramName]) {
                    // convert value to array (if still string)
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    // if no array index number specified...
                    if (typeof paramNum === 'undefined') {
                        // put the value on the end of the array
                        obj[paramName].push(paramValue);
                    }
                    // if array index number specified...
                    else {
                        // put the value at that index number
                        obj[paramName][obj[paramName].length] = paramValue;
                    }
                }
                // if param name doesn't exist yet, set it
                else {
                    obj[paramName] = paramValue;
                }
            }
        }
        return obj;
    },

    getUrlParam: function (param) {
        return $.getUrlParams()[param];
    }
});

$(function () {

    $.fn.extend({

        editableSave: function (object) {
            $(this).on('change', '.edit_div input', function (e) {
                var $this = $(this),
                    data = {};

                data['id'] = $this.parents('tr').attr('data-key');
                data[$this.attr('name')] = $this.val();

                $.ajax({
                    type: "POST",
                    url: object.actions.fastupdate,
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    cache: false,
                    success: function (response, textStatus, jqXHR) {
                        var json_text = jqXHR.responseText;
                        var obj = JSON.parse(json_text);
                        var status = parseInt(obj.status);
                        switch (status) {
                            case 0:
                                break;
                            case 1:
                                break;
                            default:
                        }
                        $.pjax.reload({container: "#notes"});
                    }
                });
            });
        },

        editableDelete: function (object) {

            var body = $('body'),
                $selector = this;
            $(body).on('change', object.table + ' .delete-all input[type=checkbox], .delete input[type=checkbox]', function () {
                var keys = $(this).parents(object.table).yiiGridView('getSelectedRows');
                if (keys.length > 0) {
                    $(object.table, $selector).siblings().filter('#section_actions').show();
                } else {
                    $(object.table, $selector).siblings().filter('#section_actions').hide();
                }
            });

            $(body).on('click', '#section_delete_all', function (e) {
                console.log(e);
                var keys = $(object.table).yiiGridView('getSelectedRows');
                var data = {
                    id: keys,
                    url: document.location.pathname + document.location.search
                };

                $.ajax({
                    type: "POST",
                    url: object.actions.delete,
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    cache: false,
                    success: function (response, textStatus, jqXHR) {
                        var json_text = jqXHR.responseText;
                        var obj = JSON.parse(json_text);
                        var status = parseInt(obj.status);
                        switch (status) {
                            case 0:
                                break;
                            case 1:
                                break;
                            default:
                        }
                        document.location = document.location.pathname + document.location.search;
                    }
                });
            });

            $(body).on('click', '#section_set_value', function (e) {
                $(this).toggleClass('active');
            });

            $(body).on('click', '#section_set_input button', function (e) {
                var $this = $(this),
                    keys = $(object.table).yiiGridView('getSelectedRows'),
                    data = {
                    id: keys,
                    price: $this.parents('#section_set_input').find('input[name=set_value_price]').val(),
                    url: document.location.pathname + document.location.search
                };
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: object.actions.changePrice,
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    traditional: true,
                    cache: false,
                    success: function (response, textStatus, jqXHR) {
                        var json_text = jqXHR.responseText;
                        var obj = JSON.parse(json_text);
                        var status = parseInt(obj.status);
                        switch (status) {
                            case 0:
                                break;
                            case 1:
                                break;
                            default:
                        }
                        document.location = document.location.pathname + document.location.search;
                    }
                });
            });
        }

    });

});

function formatState(state) {
    if (!state.id) {
        return state.text;
    }
    return state.text.replace("&nbsp;", '');
}