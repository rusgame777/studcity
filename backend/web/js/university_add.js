$(function () {

    var page_section = $('#page_section'),
        dynamic_form = $('#dynamic-form'),
        url_submit = dynamic_form.attr('action'),
        url_duplicate = url_submit.replace('update', 'duplicate');

    $(dynamic_form).on('click', '#submiting', function () {
        dynamic_form.attr('action', url_submit);
    });
    $(dynamic_form).on('click', '#duplicate', function () {
        dynamic_form.attr('action', url_duplicate);
    });
    $(dynamic_form).on('submit', function () {
    });

});