$(function () {

    var page_section = $('#page_section'),
        dynamic_form = $('#dynamic-form'),
        url_submit = dynamic_form.attr('action'),
        url_duplicate = url_submit.replace('update', 'duplicate');

    $(dynamic_form).on('click', '#submiting', function () {
        dynamic_form.attr('action', url_submit);
    });
    $(dynamic_form).on('click', '#duplicate', function () {
        dynamic_form.attr('action', url_duplicate);
    });
    $(dynamic_form).on('submit', function () {
    });

    page_section.on("change", "input[type=file]", function () {
        if (!$(this).attr('multiple')) {
            var name, checkbox;
            if ($(this).parents('.file_list').length > 0) {
                name = $(this).attr('name').replace('name_file', 'delete_name_file');
                checkbox = $('input[name="' + name + '"]');
            } else {
                name = 'delete_' + $(this).attr('name');
                checkbox = $('input[name=' + name + ']');
            }
            if (checkbox.length > 0) {
                checkbox.attr("checked", "checked");
            }
        }
    });

    $("select[name=cat]").on('change', function () {
        var cat = $(this).val(),
            position_object = $('select[name=id_position]');

        $.ajax({
            url: "/solution-orders/position/",
            type: "GET",
            data: {"cat": cat},
            cache: false,
            success: function (response) {
                position_object.html(response);
                position_object.trigger('change');
            },
            fail: function () {
                console.log(arguments);
            },
            error: function () {
                console.log(arguments);
            }
        });
    });

    $("select[name=id_position]").on('change', function () {
        var id_position = $(this).val(),
            data_object = $('#goods_value_list');

        $.ajax({
            url: "/solution-orders/data/",
            type: "GET",
            data: {"id_position": id_position},
            cache: false,
            success: function (response) {
                data_object.html(response);
                data_object.trigger('change');
            },
            fail: function () {
                console.log(arguments);
            },
            error: function () {
                console.log(arguments);
            }
        });
    });

});