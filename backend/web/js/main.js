$(function () {

    /*$(".datepicker").datepicker({
        dateFormat: "dd.mm.yy",
        minDate: new Date(2007, 1, 1),
        changeMonth: true,
        changeYear: true
    }, $.datepicker.regional["ru"]);

    var timepicker = $('.timepicker').clockpicker({
        placement: 'bottom',
        align: 'right',
        autoclose: true
    });

    $('.section_time').on('click', '.section_time_icon', function (e) {
        e.stopPropagation();
        timepicker.clockpicker('show').clockpicker('toggleView', 'minutes');
    });*/

    var pathname = document.location.pathname;

    function update_bottom() {
        var height = $(window).height() - 83;
        $('#bottom').height(height);
    }

    update_bottom();
    $(window).on('resize change', function () {
        update_bottom();
    });

    var url = ltrim(pathname, '/').split('/')[0];
    $('.submenu_title a').each(function () {
        if (url == ltrim($(this).attr('href'), '/').split('/')[0]) {
            if ($(this).attr('href') == pathname) {
                $(this).addClass('hover');
            }
            var submenu = $(this).parents('.submenu');
            var menu = submenu.prev();
            menu.find('.menu_content').addClass('hover');
            menu.find('.menu_title').removeClass('menu_close').addClass('menu_active');
            submenu.show();
        }
    });

    $('.menu_title a').each(function () {
        if (pathname == $(this).attr('href')) {
            $(this).closest('.menu_title').addClass('menu_active');
        }
    });

    $('.menu').on('click', function () {
        var submenu = $(this).next();
        if (submenu.hasClass('submenu')) {
            var menu_content = $(this).children('.menu_content');
            var menu_title = menu_content.children('.menu_title');
            var display = submenu.css('display');
            if (display == 'none') {
                submenu.toggle('normal');
                menu_content.addClass('hover');
                menu_title.removeClass('menu_close').addClass('menu_open');
            } else {
                submenu.toggle('fast');
                menu_content.removeClass('hover');
                menu_title.removeClass('menu_open').addClass('menu_close');
            }
        }
    });


    function activeteClosingNotices() {
        var continueClose = setInterval(function () {
            var length = $(page_section).find('.alert').length;
            if (length > 0) {
                $(page_section).find('.alert').eq(length - 1).find('.close').trigger('click');
            } else {
                clearInterval(continueClose);
            }
        }, 1000);
    }

    // автоматическое закрытие Alert
    var page_section = $('#page_section');
    setTimeout(function () {
        activeteClosingNotices();
    }, 5000);


});