$(function () {

    var page_section = $('#page_section'),
        dynamic_form = $('#dynamic-form'),
        url_submit = dynamic_form.attr('action');

    $(dynamic_form).on('click', '#submiting', function () {
        dynamic_form.attr('action', url_submit);
    });

});