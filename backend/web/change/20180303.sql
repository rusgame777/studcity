CREATE TABLE IF NOT EXISTS `orders_docs` (
  `id` int(10) unsigned NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `id_order` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `orders_docs`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `orders_docs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;


INSERT INTO orders_docs(title, file, id_order) SELECT REPLACE(doc, CONCAT('.', SUBSTRING_INDEX(doc, '.', -1)), '') AS title, doc AS file, id_order FROM orderdesc_solution WHERE doc <> '';

ALTER TABLE `orderdesc_solution` DROP `doc`;
