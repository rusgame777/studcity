RENAME TABLE
    content TO page_content;
RENAME TABLE
    categories TO page_categories;

ALTER TABLE `page_content`
  DROP `date`;


/* page_content */
ALTER TABLE `page_content`
  CHANGE `show_content` `show_content` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `page_content`
  ADD `date_create` DATETIME NOT NULL
  AFTER `id`,
  ADD `date_update` DATETIME NOT NULL
  AFTER `date_create`,
  ADD `who_create` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `date_update`,
  ADD `who_update` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `who_create`;

ALTER TABLE `page_content`
  CHANGE `content` `content` TEXT CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL;
ALTER TABLE `page_content`
  CHANGE `metatitle` `metatitle` TEXT CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL;
ALTER TABLE `page_content`
  CHANGE `description` `description` TEXT CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL;
ALTER TABLE `page_content`
  CHANGE `keywords` `keywords` TEXT CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL;

UPDATE page_content
SET date_create = NOW(), date_update = NOW();

ALTER TABLE `page_content`
  CHANGE `category` `id_category` INT(10) UNSIGNED NOT NULL DEFAULT '0';


/* catalog_cat */
ALTER TABLE `catalog_cat`
  ADD `date_create` DATETIME NOT NULL
  AFTER `id`,
  ADD `date_update` DATETIME NOT NULL
  AFTER `date_create`,
  ADD `who_create` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `date_update`,
  ADD `who_update` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `who_create`;

UPDATE catalog_cat
SET date_create = NOW(), date_update = NOW();


/* catalog_subcat */
ALTER TABLE `catalog_subcat`
  ADD `date_create` DATETIME NOT NULL
  AFTER `id`,
  ADD `date_update` DATETIME NOT NULL
  AFTER `date_create`,
  ADD `who_create` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `date_update`,
  ADD `who_update` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `who_create`;

ALTER TABLE `catalog_subcat`
  ADD INDEX (`id`);

UPDATE `catalog_subcat`
SET date_create = NOW(), date_update = NOW();


/* catalog_subcat_cat */
ALTER TABLE `catalog_subcat_cat`
  ADD INDEX (`id_subcat`);
ALTER TABLE `catalog_subcat_cat`
  ADD INDEX (`id_university`);
ALTER TABLE `catalog_subcat_cat`
  ADD INDEX (`id_cat`);


/* catalog_position */
ALTER TABLE `catalog_position`
  ADD `date_create` DATETIME NOT NULL
  AFTER `id`,
  ADD `date_update` DATETIME NOT NULL
  AFTER `date_create`,
  ADD `who_create` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `date_update`,
  ADD `who_update` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `who_create`;

UPDATE `catalog_position`
SET date_create = NOW(), date_update = NOW();


/* solution_cat */
ALTER TABLE `solution_cat`
  ADD `date_create` DATETIME NOT NULL
  AFTER `id`,
  ADD `date_update` DATETIME NOT NULL
  AFTER `date_create`,
  ADD `who_create` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `date_update`,
  ADD `who_update` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `who_create`;

UPDATE `solution_cat`
SET date_create = NOW(), date_update = NOW();


/* solution_position */
ALTER TABLE `solution_position`
  ADD `date_create` DATETIME NOT NULL
  AFTER `id`,
  ADD `date_update` DATETIME NOT NULL
  AFTER `date_create`,
  ADD `who_create` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `date_update`,
  ADD `who_update` VARCHAR(255)
CHARACTER SET utf8
COLLATE utf8_unicode_ci NULL
  AFTER `who_create`;

UPDATE `solution_position`
SET date_create = NOW(), date_update = NOW();


/* orderdata_solution */
ALTER TABLE `orderdata_solution`
  CHANGE `nn` `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;

/* downloads */
ALTER TABLE `downloads`
  CHANGE `bit_delete` `bit_delete` INT(10) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `orderdesc`
  CHANGE `id_option` `id_option` INT(10) UNSIGNED NOT NULL DEFAULT '0';


/* orders */
ALTER TABLE `orders`
  ADD INDEX (`id_user`);
ALTER TABLE `orderdesc`
  ADD INDEX (`subcat`);
ALTER TABLE `orders`
  ADD INDEX (`send`);
ALTER TABLE `orders`
  ADD INDEX (`id_type`);
ALTER TABLE `orders`
  ADD INDEX (`status`);
ALTER TABLE `orders`
  ADD INDEX (`date_end`);

ALTER TABLE `orders`
  CHANGE `send` `send` INT(10) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `orders`
  CHANGE `status` `status` INT(10) UNSIGNED NOT NULL DEFAULT '0';
ALTER TABLE `orders`
  CHANGE `sum` `sum` FLOAT NOT NULL DEFAULT '0';


/* admin_author */
CREATE TABLE IF NOT EXISTS `admin_author` (
  `id` int(10) unsigned NOT NULL,
  `login` text COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_entry` datetime NOT NULL DEFAULT '1930-01-01 00:00:00',
  `date_last_entry` datetime NOT NULL DEFAULT '1930-01-01 00:00:00',
  `is_disabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `admin_author`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `admin_author`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;


/* admin */
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) unsigned NOT NULL,
  `login` text COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_entry` datetime NOT NULL DEFAULT '1930-01-01 00:00:00',
  `date_last_entry` datetime NOT NULL DEFAULT '1930-01-01 00:00:00',
  `is_disabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `id_role` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `admin` (`id`, `login`, `password_hash`, `password_reset_token`, `name`, `email`, `auth_key`, `date_entry`, `date_last_entry`, `is_disabled`, `id_role`) VALUES
  (1, 'itc27', '$2y$13$DCpTA8uZikopymWhTgkLy.uKKzfhCMyKZAJ9sj26LvShSLWIFxi76', '', '', '', '8LAYgi_tiSAmFrWa9pZcUueTPCipkSXi', '2017-09-06 01:03:40', '2017-08-29 11:20:24', 0, -1),
  (2, 'admin', '$2y$13$Yjfl2UUoHmwWYgzuS6eE0eTLRTlzZAB6yzmm2MspBId3wgEiXBVuy', '', 'Администратор', 'rus-game@mail.ru', 'Q4lciu4Y6h--TYpv8SSfjjffHnlqcSQE', '2017-09-07 00:47:26', '2017-09-06 01:48:28', 0, -1);

ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `admin`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;