CREATE TABLE IF NOT EXISTS `orders_remember` (
  `id` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `date_next_send` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `orders_remember`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_order` (`id_order`);

ALTER TABLE `orders_remember`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;