<?php
namespace backend\controllers;

use backend\models\CatalogPositionUpload;
use backend\models\CatalogSubcatUpload;
use common\models\CatalogPosition;
use common\models\CatalogSubcat;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use backend\components\IP;
use backend\components\Notice;
use common\models\Admin;
use common\models\CatalogCat;
use backend\models\CatalogCatUpload;
use backend\models\CatalogCatSearch;

/**
 * CatalogCat controller
 */
class CatalogCatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }
    
    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementCatalog')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionShow()
    {
        $searchModel = new CatalogCatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0)
    {
        $model = $id == 0 ? new CatalogCat() : $this->findModel($id);
        return $this->render('form', ['model' => $model]);
    }
    
    public function actionCreate()
    {
        $model = new CatalogCat();
        $model->who_create = IP::whoCreate();
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'foto' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            $model->sort = $id;
            $model->update();
            
            $filePath = CatalogCatUpload::filePath();
            $modelUpload = new CatalogCatUpload();
            $modelUpload->load($post);
            $foto = UploadedFile::getInstance($modelUpload, 'foto');
            $modelUpload->foto = $foto;
            
            if (($foto && $foto->tempName) && ($modelUpload->validate())) {
                $nameFile = implode('', [
                    md5(microtime(true)),
                    '.',
                    $foto->extension,
                ]);
                if ($foto->saveAs($filePath . $nameFile)) {
                    $model->foto = $nameFile;
                    CatalogCatUpload::changeSize($model->foto, 'foto');
                    chmod($filePath . $model->foto, 0644);
                }
            } elseif (!empty($modelUpload->errors['foto'])) {
                $errorFilesArray['foto'] = array_merge($errorFilesArray['foto'], $modelUpload->errors['foto']);
            }
            $model->update();
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Дисциплина успешно создана', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Дисциплина создана с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        } else {
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new CatalogCat();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'foto' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            
            $filePath = CatalogCatUpload::filePath();
            $modelUpload = new CatalogCatUpload();
            $modelUpload->load($post);
            $foto = UploadedFile::getInstance($modelUpload, 'foto');
            $modelUpload->foto = $foto;
            
            if ($modelUpload->delete_foto) {
                if (is_file($filePath . $model->foto)) {
                    unlink($filePath . $model->foto);
                    $model->foto = '';
                    $model->update();
                }
            }
            
            if (($foto && $foto->tempName) && ($modelUpload->validate())) {
                $nameFile = implode('', [
                    md5(microtime(true)),
                    '.',
                    $foto->extension,
                ]);
                if ($foto->saveAs($filePath . $nameFile)) {
                    $model->foto = $nameFile;
                    CatalogCatUpload::changeSize($model->foto, 'foto');
                    chmod($filePath . $model->foto, 0644);
                    $model->update();
                }
            } elseif (!empty($modelUpload->errors['foto'])) {
                $errorFilesArray['foto'] = array_merge($errorFilesArray['foto'], $modelUpload->errors['foto']);
            }
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
        } else {
            $url = ['show'];
        }
        
        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }
    
    public function actionChangeFiles()
    {
        $catalogSubcatArr = CatalogSubcat::find()->where(['or', ['!=', 'foto', ''], ['!=', 'foto_small', '']])->all();
        foreach ($catalogSubcatArr as $catalogSubcatItem) {
            if (is_file(CatalogPositionUpload::filePath().$catalogSubcatItem->foto)) {
                rename(CatalogPositionUpload::filePath().$catalogSubcatItem->foto, CatalogSubcatUpload::filePath().$catalogSubcatItem->foto);
            }
            if (is_file(CatalogPositionUpload::filePath().$catalogSubcatItem->foto_small)) {
                rename(CatalogPositionUpload::filePath().$catalogSubcatItem->foto_small, CatalogSubcatUpload::filePath().$catalogSubcatItem->foto_small);
            }
        }
        $catalogCatArr = CatalogCat::find()->where(['!=', 'foto', ''])->all();
        foreach ($catalogCatArr as $catalogCatItem) {
            if (is_file(CatalogPositionUpload::filePath().$catalogCatItem->foto)) {
                rename(CatalogPositionUpload::filePath().$catalogCatItem->foto, CatalogCatUpload::filePath().$catalogCatItem->foto);
            }
        }
        return true;
    }
    
    public function actionChangeSort()
    {
        $status = 0;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->getRawBody() && $request->validateCsrfToken()) {
            
            $array = [];
            $inputArray = Json::decode($request->getRawBody(), true);
            
            if (empty($inputArray)) {
                $array['status'] = $status;
                return Json::encode($array);
            }
            
            CatalogCatSearch::changeSort($inputArray);
            
            $status = 1;
        }
        $array['status'] = $status;
        return Json::encode($array);
    }
    
    public function actionRemove()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = CatalogCat::findOne($id);
            if (empty($result)) {
                Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                    Url::to($url_redirect));
                return $this->redirect($url_redirect);
            }
            $filePath = CatalogCatUpload::filePath();
            if (is_file($filePath . $result->foto)) {
                unlink($filePath . $result->foto);
            }
            $result->delete();
        }
        
        Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = CatalogCat::findOne($id);
                    if (empty($result)) {
                        Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $filePath = CatalogCatUpload::filePath();
                    if (is_file($filePath . $result->foto)) {
                        unlink($filePath . $result->foto);
                    }
                    $result->delete();
                }
                
                Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    public function actionFastsave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $id = !empty($data['id']) && intval($data['id']) > 0 ? intval($data['id']) : 0;
            if ($id != 0) {
                unset($data['id']);
                $model = $id == 0 ? new CatalogCat() : $this->findModel($id);
                $model->load($data);
                if ($model->load($data) && $model->save($runValidation = true, $attributeNames = array_keys($data))) {
                    return [
                        'status' => 1,
                    ];
                }
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = CatalogCat::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}