<?php
namespace backend\controllers;

use backend\models\CatalogFoto;
use common\models\FileInfo;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use backend\components\IP;
use backend\components\Notice;
use backend\models\CatalogFiles;
use common\models\Admin;
use common\models\CatalogSubcat;
use common\models\CatalogSubcatCat;
use common\models\CatalogPosition;
use backend\models\CatalogPositionUpload;
use backend\models\CatalogPositionSearch;

/**
 * CatalogPosition controller
 */
class CatalogPositionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }
    
    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementCatalog')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionShow()
    {
        $searchModel = new CatalogPositionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0)
    {
        $model = $id == 0 ? new CatalogPosition() : $this->findModel($id);
        return $this->render('form', [
            'model' => $model,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new CatalogPosition();
        $model->who_create = IP::whoCreate();
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'file' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            $model->sort = $id;
            $model->update();
            
            if ($model->type_option == 2) {
                $filePath = CatalogPositionUpload::filePath();
                $modelUpload = new CatalogPositionUpload();
                $modelUpload->load($post);
                $file = UploadedFile::getInstance($modelUpload, 'file');
                $modelUpload->file = $file;
                
                if (($file && $file->tempName) && ($modelUpload->validate())) {
                    $nameFile = implode('', [
                        md5(microtime(true)),
                        '.',
                        $file->extension,
                    ]);
                    
                    if ($file->saveAs($filePath . $nameFile)) {
                        $model->file = $nameFile;
                        if (in_array($file->extension, CatalogPositionUpload::isPhoto())) {
                            CatalogPositionUpload::changeSize($model->file, 'file');
                            chmod($filePath . $model->file, 0644);
                        }
                    }
                } elseif (!empty($modelUpload->errors['file'])) {
                    $errorFilesArray['file'] = array_merge($errorFilesArray['file'], $modelUpload->errors['file']);
                }
            }
            
            $model->update();
            
            $url = array_merge([
                'form',
                'id' => $id,
                'edit' => 'ok',
            ], array_filter([
                'cat' => Yii::$app->request->get('cat'),
            ]));
            
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Вариант успешно создан', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Вариант создан с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new CatalogPosition();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                if (isset($arrayError['price'])) {
                    $arrayError['price' . $model->type_option] =
                        str_replace($model->attributeLabels()['price'], $model->attributeLabels()['price' .
                        $model->type_option], $arrayError['price']);
                    unset($arrayError['price']);
                }
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'file' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            
            $filePath = CatalogPositionUpload::filePath();
            if ($model->type_option == 2) {
                
                $modelUpload = new CatalogPositionUpload();
                $modelUpload->load($post);
                $file = UploadedFile::getInstance($modelUpload, 'file');
                $modelUpload->file = $file;
                
                if ($modelUpload->delete_file) {
                    if (is_file($filePath . $model->file)) {
                        unlink($filePath . $model->file);
                    }
                    $model->file = '';
                    $model->update();
                }
                
                if (($file && $file->tempName) && ($modelUpload->validate())) {
                    $nameFile = implode('', [
                        md5(microtime(true)),
                        '.',
                        $file->extension,
                    ]);
                    
                    if ($file->saveAs($filePath . $nameFile)) {
                        $model->file = $nameFile;
                        if (in_array($file->extension, CatalogPositionUpload::isPhoto())) {
                            CatalogPositionUpload::changeSize($model->file, 'file');
                            chmod($filePath . $model->file, 0644);
                        }
                        $model->update();
                    }
                } elseif (!empty($modelUpload->errors['file'])) {
                    $errorFilesArray['file'] = array_merge($errorFilesArray['file'], $modelUpload->errors['file']);
                }
            }
            
            if ($model->type_option == 1) {
                if (is_file($filePath . $model->file)) {
                    unlink($filePath . $model->file);
                }
                $model->file = '';
                $model->update();
            }
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
        } else {
            $url = ['show'];
        }
        
        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }
    
    public function actionChangeSort()
    {
        $status = 0;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->getRawBody() && $request->validateCsrfToken()) {
            
            $array = [];
            $inputArray = Json::decode($request->getRawBody(), true);
            
            if (empty($inputArray)) {
                $array['status'] = $status;
                return Json::encode($array);
            }
            
            CatalogPositionSearch::changeSort($inputArray);
            
            $status = 1;
        }
        $array['status'] = $status;
        return Json::encode($array);
    }
    
    public function actionRemove()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = CatalogPosition::findOne($id);
            if (empty($result)) {
                Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                    Url::to($url_redirect));
                return $this->redirect($url_redirect);
            }
            $filePath = CatalogPositionUpload::filePath();
            if (is_file($filePath . $result->file)) {
                unlink($filePath . $result->file);
            }
            
            $files = CatalogFiles::find()->where(['position' => $id])->all();
            foreach ($files as $file) {
                /**
                 * @var $file CatalogFiles
                 */
                if (is_file($filePath . $file->file)) {
                    unlink($filePath . $file->file);
                }
            }
            
            $files = CatalogFoto::find()->where(['position' => $id])->all();
            foreach ($files as $file) {
                /**
                 * @var $file CatalogFoto
                 */
                if (is_file($filePath . $file->file)) {
                    unlink($filePath . $file->file);
                }
            }
            
            $result->delete();
            CatalogFiles::deleteAll(['position' => $id]);
        }
        
        Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = CatalogPosition::findOne($id);
                    if (empty($result)) {
                        Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $filePath = CatalogPositionUpload::filePath();
                    if (is_file($filePath . $result->file)) {
                        unlink($filePath . $result->file);
                    }
                    
                    $files = CatalogFiles::find()->where(['position' => $id])->all();
                    foreach ($files as $file) {
                        /**
                         * @var $file CatalogFiles
                         */
                        if (is_file($filePath . $file->file)) {
                            unlink($filePath . $file->file);
                        }
                    }
                    
                    $files = CatalogFoto::find()->where(['position' => $id])->all();
                    foreach ($files as $file) {
                        /**
                         * @var $file CatalogFoto
                         */
                        if (is_file($filePath . $file->file)) {
                            unlink($filePath . $file->file);
                        }
                    }
                    
                    $result->delete();
                    CatalogFiles::deleteAll(['position' => $id]);
                    CatalogFoto::deleteAll(['position' => $id]);
                }
                
                Notice::send($message = 'Страницы успешно удалены', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    public function actionChangePrice()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            $price = array_key_exists('price', $data) ? intval($data['price']) : 0;
            if (!empty($idArr) && ($price > 0)) {
                foreach ($idArr as $id) {
                    $result = CatalogPosition::findOne($id);
                    if (empty($result)) {
                        Notice::send($message = 'Ошибка обновления цен', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $result->price = $price;
                    $result->update($runValidation = false);
                }
                
                Notice::send($message = 'Цены успешно обновлены', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    public function actionSubcat()
    {
        $html = [];
        if (Yii::$app->request->isAjax && Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $cat = Yii::$app->request->get('cat');
            $subcatCategories = CatalogSubcat::find()->select([
                CatalogSubcat::tableName() . '.id',
                CatalogSubcat::tableName() . '.title',
            ])->joinWith('cat')->where([CatalogSubcatCat::tableName() . '.id_cat' => $cat])
                ->orderBy(['title' => SORT_ASC])->asArray()->all();
            $subcatCategories = ArrayHelper::map($subcatCategories, 'id', 'title');
            foreach ($subcatCategories as $id => $value) {
                $html[] = implode('', [
                    '<option value="' . $id . '">',
                    $value,
                    '</option>',
                ]);
            }
        }
        return implode('', $html);
    }
    
    public function actionUploadFile()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post() && Yii::$app->request->validateCsrfToken()) {
            return $this->renderAjax('upload-file', [
                'data' => Yii::$app->request->post(),
            ]);
            
        }
        return true;
    }
    
    public function actionUploadFoto()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post() && Yii::$app->request->validateCsrfToken()) {
            return $this->renderAjax('upload-foto', [
                'data' => Yii::$app->request->post(),
            ]);
            
        }
        return true;
    }
    
    /*public function actionPositionFile($id)
    {
        if (!is_null($model = $this->findModel($id))) {
            if (is_file(CatalogPositionUpload::filePath() . $model->file)) {
                return Yii::$app->response->sendFile(CatalogPositionUpload::filePath() . $model->file);
            }
        }

        return Yii::$app->response->sendFile(CatalogPositionUpload::filePathDefault());

    }*/
    
    public function actionRemoveFile()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $id = Yii::$app->request->get('id');
            $model = $this->findModelFile($id);
            if (!empty($id) && !empty($model)) {
                if (is_file(CatalogFiles::filePath() . $model->file)) {
                    unlink(CatalogFiles::filePath() . $model->file);
                }
                $model->delete();
            }
        }
    }
    
    public function actionRemoveFoto()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $id = Yii::$app->request->get('id');
            $model = $this->findModelFoto($id);
            if (!empty($id) && !empty($model)) {
                if (is_file(CatalogFoto::filePath() . $model->file)) {
                    unlink(CatalogFoto::filePath() . $model->file);
                }
                $model->delete();
            }
        }
    }
    
    public function actionFoto($id)
    {
        if (!is_null($model = $this->findModelFoto($id))) {
            if (is_file(CatalogFoto::filePath() . $model->file)) {
                $nameFile = implode('.', [
                    $model->title,
                    FileInfo::getExtension($model->file),
                ]);
//                Yii::$app->response->setDownloadHeaders($nameFile, null, true);
                return Yii::$app->response->sendFile(CatalogFoto::filePath() . $model->file, $nameFile);
            }
        }
        
        return Yii::$app->response->sendFile(CatalogFoto::filePathDefault());
    }
    
    /*
    public function actionFile($id)
    {
        if (!is_null($model = $this->findModelFile($id))) {
            if (is_file(CatalogFiles::filePath() . $model->file)) {
                return Yii::$app->response->sendFile(CatalogFiles::filePath() . $model->file, implode('.', [
                    $model->title,
                    FileInfo::getExtension($model->file),
                ]));
            }
        }

        return Yii::$app->response->sendFile(CatalogFiles::filePathDefault());

    }
    */
    
    public function actionPosition()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post() && Yii::$app->request->validateCsrfToken()) {
            $post = Yii::$app->request->post();
            $model = new CatalogPosition();
            $model->who_create = IP::whoCreate();
            $model->who_update = IP::whoCreate();
            if ($model->load($post) && $model->save()) {
                return $model->id;
            } else {
                
            }
        }
        return 0;
    }
    
    public function actionFastsave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $id = !empty($data['id']) && intval($data['id']) > 0 ? intval($data['id']) : 0;
            if ($id != 0) {
                unset($data['id']);
                $model = $id == 0 ? new CatalogPosition() : $this->findModel($id);
                $model->load($data);
                if ($model->load($data) && $model->save($runValidation = true, $attributeNames = array_keys($data))) {
                    return [
                        'status' => 1,
                    ];
                }
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = CatalogPosition::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
    
    protected function findModelFile($id)
    {
        if (($model = CatalogFiles::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
    
    protected function findModelFoto($id)
    {
        if (($model = CatalogFoto::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}