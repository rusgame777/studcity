<?php
namespace backend\controllers;

use common\models\Author;
use Yii;
use yii\web\Controller;
use common\models\Admin;

/**
 * Site controller
 */
class SiteController extends Controller
{
    #public $defaultRoute = 'site/login';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }
    
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/login.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function actionLogin()
    {
        return $this->render('login');
    }
    
    public function actionLogout()
    {
        $validate = Yii::$app->request->validateCsrfToken();
        if ($validate) {
            $data = Yii::$app->request->get();
            return $this->render('logout', ['data' => $data,]);
        }
        return true;
    }
    
    public function actionError()
    {
        return true;
        return $this->render('login');
    }
    
    public function actionNorights()
    {
        $this->layout = '@app/views/layouts/main.php';
        return $this->render('norights');
    }
    
    public function beforeAction($action)
    {
        if ($action->id == 'logout') {
        } elseif ($action->id == 'error') {
            $this->redirect(['site/login'], 302)->send();
        } else {
            $admin_login = Yii::$app->request->cookies->getValue('admin_login');
            $admin_hash = Yii::$app->request->cookies->getValue('admin_hash');
            if (!empty($admin_login) && !empty($admin_hash)) {
                $admin = Admin::findByAccessToken($admin_login, $admin_hash);
                if ($action->id == 'norights') {
                    Yii::$app->view->params['id_user'] = $admin->id;
                    Yii::$app->view->params['id_role'] = $admin->id_role;
                    return true;
                }
            }
            
            $author_login = Yii::$app->request->cookies->getValue('author_login');
            $author_hash = Yii::$app->request->cookies->getValue('author_hash');
            if (!empty($author_login) && !empty($author_hash)) {
                $author = Author::findByAccessToken($author_login, $author_hash);
                if ($action->id == 'norights') {
                    Yii::$app->view->params['id_user'] = $author->id;
                    Yii::$app->view->params['id_role'] = $author->id_role;
                    return true;
                }
            }
            
            if (!empty($admin)) {
                $this->redirect(['page/show'], 302)->send();
                return true;
            }
            if (!empty($author)) {
                $this->redirect(['solution-orders/management'], 302)->send();
                return true;
            }
        }
        return true;
    }
}