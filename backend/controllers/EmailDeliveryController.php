<?php
namespace backend\controllers;

use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use backend\components\Notice;
use common\models\Admin;
use common\models\EmailTemplate;
use common\models\EmailDelivery;

/**
 * EmailDelivery controller
 */
class EmailDeliveryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }
    
    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementTemplate')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    /**
     * @return mixed
     */
    public function actionForm()
    {
        return $this->render('form', [
            'model' => new EmailDelivery(),
        ]);
    }
    
    public function actionCreate()
    {
        $model = new EmailDelivery();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'file' => [],
        ];
        $file = UploadedFile::getInstance($model, 'file');
        $model->load($post);
        $model->file = $file;
        if ($model->validate()) {
            $filePath = EmailDelivery::filePath();
            if ($file && $file->tempName) {
                $nameFile = implode('', [
                    md5(microtime(true)),
                    '.',
                    $file->extension,
                ]);
                $uploadFile = $filePath . $nameFile;
                if ($file->saveAs($uploadFile)) {
                    $template = EmailTemplate::findOne($model->cat);
                    EmailDelivery::generateContent($template, $uploadFile);
                }
            } elseif (!empty($model->errors['file'])) {
                $errorFilesArray['file'] = array_merge($errorFilesArray['file'], $model->errors['file']);
            }

            $url = ['email-delivery/form'];

            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Рассылка успешно выполнена', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Рассылка завершилась неудачей.' . $errorText, $id_type = Notice::warning, $id =
                    Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        } else {
            //
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new EmailDelivery();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
}