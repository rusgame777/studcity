<?php
namespace backend\controllers;

use backend\components\Common;
use backend\components\Notice;
use backend\models\OrdersDocs;
use backend\models\OrderSendForm;
use backend\models\OrdersFiles;
use backend\models\OrdersUpload;
use backend\models\SolutionOrdersExcel;
use common\components\Email;
use common\components\Order;
use common\components\Phone;
use common\components\SolutionOrderSend;
use common\models\Author;
use common\models\Clients;
use common\models\FileInfo;
use common\models\OrderCheck;
use common\models\Orders;
use common\models\OrdersErrors;
use common\models\OrdersRemember;
use common\models\OrderStatus;
use common\models\OrderType;
use common\models\Payments;
use common\models\Settings;
use common\models\SettingsEmail;
use common\models\SmsClient;
use common\models\SolutionCat;
use common\models\SolutionData;
use common\models\SolutionOrderData;
use common\models\SolutionOrderDesc;
use common\models\SolutionPosition;
use backend\models\OrderForm;
use common\models\UserSolution;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use common\models\Admin;
use backend\models\SolutionOrdersSearch;
use backend\models\SolutionOrdersManagement;
use backend\models\SolutionStatistics;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * SolutionOrders controller
 */
class SolutionOrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights($action));
    }
    
    public static function isManagementSolutionStatistics($action)
    {
        return in_array($action->id, [
            'show',
            'statistics',
            'excel',
        ]);
        
    }
    
    public static function isManagementSolutionOrders($action)
    {
        return in_array($action->id, [
            'management',
            'remove',
            'remove-file',
            'upload-file',
            'delete',
            'form',
            'send',
            'send-validate',
            'send-submit',
            'form',
            'validate',
            'create',
            'update',
            'data',
            'position',
        ]);
    }
    
    public function checkRights($action)
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementSolutionStatistics') &&
            self::isManagementSolutionStatistics($action) ||
            !Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementSolutionOrders') &&
            self::isManagementSolutionOrders($action)
        ) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionShow()
    {
        $searchModel = new SolutionOrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }
    
    public function actionExcel()
    {
        $searchModel = new SolutionOrdersSearch();
        $query = $searchModel->searchQuery(Yii::$app->request->queryParams);
        $data = $query->all();
        $pathFile = SolutionOrdersExcel::unload($data);
        
        if (is_file($pathFile)) {
            return Yii::$app->response->sendFile($pathFile);
        }
        return false;
    }
    
    public function actionManagement()
    {
        $searchModel = new SolutionOrdersManagement();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('management', $data);
    }
    
    public function actionStatistics()
    {
        $searchModel = new SolutionStatistics();
        $searchModel->load(Yii::$app->request->queryParams);
        return $this->render('statistics', [
            'searchModel' => $searchModel,
        ]);
    }
    
    public function actionRemove()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['management'];
        if (Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $id = Yii::$app->request->get('id');
            $result = Orders::findOne($id);
            if (empty($result)) {
                Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                    Url::to($url_redirect));
                return $this->redirect($url_redirect);
            }
            $result->delete();
            $filePath = OrdersUpload::filePath();
            
            $docs = OrdersDocs::find()->where(['id_order' => $id])->all();
            foreach ($docs as $doc) {
                /**
                 * @var $doc OrdersDocs
                 */
                if (is_file($filePath . $doc->file)) {
                    unlink($filePath . $doc->file);
                }
            }
            
            $files = OrdersFiles::find()->where(['id_order' => $id])->all();
            foreach ($files as $file) {
                /**
                 * @var $file OrdersFiles
                 */
                if (is_file($filePath . $file->file)) {
                    unlink($filePath . $file->file);
                }
            }
            
            Payments::deleteAll(['InvId' => $id]);
            OrdersDocs::deleteAll(['id_order' => $id]);
            OrdersFiles::deleteAll(['id_order' => $id]);
            OrderCheck::deleteAll(['id_order' => $id]);
            OrdersErrors::deleteAll(['id_order' => $id]);
            OrdersRemember::deleteAll(['id_order' => $id]);
            SolutionOrderData::deleteAll(['id_order' => $id]);
            SolutionOrderDesc::deleteAll(['id_order' => $id]);
        }
        
        Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['management'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = Orders::findOne($id);
                    if (empty($result)) {
                        Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $result->delete();
                    $filePath = OrdersUpload::filePath();
                    
                    $docs = OrdersDocs::find()->where(['id_order' => $id])->all();
                    foreach ($docs as $doc) {
                        /**
                         * @var $doc OrdersDocs
                         */
                        if (is_file($filePath . $doc->file)) {
                            unlink($filePath . $doc->file);
                        }
                    }
                    
                    $files = OrdersFiles::find()->where(['id_order' => $id])->all();
                    foreach ($files as $file) {
                        /**
                         * @var $file OrdersFiles
                         */
                        if (is_file($filePath . $file->file)) {
                            unlink($filePath . $file->file);
                        }
                    }
                    
                    Payments::deleteAll(['InvId' => $id]);
                    OrdersDocs::deleteAll(['id_order' => $id]);
                    OrdersFiles::deleteAll(['id_order' => $id]);
                    OrderCheck::deleteAll(['id_order' => $id]);
                    OrdersErrors::deleteAll(['id_order' => $id]);
                    OrdersRemember::deleteAll(['id_order' => $id]);
                    SolutionOrderData::deleteAll(['id_order' => $id]);
                    SolutionOrderDesc::deleteAll(['id_order' => $id]);
                }
                
                Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionSend($id = 0)
    {
        $model = $id == 0 ? new Orders() : $this->findModel($id);
        return $this->render('send', ['model' => $model]);
    }
    
    public function actionSendValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new OrderSendForm();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                
                return $arrayError;
            }
            
        }
        return false;
    }
    
    public function actionSendSubmit($id)
    {
        
        $orderSendForm = new OrderSendForm();
        $post = Yii::$app->request->post();
        if ($orderSendForm->load($post) && $orderSendForm->validate($post)) {
            
            $id_order = $post['id'];
            $orderSend = new SolutionOrderSend($id_order);
            $orderSend->response();
            
            $url = ['management'];
            
            $errorFilesArray = [];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'РГР успешно отправлена', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'РГР отправлена с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            exit;
        }
        return false;
    }
    
    public function actionUploadFile()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post() && Yii::$app->request->validateCsrfToken()) {
            return $this->renderAjax('upload', [
                'data' => Yii::$app->request->post(),
            ]);
            
        }
        return true;
    }
    
    public function actionRemoveFile()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $id = Yii::$app->request->get('id');
            $model = $this->findModelFile($id);
            if (!empty($id) && !empty($model)) {
                if (is_file(OrdersFiles::filePath() . $model->file)) {
                    unlink(OrdersFiles::filePath() . $model->file);
                }
                $model->delete();
            }
        }
    }
    
    public function actionAddUploadFile()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post() && Yii::$app->request->validateCsrfToken()) {
            return $this->renderAjax('add/upload', [
                'data' => Yii::$app->request->post(),
            ]);
            
        }
        return true;
    }
    
    public function actionAddPosition()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post() && Yii::$app->request->validateCsrfToken()) {
            $post = Yii::$app->request->post();
            $model = new OrderForm();
            if ($model->load($post) && $model->validate($post)) {
                $id_avt = md5(uniqid(rand(), 1));
                $params = [
                    'id_avt' => $id_avt,
                    'name' => $model->name,
                    'email' => $model->email,
                    'phone' => $model->phone,
                ];
                
                $client = new Clients();
                if ($client->load($params) && $client->save()) {
                    $id_client = $client->getPrimaryKey();
                    
                    $position = SolutionPosition::findOne($model->id_position);
                    $sum_all = !empty($position->price) ? $position->price : 0;
                    $cat = $model->cat;

//                $date_end = Yii::$app->formatter->asDate(strtotime("+ 3 days"), 'php:Y-m-d H:i:s');
                    $date_end = Common::getDateTime('', 'Y-m-d H:i:s', '+3 days');
                    
                    $params = [
                        'id_user' => $id_client,
                        'sum' => $sum_all,
                        'date_end' => $date_end,
                        'id_type' => OrderType::solution,
                        'send' => 1,
                        'status' => OrderStatus::send,
                    ];
                    
                    $order = new Orders();
                    if ($order->load($params) && $order->save()) {
                        $id_order = $order->getPrimaryKey();
                        
                        $params = [
                            'id_order' => $id_order,
                            'id_position' => $model->id_position,
                            'cat' => $cat,
                        ];
                        $orderDesc = new SolutionOrderDesc();
                        if ($orderDesc->load($params) && $orderDesc->save()) {
                            //
                        }
                        
                        if (isset($post['data'])) {
                            foreach ($post['data'] as $key => $data) {
                                $solutionOrderData = new SolutionOrderData();
                                $solutionOrderData->load($data, '');
                                $solutionOrderData->id_order = $id_order;
                                if ($solutionOrderData->save()) {
                                    
                                }
                            }
                        }
                        
                        $params = [
                            'id_order' => $id_order,
                        ];
                        $orderCheck = new OrderCheck();
                        $orderCheck->load($params);
                        if ($orderCheck->save()) {
                            //
                        }
                        
                        $url = [
                            'form',
                            'id' => $id_order,
                            'edit' => 'ok',
                        ];
                        
                        $errorFilesArray = [];
                        $errorText = '';
                        foreach ($errorFilesArray as $key => $array) {
                            if (!empty($array)) {
                                $errorText .= ' ' . implode(' ', $array);
                            }
                        }
                        if (empty($errorText)) {
                            Notice::send($message = 'Ваш заказ успешно создан!', $id_type = Notice::info, $id =
                                Url::to($url));
                        } else {
                            Notice::send($message = 'Ваш заказ создан с некоторыми ошибками.' . $errorText, $id_type =
                                Notice::warning, $id = Url::to($url));
                        }
                        
                        return $id_order;
                    }
                    
                }
            }
        }
        return 0;
    }
    
    public function actionAddRemoveFile()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $id = Yii::$app->request->get('id');
            $model = $this->findModelFile($id);
            if (!empty($id) && !empty($model)) {
                if (is_file(OrdersDocs::filePath() . $model->file)) {
                    unlink(OrdersDocs::filePath() . $model->file);
                }
                $model->delete();
            }
        }
        return true;
    }

//    public function actionFile($id)
//    {
//        if (!is_null($model = $this->findModelFile($id))) {
//            if (is_file(OrdersFiles::filePath() . $model->file)) {
//                return Yii::$app->response->sendFile(OrdersFiles::filePath() . $model->file, implode('.', [
//                    $model->title,
//                    FileInfo::getExtension($model->file),
//                ]));
//            }
//        }
//
//        return Yii::$app->response->sendFile(OrdersFiles::filePathDefault());
//
//    }
    
    public function actionDoc($id)
    {
        if (!is_null($model = $this->findModelDoc($id))) {
            if (is_file(OrdersDocs::filePath() . $model->file)) {
                return Yii::$app->response->sendFile(OrdersDocs::filePath() . $model->file, implode('.', [
                    $model->title,
                    FileInfo::getExtension($model->file),
                ]));
            }
        }
        
        return Yii::$app->response->sendFile(OrdersDocs::filePathDefault());
        
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0)
    {
        $model = $id == 0 ? new Orders() : $this->findModel($id);
        return $this->render('form', ['model' => $model]);
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new OrderForm();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                
                if (isset($post['data'])) {
                    $modelItems = [];
                    foreach ($post['data'] as $index => $data) {
                        $modelItem = new SolutionOrderData();
                        if ($modelItem->load($data, '')) {
                            $modelItems[$index] = $modelItem;
                        }
                    }
                    $arrayError = array_merge($arrayError, ActiveForm::validateMultiple($modelItems));
                }
                return $arrayError;
            }
            
        }
        return false;
    }
    
    public function actionCreate()
    {
        
        $model = new OrderForm();
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->validate($post)) {
            $id_avt = md5(uniqid(rand(), 1));
            $params = [
                'id_avt' => $id_avt,
                'name' => $model->name,
                'email' => $model->email,
                'phone' => $model->phone,
            ];
            
            $client = new Clients();
            if ($client->load($params) && $client->save()) {
                $id_client = $client->getPrimaryKey();
                
                $position = SolutionPosition::findOne($model->id_position);
                $sum_all = !empty($position->price) ? $position->price : 0;
                $cat = $model->cat;

//                $date_end = Yii::$app->formatter->asDate(strtotime("+ 3 days"), 'php:Y-m-d H:i:s');
                $date_end = Common::getDateTime('', 'Y-m-d H:i:s', '+3 days');
                
                $params = [
                    'id_user' => $id_client,
                    'sum' => $sum_all,
                    'date_end' => $date_end,
                    'id_type' => OrderType::solution,
                    'send' => 1,
                    'status' => OrderStatus::send,
                ];
                
                $order = new Orders();
                if ($order->load($params) && $order->save()) {
                    $id_order = $order->getPrimaryKey();
                    
                    $params = [
                        'id_order' => $id_order,
                        'id_position' => $model->id_position,
                        'cat' => $cat,
                    ];
                    $orderDesc = new SolutionOrderDesc();
                    if ($orderDesc->load($params) && $orderDesc->save()) {
                        //
                    }
                    
                    if (isset($post['data'])) {
                        foreach ($post['data'] as $key => $data) {
                            $solutionOrderData = new SolutionOrderData();
                            $solutionOrderData->load($data, '');
                            $solutionOrderData->id_order = $id_order;
                            if ($solutionOrderData->save()) {
                                
                            }
                        }
                    }
                    
                    $params = [
                        'id_order' => $id_order,
                    ];
                    $orderCheck = new OrderCheck();
                    $orderCheck->load($params);
                    if ($orderCheck->save()) {
                        //
                    }
                    
                    $url = [
                        'form',
                        'id' => $id_order,
                        'edit' => 'ok',
                    ];
                    
                    $errorFilesArray = [];
                    $errorText = '';
                    foreach ($errorFilesArray as $key => $array) {
                        if (!empty($array)) {
                            $errorText .= ' ' . implode(' ', $array);
                        }
                    }
                    if (empty($errorText)) {
                        Notice::send($message = 'Ваш заказ успешно создан!', $id_type = Notice::info, $id =
                            Url::to($url));
                    } else {
                        Notice::send($message = 'Ваш заказ создан с некоторыми ошибками.' . $errorText, $id_type =
                            Notice::warning, $id = Url::to($url));
                    }
                    
                    echo "<script>document.location='" . Url::to($url) . "';</script>";
                    exit;
                }
                
            }
        }
        return false;
    }
    
    public function actionUpdate($id)
    {
        
        $model = new OrderForm();
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->validate($post)) {
            
            $id_user = $post['id'];
            if (($client = Clients::findOne($id_user)) === null) {
                $id_avt = md5(uniqid(rand(), 1));
                $params = [
                    'id_avt' => $id_avt,
                    'name' => $model->name,
                    'email' => $model->email,
                    'phone' => $model->phone,
                ];
                $client = new Clients();
            } else {
                $params = [
                    'name' => $model->name,
                    'email' => $model->email,
                    'phone' => $model->phone,
                ];
            }
            
            $cat = $model->cat;
            if ($client->load($params) && $client->save()) {
                $id_client = $client->getPrimaryKey();
                
                if (($order = Orders::findOne(['id_user' => $id_user])) === null) {
                    $position = SolutionPosition::findOne($model->id_position);
                    $sum_all = !empty($position->price) ? $position->price : 0;
                    
                    $date_end = Common::getDateTime('', 'Y-m-d H:i:s', '+3 days');
//                    $date_end = Yii::$app->formatter->asDate(strtotime("+ 3 days"), 'php:Y-m-d H:i:s');
                    
                    $params = [
                        'id_user' => $id_client,
                        'sum' => $sum_all,
                        'date_end' => $date_end,
                        'id_type' => OrderType::solution,
                        'send' => 1,
                        'status' => OrderStatus::send,
                    ];
                    $order = new Orders();
                } else {
                    $params = [
                        'id_user' => $id_client,
                    ];
                }
                
                if ($order->load($params) && $order->save()) {
                    $id_order = $order->getPrimaryKey();
                    
                    if (($orderDesc = SolutionOrderDesc::findOne(['id_order' => $id_order])) === null) {
                        $orderDesc = new SolutionOrderDesc();
                    }
                    $params = [
                        'id_order' => $id_order,
                        'id_position' => $model->id_position,
                        'cat' => $cat,
                    ];
                    
                    if ($orderDesc->load($params) && $orderDesc->save()) {
                        //
                    }
                    
                    if (($orderCheck = OrderCheck::findOne(['id_order' => $id_order])) === null) {
                        $orderCheck = new OrderCheck();
                    }
                    $params = [
                        'id_order' => $id_order,
                    ];
                    
                    $orderCheck->load($params);
                    if ($orderCheck->save()) {
                        //
                    }
                    
                    $arrayId =
                        ArrayHelper::getColumn(SolutionOrderData::find()->where(['id_order' => $id_order])->asArray()
                            ->all(), 'id');
                    if (isset($post['data'])) {
                        foreach ($post['data'] as $key => $data) {
                            if (($solutionOrderData = SolutionOrderData::findOne($data['id'])) === null) {
                                $solutionOrderData = new SolutionOrderData();
                            }
                            
                            $solutionOrderData->load($data, '');
                            $solutionOrderData->id_order = $id_order;
                            if ($solutionOrderData->save()) {
                                $key = array_search($solutionOrderData->id, $arrayId, false);
                                if ($key !== false) {
                                    unset($arrayId[$key]);
                                }
                            } else {
//                      file_put_contents('errors.txt', var_export($modelItem->errors, true).PHP_EOL, FILE_APPEND);
                            }
                        }
                    }
                    
                    foreach ($arrayId as $itemId) {
                        $solutionOrderData = SolutionOrderData::findOne($itemId);
                        $solutionOrderData->delete();
                    }
                    
                    $url = [
                        'form',
                        'id' => $id_order,
                        'edit' => 'ok',
                    ];
                    
                    $errorFilesArray = [];
                    $errorText = '';
                    foreach ($errorFilesArray as $key => $array) {
                        if (!empty($array)) {
                            $errorText .= ' ' . implode(' ', $array);
                        }
                    }
                    if (empty($errorText)) {
                        Notice::send($message = 'Ваш заказ успешно изменен!', $id_type = Notice::info, $id =
                            Url::to($url));
                    } else {
                        Notice::send($message = 'Ваш заказ изменен с некоторыми ошибками.' . $errorText, $id_type =
                            Notice::warning, $id = Url::to($url));
                    }
                    
                    echo "<script>document.location='" . Url::to($url) . "';</script>";
                    exit;
                } else {
                }
                
            }
        }
        return false;
    }
    
    public function actionPosition()
    {
        $html = [];
        if (Yii::$app->request->isAjax && Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $cat = Yii::$app->request->get('cat');
            $positions =
                SolutionPosition::find()->where(['cat' => $cat])->orderBy(['title' => SORT_ASC])->asArray()->all();
            $positions = ArrayHelper::map($positions, 'id', 'title');
            foreach ($positions as $id => $value) {
                $html[] = implode('', [
                    '<option value="' . $id . '">',
                    $value,
                    '</option>',
                ]);
            }
        }
        return implode('', $html);
    }
    
    public function actionData()
    {
        $html = '';
        if (Yii::$app->request->isAjax && Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            
            $id_position = Yii::$app->request->get('id_position');
            $solutionParams =
                SolutionData::find()->select([new Expression("'' AS `value`"), new Expression("'0' AS `id`"), 'name'])
                    ->where(['id_position' => $id_position])->orderBy(['id' => SORT_ASC])->all();
            
            if (count($solutionParams) > 0) {
                ob_start()
                ?>
                <div class='section_title' style="margin-top: 0;"><h3>Данные</h3></div>
                <?php
                foreach ($solutionParams as $indexItem => $solutionParam) {
                    /**
                     * @var $solutionParam SolutionData
                     */
                    ?>
                    <div class="goods_value_list">
                        <div class='goods_values'>
                            <?= Html::activeHiddenInput($solutionParam, '[' . $indexItem . ']id'); ?>
                            <?= Html::activeHiddenInput($solutionParam, '[' . $indexItem . ']name'); ?>
                            <div class="goods_value">
                                <?php
                                $id = implode('-', [
                                    $solutionParam->formName(),
                                    $indexItem,
                                    'value',
                                ]);
                                echo Html::beginTag('div', ['class' => 'form-group field-' . $id . ' required']);
                                echo Html::textInput($solutionParam->formName() . '[' . $indexItem .
                                    '][value]', $solutionParam->value, [
                                    'id' => $id,
                                    'class' => 'form_input',
                                    'placeholder' => $solutionParam->name,
                                    'aria-invalid' => false,
                                ]);
                                echo Html::tag('div', '', ['class' => 'form_error']);
                                echo Html::endTag('div');
                                ?>
                                <script>
                                $(function () {
                                    $('#dynamic-form').yiiActiveForm('add', {
                                        id: '<?=$id?>',
                                        name: '[<?=$indexItem?>]value',
                                        container: '.field-<?=$id?>',
                                        input: '#<?=$id?>',
                                        error: '.form_error',
                                        enableAjaxValidation: true
                                    });
                                })
                                </script>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php
                $html = ob_get_clean();
            }
        }
        return $html;
        
    }
    
    public static function getEmail()
    {
        $emailArr = [];
        $emailList = SettingsEmail::find()->all();
        foreach ($emailList as $email) {
            if (!empty($email->email)) {
                $emailArr[] = $email->email;
            }
        }
        return $emailArr;
    }
    
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
    
    protected function findModelFile($id)
    {
        if (($model = OrdersFiles::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
    
    protected function findModelDoc($id)
    {
        if (($model = OrdersDocs::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}