<?php
namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use backend\components\Notice;
use backend\models\AdminSearch;
use backend\models\UserCat;
use common\models\Admin;
use common\models\CatalogCat;
use common\models\SolutionCat;
use common\models\UserCatalog;
use common\models\UserSolution;

/**
 * Admin controller
 */
class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }
    
    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementAdmins')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionShow()
    {
        $searchModel = new AdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0)
    {
        $model = $id == 0 ? new Admin() : $this->findModel($id);
        $catalogCat =
            CatalogCat::find()->select(['id', 'title'])->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])->asArray()
                ->all();
        $catalogCat = ArrayHelper::map($catalogCat, 'id', 'title');
        $solutionCat =
            SolutionCat::find()->select(['id', 'title'])->orderBy(['sort' => SORT_ASC, 'id' => SORT_ASC])->asArray()
                ->all();
        $solutionCat = ArrayHelper::map($solutionCat, 'id', 'title');
        
        $userCatalogCat = UserCatalog::find()->where(['id_user' => $id])->asArray()->all();
        $userCatalogCat = ArrayHelper::getColumn($userCatalogCat, 'id_cat');
        $userSolutionCat = UserSolution::find()->where(['id_user' => $id])->asArray()->all();
        $userSolutionCat = ArrayHelper::getColumn($userSolutionCat, 'id_cat');
        
        $modelCat = new UserCat([
            'catalog_cat' => $userCatalogCat,
            'solution_cat' => $userSolutionCat,
        ]);
        
        return $this->render('form', [
            'model' => $model,
            'modelCat' => $modelCat,
            'catalogCat' => $catalogCat,
            'solutionCat' => $solutionCat,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new Admin();
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
//            $model->id_role = 2;
            $model->update();
    
            if ($id > 2) {
                $modelCat = new UserCat();
                $modelCat->load($post);
                if ($modelCat->validate()) {
                    $arrayCatalogId = ArrayHelper::getColumn(UserCatalog::find()->where(['id_user' => $id])->asArray()
                        ->all(), 'id_cat');
                    if (!empty($modelCat->catalog_cat)) {
                        foreach ($modelCat->catalog_cat as $key => $id_cat) {
                            if (array_key_exists(intval($id_cat), $arrayCatalogId)) {
                                $modelItem = UserCatalog::find()->where(['id_user' => $id, 'id_cat' => $id_cat])->one();
                            } else {
                                $modelItem = new UserCatalog();
                            }
                    
                            $modelItem->load([
                                'id_cat' => $id_cat,
                                'id_user' => $id,
                            ]);
                            if ($modelItem->save()) {
                                $key = array_search($modelItem->id_cat, $arrayCatalogId, false);
                                if ($key !== false) {
                                    unset($arrayCatalogId[$key]);
                                }
                            } else {
//                            file_put_contents('errors.txt', var_export($modelItem->errors, true) . PHP_EOL, FILE_APPEND);
                            }
                        }
                    }
            
                    foreach ($arrayCatalogId as $itemCatalogId) {
                        $modelItem = UserCatalog::find()->where(['id_user' => $id, 'id_cat' => $itemCatalogId])->one();
                        $modelItem->delete();
                    }
            
            
                    $arraySolutionId = ArrayHelper::getColumn(UserSolution::find()->where(['id_user' => $id])->asArray()
                        ->all(), 'id_cat');
                    if (!empty($modelCat->solution_cat)) {
                        foreach ($modelCat->solution_cat as $key => $id_cat) {
                            if (array_key_exists(intval($id_cat), $arraySolutionId)) {
                                $modelItem = UserSolution::find()->where(['id_user' => $id, 'id_cat' => $id_cat])->one();
                            } else {
                                $modelItem = new UserSolution();
                            }
                    
                            $modelItem->load([
                                'id_cat' => $id_cat,
                                'id_user' => $id,
                            ]);
                            if ($modelItem->save()) {
                                $key = array_search($modelItem->id_cat, $arraySolutionId, false);
                                if ($key !== false) {
                                    unset($arraySolutionId[$key]);
                                }
                            } else {
//                            file_put_contents('errors.txt', var_export($modelItem->errors, true) . PHP_EOL, FILE_APPEND);
                            }
                        }
                    }
            
                    foreach ($arraySolutionId as $itemSolutionId) {
                        $modelItem = UserSolution::find()->where(['id_user' => $id, 'id_cat' => $itemSolutionId])->one();
                        $modelItem->delete();
                    }
                } else {
//                file_put_contents('errors.txt', var_export($modelCat->errors, true) . PHP_EOL, FILE_APPEND);
                }
            }
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorFilesArray = [];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Пользователь успешно создан', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Пользователь создан с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        } else {
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new Admin();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            
            if ($id > 2) {
                $modelCat = new UserCat();
                $modelCat->load($post);
                if ($modelCat->validate()) {
                    $arrayCatalogId = ArrayHelper::getColumn(UserCatalog::find()->where(['id_user' => $id])->asArray()
                        ->all(), 'id_cat');
                    if (!empty($modelCat->catalog_cat)) {
                        foreach ($modelCat->catalog_cat as $key => $id_cat) {
                            if (array_key_exists(intval($id_cat), $arrayCatalogId)) {
                                $modelItem = UserCatalog::find()->where(['id_user' => $id, 'id_cat' => $id_cat])->one();
                            } else {
                                $modelItem = new UserCatalog();
                            }
                            
                            $modelItem->load([
                                'id_cat' => $id_cat,
                                'id_user' => $id,
                            ]);
                            if ($modelItem->save()) {
                                $key = array_search($modelItem->id_cat, $arrayCatalogId, false);
                                if ($key !== false) {
                                    unset($arrayCatalogId[$key]);
                                }
                            } else {
//                            file_put_contents('errors.txt', var_export($modelItem->errors, true) . PHP_EOL, FILE_APPEND);
                            }
                        }
                    }
                    
                    foreach ($arrayCatalogId as $itemCatalogId) {
                        $modelItem = UserCatalog::find()->where(['id_user' => $id, 'id_cat' => $itemCatalogId])->one();
                        $modelItem->delete();
                    }
    
    
                    $arraySolutionId = ArrayHelper::getColumn(UserSolution::find()->where(['id_user' => $id])->asArray()
                        ->all(), 'id_cat');
                    if (!empty($modelCat->solution_cat)) {
                        foreach ($modelCat->solution_cat as $key => $id_cat) {
                            if (array_key_exists(intval($id_cat), $arraySolutionId)) {
                                $modelItem = UserSolution::find()->where(['id_user' => $id, 'id_cat' => $id_cat])->one();
                            } else {
                                $modelItem = new UserSolution();
                            }
            
                            $modelItem->load([
                                'id_cat' => $id_cat,
                                'id_user' => $id,
                            ]);
                            if ($modelItem->save()) {
                                $key = array_search($modelItem->id_cat, $arraySolutionId, false);
                                if ($key !== false) {
                                    unset($arraySolutionId[$key]);
                                }
                            } else {
//                            file_put_contents('errors.txt', var_export($modelItem->errors, true) . PHP_EOL, FILE_APPEND);
                            }
                        }
                    }
    
                    foreach ($arraySolutionId as $itemSolutionId) {
                        $modelItem = UserSolution::find()->where(['id_user' => $id, 'id_cat' => $itemSolutionId])->one();
                        $modelItem->delete();
                    }
                } else {
//                file_put_contents('errors.txt', var_export($modelCat->errors, true) . PHP_EOL, FILE_APPEND);
                }
            }
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorFilesArray = [];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
        } else {
            $url = ['show'];
        }
        
        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }
    
    public function actionRemove()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        if (Yii::$app->request->get() && Yii::$app->request->validateCsrfToken()) {
            $id = Yii::$app->request->get('id');
            $result = Admin::findOne($id);
            if ($result->id_role == 1) {
                $result->delete();
                UserCatalog::deleteAll(['id_user' => $id]);
                UserSolution::deleteAll(['id_user' => $id]);
            }
        }
        
        Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = Admin::findOne($id);
                    if (empty($result) || ($result->id_role != 1)) {
                        Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $result->delete();
                    UserCatalog::deleteAll(['id_user' => $id]);
                    UserSolution::deleteAll(['id_user' => $id]);
                }
                
                Notice::send($message = 'Страницы успешно удалены', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}