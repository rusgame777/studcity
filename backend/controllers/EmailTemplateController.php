<?php
namespace backend\controllers;

use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use backend\components\IP;
use backend\components\Notice;
use common\models\Admin;
use common\models\EmailTemplate;
use backend\models\EmailTemplateUpload;
use backend\models\EmailTemplateSearch;

/**
 * EmailTemplate controller
 */
class EmailTemplateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }
    
    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementTemplate')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionShow()
    {
        $searchModel = new EmailTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0)
    {
        $model = $id == 0 ? new EmailTemplate() : $this->findModel($id);
        return $this->render('form', [
            'model' => $model,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new EmailTemplate();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'foto' => [],
            'attachment' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            $model->sort = $id;
            $model->update();
            
            $filePath = EmailTemplateUpload::filePath();
            if (isset($post['files'])) {
                foreach ($post['files'] as $key => $fileArray) {
                    $modelUploadFile = new EmailTemplateUpload();
                    $modelUploadFile->load($fileArray, '');

                    $nameFoto = 'foto' . $key;
                    $foto = UploadedFile::getInstance($modelUploadFile, '[' . $key . ']foto');
                    $modelUploadFile->foto = $foto;
                    
                    if (($foto && $foto->tempName) && ($modelUploadFile->validate())) {
                        $nameFile = implode('', [
                            md5(microtime(true)),
                            '.',
                            $foto->extension,
                        ]);
                        if ($foto->saveAs($filePath . $nameFile)) {
                            $model->$nameFoto = $nameFile;
                            EmailTemplateUpload::changeSize($model->$nameFoto, 'foto');
                        }
                    } elseif (!empty($modelUploadFile->errors['foto'])) {
                        $errorFilesArray['foto'] =
                            array_merge($errorFilesArray['foto'], $modelUploadFile->errors['foto']);
                    }

                    $nameAttachment = 'attachment' . $key;
                    $attachment = UploadedFile::getInstance($modelUploadFile, '[' . $key . ']attachment');
                    $modelUploadFile->attachment = $attachment;

                    if (($attachment && $attachment->tempName) && ($modelUploadFile->validate())) {
                        $nameFile = implode('', [
                            md5(microtime(true)),
                            '.',
                            $attachment->extension,
                        ]);
                        if ($attachment->saveAs($filePath . $nameFile)) {
                            $model->$nameAttachment = $nameFile;
                            EmailTemplateUpload::changeSize($model->$nameAttachment, 'attachment');
                        }
                    } elseif (!empty($modelUploadFile->errors['attachment'])) {
                        $errorFilesArray['attachment'] =
                            array_merge($errorFilesArray['attachment'], $modelUploadFile->errors['attachment']);
                    }
                }
            };
            $model->update();
            
            $url = array_merge([
                'form',
                'id' => $id,
                'edit' => 'ok',
            ], array_filter([
                'cat' => Yii::$app->request->get('cat'),
            ]));

            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Шаблон успешно создан', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Шаблон создан с некоторыми ошибками.' . $errorText, $id_type = Notice::warning,
                    $id = Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        } else {
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new EmailTemplate();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'foto' => [],
            'attachment' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();

            $filePath = EmailTemplateUpload::filePath();
            if (isset($post['files'])) {
                foreach ($post['files'] as $key => $fileArray) {
                    $modelUploadFile = new EmailTemplateUpload();
                    $modelUploadFile->load($fileArray, '');

                    $nameFoto = 'foto' . $key;
                    $foto = UploadedFile::getInstance($modelUploadFile, '[' . $key . ']foto');
                    $modelUploadFile->foto = $foto;

                    if ($modelUploadFile->delete_foto) {
                        if (is_file($filePath . $model->foto)) {
                            unlink($filePath . $model->foto);
                        }
                        $model->$nameFoto = '';
                    }

                    if (($foto && $foto->tempName) && ($modelUploadFile->validate())) {
                        $nameFile = implode('', [
                            md5(microtime(true)),
                            '.',
                            $foto->extension,
                        ]);
                        if ($foto->saveAs($filePath . $nameFile)) {
                            $model->$nameFoto = $nameFile;
                            EmailTemplateUpload::changeSize($model->$nameFoto, 'foto');
                        }
                    } elseif (!empty($modelUploadFile->errors['foto'])) {
                        $errorFilesArray['foto'] =
                            array_merge($errorFilesArray['foto'], $modelUploadFile->errors['foto']);
                    }

                    $nameAttachment = 'attachment' . $key;
                    $attachment = UploadedFile::getInstance($modelUploadFile, '[' . $key . ']attachment');
                    $modelUploadFile->attachment = $attachment;

                    if ($modelUploadFile->delete_attachment) {
                        if (is_file($filePath . $model->attachment)) {
                            unlink($filePath . $model->attachment);
                        }
                        $model->$nameAttachment = '';
                    }

                    if (($attachment && $attachment->tempName) && ($modelUploadFile->validate())) {
                        $nameFile = implode('', [
                            md5(microtime(true)),
                            '.',
                            $attachment->extension,
                        ]);
                        if ($attachment->saveAs($filePath . $nameFile)) {
                            $model->$nameAttachment = $nameFile;
                            EmailTemplateUpload::changeSize($model->$nameAttachment, 'attachment');
                        }
                    } elseif (!empty($modelUploadFile->errors['attachment'])) {
                        $errorFilesArray['attachment'] =
                            array_merge($errorFilesArray['attachment'], $modelUploadFile->errors['attachment']);
                    }
                }
            };
            $model->update();
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];

            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
        } else {
            $url = ['show'];
        }
        
        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }
    
    public function actionRemove()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = EmailTemplate::findOne($id);
            if (empty($result)) {
                Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                    Url::to($url_redirect));
                return $this->redirect($url_redirect);
            }
            $filePath = EmailTemplateUpload::filePath();
            for ($i = 1; $i <= 5; $i++) {
                $foto = 'foto' . $i;
                if (is_file($filePath . $result->$foto)) {
                    unlink($filePath . $result->$foto);
                }
                $attachment = 'attachment' . $i;
                if (is_file($filePath . $result->$attachment)) {
                    unlink($filePath . $result->$attachment);
                }
            }
            $result->delete();
        }
        
        Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = EmailTemplate::findOne($id);
                    if (empty($result)) {
                        Notice::send($message = 'Ошибка удаления отзыва', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $filePath = EmailTemplateUpload::filePath();
                    for ($i = 1; $i <= 5; $i++) {
                        $foto = 'foto' . $i;
                        if (is_file($filePath . $result->$foto)) {
                            unlink($filePath . $result->$foto);
                        }
                        $attachment = 'attachment' . $i;
                        if (is_file($filePath . $result->$attachment)) {
                            unlink($filePath . $result->$attachment);
                        }
                    }
                    $result->delete();
                }
                
                Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    public function actionFastsave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $id = !empty($data['id']) && intval($data['id']) > 0 ? intval($data['id']) : 0;
            if ($id != 0) {
                unset($data['id']);
                $model = $id == 0 ? new EmailTemplate() : $this->findModel($id);
                $model->load($data);
                if ($model->load($data) && $model->save($runValidation = true, $attributeNames = array_keys($data))) {
                    return [
                        'status' => 1,
                    ];
                }
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = EmailTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}