<?php
namespace backend\controllers;

//use backend\models\UploadFileForm;
//use common\models\PageFiles;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use backend\components\IP;
use backend\components\Notice;
use common\models\Admin;
use common\models\Page;
use backend\models\PageSearch;
use yii\validators\FileValidator;

/**
 * Page controller
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error'
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';

    public function actions() {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }

    public function beforeAction($action) 
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }

    public function checkRights() {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementPages')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }

    public function actionShow() {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }

    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0) {
        $model = $id == 0 ? new Page() : $this->findModel($id);
        return $this->render('form', ['model' => $model]);
    }

    public function actionCreate() {
        $model = new Page();
        $model->who_create = IP::whoCreate();
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            $model->sort = $id;
            $model->update();

            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok'
            ];

            $errorFilesArray = [];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Страница успешно создана', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Страница создана с некоторыми ошибками.' . $errorText,
                    $id_type = Notice::warning, $id = Url::to($url));
            }


            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        } else {
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new Page();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();

            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok'
            ];

            $errorFilesArray = [];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText,
                    $id_type = Notice::warning, $id = Url::to($url));
            }

        } else {
            $url = ['show'];
        }

        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }


    public function actionChangeSort() {
        $status = 0;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->getRawBody() && $request->validateCsrfToken()) {

            $array = [];
            $inputArray = Json::decode($request->getRawBody(), true);

            if (empty($inputArray)) {
                $array['status'] = $status;
                return Json::encode($array);
            }

            PageSearch::changeSort($inputArray);

            $status = 1;
        }
        $array['status'] = $status;
        return Json::encode($array);
    }

    public function actionRemove() {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = Page::findOne($id);
            if (empty($result) || $result->indic == 1) {
                Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning,
                    $id = Url::to($url_redirect));
                return $this->redirect($url_redirect);
            }
    
            $result->delete();
        }
        
        Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = Page::findOne($id);
                    if (empty($result) || $result->indic == 1) {
                        Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $result->delete();
                }
                
                Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    public function actionFastsave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $id = !empty($data['id']) && intval($data['id']) > 0 ? intval($data['id']) : 0;
            if ($id != 0) {
                unset($data['id']);
                $model = $id == 0 ? new Page() : $this->findModel($id);
                $model->load($data);
                if ($model->load($data) && $model->save($runValidation = true, $attributeNames = array_keys($data))) {
                    return [
                        'status' => 1,
                    ];
                }
            }
        }
        
        return [
            'status' => 0,
        ];
    }

    protected function findModel($id) {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }

}