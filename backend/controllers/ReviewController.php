<?php
namespace backend\controllers;

use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use backend\components\Notice;
use common\models\Admin;
use common\models\Review;
use common\models\ReviewReply;
use backend\models\ReviewSearch;

/**
 * Review controller
 */
class ReviewController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }
    
    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementReviews')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionShow()
    {
        $searchModel = new ReviewSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0)
    {
        $model = $this->findModel($id);
        $model = null !== $model ? $model : new Review();
        return $this->render('form', ['model' => $model]);
    }
    
    public function actionCreate()
    {
        $model = new Review();
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            $model->update();
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorFilesArray = [];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Отзыв успешно создан', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Отзыв создан с некоторыми ошибками.' . $errorText, $id_type = Notice::warning,
                    $id = Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        } else {
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new Review();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorFilesArray = [];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
        } else {
            $url = ['show'];
        }
        
        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }
    
    public function actionRemove()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = Review::findOne($id);
            $result->delete();
        }
        
        Notice::send($message = 'Отзыв успешно удален', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = Review::findOne($id);
                    if (empty($result)) {
                        Notice::send($message = 'Ошибка удаления отзыва', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $result->delete();
                }
                
                Notice::send($message = 'Отзыв успешно удален', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    public function actionCancel()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = Review::findOne($id);
            $result->subm = Review::STATUS_INACTIVE;
            $result->update();
        }
        
        Notice::send($message = 'Отзыв успешно убран из публикации', $id_type = Notice::info, $id =
            Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionSubmit()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = Review::findOne($id);
            $result->subm = Review::STATUS_ACTIVE;
            $result->update();
            ReviewReply::deleteAll(['comment' => $id]);
        }
        
        Notice::send($message = 'Отзыв успешно опубликован', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}