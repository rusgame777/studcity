<?php
namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;
use backend\components\Notice;
use common\models\Admin;
use common\models\Settings;
use common\models\SettingsEmail;

/**
 * Settings controller
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';

    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }

    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }

    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementSettings')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }

    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 1)
    {
        $model = $id == 0 ? new Settings() : $this->findModel($id);
        return $this->render('form', [
            'model' => $model,
        ]);
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new Settings;
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));

                if (isset($post['email'])) {
                    $modelItems = [];
                    foreach ($post['email'] as $index => $email) {
                        $modelItem = new SettingsEmail();
                        if ($modelItem->load($email, '')) {
                            $modelItems[$index] = $modelItem;
                        }
                    }
                    if (count($post['email']) > 1) {
                        $arrayError = array_merge($arrayError, ActiveForm::validateMultiple($modelItems));
                    }
                }
                return $arrayError;
            } else {
            }
        }
        return false;
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        $errorFilesArray = [
        ];

        if ($model->load($post) && $model->save()) {
            $id_user = Yii::$app->view->params['id_user'];

            $arrayId =
                ArrayHelper::getColumn(SettingsEmail::find()->where(['id_user' => $id_user])->asArray()->all(), 'id');
            if (isset($post['email'])) {
                foreach ($post['email'] as $key => $email) {
                    $modelItem = $this->findModelData($email['id']);
                    if (null == $modelItem) {
                        $modelItem = new SettingsEmail();
                    }
                    $modelItem->load($email, '');
                    $modelItem->id_user = $id_user;
                    if ($modelItem->save()) {
                        $key = array_search($modelItem->id, $arrayId, false);
                        if ($key !== false) {
                            unset($arrayId[$key]);
                        }
                    } else {
//                      file_put_contents('errors.txt', var_export($modelItem->errors, true).PHP_EOL, FILE_APPEND);
                    }
                }
            }

            foreach ($arrayId as $id_item) {
                $modelItem = SettingsEmail::findOne($id_item);
                $modelItem->delete();
            }

            $url = ['settings/form'];
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }

        } else {
            $url = ['settings/form'];
        }

        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }

    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }

    protected function findModelData($id)
    {
        if (($model = SettingsEmail::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}