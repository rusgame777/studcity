<?php
namespace backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use backend\components\IP;
use backend\components\Notice;
use common\models\Admin;
use common\models\SolutionPosition;
use backend\models\SolutionData;
use backend\models\SolutionPositionUpload;
use backend\models\SolutionPositionSearch;

/**
 * SolutionPosition controller
 */
class SolutionPositionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //                'user' => 'common\models\Users',
                'rules' => [
                    [
                        'actions' => [
                            'login',
                            'error',
                        ],
                        // Define specific actions
                        'allow' => true,
                        // Has access
                        'roles' => ['@'],
                        // '@' All logged in users / or your access role e.g. 'admin', 'user'
                    ],
                    [
                        'allow' => false,
                        // Do not have access
                        'roles' => ['?'],
                        // Guests '?'
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $params = [];
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/layouts/main.php';
    
    public function actions()
    {
        return [
            'errorHandler' => [
                'errorAction' => 'site/login',
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        return (Admin::checkAuth($this) && self::checkRights());
    }
    
    public function checkRights()
    {
        if (!Yii::$app->authManager->checkAccess(Yii::$app->view->params['id_role'], 'managementSolution')) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionShow()
    {
        $searchModel = new SolutionPositionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ];
        return $this->render('show', $data);
    }
    
    /* @param integer $id
     * @return mixed
     */
    public function actionForm($id = 0)
    {
        $model = $id == 0 ? new SolutionPosition() : $this->findModel($id);
        return $this->render('form', [
            'model' => $model,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new SolutionPosition();
        $model->who_create = IP::whoCreate();
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'foto' => [],
            'foto_small' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            $model->sort = $id;
            $model->update();
            
            $filePath = SolutionPositionUpload::filePath();
            $modelUpload = new SolutionPositionUpload();
            $modelUpload->load($post);
            $foto = UploadedFile::getInstance($modelUpload, 'foto');
            $modelUpload->foto = $foto;
            
            if (($foto && $foto->tempName) && ($modelUpload->validate())) {
                $nameFile = implode('', [
                    md5(microtime(true)),
                    '.',
                    $foto->extension,
                ]);
                $nameSmallFile = implode('', [
                    md5(microtime(true)),
                    '.',
                    $foto->extension,
                ]);
                if ($foto->saveAs($filePath . $nameFile)) {
                    $model->foto = $nameFile;
                    SolutionPositionUpload::changeSize($model->foto, 'foto');
                    chmod($filePath . $model->foto, 0644);
                    $model->foto_small = $nameSmallFile;
                    copy($filePath . $model->foto, $filePath . $model->foto_small);
                    SolutionPositionUpload::changeSize($model->foto_small, 'foto_small');
                    chmod($filePath . $model->foto_small, 0644);
                }
            } elseif (!empty($modelUpload->errors['foto'])) {
                $errorFilesArray['foto'] = array_merge($errorFilesArray['foto'], $modelUpload->errors['foto']);
            }
            $model->update();
            
            if (isset($post['data'])) {
                foreach ($post['data'] as $key => $data) {
                    $modelItem = new SolutionData();
                    $modelItem->load($data, '');
                    $modelItem->id_position = $id;
                    if ($modelItem->save()) {
                        //
                    } else {
//                      file_put_contents('errors.txt', var_export($modelItem->errors, true).PHP_EOL, FILE_APPEND);
                    }
                }
            }
            
            $url = array_merge([
                'form',
                'id' => $id,
                'edit' => 'ok',
            ], array_filter([
                'cat' => Yii::$app->request->get('cat'),
            ]));
            
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'РГР успешно создана', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'РГР создана с некоторыми ошибками.' . $errorText, $id_type = Notice::warning,
                    $id = Url::to($url));
            }
            
            echo "<script>document.location='" . Url::to($url) . "';</script>";
            return true;
        } else {
        }
        return false;
    }
    
    public function actionValidate()
    {
        $arrayError = [];
        
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            $model = new SolutionPosition();
            $post = Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $arrayError = array_merge($arrayError, ActiveForm::validate($model));
                
                if (isset($post['data'])) {
                    $modelItems = [];
                    foreach ($post['data'] as $index => $data) {
                        $modelItem = new SolutionData();
                        if ($modelItem->load($data, '')) {
                            $modelItems[$index] = $modelItem;
                        }
                    }
                    if (count($post['data']) > 1) {
                        $arrayError = array_merge($arrayError, ActiveForm::validateMultiple($modelItems));
                    }
                }
                return $arrayError;
            } else {
            }
        }
        return false;
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->who_update = IP::whoCreate();
        $post = Yii::$app->request->post();
        $errorFilesArray = [
            'foto' => [],
            'foto_small' => [],
        ];
        
        if ($model->load($post) && $model->save()) {
            $id = $model->getPrimaryKey();
            
            $filePath = SolutionPositionUpload::filePath();
            $modelUpload = new SolutionPositionUpload();
            $modelUpload->load($post);
            $foto = UploadedFile::getInstance($modelUpload, 'foto');
            $modelUpload->foto = $foto;
            
            if ($modelUpload->delete_foto) {
                if (is_file($filePath . $model->foto)) {
                    unlink($filePath . $model->foto);
                }
                if (is_file($filePath . $model->foto_small)) {
                    unlink($filePath . $model->foto_small);
                }
                $model->foto = '';
                $model->foto_small = '';
                $model->update();
            }
            
            if (($foto && $foto->tempName) && ($modelUpload->validate())) {
                $nameFile = implode('', [
                    md5(microtime(true)),
                    '.',
                    $foto->extension,
                ]);
                $nameSmallFile = implode('', [
                    md5(microtime(true)),
                    '.',
                    $foto->extension,
                ]);
                if ($foto->saveAs($filePath . $nameFile)) {
                    $model->foto = $nameFile;
                    SolutionPositionUpload::changeSize($model->foto, 'foto');
                    chmod($filePath . $model->foto, 0644);
                    $model->foto_small = $nameSmallFile;
                    copy($filePath . $model->foto, $filePath . $model->foto_small);
                    SolutionPositionUpload::changeSize($model->foto_small, 'foto_small');
                    chmod($filePath . $model->foto_small, 0644);
                    $model->update();
                }
            } elseif (!empty($modelUpload->errors['foto'])) {
                $errorFilesArray['foto'] = array_merge($errorFilesArray['foto'], $modelUpload->errors['foto']);
            }
            
            $arrayId =
                ArrayHelper::getColumn(SolutionData::find()->where(['id_position' => $id])->asArray()->all(), 'id');
            if (isset($post['data'])) {
                foreach ($post['data'] as $key => $data) {
                    $modelItem = $this->findModelData($data['id']);
                    if (null == $modelItem) {
                        $modelItem = new SolutionData();
                    }
                    $modelItem->load($data, '');
                    $modelItem->id_position = $id;
                    if ($modelItem->save()) {
                        $key = array_search($modelItem->id, $arrayId, false);
                        if ($key !== false) {
                            unset($arrayId[$key]);
                        }
                    } else {
//                     file_put_contents('errors.txt', var_export($modelItem->errors, true).PHP_EOL, FILE_APPEND);
                    }
                }
            }
            
            foreach ($arrayId as $id_item) {
                $modelItem = SolutionData::findOne($id_item);
                $modelItem->delete();
            }
            
            $url = [
                'form',
                'id' => $id,
                'edit' => 'ok',
            ];
            
            $errorText = '';
            foreach ($errorFilesArray as $key => $array) {
                if (!empty($array)) {
                    $errorText .= ' ' . implode(' ', $array);
                }
            }
            if (empty($errorText)) {
                Notice::send($message = 'Правки успешно внесены.', $id_type = Notice::info, $id = Url::to($url));
            } else {
                Notice::send($message = 'Правки внесены с некоторыми ошибками.' . $errorText, $id_type =
                    Notice::warning, $id = Url::to($url));
            }
            
        } else {
            $url = ['show'];
        }
        
        echo "<script>document.location='" . Url::to($url) . "';</script>";
        return true;
//        return $this->redirect($url, 302);
    }
    
    public function actionChangeSort()
    {
        $status = 0;
        $request = Yii::$app->request;
        if ($request->isAjax && $request->getRawBody() && $request->validateCsrfToken()) {
            
            $array = [];
            $inputArray = Json::decode($request->getRawBody(), true);
            
            if (empty($inputArray)) {
                $array['status'] = $status;
                return Json::encode($array);
            }
            
            SolutionPositionSearch::changeSort($inputArray);
            
            $status = 1;
        }
        $array['status'] = $status;
        return Json::encode($array);
    }
    
    public function actionRemove()
    {
        $url_redirect = !empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : ['show'];
        $request = Yii::$app->request;
        if ($request->get() && $request->validateCsrfToken()) {
            $id = $request->get('id');
            $result = SolutionPosition::findOne($id);
            if (empty($result)) {
                Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                    Url::to($url_redirect));
                return $this->redirect($url_redirect);
            }
            $filePath = SolutionPositionUpload::filePath();
            if (is_file($filePath . $result->foto)) {
                unlink($filePath . $result->foto);
            }
            if (is_file($filePath . $result->foto_small)) {
                unlink($filePath . $result->foto_small);
            }
            $result->delete();
            SolutionData::deleteAll(['id_position' => $id]);
        }
        
        Notice::send($message = 'Страница успешно удалена', $id_type = Notice::info, $id = Url::to($url_redirect));
        return $this->redirect($url_redirect);
    }
    
    public function actionDelete()
    {
        $url_redirect = ['show'];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody() && Yii::$app->request->validateCsrfToken()) {
            $data = Json::decode(Yii::$app->request->getRawBody());
            $idArr = array_key_exists('id', $data) ? $data['id'] : [];
            $url = array_key_exists('url', $data) ? $data['url'] : Url::to($url_redirect);
            if (!empty($idArr)) {
                foreach ($idArr as $id) {
                    $result = SolutionPosition::findOne($id);
                    if (empty($result)) {
                        Notice::send($message = 'Ошибка удаления страницы', $id_type = Notice::warning, $id =
                            Url::to($url_redirect));
                        return [
                            'status' => 0,
                        ];
                    }
                    $filePath = SolutionPositionUpload::filePath();
                    if (is_file($filePath . $result->foto)) {
                        unlink($filePath . $result->foto);
                    }
                    if (is_file($filePath . $result->foto_small)) {
                        unlink($filePath . $result->foto_small);
                    }
                    $result->delete();
                    SolutionData::deleteAll(['id_position' => $id]);
                }
                
                Notice::send($message = 'Страницы успешно удалены', $id_type = Notice::info, $id = $url);
                return [
                    'status' => 1,
                ];
            }
        }
        
        return [
            'status' => 0,
        ];
    }
    
    protected function findModel($id)
    {
        if (($model = SolutionPosition::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
    
    protected function findModelData($id)
    {
        if (($model = SolutionData::findOne($id)) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}