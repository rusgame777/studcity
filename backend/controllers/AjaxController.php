<?php
namespace backend\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use common\models\Admin;

/**
 * Ajax controller
 */
class AjaxController extends Controller
{
    public $layout = '@app/views/layouts/main.php';
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        if ($action->id != 'login') {
            return (Admin::checkAuth($this) && self::checkRights($action));
        } else {
            return true;
        }
        
    }
    
    public function checkRights($action)
    {
        $redirect = false;
        $auth = Yii::$app->authManager;
        $id_role = Yii::$app->view->params['id_role'];
        if ((in_array($action->id, [
                'catalog-statistics',
            ])) && (!$auth->checkAccess($id_role, 'managementCatalogStatistics'))
        ) {
            $redirect = true;
        }
        
        if ((in_array($action->id, [
                'solution-statistics',
            ])) && (!$auth->checkAccess($id_role, 'managementSolutionStatistics'))
        ) {
            $redirect = true;
        }
        
        if ($redirect) {
            $this->redirect('/site/norights/')->send();
            return false;
        }
        return true;
    }
    
    public function actionLogin()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->getRawBody()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('login', ['data' => $data]);
        }
        return true;
    }
    
    public function actionCatalogChange()
    {
        if (Yii::$app->request->isGet) {
            return $this->renderAjax('catalog/change');
        }
        return true;
    }
    
    public function actionCatalogStatistics()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('catalog/statistics', [
                'data' => Json::decode($data),
            ]);
        }
        return true;
    }
    
    public function actionSolutionStatistics()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->validateCsrfToken()) {
            $data = Yii::$app->request->getRawBody();
            return $this->renderAjax('solution/statistics', [
                'data' => Json::decode($data),
            ]);
        }
        return true;
    }
}