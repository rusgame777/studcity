<?php
return [
    'managementReviews' => [
        'type' => 2,
        'description' => 'Management of reviews',
    ],
    'managementPages' => [
        'type' => 2,
        'description' => 'Management of pages',
    ],
    'managementCatalog' => [
        'type' => 2,
        'description' => 'Management of catalog',
    ],
    'managementCatalogStatistics' => [
        'type' => 2,
        'description' => 'Management of catalog\'s statistics',
    ],
    'managementTemplate' => [
        'type' => 2,
        'description' => 'Template\'s management',
    ],
    'managementSolution' => [
        'type' => 2,
        'description' => 'Solution management',
    ],
    'managementSolutionStatistics' => [
        'type' => 2,
        'description' => 'Management of solution\'s statistics',
    ],
    'managementSolutionOrders' => [
        'type' => 2,
        'description' => 'Management of solution\'s orders',
    ],
    'managementAdmins' => [
        'type' => 2,
        'description' => 'Admins management',
    ],
    'managementSettings' => [
        'type' => 2,
        'description' => 'Admins management',
    ],
    'author' => [
        'type' => 1,
        'children' => [
            'managementSolutionOrders',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'author',
            'managementCatalog',
            'managementCatalogStatistics',
            'managementSolution',
            'managementSolutionStatistics',
        ],
    ],
    'superadmin' => [
        'type' => 1,
        'children' => [
            'admin',
            'author',
            'managementReviews',
            'managementPages',
            'managementTemplate',
            'managementAdmins',
            'managementSettings',
        ],
    ],
];
