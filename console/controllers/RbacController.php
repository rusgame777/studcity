<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
                
        // добавляем разрешение "managementReviews"
        $managementReviews = $auth->createPermission('managementReviews');
        $managementReviews->description = 'Management of reviews';
        $auth->add($managementReviews);
        
        // добавляем разрешение "managementPages"
        $managementPages = $auth->createPermission('managementPages');
        $managementPages->description = 'Management of pages';
        $auth->add($managementPages);
        
        // добавляем разрешение "managementCatalog"
        $managementCatalog = $auth->createPermission('managementCatalog');
        $managementCatalog->description = 'Management of catalog';
        $auth->add($managementCatalog);
    
        // добавляем разрешение "managementCatalogStatistics"
        $managementCatalogStatistics = $auth->createPermission('managementCatalogStatistics');
        $managementCatalogStatistics->description = 'Management of catalog\'s statistics';
        $auth->add($managementCatalogStatistics);
        
        // добавляем разрешение "managementTemplate"
        $managementTemplate = $auth->createPermission('managementTemplate');
        $managementTemplate->description = "Template's management";
        $auth->add($managementTemplate);
        
        // добавляем разрешение "managementSolution"
        $managementSolution = $auth->createPermission('managementSolution');
        $managementSolution->description = 'Solution management';
        $auth->add($managementSolution);
    
        // добавляем разрешение "managementSolutionStatistics"
        $managementSolutionStatistics = $auth->createPermission('managementSolutionStatistics');
        $managementSolutionStatistics->description = 'Management of solution\'s statistics';
        $auth->add($managementSolutionStatistics);
    
        // добавляем разрешение "managementSolutionStatistics"
        $managementSolutionOrders = $auth->createPermission('managementSolutionOrders');
        $managementSolutionOrders->description = 'Management of solution\'s orders';
        $auth->add($managementSolutionOrders);
        
        // добавляем разрешение "managementAdmins"
        $managementAdmins = $auth->createPermission('managementAdmins');
        $managementAdmins->description = 'Admins management';
        $auth->add($managementAdmins);

        // добавляем разрешение "managementSettings"
        $managementSettings = $auth->createPermission('managementSettings');
        $managementSettings->description = 'Admins management';
        $auth->add($managementSettings);
                
        // добавляем роль "author" и даём роли нужные разрешения
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $managementSolutionOrders);
        
        // добавляем роль "admin" и даём роли нужные разрешения
        // а также все разрешения роли "author"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $author);
        $auth->addChild($admin, $managementCatalog);
        $auth->addChild($admin, $managementCatalogStatistics);
        $auth->addChild($admin, $managementSolution);
        $auth->addChild($admin, $managementSolutionStatistics);

        // добавляем роль "superadmin" и даём роли нужные разрешения
        // а также все разрешения роли "admin"
        $superadmin = $auth->createRole('superadmin');
        $auth->add($superadmin);
        $auth->addChild($superadmin, $admin);
        $auth->addChild($superadmin, $author);
        $auth->addChild($superadmin, $managementReviews);
        $auth->addChild($superadmin, $managementPages);
        $auth->addChild($superadmin, $managementTemplate);
        $auth->addChild($superadmin, $managementAdmins);
        $auth->addChild($superadmin, $managementSettings);
        
        // Назначение ролей пользователям. 1 и 2 это IDs возвращаемые IdentityInterface::getId()
        $auth->assign($author, 2);
        $auth->assign($admin, 1);
        $auth->assign($superadmin, -1);
    }
}