<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-redis' => 
  array (
    'name' => 'yiisoft/yii2-redis',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/redis' => $vendorDir . '/yiisoft/yii2-redis',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.8.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'leandrogehlen/yii2-treegrid' => 
  array (
    'name' => 'leandrogehlen/yii2-treegrid',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@leandrogehlen/treegrid' => $vendorDir . '/leandrogehlen/yii2-treegrid',
    ),
  ),
  'wbraganca/yii2-dynamicform' => 
  array (
    'name' => 'wbraganca/yii2-dynamicform',
    'version' => '2.0.2.0',
    'alias' => 
    array (
      '@wbraganca/dynamicform' => $vendorDir . '/wbraganca/yii2-dynamicform',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.11.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid',
    ),
  ),
  'liyunfang/yii2-widget-linkpager' => 
  array (
    'name' => 'liyunfang/yii2-widget-linkpager',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@liyunfang/pager' => $vendorDir . '/liyunfang/yii2-widget-linkpager',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => '1.3.4.0',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x',
    ),
  ),
  'kartik-v/yii2-editable' => 
  array (
    'name' => 'kartik-v/yii2-editable',
    'version' => '1.7.6.0',
    'alias' => 
    array (
      '@kartik/editable' => $vendorDir . '/kartik-v/yii2-editable',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient',
    ),
  ),
  'himiklab/yii2-recaptcha-widget' => 
  array (
    'name' => 'himiklab/yii2-recaptcha-widget',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@himiklab/yii2/recaptcha' => $vendorDir . '/himiklab/yii2-recaptcha-widget/src',
      '@himiklab/yii2/recaptcha/tests' => $vendorDir . '/himiklab/yii2-recaptcha-widget/tests',
    ),
  ),
);
